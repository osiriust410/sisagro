"""sisagro URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from django.conf import settings
from django.conf.urls.static import static

from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required

urlpatterns = [
    # url(r'', login_required(TemplateView.as_view(template_name='inicio/login.html')), name='login_user'),
    url(r'^admin/', admin.site.urls),
    url(r'^', include("apps.inicio.urls", namespace='home_base')),
    url(r'^inicio/', include("apps.inicio.urls", namespace='inicio')),
    url(r'^administracion/', include("apps.administracion.urls", namespace='administracion')),
    url(r'^planificacion/', include("apps.planificacion.urls", namespace='planificacion')),
    url(r'^agricola/', include("apps.agricola.urls", namespace='agricola')),
    url(r'^reportes/', include("apps.reportes.urls", namespace='reportes')),
    url(r'^select2/', include('django_select2.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
