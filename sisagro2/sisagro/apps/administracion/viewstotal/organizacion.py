import json

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.serializers import serialize
from django.http import HttpResponse
from django.shortcuts import render
from django.views import View
from django.views.generic import TemplateView, ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from apps.administracion.formstotal.organizacion import OrganizacionForm, OrganizacionUbiegoForm
from apps.administracion.models import Organizacion
from apps.inicio.mixin import LoginRequiredMixin
from apps.inicio.models import AuditableMixin, CentroPoblado, Provincia, Distrito, Caserio


class OrganizacionTemplateView(LoginRequiredMixin, TemplateView):

    template_name = "administracion/organizacion/inicio.html"
    form_class = OrganizacionUbiegoForm

    def get_context_data(self, **kwargs):

        context = super(OrganizacionTemplateView, self).get_context_data(**kwargs)
        context['form'] = self.form_class
        return context


class OrganizacionListView(LoginRequiredMixin, ListView):


    def get(self, request):

        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        idcaserio = int(request.GET.get('caserio', -1))
        # data = {}
        orderby = ""

        if order != 'asc':
            orderby = "-"

        organizacion = Organizacion.objects.filter(caserio_id=idcaserio).order_by('-id')

        if len(search) > 0:

            organizacion = organizacion.filter(nombre__icontains=search)
            # profesion = Profesion.objects.filter(Q(nombre__startswith=search) | Q(apellido__startswith=search) | Q(dni__startswith=search))

        if sort != '':
            organizacion = organizacion.order_by(orderby + sort)

        paginador = Paginator(organizacion, limit)

        try:
            organizacion = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            organizacion = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            organizacion = paginador.page(paginador.num_pages)

        return render(
            request,
            'administracion/organizacion/datos.json',
            {
                'organizaciones': organizacion,
                'total': paginador.count
            }
        )


class OrganizacionCreateView(LoginRequiredMixin, AuditableMixin, CreateView):

    model = Organizacion
    form_class = OrganizacionForm
    template_name = "administracion/organizacion/formulario.html"

    def form_valid(self, form):

        super(OrganizacionCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):

        super(OrganizacionCreateView, self).form_invalid(form)

        return render(self.request, self.template_name, {'form': form})


class OrganizacionUpdateView(LoginRequiredMixin, AuditableMixin, UpdateView):

    model = Organizacion
    form_class = OrganizacionForm
    template_name = "administracion/organizacion/formulario.html"


    def dispatch(self, *args, **kwargs):

        self.organizacion_id = kwargs['pk']
        return super(OrganizacionUpdateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):

        super(OrganizacionUpdateView, self).form_valid(form)
        organizacion = Organizacion.objects.get(pk=self.organizacion_id)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True

        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):

        super(OrganizacionUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

    def get_context_data(self, **kwargs):
        context = super(OrganizacionUpdateView, self).get_context_data(**kwargs)
        context["organizacion_id"] = self.organizacion_id
        return context

class OrganizacionDeleteView(LoginRequiredMixin, DeleteView):

    model = Organizacion
    form_class = OrganizacionForm
    template_name = "administracion/organizacion/eliminar.html"

    def dispatch(self, *args, **kwargs):

        self.organizacion_id = kwargs['pk']
        return super(OrganizacionDeleteView, self).dispatch(*args, **kwargs)

    def get_success_url(self):

        pass

    def delete(self, request, *args, **kwargs):

        self.object = self.get_object()
        self.object.delete()
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def get_context_data(self, **kwargs):

        context = super(OrganizacionDeleteView, self).get_context_data(**kwargs)
        context["organizacion_id"] = self.organizacion_id
        return context



class UbiegoCaserioListView(View):

    def get(self, request,*args, **kwargs):

        valor = int(kwargs['valor'])
        pk = int(kwargs['pk'])
        if valor == 0:

            objeto = Provincia.objects.filter(departamento_id = pk)

        elif valor == 1:

            objeto = Distrito.objects.filter(provincia_id = pk)


        elif valor == 3:

            objeto = CentroPoblado.objects.filter(distrito_id = pk)

        elif valor == 4:

            objeto = Caserio.objects.filter(centro_problado_id = pk)


        return HttpResponse(serialize('json', objeto, ))

