# -*- coding: utf-8 -*-
import json

from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.models import User
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import FormView, TemplateView, ListView, CreateView, UpdateView, DeleteView

from ..formstotal.acceso import AccesoForm
from ..models import Persona, Menu, Acceso
from ..formstotal.usuario import UsuarioInsertForm, UsuarioUpdateForm, PasswordChangeCustomForm
from ...inicio.mixin import LoginRequiredMixin
from ...inicio.models import AuditableMixin

class UsuarioView(LoginRequiredMixin, TemplateView):
    template_name = "administracion/usuario/inicio.html"

class UsuarioListView(LoginRequiredMixin, ListView):

    def get(self, request):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')

        orderby = ""
        if order != 'asc':
            orderby = "-"

        usuarios = User.objects.all().order_by('-id')

        if len(search) > 0:
            # usuarios = usuarios.filter(username__icontains=search)
            usuarios = usuarios.filter(
                Q(username__icontains=search) |
                Q(persona__nombres__icontains=search) |
                Q(persona__apepat__icontains=search) |
                Q(persona__apemat__icontains=search)
            )

        if sort != '':
            sort = "persona" if sort == "persona__apepat+persona__apemat+persona__nombres" else sort
            usuarios = usuarios.order_by(orderby + sort)

        paginador = Paginator(usuarios, limit)

        try:
            usuarios = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            usuarios = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            usuarios = paginador.page(paginador.num_pages)

        return render(
            request,
            'administracion/usuario/datos.json',
            {
                'usuarios': usuarios,
                'total': paginador.count
            }
        )

class UsuarioCreateView(CreateView):
    model = User
    form_class = UsuarioInsertForm
    template_name = "administracion/usuario/formulario.html"

    def form_valid(self, form):
        user = super(UsuarioCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        user = super(UsuarioCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form })

class UsuarioUpdateView(AuditableMixin, UpdateView):
    model = User
    form_class = UsuarioUpdateForm
    template_name = "administracion/usuario/formulario.html"

    def form_valid(self, form):
        super(UsuarioUpdateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(UsuarioUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class UsuarioDeleteView(DeleteView):
    model = User
    form_class = UsuarioInsertForm
    template_name = "administracion/usuario/eliminar.html"

    def delete(self, request, *args, **kwargs):
        try:
            persona = self.get_object().persona
        except:
            persona = None
        if persona:
            persona.user = None
            persona.save()
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        try:
            self.get_object().delete()
        except Exception as e:
            if persona:
                persona.user = self.get_object()
                persona.save()
            msg['value'] = False
            return render(request, self.template_name, {'object': self.get_object(), 'delete_error': str(e)})
        return HttpResponse(json.dumps(msg), content_type='application/json')

class UsuarioPersonaListView(ListView):

    def get(self, request):
        filtro = request.GET.get("filtro", "")
        personas = Persona.objects.all().order_by('apepat', 'apemat', 'nombres')
        if len(filtro) > 0:
            personas = personas.filter(
                Q(nombres__icontains=filtro) |
                Q(apepat__icontains=filtro) |
                Q(apemat__icontains=filtro)
            )
        return render(
            request,
            'administracion/usuario/datospersonas.json',
            {
                'personas': personas,
                'total': personas.count
            }
        )

class UsuarioPermisosView(FormView):
    model = Acceso
    form_class = AccesoForm
    template_name = "administracion/usuario/permisos.html"

    def dispatch(self, request, *args, **kwargs):
        self.iduser = kwargs['pk']
        self.usuario = User.objects.get(pk=self.iduser)
        personas = Persona.objects.filter(user=self.usuario)
        persona = None
        if personas.count() > 0:
            self.persona = personas[0]
        return super(UsuarioPermisosView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(UsuarioPermisosView, self).get_context_data(**kwargs)
        opciones = Menu.objects.filter(
            padre=None
        ).order_by("orden")
        menus = []
        for menu in Menu.objects.all().order_by("orden"):
            acc = {"padre":menu.padre_id, "nombre": menu.nombre, "id": menu.id, "estado": False}
            #
            if self.persona != None:
                accuser = menu.acceso_set.filter(persona=self.persona, menu=menu)
                if accuser.count() > 0:
                    acc["estado"] = True
            #
            menus.append(acc)
        context["is_superuser"] = self.usuario.is_superuser
        context["opciones"] = opciones
        context["menus"] = menus
        return context

    def post(self, request, *args, **kwargs):
        _acc = []
        for cc in request.POST:
            cc = str(cc)
            if cc.startswith("acc_"):
                codacc = str(cc).replace('acc_', '')
                _acc.append(codacc)
        # Eliminamos los menus que no estén en la lista
        Acceso.objects.exclude(
            menu_id__in=_acc,
            persona=self.persona
        ).delete()
        for _idac in _acc:
            _racc = Acceso.objects.filter(
                menu_id=_idac,
                persona=self.persona
            )
            if _racc.count() == 0:
                Acceso(
                    menu_id=_idac,
                    persona=self.persona,
                    creador=request.user
                ).save()
        #
        msg = {}
        msg['msg'] = "Permisos Actualizados Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

def UsuarioCambiarPasswordView(request):
    if request.method == 'POST':
        form = PasswordChangeCustomForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            return render(request, 'administracion/usuario/cambiarpassword.html', {
                'change_ok': "Su contraseña se ha cambiado correctamente"
            })
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeCustomForm(request.user)
    return render(request, 'administracion/usuario/cambiarpassword.html', {
        'form': form
    })

