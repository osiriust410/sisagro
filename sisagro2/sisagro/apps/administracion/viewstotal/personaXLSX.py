# -*- coding: utf-8 -*-
import xlwt

from django.http import HttpResponse
from ..models import Persona
from datetime import datetime
from django.utils import formats

def ListaPersonasXLSX(request):

    personas = enumerate(Persona.objects.all().order_by('-id'))

    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="Personas_' + str(formats.date_format(datetime.now(), "d-m-Y")) + '.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Lista')

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    
    #Encabezado
    row_num = row_num + 1
    columns = ['', '', '', 'Dirección Regional de Agricultura Cajamarca', '', '', '', '', '', '']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    row_num = row_num + 1
    columns = ['', '', '', 'SisDRAC', '', '', '', '', '', '']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    row_num = row_num + 1
    columns = ['', '', '', 'Lista de Personas', '', '', '', '', '', '']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    #Encabezados
    row_num = row_num + 3
    columns = ['Nro', 'DNI', 'Nombres', 'Sexo', 'Fecha Nacimiento']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    #Llenando datos
    font_style = xlwt.XFStyle()
    for i, persona in personas:
        row_num = row_num + 1

        columns = [
                        i + 1, 
                        str(persona.dni), 
                        "%s %s %s" % (persona.apepat, persona.apemat, persona.nombres),
                        str(persona.sexo),
                        str(formats.date_format(persona.fechanacimiento, "d-m-Y")),
                    ]

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    ws.write(row_num + 3, 2, 'Reporte generado el ' + str(formats.date_format(datetime.now(), "d-m-Y h:i:s a")), font_style)

    wb.save(response)
    return response