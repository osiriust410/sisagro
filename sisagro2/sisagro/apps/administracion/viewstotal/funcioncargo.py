from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render, redirect
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView, ListView
from django.http import HttpResponse, HttpResponseRedirect
import json

from ...inicio.models import AuditableMixin
from ..models import FuncionCargo
from ..formstotal.funcioncargo import FuncionCargoForm
from ...inicio.mixin import LoginRequiredMixin

class FuncionCargoView(LoginRequiredMixin, TemplateView):
    template_name = "administracion/funcioncargo/inicio.html"

class FuncionCargoListView(LoginRequiredMixin, ListView):

    def get(self, request):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')

        orderby = ""
        if order != 'asc':
            orderby = "-"

        funcionescargos = FuncionCargo.objects.all().order_by('-id')

        if len(search) > 0:
            funcionescargos = funcionescargos.filter(nombre__icontains=search)
            # profesion = Profesion.objects.filter(Q(nombre__startswith=search) | Q(apellido__startswith=search) | Q(dni__startswith=search))

        if sort != '':
            funcionescargos = funcionescargos.order_by(orderby + sort)

        paginador = Paginator(funcionescargos, limit)

        try:
            funcionescargos = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            funcionescargos = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            funcionescargos = paginador.page(paginador.num_pages)

        return render(
            request,
            'administracion/funcioncargo/datos.json',
            {
                'funcionescargos': funcionescargos,
                'total': paginador.count
            }
        )

class FuncionCargoCreateView(AuditableMixin, CreateView):
    model = FuncionCargo
    form_class = FuncionCargoForm
    template_name = "administracion/funcioncargo/formulario.html"

    def form_valid(self, form):
        super(FuncionCargoCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(FuncionCargoCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class FuncionCargoUpdateView(AuditableMixin, UpdateView):
    model = FuncionCargo
    form_class = FuncionCargoForm
    template_name = "administracion/funcioncargo/formulario.html"

    def dispatch(self, *args, **kwargs):
        self.funcioncargo_id = kwargs['pk']
        return super(FuncionCargoUpdateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        super(FuncionCargoUpdateView, self).form_valid(form)
        funcioncargo = FuncionCargo.objects.get(pk=self.funcioncargo_id)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(FuncionCargoUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

    def get_context_data(self, **kwargs):
        context = super(FuncionCargoUpdateView, self).get_context_data(**kwargs)
        context["funcioncargo_id"] = self.funcioncargo_id
        return context

class FuncionCargoDeleteView(DeleteView):
    model = FuncionCargo
    form_class = FuncionCargoForm
    template_name = "administracion/funcioncargo/eliminar.html"

    def dispatch(self, *args, **kwargs):
        self.funcioncargo_id = kwargs['pk']
        return super(FuncionCargoDeleteView, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def get_context_data(self, **kwargs):
        context = super(FuncionCargoDeleteView, self).get_context_data(**kwargs)
        context["funcioncargo_id"] = self.funcioncargo_id
        return context

