function actionFormatter(value, row, index) {
	reg = [
	    '<a class="btn-action edit " href="javascript:void(0)" title="Editar" role="button">',
	    '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>',
	    '<a class="btn-action remove" href="javascript:void(0)" title="Eliminar">',
	    '<span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>',
	];
	if (!row.esadmin) {
        reg.push('<a class="btn-action permisos" href="javascript:void(0)" title="Permisos">');
        reg.push('<span class="glyphicon glyphicon-user" aria-hidden="true"></span></a>');
    }
	return reg.join('');
}
window.actionEvents = {
	'click .edit': function (e, value, row, index) {
	    idValue = row['id'];
	    opcion = "Editar";
	    url_post = url_editar + idValue;
		$modalForm.modal('show');
		$modalForm.waitMe({text:"Cargando..."});
	},
	'click .remove': function (e, value, row, index) {
		opcion = "Eliminar";
		idValue = row['id'];
		url_post = url_eliminar + idValue;
		$modalForm.modal('show');
		$modalForm.waitMe({text:"Cargando..."});
	},
	'click .permisos': function (e, value, row, index) {
		opcion = "Permisos";
		idValue = row['id'];
		url_post = url_permisos + idValue;
		$modalForm.modal('show');
		$modalForm.waitMe({text:"Cargando..."});
	}
};
$( document ).ready(function() {
	$("#btnNuevo").click(function(event) {
		opcion = "Nuev" + $formGenero;
		idValue = 0;
		url_post = url_crear;
		$modalForm.modal('show');
		$modalForm.waitMe({text:"Cargando..."});
	});
	$modalForm.on('show.bs.modal', function (e) {
		btnSave = $modalForm.find("#btnGuardar");
		$(this).find('.modal-body').html("");
		url_load = "";
		if(opcion=="Permisos") {
			$txtBtn = "Guardando Permisos";
			url_load = url_permisos + idValue;
			$(this).find('h4').text("Permisos de " + $formTitulo);
			btnSave.html("Guardar");
		}
		else if(opcion=="Eliminar") {
			$txtBtn = "Eliminando";
			url_load = url_eliminar + idValue;
			$(this).find('h4').text("Eliminar " + $formTitulo);
			btnSave.html("Eliminar");
		}
		else {
			btnSave.html("Guardar");
			if(idValue==0){
				$txtBtn = "Guardando";
				url_load = url_crear;
                $(this).find('h4').text("Nuev" + $formGenero + " " + $formTitulo);
            }else{
				$txtBtn = "Actualizando";
				url_load = url_editar + idValue;
                $(this).find('h4').text("Editanto " + $formTitulo);
            }
        }
        $(this).find('.modal-body').load(url_load, function(e) {
        	$modalForm.waitMe('hide');
        	if ($('#' + $firstField)) {
        		setTimeout(function() { $('#' + $firstField).focus(); }, 500);
            }
		});
	});
	$modalForm.on('shown.bs.modal', function(e) {
    });
	$("#btnGuardar").on('click', function(event) {
		$modalForm.waitMe({text:$txtBtn+"..."});
		form = $formView.serialize();
		$.post(url_post, form).done(function(data) {
			if(data.value){
				toastr["success"](data.msg);
				$modalForm.waitMe('hide');
				$modalForm.modal('hide');
				if (opcion!="Permisos") $tablaView.bootstrapTable('refresh');
			}else{
				toastr["error"](data.msg);
				$modalForm.waitMe('hide');
				$modalForm.find('.modal-body').html(data);
			}
		});
	});
});
