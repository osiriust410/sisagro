from django.conf.urls import url

from .viewstotal import funcioncargo as funcar
from .viewstotal import area
from .viewstotal import usuario as usu
from .viewstotal import persona, trabajador
from .viewstotal import organizacion as org
from .viewstotal.personaXLSX import *
from .viewstotal.areaXLSX import *
from .viewstotal.funcioncargoXLSX import *
from .viewstotal.organizacionXLSX import *

urlpatterns = [
    # url(r'^modelo/accion/$', ModeloAccion, name='modelo_accion'),

	# FUNCION / CARGO
    url(r'^funcioncargo/$', funcar.FuncionCargoView.as_view(), name='funcioncargo_inicio'),
    url(r'^funcioncargo/listar/$', funcar.FuncionCargoListView.as_view(), name='funcioncargo_listar'),
    url(r'^funcioncargo/crear/$', funcar.FuncionCargoCreateView.as_view(), name='funcioncargo_crear'),
    url(r'^funcioncargo/actualizar/(?P<pk>\d+)$', funcar.FuncionCargoUpdateView.as_view(), name='funcioncargo_actualizar'),
    url(r'^funcioncargo/eliminar/(?P<pk>\d+)$', funcar.FuncionCargoDeleteView.as_view(), name='funcioncargo_eliminar'),
    url(r'^funcioncargo/xlsx/$', ListaFuncionCargoXLSX, name='funcioncargo_xlsx'),

  	# AREA
  	url(r'^area/$', area.AreaView.as_view(), name='area_inicio'),
    url(r'^area/listar/$', area.AreaListView.as_view(), name='area_listar'),
    url(r'^area/crear/$', area.AreaCreateView.as_view(), name='area_crear'),
    url(r'^area/actualizar/(?P<pk>\d+)$', area.AreaUpdateView.as_view(), name='area_actualizar'),
    url(r'^area/eliminar/(?P<pk>\d+)$', area.AreaDeleteView.as_view(), name='area_eliminar'),
    url(r'^area/xlsx/$', ListaAreasXLSX, name='area_xlsx'),

    # USUARIO
    url(r'^usuario/$', usu.UsuarioView.as_view(), name='usuario_inicio'),
    url(r'^usuario/listar$', usu.UsuarioListView.as_view(), name='usuario_listar'),
    url(r'^usuario/crear$', usu.UsuarioCreateView.as_view(), name='usuario_crear'),
    url(r'^usuario/actualizar/(?P<pk>\d+)$', usu.UsuarioUpdateView.as_view(), name='usuario_actualizar'),
    url(r'^usuario/eliminar/(?P<pk>\d+)$', usu.UsuarioDeleteView.as_view(), name='usuario_eliminar'),
    url(r'^usuario/persona/listar$', usu.UsuarioPersonaListView.as_view(), name='usuario_persona_listar'),
    url(r'^usuario/permisos/(?P<pk>\d+)$', usu.UsuarioPermisosView.as_view(), name='usuario_permisos'),
    url(r'^usuario/cambiarpassword$', usu.UsuarioCambiarPasswordView, name='usuario_cambiarpassword'),

    # PERSONA
    url(r'^persona/$', persona.PersonaView.as_view(), name='persona_inicio'),
    url(r'^persona/listar/$', persona.PersonaListView.as_view(), name='persona_listar'),
    url(r'^persona/crear/$', persona.PersonaCreateView.as_view(), name='persona_crear'),
    url(r'^persona/actualizar/(?P<pk>\d+)$', persona.PersonaUpdateView.as_view(), name='persona_actualizar'),
    url(r'^persona/eliminar/(?P<pk>\d+)$', persona.PersonaDeleteView.as_view(), name='persona_eliminar'),
    url(r'^persona/xlsx/$', ListaPersonasXLSX, name='persona_xlsx'),

    # TRABAJADOR
    url(r'^trabajador/$', trabajador.TrabajadorView.as_view(), name='trabajador_inicio'),
    url(r'^trabajador/listar/$', trabajador.TrabajadorListView.as_view(), name='trabajador_listar'),
    url(r'^trabajador/crear/$', trabajador.TrabajadorCreateView.as_view(), name='trabajador_crear'),
    url(r'^trabajador/actualizar/(?P<pk>\d+)$', trabajador.TrabajadorUpdateView.as_view(), name='trabajador_actualizar'),
    url(r'^trabajador/eliminar/(?P<pk>\d+)$', trabajador.TrabajadorDeleteView.as_view(), name='trabajador_eliminar'),

    # ORGANIZACION
  	url(r'^organizacion/$', org.OrganizacionTemplateView.as_view(), name='organizacion_inicio'),
	url(r'^organizacion/listar/$', org.OrganizacionListView.as_view(), name='organizacion_listar'),
	url(r'^organizacion/crear/$', org.OrganizacionCreateView.as_view(), name='organizacion_crear'),
	url(r'^organizacion/actualizar/(?P<pk>\d+)$', org.OrganizacionUpdateView.as_view(), name='organizacion_actualizar'),
	url(r'^organizacion/eliminar/(?P<pk>\d+)$', org.OrganizacionDeleteView.as_view(), name='organizacion_eliminar'),
    url(r'^organizacion/ubigeo/(?P<pk>\d+)/(?P<valor>\d+)/$', org.UbiegoCaserioListView.as_view(), name='caserio_ubiego'),
    url(r'^organizacion/xlsx/(?P<provincia>\d+)/(?P<distrito>\d+)/(?P<centropoblado>\d+)/$', ListaOrganizacionesCargoXLSX, name='caserio_ubiego'),

]
