# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import *
from django.contrib import admin

# Register your models here.
class AdminFuncionCargo(admin.ModelAdmin):
	list_display = ["id", "nombre"]
	# list_filter = ["nombre"]
	search_fields = ["nombre"]
	#list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminArea(admin.ModelAdmin):
	list_display = ["id", "nombre", "abreviatura", "estado"]
	# list_filter = ["nombre"]
	search_fields = ["nombre", "abreviatura"]
	#list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminPersona(admin.ModelAdmin):
	list_display = ["id", "dni", "nombres", "apepat", "apemat", "sexo"]
	# list_filter = ["dni", "nombres", "apepat", "apemat"]
	search_fields = ["dni", "nombres", "apepat", "apemat"]
	#list_editable = ["nombre", "estado"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminTrabajador(admin.ModelAdmin):
	list_display = ["id", "persona", "area", "funcioncargo", "fechainicio", "fechafin"]
	# list_filter = ["nombre"]
	search_fields = ["persona__nombres", "persona__apepat", "persona__apemat", "persona__dni", "area__nombre"]
	#list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminMenu(admin.ModelAdmin):
	list_display = ["id", "nombre", "ruta", "padre", "orden", "estado"]
	list_filter = ["padre"]
	search_fields = ["nombre", "ruta"]
	list_editable = ["nombre", "ruta", "padre", "orden", "estado"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminAcceso(admin.ModelAdmin):
	list_display = ["id", "persona", "menu"]
	# list_filter = ["nombre"]
	search_fields = ["persona__dni", "persona__nombres", "menu__nombre"]
	#list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminOrganizacion(admin.ModelAdmin):
	list_display = ["id", "nombre", "direccion", "ruc"]
	# list_filter = ["nombre"]
	search_fields = ["nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

admin.site.register(FuncionCargo, AdminFuncionCargo)
admin.site.register(Area, AdminArea)
admin.site.register(Persona, AdminPersona)
admin.site.register(Trabajador, AdminTrabajador)
admin.site.register(Menu, AdminMenu)
admin.site.register(Acceso, AdminAcceso)
admin.site.register(Organizacion, AdminOrganizacion)
