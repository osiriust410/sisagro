from django import forms
from apps.inicio.models import CentroPoblado, Departamento, Provincia, Distrito, Caserio,TipoOrganizacion
from apps.administracion.models import Organizacion
from django_select2.forms import ModelSelect2Widget

class OrganizacionForm(forms.ModelForm):
	tipoorganizacion = forms.ModelChoiceField(
		queryset=TipoOrganizacion.objects.all(),
		label=u'Tipo Organizacion',
		widget=ModelSelect2Widget(
			queryset=TipoOrganizacion.objects.all().order_by("nombre"),
			search_fields=['nombre__icontains'],
			max_results=10,
		)
	)

	class Meta:
		model = Organizacion
		fields = [
			'nombre','direccion',
			'ruc','area','rendimiento','volumen',
			'caserio', 'tipoorganizacion'
		]


	def __init__(self, *args, **kwargs):
		super(OrganizacionForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			if field.widget.__class__ == forms.NumberInput:
				dp = 2 if type(field) == forms.DecimalField else 0
				dpe = 0.01 if type(field) == forms.DecimalField else 1
				field.widget.attrs = {"class": 'touch-spin', "data-max": "Infinity", "data-decimals": dp, "data-step": dpe,
									  "style": "text-align:right", "data-forcestepdivisibility": "none"}
				if self.instance.pk == None:
					field.initial = 0
			else:
				field.widget.attrs['class'] = 'form-control'
		self.fields['caserio'].widget = forms.HiddenInput()
		# self.fields['distrito'].widget = forms.CharField(max_length=50)


class OrganizacionUbiegoForm(forms.Form):

	departamento = forms.ModelChoiceField(
		queryset=Departamento.objects.all(),
		widget=ModelSelect2Widget(
			search_fields=['nombre__icontains'],
			queryset=Departamento.objects.order_by("nombre"),
		)
	)


	provincia = forms.ModelChoiceField(
		queryset=Provincia.objects.all(),
		widget=ModelSelect2Widget(
			search_fields=['nombre__icontains'],
			dependent_fields={'departamento':'departamento'},
			max_results=500,
			queryset=Provincia.objects.order_by("nombre"),
		)
	)

	distrito = forms.ModelChoiceField(
		queryset=Distrito.objects.all(),
		widget=ModelSelect2Widget(
			search_fields=['nombre__icontains'],
			dependent_fields={'provincia': 'provincia'},
			max_results=500,
			queryset=Distrito.objects.order_by("nombre"),

		)
	)
	centropoblado = forms.ModelChoiceField(
		queryset=CentroPoblado.objects.all(),
		widget=ModelSelect2Widget(
			search_fields=['nombre__icontains'],
			dependent_fields={'distrito': 'distrito'},
			max_results=500,
			queryset=CentroPoblado.objects.order_by("nombre"),
		)
	)	
	caserios = forms.ModelChoiceField(
		queryset=Caserio.objects.all(),
		widget=ModelSelect2Widget(
			search_fields=['nombre__icontains'],
			dependent_fields={'centropoblado': 'centropoblado'},
			max_results=500,
			queryset=Caserio.objects.order_by("nombre"),
		)
	)

	def __init__(self, *args, **kwargs):
		super(OrganizacionUbiegoForm, self).__init__(*args, **kwargs)
