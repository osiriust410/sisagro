# -*- coding: utf-8 -*-
import unicodedata

from django.contrib.auth import password_validation
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import User, AbstractUser
from django.forms import ModelForm
from django import forms
from django.utils.translation import ugettext, ugettext_lazy as _
from django.views.generic.edit import FormMixin
from django_select2.forms import Select2Widget, ModelSelect2Widget

from sisagro.widgets.inputcheck import ModelCheckWidget
from ...administracion.models import Persona


class UsuarioInsertForm(FormMixin, ModelForm):
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
        'personal_exist': "Esta persona ya tiene cuenta de usuario",
    }

    password1 = forms.Field(
        label='Contraseña',
        widget=forms.PasswordInput,
    )
    password2 = forms.Field(
        label='Repetir',
        widget=forms.PasswordInput,
        help_text=_("Enter the same password as before, for verification."),
    )
    persona = forms.ModelChoiceField(
        queryset=Persona.objects.all(),
        label=u'Persona',
        widget=ModelSelect2Widget(
            model=Persona,
            queryset=Persona.objects.all().order_by("apepat", "apemat", "nombres"),
            search_fields=['apepat__icontains', 'apemat__icontains', 'nombres__icontains', 'dni__icontains'],
            max_results=10,
        )
    )

    is_superuser = ModelCheckWidget(label="Administrador", textYES="Si", textNO="No", initial=False)
    is_active = ModelCheckWidget(label="Estado", textYES="Activo", textNO="Inactivo", initial=True)

    def __init__(self, *args, **kwargs):
        super(UsuarioInsertForm, self).__init__(*args, **kwargs)
        for i, (fname, field) in enumerate(self.fields.iteritems()):
            field.widget.attrs['class'] = 'form-control'
        self.fields["username"].label = "Usuario"
        self.fields["is_superuser"].label = "Administrador"
        self.fields["is_active"].label = "Estado"

    class Meta:
        model = User
        fields = ['username', 'password1', 'password2', 'is_superuser', 'is_active', 'persona']

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        self.instance.username = self.cleaned_data.get('username')
        password_validation.validate_password(self.cleaned_data.get('password2'), self.instance)
        return password2

    def clean_persona(self):
        objper = self.cleaned_data.get("persona")
        print(objper.user)
        if objper != None and objper.user != None:
            raise forms.ValidationError(
                self.error_messages['personal_exist'],
                code='personal_exist',
            )
        return objper

    #
    def save(self, commit=True):
        user = super(UsuarioInsertForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
            persona = self.cleaned_data.get("persona")
            persona.user = user
            persona.save()
        return user


class UsuarioUpdateForm(FormMixin, ModelForm):
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
        'personal_exist': "La persona ya tiene cuenta de usuario",
    }

    password1 = forms.Field(
        label='Contraseña',
        required=False,
        widget=forms.PasswordInput,
    )
    password2 = forms.Field(
        label='Repetir',
        required=False,
        widget=forms.PasswordInput,
        help_text=_("Enter the same password as before, for verification."),
    )

    persona = forms.ModelChoiceField(
        queryset=Persona.objects.all().order_by("apepat", "apemat", "nombres"),
        label=u'Persona',
        widget=ModelSelect2Widget(
            model=Persona,
            search_fields=['apepat__icontains', 'apemat__icontains', 'nombres__icontains', 'dni__icontains'],
            max_results=10,
        )
    )

    def __init__(self, *args, **kwargs):
        super(UsuarioUpdateForm, self).__init__(*args, **kwargs)
        for i, (fname, field) in enumerate(self.fields.iteritems()):
            field.widget.attrs['class'] = 'form-control'
        self.fields["username"].label = "Usuario"
        self.fields["is_superuser"].label = "Administrador"
        self.fields["is_active"].label = "Estado"
        per = Persona.objects.filter(user=self.instance.id)
        if per.count() > 0:
            self.initial.setdefault("persona", per[0])

    class Meta:
        model = User
        fields = ['username', 'password1', 'password2', 'is_superuser', 'is_active', 'persona']

    def clean_password1(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        self.instance.username = self.cleaned_data.get('username')
        if len(self.cleaned_data.get('password1')) > 0:
            password_validation.validate_password(self.cleaned_data.get('password1'), self.instance)
        return password1

    def clean_persona(self):
        objper = self.cleaned_data.get("persona")
        if objper != None:
            # per = Persona.objects.get(pk=persona)
            if objper.user != None and objper.user.id != self.instance.id:
                raise forms.ValidationError(
                    self.error_messages['personal_exist'],
                    code='personal_exist',
                )
        return objper

    #
    def save(self, commit=True):
        user = super(UsuarioUpdateForm, self).save(commit=False)
        if len(self.cleaned_data.get('password1')) > 0:
            user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
            if self.cleaned_data["persona"] != None:
                persona = self.cleaned_data["persona"]
                persona.user = user
                persona.save()
            else:
                pers = Persona.objects.filter(user=user)
                if pers.count() > 0:
                    for per in pers:
                        per.user = None
                        per.save()
        return user


class PasswordChangeCustomForm(PasswordChangeForm):
    def __init__(self, user, *args, **kwargs):
        super(PasswordChangeCustomForm, self).__init__(user, *args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
