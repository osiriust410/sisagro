# -*- coding: utf-8 -*-
import datetime
from django.forms import ModelForm
from django import forms
from django_select2.forms import Select2Widget

from ...inicio.funciones import CalcularEdad
from ..models import Persona
from datetimewidget.widgets import DateWidget

class PersonaForm(ModelForm):

	fechanacimiento = forms.DateField(
		label=u'Fecha nacimiento',
		required=True,
		widget=DateWidget(
			usel10n=True,
			bootstrap_version=3,
			attrs={'data-datepicker-type': "4", "data-provide": "datepicker-inline"},
			options={
				'autoclose': 'true',
				'clearBtn': 'false'
			},
		)
	)

	sexo = forms.ChoiceField(
		label=u"Sexo",
		choices=Persona.Sexos_Opciones,
		widget=Select2Widget(
			attrs={'data-minimum-results-for-search': 'Infinity'}
		)
	)

	class Meta:
		model = Persona
		fields = ['dni', 'nombres', 'apepat', 'apemat', 'sexo', 'fechanacimiento']

	def __init__(self, *args, **kwargs):
		super(PersonaForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'
		self.fields['dni'].label = 'DNI'
		self.fields['apepat'].label = 'Apellido Paterno'
		self.fields['apemat'].label = 'Apellido Materno'
		self.fields['fechanacimiento'].label = 'Fec. de Nac.'
		# self.fields['user'].label = 'Usuario'

	def clean_fechanacimiento(self):
		fecnac = self.cleaned_data["fechanacimiento"]
		edad = CalcularEdad(fecnac, datetime.datetime.now().date())
		if edad < 18:
			self.add_error("fechanacimiento", u"Solo se pueden registrar mayores de 18 años")
		return fecnac

