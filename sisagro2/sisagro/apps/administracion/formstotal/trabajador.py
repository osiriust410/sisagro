# -*- coding: utf-8 -*-
from django import forms

from sisagro.widgets.inputcheck import ModelCheckWidget
from ..models import Trabajador, Persona, FuncionCargo, Area
from ...inicio.models import Departamento, Especialidad, Profesion, Provincia
from django_select2.forms import ModelSelect2Widget, Select2Widget
from datetimewidget.widgets import DateTimeWidget, DateWidget, TimeWidget


class TrabajadorForm(forms.ModelForm):
    persona = forms.ModelChoiceField(
        queryset=Persona.objects.all(),
        label=u'Persona',
        widget=ModelSelect2Widget(
            queryset=Persona.objects.all().order_by("apepat", "apemat", "nombres"),
            search_fields=['nombres__icontains'],
            max_results=10,
        )
    )
    area = forms.ModelChoiceField(
        queryset=Area.objects.all(),
        label=u'Area',
        widget=ModelSelect2Widget(
            queryset=Area.objects.all().order_by("nombre"),
            search_fields=['nombre__icontains'],
            max_results=10,
        )
    )
    funcioncargo = forms.ModelChoiceField(
        queryset=FuncionCargo.objects.all(),
        label=u'Funcio Cargo',
        widget=ModelSelect2Widget(
            queryset=FuncionCargo.objects.all().order_by("nombre"),
            search_fields=['nombre__icontains'],
            max_results=10,
        )
    )
    especialidad = forms.ModelChoiceField(
        queryset=Especialidad.objects.all(),
        label=u'Especialidad',
        widget=ModelSelect2Widget(
            queryset=Especialidad.objects.all().order_by("nombre"),
            search_fields=['nombre__icontains'],
            max_results=10,
        )
    )
    profesion = forms.ModelChoiceField(
        queryset=Profesion.objects.all(),
        label=u'Profesion',
        widget=ModelSelect2Widget(
            queryset=Profesion.objects.all().order_by("nombre"),
            search_fields=['nombre__icontains'],
            max_results=10,
        )
    )

    provincia = forms.ModelChoiceField(
        queryset=Provincia.objects.all(),
        label=u'Provincia',
        widget=ModelSelect2Widget(
            queryset=Provincia.objects.filter(departamento_id=Departamento.objects.get(codigo='06')).order_by("nombre"),
            search_fields=['nombre__icontains'],
            max_results=10,
        )
    )

    fechainicio = forms.DateField(
        label=u'Fecha Inicio',
        widget=DateWidget(
            usel10n=True,
            bootstrap_version=3,
            attrs={'data-datepicker-type': "4", "data-provide": "datepicker-inline"},
            options={
                'autoclose': 'true'
            }
        )

    )

    fechafin = forms.DateField(
        label=u'Fecha Fin',
        widget=DateWidget(
            usel10n=True,
            bootstrap_version=3,
            attrs={'data-datepicker-type': "4", "data-provide": "datepicker-inline"},
            options={
                'autoclose': 'true'
            }
        )

    )

    estado = ModelCheckWidget(label="Estado", textYES="Activo", textNO="Inactivo", initial=True)

    class Meta:
        model = Trabajador
        fields = [
            'persona', 'area', 'funcioncargo', 'especialidad', 'profesion', 'provincia',
            'direccion', 'modalidad', 'fechainicio', 'fechafin', 'estado'
        ]

    def __init__(self, *args, **kwargs):
        super(TrabajadorForm, self).__init__(*args, **kwargs)
        for i, (fname, field) in enumerate(self.fields.iteritems()):
            field.widget.attrs['class'] = 'form-control'
        self.fields['funcioncargo'].label = 'Cargo o funcion'
        self.fields['fechainicio'].label = 'Fecha Inicio'
        self.fields['fechafin'].label = 'Fecha Fin'

    def clean(self):
        if self.cleaned_data.get("persona", None) != None:
            if Trabajador.objects.filter(persona=self.cleaned_data["persona"]).exclude(pk=self.instance.pk).count() > 0:
                self.add_error("persona", u"Esta persona ya es trabajador")
