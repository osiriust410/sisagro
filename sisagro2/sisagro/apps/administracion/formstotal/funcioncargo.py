# -*- coding: utf-8 -*-
from django.forms import ModelForm
from ..models import FuncionCargo

class FuncionCargoForm(ModelForm):

	def __init__(self, *args, **kwargs):
		super(FuncionCargoForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'

	class Meta:
		model = FuncionCargo
		fields = ['nombre']

