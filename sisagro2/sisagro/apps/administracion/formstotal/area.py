# -*- coding: utf-8 -*-
from django.forms import ModelForm

from sisagro.widgets.inputcheck import ModelCheckWidget
from ..models import Area

class AreaForm(ModelForm):

	estado = ModelCheckWidget(label="Estado", textYES="Activo", textNO="Inactivo", initial=True)
	esagencia = ModelCheckWidget(label="Es Agencia", textYES="Si", textNO="No", initial=False)

	def __init__(self, *args, **kwargs):
		super(AreaForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'

	class Meta:
		model = Area
		fields = ['nombre', 'abreviatura', 'estado', 'esagencia']

