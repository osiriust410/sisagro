# -*- coding: utf-8 -*-
from django.forms import ModelForm
from ..models import Acceso

class AccesoForm(ModelForm):

	def __init__(self, *args, **kwargs):
		super(AccesoForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'

	class Meta:
		model = Acceso
		fields = '__all__'
