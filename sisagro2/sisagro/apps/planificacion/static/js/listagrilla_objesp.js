$modalForm_oeLoaded = null;
$grillaOtrasAccionesButton_oe = [];
//$grillaOtrasAccionesAction = {}

function actionFormatter(value, row, index) {
	return [
	    '<a class="btn-action edit " href="javascript:void(0)" title="Editar" role="button">',
	    '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>',
	    '<a class="btn-action remove" href="javascript:void(0)" title="Eliminar">',
	    '<span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>',
	].concat($grillaOtrasAccionesButton_oe).join('');
}
window.actionEventsOE = {
    'click .edit': function (e, value, row, index) {
        idValue_oe = row['id'];
        opcion_oe = "Editar";
        url_post_oe = url_editar_oe + idValue_oe;
        $modalForm_oe.modal('show');
        $modalForm_oe.waitMe({text: "Cargando..."});
    },
    'click .remove': function (e, value, row, index) {
        opcion_oe = "Eliminar";
        idValue_oe = row['id'];
        url_post_oe = url_eliminar_oe + idValue_oe;
        $modalForm_oe.modal('show');
        $modalForm_oe.waitMe({text: "Cargando..."});
    },
};
$( document ).ready(function() {
	$("#btnNuevoObjEsp").click(function(event) {
		opcion_oe = "Nuev" + $formGenero_oe;
		idValue_oe = 0;
		url_post_oe = url_crear_oe;
		$modalForm_oe.modal('show');
		$modalForm_oe.waitMe({text:"Cargando..."});
	});
	$modalForm_oe.on('show.bs.modal', function (e) {
		btnSave = $modalForm_oe.find("#btnGuardarObjEsp");
		$(this).find('.modal-body').html("");
		if(opcion_oe=="Eliminar") {
			$txtBtn = "Eliminando";
			url_load = url_eliminar_oe + idValue_oe;
			$(this).find('h4').text("Eliminar " + $formTitulo_oe);
			btnSave.html("Eliminar");
		}
		else {
			btnSave.html("Guardar");
			if(opcion_oe.indexOf("Nuev") > -1 || opcion_oe == "Editar") {
                if (idValue_oe == 0) {
                    $txtBtn = "Guardando";
                    url_load = url_crear_oe;
                    $(this).find('h4').text("Nuev" + $formGenero_oe + " " + $formTitulo_oe);
                } else {
                    $txtBtn = "Actualizando";
                    url_load = url_editar_oe + idValue_oe;
                    $(this).find('h4').text("Editanto " + $formTitulo_oe);
                }
            }
            else {
				$txtBtn = "Guardando";
				$(this).find('h4').text(opcion_oe + " - " + $formTitulo_oe);
			}
        }
        $(this).find('.modal-body').load(url_load, function(e) {
        	$modalForm_oe.waitMe('hide');
        	if ($modalForm_oeLoaded) eval($modalForm_oeLoaded);
        	if ($('#' + $firstField_oe)) {
        		setTimeout(function() { $('#' + $firstField_oe).focus(); }, 500);
            }
		});
	});
	$modalForm_oe.on('shown.bs.modal', function(e) {
    });
	$("#btnGuardarObjEsp").on('click', function(event) {
		$formView_oe.waitMe({text:$txtBtn+"..."});
		form = $formView_oe.serialize();
		$.post(url_post_oe, form).done(function(data) {
			if(data.value){
				toastr["success"](data.msg);
				$formView_oe.waitMe('hide');
				$modalForm_oe.modal('hide');
				$tablaView_oe.bootstrapTable('refresh');
			}else{
				toastr["error"](data.msg);
				$formView_oe.waitMe('hide');
				$modalForm_oe.find('.modal-body').html(data);
			}
		});
	});
});
