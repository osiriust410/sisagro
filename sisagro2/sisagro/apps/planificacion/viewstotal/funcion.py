from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView, ListView
from django.http import HttpResponse, HttpResponseRedirect
import json

from ...inicio.models import AuditableMixin
from ..models import Funcion
from ..formstotal.funcion import FuncionForm
from ...inicio.mixin import LoginRequiredMixin

class FuncionView(LoginRequiredMixin, TemplateView):
    template_name = "planificacion/funcion/inicio.html"

class FuncionListView(LoginRequiredMixin, ListView):

    def get(self, request):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        # data = {}
        orderby = ""
        if order != 'asc':
            orderby = "-"

        funciones = Funcion.objects.all().order_by('-id')

        if len(search) > 0:
            funciones = funciones.filter(nombre__icontains=search)
            # profesion = Profesion.objects.filter(Q(nombre__startswith=search) | Q(apellido__startswith=search) | Q(dni__startswith=search))

        if sort != '':
            funciones = funciones.order_by(orderby + sort)

        paginador = Paginator(funciones, limit)

        try:
            funciones = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            funciones = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            funciones = paginador.page(paginador.num_pages)

        return render(
            request,
            'planificacion/funcion/datos.json',
            {
                'funciones': funciones,
                'total': paginador.count
            }
        )

class FuncionCreateView(LoginRequiredMixin, AuditableMixin, CreateView):
    model = Funcion
    form_class = FuncionForm
    template_name = "planificacion/funcion/formulario.html"

    def form_valid(self, form):
        super(FuncionCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(FuncionCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class FuncionUpdateView(LoginRequiredMixin, AuditableMixin, UpdateView):
    model = Funcion
    form_class = FuncionForm
    template_name = "planificacion/funcion/formulario.html"

    def dispatch(self, *args, **kwargs):
        self.funcion_id = kwargs['pk']
        return super(FuncionUpdateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        super(FuncionUpdateView, self).form_valid(form)
        funcion = Funcion.objects.get(pk=self.funcion_id)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(FuncionUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

    def get_context_data(self, **kwargs):
        context = super(FuncionUpdateView, self).get_context_data(**kwargs)
        context["Funcion_id"] = self.funcion_id
        return context

class FuncionDeleteView(LoginRequiredMixin, DeleteView):
    model = Funcion
    form_class = FuncionForm
    template_name = "planificacion/funcion/eliminar.html"

    def dispatch(self, *args, **kwargs):
        self.Funcion_id = kwargs['pk']
        return super(FuncionDeleteView, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def get_context_data(self, **kwargs):
        context = super(FuncionDeleteView, self).get_context_data(**kwargs)
        context["Funcion_id"] = self.Funcion_id
        return context

