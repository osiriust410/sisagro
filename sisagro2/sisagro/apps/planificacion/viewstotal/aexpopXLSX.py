# -*- coding: utf-8 -*-
import xlwt

from django.http import HttpResponse
from ..models import AeXPop
from datetime import datetime
from django.utils import formats

def ListaAeXPopXLSX(request):

    aexpops = enumerate(AeXPop.objects.all().order_by('accionestrategica__objetivoespecifico__anio', 'accionestrategica__objetivoespecifico__estado'))

    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="AeXPop_' + str(formats.date_format(datetime.now(), "d-m-Y")) + '.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Lista')

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    
    #Encabezado
    row_num = row_num + 1
    columns = ['', '', '', 'Dirección Regional de Agricultura Cajamarca', '', '', '', '', '', '']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    row_num = row_num + 1
    columns = ['', '', '', 'SisDRAC', '', '', '', '', '', '']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    row_num = row_num + 1
    columns = ['', '', '', 'Lista de Acciones Estratégicas Producto Proyecto', '', '', '', '', '', '']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    #Encabezados
    row_num = row_num + 3
    columns = [
                'Nro', 
                'Programa Presupuestal',
                'Producto Proyecto',
                'PDRC - Código', 
                'PDRC', 
                'Periodo PDRC', 
                'Eje PDRC', 
                'Objetivo General', 
                'Objetivo Específico', 
                'Año', 
                'Estado', 
                'Acción Estratégica Código', 
                'Acción Estratégica']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    #Llenando datos
    font_style = xlwt.XFStyle()
    for i, aexpop in aexpops:
        row_num = row_num + 1

        columns = [
                        i + 1, 
                        "%s" % (aexpop.productoproyecto.programapresupuestal), 
                        "%s" % (aexpop.productoproyecto.nombre), 
                        "%s" % (aexpop.accionestrategica.objetivoespecifico.objetivopdrc.pdrc.codigo), 
                        "%s" % (aexpop.accionestrategica.objetivoespecifico.objetivopdrc.pdrc.descripcion), 
                        "%s" % (aexpop.accionestrategica.objetivoespecifico.objetivopdrc.pdrc.periodo),
                        "%s" % (aexpop.accionestrategica.objetivoespecifico.objetivopdrc.pdrc.ejeecon), 
                        "%s - %s" % (aexpop.accionestrategica.objetivoespecifico.objetivopdrc.codigo, aexpop.accionestrategica.objetivoespecifico.objetivopdrc.descripcion), 
                        "%s" % (aexpop.accionestrategica.objetivoespecifico.descripcion), 
                        "%s" % (aexpop.accionestrategica.objetivoespecifico.anio), 
                        "%s" % (aexpop.accionestrategica.objetivoespecifico.estado), 
                        "%s" % (aexpop.accionestrategica.codigo),
                        "%s" % (aexpop.accionestrategica.nombre),
                    ]

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    ws.write(row_num + 3, 2, 'Reporte generado el ' + str(formats.date_format(datetime.now(), "d-m-Y h:i:s a")), font_style)

    wb.save(response)
    return response