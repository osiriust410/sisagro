import json

from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import FormView, DetailView

from ..models import Actividad, MetaFF
from ..formstotal.metaffprogramado import MetaFFTipoForm, MetaFFProdProyForm, MetaFFAccEstForm, MetaFFActividadForm
from ...inicio.mixin import LoginRequiredMixin


class MetaFFETipoView(LoginRequiredMixin, FormView):
    template_name = "planificacion/metaffejecutado/tipo.html"
    form_class = MetaFFTipoForm

    def get_context_data(self, **kwargs):
        data = super(MetaFFETipoView, self).get_context_data(**kwargs)
        data['formTipo'] = data.get('form')
        return data

class MetaFFEProdProyView(LoginRequiredMixin, FormView):
    template_name = "planificacion/metaffejecutado/productoproyecto.html"
    form_class = MetaFFProdProyForm

    def get_form_kwargs(self):
        kwargs = super(MetaFFEProdProyView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(MetaFFEProdProyView, self).get_context_data(**kwargs)
        data['formProdProy'] = data.get('form')
        return data

class MetaFFEAccEstView(LoginRequiredMixin, FormView):
    template_name = "planificacion/metaffejecutado/accionestrategica.html"
    form_class = MetaFFAccEstForm

    def get_form_kwargs(self):
        kwargs = super(MetaFFEAccEstView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(MetaFFEAccEstView, self).get_context_data(**kwargs)
        data['formAccEst'] = data.get('form')
        return data

class MetaFFEActividadView(LoginRequiredMixin, FormView):
    template_name = "planificacion/metaffejecutado/actividad.html"
    form_class = MetaFFActividadForm

    def get_form_kwargs(self):
        kwargs = super(MetaFFEActividadView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(MetaFFEActividadView, self).get_context_data(**kwargs)
        data['formActividad'] = data.get('form')
        return data

class MetaFFETableView(LoginRequiredMixin, DetailView):
    template_name = "planificacion/metaffejecutado/table.html"
    model = Actividad
    context_object_name = "oActividad"

    def render_to_response(self, context, **response_kwargs):
        context["oMetasff"] = self.object.metaff_set.order_by("mes__numero")
        _result = super(MetaFFETableView, self).render_to_response(context, **response_kwargs)
        return _result

def MetaFFEUpdateView(request):
    msg = {}
    msg["pk"] = request.POST["pk"]
    msg["value"] = request.POST["value"]
    msg['success'] = True
    try:
        metaff = MetaFF.objects.get(pk=msg["pk"])
        if request.POST["tipo"] == "fis":
            metaff.metafisica_ejecutada = msg["value"]
        if request.POST["tipo"] == "fin":
            metaff.metafinanciera_ejecutada = msg["value"]
        metaff.save()
    except Exception as e:
        msg['success'] = False
    return HttpResponse(json.dumps(msg), content_type='application/json')



