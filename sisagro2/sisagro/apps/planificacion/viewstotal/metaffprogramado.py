import json

from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import FormView, DetailView

from ..models import Actividad, MetaFF
from ..formstotal.metaffprogramado import MetaFFTipoForm, MetaFFProdProyForm, MetaFFAccEstForm, MetaFFActividadForm
from ...inicio.mixin import LoginRequiredMixin


class MetaFFPTipoView(LoginRequiredMixin, FormView):
    template_name = "planificacion/metaffprogramado/tipo.html"
    form_class = MetaFFTipoForm

    def get_context_data(self, **kwargs):
        data = super(MetaFFPTipoView, self).get_context_data(**kwargs)
        data['formTipo'] = data.get('form')
        return data

class MetaFFPProdProyView(LoginRequiredMixin, FormView):
    template_name = "planificacion/metaffprogramado/productoproyecto.html"
    form_class = MetaFFProdProyForm

    def get_form_kwargs(self):
        kwargs = super(MetaFFPProdProyView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(MetaFFPProdProyView, self).get_context_data(**kwargs)
        data['formProdProy'] = data.get('form')
        return data

class MetaFFPAccEstView(LoginRequiredMixin, FormView):
    template_name = "planificacion/metaffprogramado/accionestrategica.html"
    form_class = MetaFFAccEstForm

    def get_form_kwargs(self):
        kwargs = super(MetaFFPAccEstView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(MetaFFPAccEstView, self).get_context_data(**kwargs)
        data['formAccEst'] = data.get('form')
        return data

class MetaFFPActividadView(LoginRequiredMixin, FormView):
    template_name = "planificacion/metaffprogramado/actividad.html"
    form_class = MetaFFActividadForm

    def get_form_kwargs(self):
        kwargs = super(MetaFFPActividadView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(MetaFFPActividadView, self).get_context_data(**kwargs)
        data['formActividad'] = data.get('form')
        return data

class MetaFFPTableView(LoginRequiredMixin, DetailView):
    template_name = "planificacion/metaffprogramado/table.html"
    model = Actividad
    context_object_name = "oActividad"

    def render_to_response(self, context, **response_kwargs):
        context["oMetasff"] = self.object.metaff_set.order_by("mes__numero")
        _result = super(MetaFFPTableView, self).render_to_response(context, **response_kwargs)
        return _result

def MetaFFPUpdateView(request):
    msg = {}
    msg["pk"] = request.POST["pk"]
    msg["value"] = request.POST["value"]
    msg['success'] = True
    try:
        metaff = MetaFF.objects.get(pk=msg["pk"])
        if request.POST["tipo"] == "fis":
            metaff.metafisica_programada = msg["value"]
        if request.POST["tipo"] == "fin":
            metaff.metafinanciera_programada = msg["value"]
        metaff.save()
    except Exception as e:
        msg['success'] = False
    return HttpResponse(json.dumps(msg), content_type='application/json')



