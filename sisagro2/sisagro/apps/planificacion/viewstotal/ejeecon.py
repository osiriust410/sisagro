from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView, ListView
from django.http import HttpResponse
import json

from ...inicio.models import AuditableMixin
from ..formstotal.ejeecon import EjeEconForm
from ...inicio.mixin import LoginRequiredMixin
from ..models import EjeEcon

class EjeEconView(LoginRequiredMixin, TemplateView):
    template_name = "planificacion/ejeecon/inicio.html"

class EjeEconListView(LoginRequiredMixin, ListView):

    def get(self, request):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        # data = {}
        orderby = ""
        if order != 'asc':
            orderby = "-"

        ejesecons = EjeEcon.objects.all().order_by('-id')

        if len(search) > 0:
            ejesecons = ejesecons.filter(nombre__icontains=search)
            # profesion = Profesion.objects.filter(Q(nombre__startswith=search) | Q(apellido__startswith=search) | Q(dni__startswith=search))

        if sort != '':
            ejesecons = ejesecons.order_by(orderby + sort)

        paginador = Paginator(ejesecons, limit)

        try:
            ejesecons = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            ejesecons = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            ejesecons = paginador.page(paginador.num_pages)

        return render(
            request,
            'planificacion/ejeecon/datos.json',
            {
                'ejesecons': ejesecons,
                'total': paginador.count
            }
        )

class EjeEconCreateView(AuditableMixin, CreateView):
    model = EjeEcon
    form_class = EjeEconForm
    template_name = "planificacion/ejeecon/formulario.html"

    def form_valid(self, form):
        super(EjeEconCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(EjeEconCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class EjeEconUpdateView(AuditableMixin, UpdateView):
    model = EjeEcon
    form_class = EjeEconForm
    template_name = "planificacion/ejeecon/formulario.html"

    def dispatch(self, *args, **kwargs):
        self.ejeecon_id = kwargs['pk']
        return super(EjeEconUpdateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        super(EjeEconUpdateView, self).form_valid(form)
        ejeecon = EjeEcon.objects.get(pk=self.ejeecon_id)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(EjeEconUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

    def get_context_data(self, **kwargs):
        context = super(EjeEconUpdateView, self).get_context_data(**kwargs)
        context["ejeecon_id"] = self.ejeecon_id
        return context

class EjeEconDeleteView(DeleteView):
    model = EjeEcon
    form_class = EjeEconForm
    template_name = "planificacion/ejeecon/eliminar.html"

    def dispatch(self, *args, **kwargs):
        self.ejeecon_id = kwargs['pk']
        return super(EjeEconDeleteView, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def get_context_data(self, **kwargs):
        context = super(EjeEconDeleteView, self).get_context_data(**kwargs)
        context["ejeecon_id"] = self.ejeecon_id
        return context

