from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView, ListView
from django.http import HttpResponse, HttpResponseRedirect
import json

from ...inicio.models import AuditableMixin
from ..models import Periodo
from ..formstotal.periodo import PeriodoForm
from ...inicio.mixin import LoginRequiredMixin

class PeriodoView(LoginRequiredMixin, TemplateView):
    template_name = "planificacion/periodo/inicio.html"

class PeriodoListView(LoginRequiredMixin, ListView):

    def get(self, request):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        # data = {}
        orderby = ""
        if order != 'asc':
            orderby = "-"

        periodo = Periodo.objects.all().order_by('-id')

        if len(search) > 0:
            periodo = periodo.filter(nombre__icontains=search)
            # profesion = Profesion.objects.filter(Q(nombre__startswith=search) | Q(apellido__startswith=search) | Q(dni__startswith=search))

        if sort != '':
            periodo = periodo.order_by(orderby + sort)

        paginador = Paginator(periodo, limit)

        try:
            periodo = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            periodo = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            periodo = paginador.page(paginador.num_pages)

        return render(
            request,
            'planificacion/periodo/datos.json',
            {
                'periodo': periodo,
                'total': paginador.count
            }
        )

class PeriodoCreateView(LoginRequiredMixin, AuditableMixin, CreateView):
    model = Periodo
    form_class = PeriodoForm
    template_name = "planificacion/periodo/formulario.html"

    def form_valid(self, form):
        super(PeriodoCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(PeriodoCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class PeriodoUpdateView(LoginRequiredMixin, AuditableMixin, UpdateView):
    model = Periodo
    form_class = PeriodoForm
    template_name = "planificacion/periodo/formulario.html"

    def dispatch(self, *args, **kwargs):
        self.periodo_id = kwargs['pk']
        return super(PeriodoUpdateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        super(PeriodoUpdateView, self).form_valid(form)
        periodo = Periodo.objects.get(pk=self.periodo_id)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(PeriodoUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

    def get_context_data(self, **kwargs):
        context = super(PeriodoUpdateView, self).get_context_data(**kwargs)
        context["periodo_id"] = self.periodo_id
        return context

class PeriodoDeleteView(LoginRequiredMixin, DeleteView):
    model = Periodo
    form_class = PeriodoForm
    template_name = "planificacion/periodo/eliminar.html"

    def dispatch(self, *args, **kwargs):
        self.periodo_id = kwargs['pk']
        return super(PeriodoDeleteView, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def get_context_data(self, **kwargs):
        context = super(PeriodoDeleteView, self).get_context_data(**kwargs)
        context["periodo_id"] = self.periodo_id
        return context

