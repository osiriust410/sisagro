import json

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import FormView, DetailView, ListView, CreateView, UpdateView, DeleteView

from ..formstotal.aexpop import AeXPopProgPptalForm, AeXPopProdProyForm, AeXPopForm
from ...inicio.models import AuditableMixin
from ..models import ProductoProyecto, AeXPop
from ...inicio.mixin import LoginRequiredMixin


class AeXPopSelectView(LoginRequiredMixin, FormView):
    template_name = "planificacion/aexpop/progpptal.html"
    form_class = AeXPopProgPptalForm

    def get_form_kwargs(self):
        kwargs = super(AeXPopSelectView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(AeXPopSelectView, self).get_context_data(**kwargs)
        data['formProgPptal'] = data.get('form')
        return data

class AeXPopProdProyView(LoginRequiredMixin, FormView):
    template_name = "planificacion/aexpop/prodproy.html"
    form_class = AeXPopProdProyForm

    def get_form_kwargs(self):
        kwargs = super(AeXPopProdProyView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(AeXPopProdProyView, self).get_context_data(**kwargs)
        data['formProdProy'] = data.get('form')
        return data

class AeXPopTableView(LoginRequiredMixin, DetailView):
    template_name = "planificacion/aexpop/table.html"
    model = ProductoProyecto
    context_object_name = "oProdProy"

class AeXPopListView(LoginRequiredMixin, ListView):

    def get(self, request, *args, **kwargs):

        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        ppid = kwargs.get('prodproy', 0)

        orderby = ""
        if order != 'asc':
            orderby = "-"

        aexpops = AeXPop.objects.filter(productoproyecto_id=ppid).order_by('-id')

        if len(search) > 0:
            aexpops = aexpops.filter(
                Q(accionestrategica__nombre__icontains=search) |
                Q(accionestrategica__codigo__icontains=search)
            )

        if sort != '':
            aexpops = aexpops.order_by(orderby + sort)

        paginador = Paginator(aexpops, limit)

        try:
            aexpops = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            aexpops = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            aexpops = paginador.page(paginador.num_pages)

        return render(
            request,
            'planificacion/aexpop/datos.json',
            {
                'aexpops': aexpops,
                'total': paginador.count
            }
        )

class AeXPopCreateView(AuditableMixin, CreateView):
    model = AeXPop
    form_class = AeXPopForm
    template_name = "planificacion/aexpop/formulario.html"

    def form_valid(self, form):
        super(AeXPopCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(AeXPopCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class AeXPopUpdateView(AuditableMixin, UpdateView):
    model = AeXPop
    form_class = AeXPopForm
    template_name = "planificacion/aexpop/formulario.html"

    def form_valid(self, form):
        super(AeXPopUpdateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(AeXPopUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class AeXPopDeleteView(DeleteView):
    model = AeXPop
    form_class = AeXPopForm
    template_name = "planificacion/aexpop/eliminar.html"

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        try:
            self.get_object().delete()
        except Exception as e:
            msg['value'] = False
            return render(request, self.template_name, {'object': self.get_object(), 'delete_error': str(e)})
        return HttpResponse(json.dumps(msg), content_type='application/json')
