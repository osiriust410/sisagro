from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView, ListView
from django.http import HttpResponse, HttpResponseRedirect
import json

from ...inicio.models import AuditableMixin
from ..models import DivisionFuncional
from ..formstotal.divisionfuncional import DivisionFuncionalForm
from ...inicio.mixin import LoginRequiredMixin

class DivisionFuncionalView(LoginRequiredMixin, TemplateView):
    template_name = "planificacion/divisionfuncional/inicio.html"

class DivisionFuncionalListView(LoginRequiredMixin, ListView):

    def get(self, request):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        # data = {}
        orderby = ""
        if order != 'asc':
            orderby = "-"

        divisionfuncional = DivisionFuncional.objects.all().order_by('-id')

        if len(search) > 0:
            divisionfuncional = divisionfuncional.filter(nombre__icontains=search)
            # profesion = Profesion.objects.filter(Q(nombre__startswith=search) | Q(apellido__startswith=search) | Q(dni__startswith=search))

        if sort != '':
            divisionfuncional = divisionfuncional.order_by(orderby + sort)

        paginador = Paginator(divisionfuncional, limit)

        try:
            divisionfuncional = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            divisionfuncional = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            divisionfuncional = paginador.page(paginador.num_pages)

        return render(
            request,
            'planificacion/divisionfuncional/datos.json',
            {
                'divisionfuncional': divisionfuncional,
                'total': paginador.count
            }
        )

class DivisionFuncionalCreateView(LoginRequiredMixin, AuditableMixin, CreateView):
    model = DivisionFuncional
    form_class = DivisionFuncionalForm
    template_name = "planificacion/divisionfuncional/formulario.html"

    def form_valid(self, form):
        super(DivisionFuncionalCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(DivisionFuncionalCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class DivisionFuncionalUpdateView(LoginRequiredMixin, AuditableMixin, UpdateView):
    model = DivisionFuncional
    form_class = DivisionFuncionalForm
    template_name = "planificacion/grupofuncional/formulario.html"

    def dispatch(self, *args, **kwargs):
        self.grupofuncional_id = kwargs['pk']
        return super(DivisionFuncionalUpdateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        super(DivisionFuncionalUpdateView, self).form_valid(form)
        grupofuncional = DivisionFuncional.objects.get(pk=self.grupofuncional_id)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(DivisionFuncionalUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

    def get_context_data(self, **kwargs):
        context = super(DivisionFuncionalUpdateView, self).get_context_data(**kwargs)
        context["divisionfuncional_id"] = self.grupofuncional_id
        return context

class DivisionFuncionalDeleteView(LoginRequiredMixin, DeleteView):
    model = DivisionFuncional
    form_class = DivisionFuncionalForm
    template_name = "planificacion/divisionfuncional/eliminar.html"

    def dispatch(self, *args, **kwargs):
        self.divisionfuncional_id = kwargs['pk']
        return super(DivisionFuncionalDeleteView, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def get_context_data(self, **kwargs):
        context = super(DivisionFuncionalDeleteView, self).get_context_data(**kwargs)
        context["divisionfuncional_id"] = self.divisionfuncional_id
        return context

