import json

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import FormView, DetailView, ListView, CreateView, UpdateView, DeleteView

from ...inicio.models import AuditableMixin
from ..models import Actividad, SubActividadComponente
from ..formstotal.subactividadcomponente import SubActividadComponenteProdProyForm, SubActividadComponenteAccEstForm, \
    SubActividadComponenteActividadForm, SubActividadComponenteForm
from ...inicio.mixin import LoginRequiredMixin


class SubActividadComponenteProdProyView(LoginRequiredMixin, FormView):
    template_name = "planificacion/subactividadcomponente/prodproy.html"
    form_class = SubActividadComponenteProdProyForm

    def get_form_kwargs(self):
        kwargs = super(SubActividadComponenteProdProyView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(SubActividadComponenteProdProyView, self).get_context_data(**kwargs)
        data['formProdProy'] = data.get('form')
        return data

class SubActividadComponenteAccEstView(LoginRequiredMixin, FormView):
    template_name = "planificacion/subactividadcomponente/accest.html"
    form_class = SubActividadComponenteAccEstForm

    def get_form_kwargs(self):
        kwargs = super(SubActividadComponenteAccEstView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(SubActividadComponenteAccEstView, self).get_context_data(**kwargs)
        data['formAccEst'] = data.get('form')
        return data

class SubActividadComponenteActividadView(LoginRequiredMixin, FormView):
    template_name = "planificacion/subactividadcomponente/actividad.html"
    form_class = SubActividadComponenteActividadForm

    def get_form_kwargs(self):
        kwargs = super(SubActividadComponenteActividadView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(SubActividadComponenteActividadView, self).get_context_data(**kwargs)
        data['formActividad'] = data.get('form')
        return data

class SubActividadComponenteTablaView(LoginRequiredMixin, DetailView):
    template_name = "planificacion/subactividadcomponente/tabla.html"
    model = Actividad
    context_object_name = "oActividad"

class SubActividadComponenteListView(LoginRequiredMixin, ListView):

    def get(self, request, *args, **kwargs):

        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        actid = kwargs.get('act', 0)

        orderby = ""
        if order != 'asc':
            orderby = "-"

        subactividades = SubActividadComponente.objects.filter(actividad_id=actid).order_by('-id')

        if len(search) > 0:
            subactividades = subactividades.filter(
                Q(nombre__icontains=search) |
                Q(codigo__icontains=search)
            )

        if sort != '':
            subactividades = subactividades.order_by(orderby + sort)

        paginador = Paginator(subactividades, limit)

        try:
            subactividades = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            subactividades = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            subactividades = paginador.page(paginador.num_pages)

        return render(
            request,
            'planificacion/subactividadcomponente/datos.json',
            {
                'subactividades': subactividades,
                'total': paginador.count
            }
        )

class SubActividadComponenteCreateView(AuditableMixin, CreateView):
    model = SubActividadComponente
    form_class = SubActividadComponenteForm
    template_name = "planificacion/subactividadcomponente/formulario.html"

    def form_valid(self, form):
        super(SubActividadComponenteCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(SubActividadComponenteCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class SubActividadComponenteUpdateView(AuditableMixin, UpdateView):
    model = SubActividadComponente
    form_class = SubActividadComponenteForm
    template_name = "planificacion/subactividadcomponente/formulario.html"

    def form_valid(self, form):
        super(SubActividadComponenteUpdateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(SubActividadComponenteUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class SubActividadComponenteDeleteView(DeleteView):
    model = SubActividadComponente
    form_class = SubActividadComponenteForm
    template_name = "planificacion/subactividadcomponente/eliminar.html"

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        try:
            self.get_object().delete()
        except Exception as e:
            msg['value'] = False
            return render(request, self.template_name, {'object': self.get_object(), 'delete_error': str(e)})
        return HttpResponse(json.dumps(msg), content_type='application/json')



