# -*- coding: utf-8 -*-
import xlwt

from django.http import HttpResponse
from ..models import ObjetivoPdrc
from datetime import datetime
from django.utils import formats

def ListaObjetivoPdrcXLSX(request):

    objetivos = enumerate(ObjetivoPdrc.objects.all())

    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="ObjetivoPdrc_' + str(formats.date_format(datetime.now(), "d-m-Y")) + '.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Lista')

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    
    #Encabezado
    row_num = row_num + 1
    columns = ['', '', '', 'Dirección Regional de Agricultura Cajamarca', '', '', '', '', '', '']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    row_num = row_num + 1
    columns = ['', '', '', 'SisDRAC', '', '', '', '', '', '']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    row_num = row_num + 1
    columns = ['', '', '', 'PDRC Objetivos', '', '', '', '', '', '']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    #Encabezados
    row_num = row_num + 3
    columns = ['Nro', 'Periodo', 'Eje Económico', 'PDR Código', 'PDRC Descripcion', 'PDRC Estado', 'Código', 'Descripción', 'Estado']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    #Llenando datos
    font_style = xlwt.XFStyle()
    for i, objetivo in objetivos:
        row_num = row_num + 1

        columns = [
                        i + 1, 
                        "%s" % (objetivo.pdrc.periodo),
                        "%s" % (objetivo.pdrc.ejeecon),
                        "%s" % (objetivo.pdrc.codigo),
                        "%s" % (objetivo.pdrc.descripcion),
                        "%s" % (objetivo.pdrc.estado),
                        "%s" % (objetivo.codigo),
                        "%s" % (objetivo.descripcion),
                        "%s" % (objetivo.estado),
                    ]

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    ws.write(row_num + 3, 2, 'Reporte generado el ' + str(formats.date_format(datetime.now(), "d-m-Y h:i:s a")), font_style)

    wb.save(response)
    return response