from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView, ListView
from django.http import HttpResponse
import json

from ..models import ActividadCatalogo
from ...inicio.models import AuditableMixin
from ..formstotal.actividadcatalogo import ActividadCatalogoForm
from ...inicio.mixin import LoginRequiredMixin

class ActividadCatalogoView(LoginRequiredMixin, TemplateView):
    template_name = "planificacion/actividadcatalogo/inicio.html"

class ActividadCatalogoListView(LoginRequiredMixin, ListView):

    def get(self, request):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        # data = {}
        orderby = ""
        if order != 'asc':
            orderby = "-"

        actividadescatalogos = ActividadCatalogo.objects.all().order_by('-id')

        if len(search) > 0:
            actividadescatalogos = actividadescatalogos.filter(nombre__icontains=search)

        if sort != '':
            actividadescatalogos = actividadescatalogos.order_by(orderby + sort)

        paginador = Paginator(actividadescatalogos, limit)

        try:
            actividadescatalogos = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            actividadescatalogos = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            actividadescatalogos = paginador.page(paginador.num_pages)

        return render(
            request,
            'planificacion/actividadcatalogo/datos.json',
            {
                'actividadescatalogos': actividadescatalogos,
                'total': paginador.count
            }
        )

class ActividadCatalogoCreateView(AuditableMixin, CreateView):
    model = ActividadCatalogo
    form_class = ActividadCatalogoForm
    template_name = "planificacion/actividadcatalogo/formulario.html"

    def form_valid(self, form):
        super(ActividadCatalogoCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(ActividadCatalogoCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class ActividadCatalogoUpdateView(AuditableMixin, UpdateView):
    model = ActividadCatalogo
    form_class = ActividadCatalogoForm
    template_name = "planificacion/actividadcatalogo/formulario.html"

    def form_valid(self, form):
        super(ActividadCatalogoUpdateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(ActividadCatalogoUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class ActividadCatalogoDeleteView(DeleteView):
    model = ActividadCatalogo
    form_class = ActividadCatalogoForm
    template_name = "planificacion/actividadcatalogo/eliminar.html"

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        try:
            self.get_object().delete()
        except Exception as e:
            msg['value'] = False
            return render(request, self.template_name, {'object': self.get_object(), 'delete_error': str(e)})
        return HttpResponse(json.dumps(msg), content_type='application/json')


