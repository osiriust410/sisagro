import json

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import FormView, DetailView, ListView, CreateView, UpdateView, DeleteView

from ...inicio.models import AuditableMixin
from ..models import AeXPop, Actividad
from ..formstotal.actividad import ActividadProdProyForm, ActividadAccEstForm, ActividadForm, ActividadCadProForm
from ...inicio.mixin import LoginRequiredMixin


class ActividadProdProyView(LoginRequiredMixin, FormView):
    template_name = "planificacion/actividad/prodproy.html"
    form_class = ActividadProdProyForm

    def get_form_kwargs(self):
        kwargs = super(ActividadProdProyView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(ActividadProdProyView, self).get_context_data(**kwargs)
        data['formProdProy'] = data.get('form')
        return data

class ActividadAccEstView(LoginRequiredMixin, FormView):
    template_name = "planificacion/actividad/accest.html"
    form_class = ActividadAccEstForm

    def get_form_kwargs(self):
        kwargs = super(ActividadAccEstView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(ActividadAccEstView, self).get_context_data(**kwargs)
        data['formAccEst'] = data.get('form')
        return data

class ActividadTableView(LoginRequiredMixin, DetailView):
    template_name = "planificacion/actividad/tabla.html"
    model = AeXPop
    context_object_name = "oAeXPop"

    def get_context_data(self, **kwargs):
        data = super(ActividadTableView, self).get_context_data(**kwargs)
        data["formCadPro"] = ActividadCadProForm
        return data

class ActividadListView(LoginRequiredMixin, ListView):

    def get(self, request, *args, **kwargs):

        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        aexpopid = kwargs.get('aexpop', 0)
        cadproid = kwargs.get('cadpro', 0)

        orderby = ""
        if order != 'asc':
            orderby = "-"

        actividades = Actividad.objects.filter(aexpop_id=aexpopid, cadenaproductiva_id=cadproid).order_by('-id')

        if len(search) > 0:
            actividades = actividades.filter(
                # Q(cadenaproductiva__nombre__icontains=search) |
                Q(nombre__icontains=search)
            )

        if sort != '':
            actividades = actividades.order_by(orderby + sort)

        paginador = Paginator(actividades, limit)

        try:
            actividades = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            actividades = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            actividades = paginador.page(paginador.num_pages)

        return render(
            request,
            'planificacion/actividad/datos.json',
            {
                'actividades': actividades,
                'total': paginador.count
            }
        )

class ActividadCreateView(AuditableMixin, CreateView):
    model = Actividad
    form_class = ActividadForm
    template_name = "planificacion/actividad/formulario.html"

    def form_valid(self, form):
        form.request = self.request
        super(ActividadCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        form.request = self.request
        super(ActividadCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class ActividadUpdateView(AuditableMixin, UpdateView):
    model = Actividad
    form_class = ActividadForm
    template_name = "planificacion/Actividad/formulario.html"

    def form_valid(self, form):
        form.request = self.request
        super(ActividadUpdateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        form.request = self.request
        super(ActividadUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class ActividadDeleteView(DeleteView):
    model = Actividad
    form_class = ActividadForm
    template_name = "planificacion/Actividad/eliminar.html"

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        try:
            self.get_object().delete()
        except Exception as e:
            msg['value'] = False
            return render(request, self.template_name, {'object': self.get_object(), 'delete_error': str(e)})
        return HttpResponse(json.dumps(msg), content_type='application/json')



