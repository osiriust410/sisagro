# -*- coding: utf-8 -*-
import xlwt

from django.http import HttpResponse
from ..models import ProductoProyecto
from datetime import datetime
from django.utils import formats

def ListaProductoProyectoXLSX(request):

    productos = enumerate(ProductoProyecto.objects.all().order_by('codigo'))

    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="ProductosPys_' + str(formats.date_format(datetime.now(), "d-m-Y")) + '.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Lista')

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    
    #Encabezado
    row_num = row_num + 1
    columns = ['', '', '', 'Dirección Regional de Agricultura Cajamarca', '', '', '', '', '', '']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    row_num = row_num + 1
    columns = ['', '', '', 'SisDRAC', '', '', '', '', '', '']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    row_num = row_num + 1
    columns = ['', '', '', 'Producto Proyecto', '', '', '', '', '', '']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    #Encabezados
    row_num = row_num + 3
    columns = ['Nro', 'Código', 'Nombre', 'Descripción', 'Indicador', 'Unidad de Medida', 'UM', 'Programa Presupuestal', 'Tipo']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    #Llenando datos
    font_style = xlwt.XFStyle()
    for i, producto in productos:
        row_num = row_num + 1

        columns = [
                        i + 1, 
                        "%s" % (producto.codigo),
                        "%s" % (producto.nombre),
                        "%s" % (producto.descripcion),
                        "%s" % (producto.indicador),
                        "%s" % (producto.unidadmedida.nombre),
                        "%s" % (producto.unidadmedida.abreviatura),
                        "%s" % (producto.programapresupuestal),
                        "%s" % (producto.Tipo()), 
                    ]

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    ws.write(row_num + 3, 2, 'Reporte generado el ' + str(formats.date_format(datetime.now(), "d-m-Y h:i:s a")), font_style)

    wb.save(response)
    return response