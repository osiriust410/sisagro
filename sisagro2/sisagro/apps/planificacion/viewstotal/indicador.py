from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView, ListView
from django.http import HttpResponse, HttpResponseRedirect
import json

from ...inicio.models import AuditableMixin
from ..models import Indicador
from ..formstotal.indicador import IndicadorForm
from ...inicio.mixin import LoginRequiredMixin

class IndicadorView(LoginRequiredMixin, TemplateView):
    template_name = "planificacion/indicador/inicio.html"

class IndicadorListView(LoginRequiredMixin, ListView):

    def get(self, request):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        # data = {}
        orderby = ""
        if order != 'asc':
            orderby = "-"

        indicador = Indicador.objects.all().order_by('-id')

        if len(search) > 0:
            indicador = indicador.filter(nombre__icontains=search)
            # profesion = Profesion.objects.filter(Q(nombre__startswith=search) | Q(apellido__startswith=search) | Q(dni__startswith=search))

        if sort != '':
            indicador = indicador.order_by(orderby + sort)

        paginador = Paginator(indicador, limit)

        try:
            indicador = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            indicador = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            indicador = paginador.page(paginador.num_pages)

        return render(
            request,
            'planificacion/indicador/datos.json',
            {
                'indicador': indicador,
                'total': paginador.count
            }
        )

class IndicadorCreateView(LoginRequiredMixin, AuditableMixin, CreateView):
    model = Indicador
    form_class = IndicadorForm
    template_name = "planificacion/indicador/formulario.html"

    def form_valid(self, form):
        super(IndicadorCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(IndicadorCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class IndicadorUpdateView(LoginRequiredMixin, AuditableMixin, UpdateView):
    model = Indicador
    form_class = IndicadorForm
    template_name = "planificacion/indicador/formulario.html"

    def dispatch(self, *args, **kwargs):
        self.indicador_id = kwargs['pk']
        return super(IndicadorUpdateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        super(IndicadorUpdateView, self).form_valid(form)
        indicador = Indicador.objects.get(pk=self.indicador_id)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(IndicadorUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

    def get_context_data(self, **kwargs):
        context = super(IndicadorUpdateView, self).get_context_data(**kwargs)
        context["indicador_id"] = self.indicador_id
        return context

class IndicadorDeleteView(LoginRequiredMixin, DeleteView):
    model = Indicador
    form_class = IndicadorForm
    template_name = "planificacion/indicador/eliminar.html"

    def dispatch(self, *args, **kwargs):
        self.indicador_id = kwargs['pk']
        return super(IndicadorDeleteView, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def get_context_data(self, **kwargs):
        context = super(IndicadorDeleteView, self).get_context_data(**kwargs)
        context["indicador_id"] = self.indicador_id
        return context

