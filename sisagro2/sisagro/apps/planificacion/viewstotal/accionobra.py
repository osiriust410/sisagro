import json

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import FormView, DetailView, ListView, CreateView, UpdateView, DeleteView

from ...inicio.models import AuditableMixin
from ..models import AeXPop, AccionObra
from ..formstotal.accionobra import AccionObraProdProyForm, AccionObraAccEstForm, AccionObraForm
from ...inicio.mixin import LoginRequiredMixin


class AccionObraProdProyView(LoginRequiredMixin, FormView):
    template_name = "planificacion/accionobra/prodproy.html"
    form_class = AccionObraProdProyForm

    def get_form_kwargs(self):
        kwargs = super(AccionObraProdProyView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(AccionObraProdProyView, self).get_context_data(**kwargs)
        data['formProdProy'] = data.get('form')
        return data

class AccionObraAccEstView(LoginRequiredMixin, FormView):
    template_name = "planificacion/accionobra/accest.html"
    form_class = AccionObraAccEstForm

    def get_form_kwargs(self):
        kwargs = super(AccionObraAccEstView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(AccionObraAccEstView, self).get_context_data(**kwargs)
        data['formAccEst'] = data.get('form')
        return data

class AccionObraTableView(LoginRequiredMixin, DetailView):
    template_name = "planificacion/accionobra/tabla.html"
    model = AeXPop
    context_object_name = "oAeXPop"

class AccionObraListView(LoginRequiredMixin, ListView):

    def get(self, request, *args, **kwargs):

        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        aexpopid = kwargs.get('aexpop', 0)

        orderby = ""
        if order != 'asc':
            orderby = "-"

        accionesobras = AccionObra.objects.filter(aexpop_id=aexpopid).order_by('-id')

        if len(search) > 0:
            accionesobras = accionesobras.filter(
                Q(cadenaproductiva__nombre__icontains=search) |
                Q(caserio__nombre__icontains=search)
            )

        if sort != '':
            accionesobras = accionesobras.order_by(orderby + sort)

        paginador = Paginator(accionesobras, limit)

        try:
            accionesobras = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            accionesobras = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            accionesobras = paginador.page(paginador.num_pages)

        return render(
            request,
            'planificacion/accionobra/datos.json',
            {
                'accionesobras': accionesobras,
                'total': paginador.count
            }
        )

class AccionObraCreateView(AuditableMixin, CreateView):
    model = AccionObra
    form_class = AccionObraForm
    template_name = "planificacion/accionobra/formulario.html"

    def form_valid(self, form):
        super(AccionObraCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(AccionObraCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class AccionObraUpdateView(AuditableMixin, UpdateView):
    model = AccionObra
    form_class = AccionObraForm
    template_name = "planificacion/accionobra/formulario.html"

    def form_valid(self, form):
        super(AccionObraUpdateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(AccionObraUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class AccionObraDeleteView(DeleteView):
    model = AccionObra
    form_class = AccionObraForm
    template_name = "planificacion/accionobra/eliminar.html"

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        try:
            self.get_object().delete()
        except Exception as e:
            msg['value'] = False
            return render(request, self.template_name, {'object': self.get_object(), 'delete_error': str(e)})
        return HttpResponse(json.dumps(msg), content_type='application/json')



