import json

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import FormView, DetailView, ListView, CreateView, UpdateView, DeleteView

from ...inicio.models import AuditableMixin
from ..models import ObjetivoEspecifico, AccionEstrategica
from ..formstotal.accionestrategica import AccionEstrategicaPdrcForm, \
    AccionEstrategicaObjPdrcForm, AccionEstrategicaObjEspPdrcForm, AccionEstrategicaForm
from ...inicio.mixin import LoginRequiredMixin

class AccionEstrategicaPdrcView(LoginRequiredMixin, FormView):
    template_name = "planificacion/accionestrategica/pdrc.html"
    form_class = AccionEstrategicaPdrcForm

    def get_form_kwargs(self):
        kwargs = super(AccionEstrategicaPdrcView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(AccionEstrategicaPdrcView, self).get_context_data(**kwargs)
        data['formPdrc'] = data.get('form')
        return data

class AccionEstrategicaObjPdrcView(LoginRequiredMixin, FormView):
    template_name = "planificacion/accionestrategica/objpdrc.html"
    form_class = AccionEstrategicaObjPdrcForm

    def get_form_kwargs(self):
        kwargs = super(AccionEstrategicaObjPdrcView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(AccionEstrategicaObjPdrcView, self).get_context_data(**kwargs)
        data['formObjPdrc'] = data.get('form')
        return data

class AccionEstrategicaObjEspPdrcView(LoginRequiredMixin, FormView):
    template_name = "planificacion/accionestrategica/objesppdrc.html"
    form_class = AccionEstrategicaObjEspPdrcForm

    def get_form_kwargs(self):
        kwargs = super(AccionEstrategicaObjEspPdrcView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(AccionEstrategicaObjEspPdrcView, self).get_context_data(**kwargs)
        data['formObjEspPdrc'] = data.get('form')
        return data

class AccionEstrategicaTableView(LoginRequiredMixin, DetailView):
    template_name = "planificacion/accionestrategica/accionestrategica.html"
    model = ObjetivoEspecifico
    context_object_name = "ObjEsp"

class AccionEstrategicaListView(LoginRequiredMixin, ListView):

    def get(self, request, *args, **kwargs):

        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        objespid = kwargs.get('objesp', 0)

        orderby = ""
        if order != 'asc':
            orderby = "-"

        accionesestrategicas = AccionEstrategica.objects.filter(objetivoespecifico_id=objespid).order_by('-id')

        if len(search) > 0:
            accionesestrategicas = accionesestrategicas.filter(
                Q(nombre__icontains=search) |
                Q(codigo__icontains=search)
            )

        if sort != '':
            accionesestrategicas = accionesestrategicas.order_by(orderby + sort)

        paginador = Paginator(accionesestrategicas, limit)

        try:
            accionesestrategicas = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            accionesestrategicas = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            accionesestrategicas = paginador.page(paginador.num_pages)

        return render(
            request,
            'planificacion/accionestrategica/datos.json',
            {
                'accionesestrategicas': accionesestrategicas,
                'total': paginador.count
            }
        )

class AccionEstrategicaCreateView(AuditableMixin, CreateView):
    model = AccionEstrategica
    form_class = AccionEstrategicaForm
    template_name = "planificacion/accionestrategica/formulario.html"

    def form_valid(self, form):
        super(AccionEstrategicaCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(AccionEstrategicaCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class AccionEstrategicaUpdateView(AuditableMixin, UpdateView):
    model = AccionEstrategica
    form_class = AccionEstrategicaForm
    template_name = "planificacion/accionestrategica/formulario.html"

    def form_valid(self, form):
        super(AccionEstrategicaUpdateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(AccionEstrategicaUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class AccionEstrategicaDeleteView(DeleteView):
    model = AccionEstrategica
    form_class = AccionEstrategicaForm
    template_name = "planificacion/accionestrategica/eliminar.html"

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        try:
            self.get_object().delete()
        except Exception as e:
            msg['value'] = False
            return render(request, self.template_name, {'object': self.get_object(), 'delete_error': str(e)})
        return HttpResponse(json.dumps(msg), content_type='application/json')


