import json

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import FormView, DetailView, ListView, CreateView, UpdateView, DeleteView

from ...inicio.models import AuditableMixin
from ..models import ProgramaPresupuestal, ProductoProyecto
from ..formstotal.productoproyecto import ProductoProyectoSelectForm, ProductoProyectoForm
from ...inicio.mixin import LoginRequiredMixin


class ProductoView(LoginRequiredMixin, FormView):
    template_name = "planificacion/productoproyecto/prodinicio.html"
    form_class = ProductoProyectoSelectForm

    def get_form_kwargs(self):
        kwargs = super(ProductoView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(ProductoView, self).get_context_data(**kwargs)
        data['formProgPptal'] = data.get('form')
        return data

class ProyectoView(LoginRequiredMixin, FormView):
    template_name = "planificacion/productoproyecto/proyinicio.html"
    form_class = ProductoProyectoSelectForm

    def get_form_kwargs(self):
        kwargs = super(ProyectoView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(ProyectoView, self).get_context_data(**kwargs)
        data['formProgPptal'] = data.get('form')
        return data

class ProductoProyectoTableView(LoginRequiredMixin, DetailView):
    template_name = "planificacion/productoproyecto/tabla.html"
    model = ProgramaPresupuestal
    context_object_name = "oProgPptal"
    tipoPP = None

    def get(self, request, *args, **kwargs):
        self.tipoPP = kwargs.get("tipo", "")
        _result = super(ProductoProyectoTableView, self).get(request, *args, **kwargs)
        return _result

    def render_to_response(self, context, **response_kwargs):
        _result = super(ProductoProyectoTableView, self).render_to_response(context, **response_kwargs)
        context["tipoPP"] = self.tipoPP
        return _result

class ProductoProyectoListView(LoginRequiredMixin, ListView):

    def get(self, request, *args, **kwargs):

        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        ppid = kwargs.get('progptal', 0)
        tipo = kwargs.get('tipo', "")

        orderby = ""
        if order != 'asc':
            orderby = "-"

        productosproyectos = ProductoProyecto.objects.filter(
            programapresupuestal_id=ppid,
            tipo=tipo.upper()
        ).order_by('-id')

        if len(search) > 0:
            productosproyectos = productosproyectos.filter(
                Q(nombre__icontains=search) |
                Q(codigo__icontains=search) |
                Q(descripcion__icontains=search)
            )

        if sort != '':
            productosproyectos = productosproyectos.order_by(orderby + sort)

        paginador = Paginator(productosproyectos, limit)

        try:
            productosproyectos = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            productosproyectos = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            productosproyectos = paginador.page(paginador.num_pages)

        return render(
            request,
            'planificacion/productoproyecto/datos.json',
            {
                'productosproyectos': productosproyectos,
                'total': paginador.count
            }
        )

class ProductoProyectoCreateView(AuditableMixin, CreateView):
    model = ProductoProyecto
    form_class = ProductoProyectoForm
    template_name = "planificacion/productoproyecto/formulario.html"

    def get_form_kwargs(self):
        kwargs = super(ProductoProyectoCreateView, self).get_form_kwargs()
        kwargs['kwargs'] = self.kwargs
        kwargs['request'] = self.request
        return kwargs

    def form_valid(self, form):
        super(ProductoProyectoCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(ProductoProyectoCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class ProductoProyectoUpdateView(AuditableMixin, UpdateView):
    model = ProductoProyecto
    form_class = ProductoProyectoForm
    template_name = "planificacion/productoproyecto/formulario.html"

    def get_form_kwargs(self):
        kwargs = super(ProductoProyectoUpdateView, self).get_form_kwargs()
        kwargs['kwargs'] = self.kwargs
        kwargs['request'] = self.request
        return kwargs

    def form_valid(self, form):
        super(ProductoProyectoUpdateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(ProductoProyectoUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class ProductoProyectoDeleteView(DeleteView):
    model = ProductoProyecto
    form_class = ProductoProyectoForm
    template_name = "planificacion/productoproyecto/eliminar.html"

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        try:
            self.get_object().delete()
        except Exception as e:
            msg['value'] = False
            return render(request, self.template_name, {'object': self.get_object(), 'delete_error': str(e)})
        return HttpResponse(json.dumps(msg), content_type='application/json')
