from django.conf.urls import url

from .viewstotal import cadenaproductiva as cadpro
from .viewstotal import divisionfuncional as divfun
from .viewstotal import ejeecon as ejeco
from .viewstotal import funcion, pdrc, pdrcobjetivo
from .viewstotal import grupofuncional as grufun
from .viewstotal import indicador as indic
from .viewstotal import metaffprogramado as mffp
from .viewstotal import metaffejecutado as mffe
from .viewstotal import periodo as peri
from .viewstotal import subactividadcomponente as subactcpte
from .viewstotal import accionestrategica as accest
from .viewstotal import productoproyecto as propry
from .viewstotal import pdrcobjetivoespecifico as pdrcobjesp
from .viewstotal import aexpop
from .viewstotal import accionobra as accobr
from .viewstotal import actividad
from .viewstotal import programapresupuestal as propres
from .viewstotal import actividadcatalogo as actcat
from .viewstotal import subactividadcatalogo as subactcat
from .viewstotal import tareacatalogo as tarcat
from .viewstotal.accionestrategicaXLSX import *
from .viewstotal.programapresupuestalXLSX import *
from .viewstotal.productoproyectoXLSX import *
from .viewstotal.periodoXLSX import *
from .viewstotal.ejeeconXLSX import *
from .viewstotal.pdrcXLSX import *
from .viewstotal.pdrcobjetivoXLSX import *
from .viewstotal.pdrcobjetivoespecificoXLSX import *
from .viewstotal.aexpopXLSX import *
from .viewstotal.divisionfuncionalXLSX import *
from .viewstotal.funcionXLSX import *
from .viewstotal.grupofuncionalXLSX import *
from .viewstotal.cadenaproductivaXLSX import *

urlpatterns = [

    # ESPECIALIDAD
    url(r'^periodo/$', peri.PeriodoView.as_view(), name='periodo_inicio'),
    url(r'^periodo/listar/$', peri.PeriodoListView.as_view(), name='periodo_listar'),
    url(r'^periodo/crear/$', peri.PeriodoCreateView.as_view(), name='periodo_crear'),
    url(r'^periodo/actualizar/(?P<pk>\d+)$', peri.PeriodoUpdateView.as_view(), name='periodo_actualizar'),
    url(r'^periodo/eliminar/(?P<pk>\d+)$', peri.PeriodoDeleteView.as_view(), name='periodo_eliminar'),
    url(r'^periodo/xlsx/$', ListaPeriodosXLSX, name='periodo_xlsx'),

    # GRUPO FUNCIONAL
    url(r'^grupofuncional/$', grufun.GrupoFuncionalView.as_view(), name='grupofuncional_inicio'),
    url(r'^grupofuncional/listar/$', grufun.GrupoFuncionalListView.as_view(), name='grupofuncional_listar'),
    url(r'^grupofuncional/crear/$', grufun.GrupoFuncionalCreateView.as_view(), name='grupofuncional_crear'),
    url(r'^grupofuncional/actualizar/(?P<pk>\d+)$', grufun.GrupoFuncionalUpdateView.as_view(), name='grupofuncional_actualizar'),
    url(r'^grupofuncional/eliminar/(?P<pk>\d+)$', grufun.GrupoFuncionalDeleteView.as_view(), name='grupofuncional_eliminar'),
    url(r'^grupofuncional/xlsx/$', ListaGrupoFuncionalXLSX, name='grupofuncional_xlsx'),

    # INDICADOR
    url(r'^indicador/$', indic.IndicadorView.as_view(), name='indicador_inicio'),
    url(r'^indicador/listar/$', indic.IndicadorListView.as_view(), name='indicador_listar'),
    url(r'^indicador/crear/$', indic.IndicadorCreateView.as_view(), name='indicador_crear'),
    url(r'^indicador/actualizar/(?P<pk>\d+)$', indic.IndicadorUpdateView.as_view(), name='indicador_actualizar'),
    url(r'^indicador/eliminar/(?P<pk>\d+)$', indic.IndicadorDeleteView.as_view(), name='indicador_eliminar'),

    # EJE ECONOMICO
    url(r'^ejeecon/$', ejeco.EjeEconView.as_view(), name='ejeecon_inicio'),
    url(r'^ejeecon/listar/$', ejeco.EjeEconListView.as_view(), name='ejeecon_listar'),
    url(r'^ejeecon/crear/$', ejeco.EjeEconCreateView.as_view(), name='ejeecon_crear'),
    url(r'^ejeecon/actualizar/(?P<pk>\d+)$', ejeco.EjeEconUpdateView.as_view(), name='ejeecon_actualizar'),
    url(r'^ejeecon/eliminar/(?P<pk>\d+)$', ejeco.EjeEconDeleteView.as_view(), name='ejeecon_eliminar'),
    url(r'^ejeecon/xlsx/$', ListaEjesXLSX, name='ejeecon_xlsx'),

    # CADENA PRODUCTIVA
    url(r'^cadenaproductiva/$', cadpro.CadenaProductivaView.as_view(), name='cadenaproductiva_inicio'),
    url(r'^cadenaproductiva/listar/$', cadpro.CadenaProductivaListView.as_view(), name='cadenaproductiva_listar'),
    url(r'^cadenaproductiva/crear/$', cadpro.CadenaProductivaCreateView.as_view(), name='cadenaproductiva_crear'),
    url(r'^cadenaproductiva/actualizar/(?P<pk>\d+)$', cadpro.CadenaProductivaUpdateView.as_view(), name='cadenaproductiva_actualizar'),
    url(r'^cadenaproductiva/eliminar/(?P<pk>\d+)$', cadpro.CadenaProductivaDeleteView.as_view(), name='cadenaproductiva_eliminar'),
    url(r'^cadenaproductiva/xlsx/$', CadenaProductivaXLSX, name='cadenaproductiva_xlsx'),

    # FUNCION
  	url(r'^funcion/$', funcion.FuncionView.as_view(), name='funcion_inicio'),
    url(r'^funcion/listar/$', funcion.FuncionListView.as_view(), name='funcion_listar'),
    url(r'^funcion/crear/$', funcion.FuncionCreateView.as_view(), name='funcion_crear'),
    url(r'^funcion/actualizar/(?P<pk>\d+)$', funcion.FuncionUpdateView.as_view(), name='funcion_actualizar'),
    url(r'^funcion/eliminar/(?P<pk>\d+)$', funcion.FuncionDeleteView.as_view(), name='funcion_eliminar'),
    url(r'^funcion/xlsx/$', ListaFuncionXLSX, name='funcion_xlsx'),

    # PDRC
  	url(r'^pdrc/$', pdrc.PdrcView.as_view(), name='pdrc_inicio'),
	url(r'^pdrc/listar/$', pdrc.PdrcListView.as_view(), name='pdrc_listar'),
	url(r'^pdrc/crear/$', pdrc.PdrcCreateView.as_view(), name='pdrc_crear'),
	url(r'^pdrc/actualizar/(?P<pk>\d+)$', pdrc.PdrcUpdateView.as_view(), name='pdrc_actualizar'),
    url(r'^pdrc/eliminar/(?P<pk>\d+)$', pdrc.PdrcDeleteView.as_view(), name='pdrc_eliminar'),
	url(r'^pdrc/xlsx/$', ListaPDRCsXLSX, name='pdrc_xlsx'),

    # DIVISION FUNCIONAL
    url(r'^divisionfuncional/$', divfun.DivisionFuncionalView.as_view(), name='divisionfuncional_inicio'),
    url(r'^divisionfuncional/listar/$', divfun.DivisionFuncionalListView.as_view(), name='divisionfuncional_listar'),
    url(r'^divisionfuncional/crear/$', divfun.DivisionFuncionalCreateView.as_view(), name='divisionfuncional_crear'),
    url(r'^divisionfuncional/actualizar/(?P<pk>\d+)$', divfun.DivisionFuncionalUpdateView.as_view(), name='divisionfuncional_actualizar'),
    url(r'^divisionfuncional/eliminar/(?P<pk>\d+)$', divfun.DivisionFuncionalDeleteView.as_view(), name='divisionfuncional_eliminar'),
    url(r'^divisionfuncional/xlsx/$', ListaDivisionFuncionaXLSX, name='divisionfuncional_xlsx'),

    # META FISICA FINANCIERA - programada
    url(r'^metaffprogramado/$', mffp.MetaFFPTipoView.as_view(), name='metaffprogramado_inicio'),
    url(r'^metaffprogramado/prodproy/$', mffp.MetaFFPProdProyView.as_view(), name='metaffprogramado_prodproy'),
    url(r'^metaffprogramado/accest/$', mffp.MetaFFPAccEstView.as_view(), name='metaffprogramado_accionestrategica'),
    url(r'^metaffprogramado/actividad/$', mffp.MetaFFPActividadView.as_view(), name='metaffprogramado_actividad'),
    url(r'^metaffprogramado/tabla/(?P<pk>\d+)$', mffp.MetaFFPTableView.as_view(), name='metaffprogramado_tabla'),
    url(r'^metaffprogramado/actualizarmeta/$', mffp.MetaFFPUpdateView, name='metaffprogramado_actualizar'),

    # META FISICA FINANCIERA - ejecutada
    url(r'^metaffejecutado/$', mffe.MetaFFETipoView.as_view(), name='metaffejecutado_inicio'),
    url(r'^metaffejecutado/prodproy/$', mffe.MetaFFEProdProyView.as_view(), name='metaffejecutado_prodproy'),
    url(r'^metaffejecutado/accest/$', mffe.MetaFFEAccEstView.as_view(), name='metaffejecutado_accionestrategica'),
    url(r'^metaffejecutado/actividad/$', mffe.MetaFFEActividadView.as_view(), name='metaffejecutado_actividad'),
    url(r'^metaffejecutado/tabla/(?P<pk>\d+)$', mffe.MetaFFETableView.as_view(), name='metaffejecutado_tabla'),
    url(r'^metaffejecutado/actualizarmeta/$', mffe.MetaFFEUpdateView, name='metaffejecutado_actualizar'),

    # SUB ACTIVIDAD COMPONENTE
    url(r'^subactividadcomponente/$', subactcpte.SubActividadComponenteProdProyView.as_view(), name='subactividadcomponente_inicio'),
    url(r'^subactividadcomponente/accest/$', subactcpte.SubActividadComponenteAccEstView.as_view(), name='subactividadcomponente_accionestrategica'),
    url(r'^subactividadcomponente/actividad/$', subactcpte.SubActividadComponenteActividadView.as_view(), name='subactividadcomponente_actividad'),
    url(r'^subactividadcomponente/tabla/(?P<pk>\d+)$', subactcpte.SubActividadComponenteTablaView.as_view(), name='subactividadcomponente_tabla'),
    url(r'^subactividadcomponente/listar/(?P<act>\d+)$', subactcpte.SubActividadComponenteListView.as_view(), name='subactividadcomponente_listar'),
    url(r'^subactividadcomponente/crear/$', subactcpte.SubActividadComponenteCreateView.as_view(), name='subactividadcomponente_crear'),
    url(r'^subactividadcomponente/actualizar/(?P<pk>\d+)$', subactcpte.SubActividadComponenteUpdateView.as_view(), name='subactividadcomponente_actualizar'),
    url(r'^subactividadcomponente/eliminar/(?P<pk>\d+)$', subactcpte.SubActividadComponenteDeleteView.as_view(), name='subactividadcomponente_eliminar'),

    # OBJETIVOS PDRC
    url(r'^pdrcobjetivo/$', pdrcobjetivo.PdrcObjetivoView.as_view(), name='pdrcobjetivo_inicio'),
    url(r'^pdrcobjetivo/listar/$', pdrcobjetivo.PdrcObjetivoListView.as_view(), name='pdrcobjetivo_listar'),
    url(r'^pdrcobjetivo/crear/$', pdrcobjetivo.PdrcObjetivoCreateView.as_view(), name='pdrcobjetivo_crear'),
    url(r'^pdrcobjetivo/actualizar/(?P<pk>\d+)$', pdrcobjetivo.PdrcObjetivoUpdateView.as_view(), name='pdrcobjetivo_actualizar'),
    url(r'^pdrcobjetivo/eliminar/(?P<pk>\d+)$', pdrcobjetivo.PdrcObjetivoDeleteView.as_view(), name='pdrcobjetivo_eliminar'),
    url(r'^pdrcobjetivo/xlsx/$', ListaObjetivoPdrcXLSX, name='pdrcobjetivo_xlsx'),

    # OBJETIVOS ESPECIFICOS
    url(r'^pdrcobjesp/$', pdrcobjesp.ObjetivoEspecificoPdrcView.as_view(), name='objetivoespecifico_inicio'),
    url(r'^pdrcobjesp/pdrc/obj$', pdrcobjesp.ObjetivoEspecificoObjPdrcView.as_view(), name='objetivoespecifico_objpdrc'),
    url(r'^pdrcobjesp/(?P<pk>\d+)$', pdrcobjesp.PdrcObjEspView.as_view(), name='pdrcobjesp_inicio'),
    url(r'^pdrcobjetivo/listar/(?P<pkesp>\d+)$', pdrcobjesp.PdrcObjEspListView.as_view(), name='pdrcobjesp_listar'),
    url(r'^pdrcobjesp/crear/$', pdrcobjesp.ObjetivoEspecificoCreateView.as_view(), name='pdrcobjesp_crear'),
    url(r'^pdrcobjesp/actualizar/(?P<pk>\d+)$', pdrcobjesp.ObjetivoEspecificoUpdateView.as_view(), name='pdrcobjesp_actualizar'),
    url(r'^pdrcobjesp/eliminar/(?P<pk>\d+)$', pdrcobjesp.ObjetivoEspecificoDeleteView.as_view(), name='pdrcobjesp_eliminar'),
    url(r'^pdrcobjesp/xlsx/(?P<inicio>\d+)/(?P<fin>\d+)/$', ListaObjetivoEspecificoXLSX, name='pdrcobjesp_xlsx'),

    # ACCIONES ESTRATEGICAS
    url(r'^accionestrategica/$', accest.AccionEstrategicaPdrcView.as_view(), name='accionestrategica_inicio'),
    url(r'^accionestrategica/pdrc/obj$', accest.AccionEstrategicaObjPdrcView.as_view(), name='accionestrategica_objpdrc'),
    url(r'^accionestrategica/pdrc/obj/esp$', accest.AccionEstrategicaObjEspPdrcView.as_view(), name='accionestrategica_objesppdrc'),
    url(r'^accionestrategica/oe/(?P<pk>\d+)$', accest.AccionEstrategicaTableView.as_view(), name='accionestrategica_tabla'),
    url(r'^accionestrategica/listar/(?P<objesp>\d+)$', accest.AccionEstrategicaListView.as_view(), name='accionestrategica_listar'),
    url(r'^accionestrategica/crear/$', accest.AccionEstrategicaCreateView.as_view(), name='accionestrategica_crear'),
    url(r'^accionestrategica/actualizar/(?P<pk>\d+)$', accest.AccionEstrategicaUpdateView.as_view(), name='accionestrategica_actualizar'),
    url(r'^accionestrategica/eliminar/(?P<pk>\d+)$', accest.AccionEstrategicaDeleteView.as_view(), name='accionestrategica_eliminar'),
    url(r'^accionestrategica/xlsx/$', ListaAccionEstrategicaXLSX, name='accionestrategica_xlsx'),

    # PRODUCTO/PROYECTO
    url(r'^producto/$', propry.ProductoView.as_view(), name='producto_inicio'),
    url(r'^proyecto/$', propry.ProyectoView.as_view(), name='proyecto_inicio'),
    url(r'^productoproyecto/tabla/(?P<tipo>\w+)/(?P<pk>\d+)$', propry.ProductoProyectoTableView.as_view(), name='productoproyecto_tabla'),
    url(r'^productoproyecto/listar/(?P<tipo>\w+)/(?P<progptal>\d+)$', propry.ProductoProyectoListView.as_view(), name='productoproyecto_listar'),
    url(r'^productoproyecto/crear/(?P<tipo>\w+)/$', propry.ProductoProyectoCreateView.as_view(), name='productoproyecto_crear'),
    url(r'^productoproyecto/actualizar/(?P<pk>\d+)$', propry.ProductoProyectoUpdateView.as_view(), name='productoproyecto_actualizar'),
    url(r'^productoproyecto/eliminar/(?P<pk>\d+)$', propry.ProductoProyectoDeleteView.as_view(), name='productoproyecto_eliminar'),
    url(r'^productoproyecto/xlsx/$', ListaProductoProyectoXLSX, name='productoproyecto_xlsx'),

    # ACCIONES ESTRATEGICAS DE LOS PRODUCTOS/PROYECTOS
    url(r'^aexpop/$', aexpop.AeXPopSelectView.as_view(), name='aexpop_inicio'),
    url(r'^aexpop/prodproy/$', aexpop.AeXPopProdProyView.as_view(), name='aexpop_prodproy'),
    url(r'^aexpop/tabla/(?P<pk>\d+)$', aexpop.AeXPopTableView.as_view(), name='aexpop_tabla'),
    url(r'^aexpop/listar/(?P<prodproy>\d+)$', aexpop.AeXPopListView.as_view(), name='aexpop_listar'),
    url(r'^aexpop/crear/$', aexpop.AeXPopCreateView.as_view(), name='aexpop_crear'),
    url(r'^aexpop/actualizar/(?P<pk>\d+)$', aexpop.AeXPopUpdateView.as_view(), name='aexpop_actualizar'),
    url(r'^aexpop/eliminar/(?P<pk>\d+)$', aexpop.AeXPopDeleteView.as_view(), name='aexpop_eliminar'),
    url(r'^aexpop/xlsx/$', ListaAeXPopXLSX, name='aexpop_xlsx'),

    # ACCION OBRA
    url(r'^accionobra/$', accobr.AccionObraProdProyView.as_view(), name='accionobra_inicio'),
    url(r'^accionobra/accest/$', accobr.AccionObraAccEstView.as_view(), name='accionobra_accest'),
    url(r'^accionobra/tabla/(?P<pk>\d+)$', accobr.AccionObraTableView.as_view(), name='accionobra_tabla'),
    url(r'^accionobra/listar/(?P<aexpop>\d+)$', accobr.AccionObraListView.as_view(), name='accionobra_listar'),
    url(r'^accionobra/crear/$', accobr.AccionObraCreateView.as_view(), name='accionobra_crear'),
    url(r'^accionobra/actualizar/(?P<pk>\d+)$', accobr.AccionObraUpdateView.as_view(), name='accionobra_actualizar'),
    url(r'^accionobra/eliminar/(?P<pk>\d+)$', accobr.AccionObraDeleteView.as_view(), name='accionobra_eliminar'),

    # ACCIONES ESTRATEGICAS DE LOS PRODUCTOS/PROYECTOS
    url(r'^programapresupuestal/$', propres.ProgramaPresupuestalView.as_view(), name='programapresupuestal_inicio'),
    url(r'^programapresupuestal/listar/$', propres.ProgramaPresupuestalListView.as_view(), name='programapresupuestal_listar'),
    url(r'^programapresupuestal/crear/$', propres.ProgramaPresupuestalCreateView.as_view(), name='programapresupuestal_crear'),
    url(r'^programapresupuestal/actualizar/(?P<pk>\d+)$', propres.ProgramaPresupuestalUpdateView.as_view(), name='programapresupuestal_actualizar'),
    url(r'^programapresupuestal/eliminar/(?P<pk>\d+)$', propres.ProgramaPresupuestalDeleteView.as_view(), name='programapresupuestal_eliminar'),
    url(r'^programapresupuestal/xlsx/$', ListaProgramaPresupuestalXLSX, name='programapresupuestal_xlsx'),

    # ACTIVIDADES
    url(r'^actividad/$', actividad.ActividadProdProyView.as_view(), name='actividad_inicio'),
    url(r'^actividad/accest/$', actividad.ActividadAccEstView.as_view(), name='actividad_accest'),
    url(r'^actividad/tabla/(?P<pk>\d+)$', actividad.ActividadTableView.as_view(), name='actividad_tabla'),
    url(r'^actividad/listar/(?P<aexpop>\d+)/(?P<cadpro>\d+)$', actividad.ActividadListView.as_view(), name='actividad_listar'),
    url(r'^actividad/crear/$', actividad.ActividadCreateView.as_view(), name='actividad_crear'),
    url(r'^actividad/actualizar/(?P<pk>\d+)$', actividad.ActividadUpdateView.as_view(), name='actividad_actualizar'),
    url(r'^actividad/eliminar/(?P<pk>\d+)$', actividad.ActividadDeleteView.as_view(), name='actividad_eliminar'),

    # CATALOGO DE ACTIVIDADES
    url(r'^actividadcatalogo/$', actcat.ActividadCatalogoView.as_view(), name='actividadcatalogo_inicio'),
    url(r'^actividadcatalogo/listar/$', actcat.ActividadCatalogoListView.as_view(), name='actividadcatalogo_listar'),
    url(r'^actividadcatalogo/crear/$', actcat.ActividadCatalogoCreateView.as_view(), name='actividadcatalogo_crear'),
    url(r'^actividadcatalogo/actualizar/(?P<pk>\d+)$', actcat.ActividadCatalogoUpdateView.as_view(), name='actividadcatalogo_actualizar'),
    url(r'^actividadcatalogo/eliminar/(?P<pk>\d+)$', actcat.ActividadCatalogoDeleteView.as_view(), name='actividadcatalogo_eliminar'),

    # CATALOGO DE SUB ACTIVIDADES
    url(r'^subactividadcatalogo/$', subactcat.SubActividadCatalogoView.as_view(), name='subactividadcatalogo_inicio'),
    url(r'^subactividadcatalogo/listar/$', subactcat.SubActividadCatalogoListView.as_view(), name='subactividadcatalogo_listar'),
    url(r'^subactividadcatalogo/crear/$', subactcat.SubActividadCatalogoCreateView.as_view(), name='subactividadcatalogo_crear'),
    url(r'^subactividadcatalogo/actualizar/(?P<pk>\d+)$', subactcat.SubActividadCatalogoUpdateView.as_view(), name='subactividadcatalogo_actualizar'),
    url(r'^subactividadcatalogo/eliminar/(?P<pk>\d+)$', subactcat.SubActividadCatalogoDeleteView.as_view(), name='subactividadcatalogo_eliminar'),

    # CATALOGO DE TAREAS
    url(r'^tareacatalogo/$', tarcat.TareaCatalogoView.as_view(), name='tareacatalogo_inicio'),
    url(r'^tareacatalogo/listar/$', tarcat.TareaCatalogoListView.as_view(), name='tareacatalogo_listar'),
    url(r'^tareacatalogo/crear/$', tarcat.TareaCatalogoCreateView.as_view(), name='tareacatalogo_crear'),
    url(r'^tareacatalogo/actualizar/(?P<pk>\d+)$', tarcat.TareaCatalogoUpdateView.as_view(), name='tareacatalogo_actualizar'),
    url(r'^tareacatalogo/eliminar/(?P<pk>\d+)$', tarcat.TareaCatalogoDeleteView.as_view(), name='tareacatalogo_eliminar'),

]
