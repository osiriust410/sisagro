from django.forms import ModelForm

from sisagro.widgets.inputcheck import ModelCheckWidget
from ..models import Periodo

class PeriodoForm(ModelForm):

	estado = ModelCheckWidget(label="Estado", textYES="Activo", textNO="Inactivo", initial=True)

	def __init__(self, *args, **kwargs):
		super(PeriodoForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'

	class Meta:
		model = Periodo
		fields = ['nombre','estado']

