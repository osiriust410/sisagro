import datetime
from django.forms import ModelForm
from django import forms
from django.utils.encoding import force_text
from django_select2.forms import ModelSelect2Widget

from sisagro.widgets.inputcheck import ModelCheckWidget
from ..models import Periodo, EjeEcon, Pdrc, ObjetivoPdrc, ObjetivoEspecifico


class widgetOpPdrc(ModelSelect2Widget):
    search_fields = ["codigo__icontains", "descripcion__icontains"]
    queryset = Pdrc.objects.order_by("descripcion")
    max_results = 10

    def label_from_instance(self, obj):
        return force_text(obj.descripcion) + " (" + force_text(obj.periodo.nombre) + "-" + force_text(
            obj.ejeecon.nombre) + ")"


class widgetObjPdrc(ModelSelect2Widget):
    search_fields = ["codigo__icontains", "descripcion__icontains"]
    model = ObjetivoPdrc
    max_results = 10

    def label_from_instance(self, obj):
        return force_text(obj.codigo) + " " + force_text(obj.descripcion)


class ObjetivoEspecificoPdrcForm(forms.Form):
    oppdrc = forms.ChoiceField(
        required=False,
        label=u'Plan de Desarrollo',
        widget=widgetOpPdrc(
            attrs={'id': 'cb_pdrc', 'class': 'formPdrc_load', 'data-allow-clear': 'false'}
        )
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(ObjetivoEspecificoPdrcForm, self).__init__(*args, **kwargs)


class ObjetivoEspecificoObjPdrcForm(forms.Form):
    opobjpdrc = forms.ChoiceField(
        required=False,
        label=u'Objetivo General',
        widget=widgetObjPdrc(
            attrs={'id': 'cb_objpdrc', 'class': 'formObjPdrc_load', 'data-allow-clear': 'false'}
        )
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(ObjetivoEspecificoObjPdrcForm, self).__init__(*args, **kwargs)
        self.fields["opobjpdrc"].widget.queryset = ObjetivoPdrc.objects.filter(
            pdrc_id=self.request.GET.get("oppdrc", 0)
        ).order_by("descripcion")


class ObjetivoEspecificoForm(ModelForm):

    estado = ModelCheckWidget(label="Estado", textYES="Activo", textNO="Inactivo", initial=True)

    objetivoid = forms.IntegerField(
        widget=forms.HiddenInput
    )

    def __init__(self, *args, **kwargs):
        super(ObjetivoEspecificoForm, self).__init__(*args, **kwargs)
        for i, (fname, field) in enumerate(self.fields.iteritems()):
            field.widget.attrs['class'] = 'form-control'

    class Meta:
        model = ObjetivoEspecifico
        fields = ['codigo', 'descripcion', 'estado']

    def clean(self):
        self.instance.objetivopdrc_id = self.cleaned_data["objetivoid"]
        self.instance.anio = datetime.datetime.now().year
