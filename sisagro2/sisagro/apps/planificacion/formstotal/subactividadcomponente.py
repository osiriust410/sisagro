# -*- coding: utf-8 -*-
from django import forms
from django.utils.encoding import force_text
from django_select2.forms import ModelSelect2Widget

from apps.inicio.models import UnidadMedida
from ..models import CadenaProductiva, Actividad, SubActividadComponente, Indicador, ProductoProyecto, AeXPop, \
    SubActividadCatalogo


class widgetAccEst(ModelSelect2Widget):
    search_fields=["accionestrategica__nombre__icontains"]
    max_results=10

    def label_from_instance(self, obj):
        return force_text(obj.accionestrategica.codigo) + " " + force_text(obj.accionestrategica.nombre)

class SubActividadComponenteProdProyForm(forms.Form):
    opprodproy = forms.ChoiceField(
        label=u'Producto',
        required=False,
        widget=ModelSelect2Widget(
            attrs={'id': 'cb_prodproy', 'class': 'formProdProy_load', 'data-allow-clear': 'false'},
            queryset=ProductoProyecto.objects.filter(tipo=ProductoProyecto.tipoProdProy[0][0]).order_by("nombre"),
            model=ProductoProyecto,
            search_fields=['nombre__icontains'],
            max_results=10,
        )
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(SubActividadComponenteProdProyForm, self).__init__(*args, **kwargs)

class SubActividadComponenteAccEstForm(forms.Form):
    opaccest = forms.ChoiceField(
        required=False,
        label=u'Acción Estratégica',
        widget=widgetAccEst(
            attrs={'id': 'cb_accest', 'class': 'formAccEst_load', 'data-allow-clear': 'false'}
        )
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(SubActividadComponenteAccEstForm, self).__init__(*args, **kwargs)
        self.fields["opaccest"].widget.queryset = AeXPop.objects.filter(
            productoproyecto_id=self.request.GET.get("opprodproy", 0)
        ).order_by("accionestrategica__nombre")

class SubActividadComponenteActividadForm(forms.Form):
    opactividad = forms.ChoiceField(
        required=False,
        label=u'Actividad',
        widget=ModelSelect2Widget(
            attrs={'id': 'cb_actividad', 'class': 'formActividad_load', 'data-allow-clear': 'false'},
            search_fields=["subactividadcatalogo__nombre__icontains"],
            max_results=10
        )
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(SubActividadComponenteActividadForm, self).__init__(*args, **kwargs)
        self.fields["opactividad"].widget.queryset = Actividad.objects.filter(
            aexpop_id=self.request.GET.get("opaccest", 0)
        ).order_by("actividadcatalogo__nombre")

class SubActividadComponenteForm(forms.ModelForm):
    actividadid = forms.IntegerField(
        widget=forms.HiddenInput
    )

    indicador = forms.ModelChoiceField(
        label=u"Indicador",
        required=True,
        queryset=Indicador.objects.all(),
        widget=ModelSelect2Widget(
            queryset=Indicador.objects.order_by("nombre"),
            search_fields=["nombre__icontains"],
            max_results=10
        )
    )

    unidadmedida = forms.ModelChoiceField(
        label=u"Unidad de Medida",
        required=True,
        queryset=UnidadMedida.objects.all(),
        widget=ModelSelect2Widget(
            queryset=UnidadMedida.objects.order_by("nombre"),
            search_fields=["nombre__icontains"],
            max_results=10
        )
    )

    subactividadcatalogo = forms.ModelChoiceField(
        label=u"Nombre",
        required=True,
        queryset=SubActividadCatalogo.objects.all(),
        widget=ModelSelect2Widget(
            queryset=SubActividadCatalogo.objects.order_by("nombre"),
            search_fields=["nombre__icontains"],
            max_results=10
        )
    )

    def __init__(self, *args, **kwargs):
        super(SubActividadComponenteForm, self).__init__(*args, **kwargs)
        for i, (fname, field) in enumerate(self.fields.iteritems()):
            field.widget.attrs['class'] = 'form-control'
        self.fields["codigo"].label = u"Código"

    class Meta:
        model = SubActividadComponente
        fields = ['codigo', 'subactividadcatalogo', 'indicador', 'unidadmedida']

    def clean(self):
        self.instance.actividad_id = self.cleaned_data["actividadid"]

