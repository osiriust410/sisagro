from django.forms import ModelForm
from django import forms
from django_select2.forms import Select2Widget

from sisagro.widgets.inputcheck import ModelCheckWidget
from ..models import CadenaProductiva

class CadenaProductivaForm(ModelForm):

	estado = ModelCheckWidget(label="Estado", textYES="Activo", textNO="Inactivo", initial=True)

	tipo = forms.ChoiceField(
		label=u'Tipo',
		required=True,
		choices=CadenaProductiva.cadProd,
		widget=Select2Widget(attrs={'data-minimum-results-for-search': 'Infinity'})
	)

	def __init__(self, *args, **kwargs):
		super(CadenaProductivaForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'

	class Meta:
		model = CadenaProductiva
		fields = ['nombre', 'tipo', 'estado']

