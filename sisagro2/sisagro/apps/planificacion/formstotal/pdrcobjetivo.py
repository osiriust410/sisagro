# -*- coding: utf-8 -*-
from django.forms import ModelForm
from django import forms
from django.utils.encoding import force_text
from django_select2.forms import ModelSelect2Widget

from sisagro.widgets.inputcheck import ModelCheckWidget
from ..models import ObjetivoPdrc, Periodo, EjeEcon, Pdrc


class PdrcObjetivoOpcionesForm(forms.Form):

	oppdrc = forms.ModelChoiceField(
		queryset=Pdrc.objects.all(),
		label=u'Plan de Desarrollo',
        widget=ModelSelect2Widget(
			queryset=Pdrc.objects.order_by("descripcion"),
            search_fields=['codigo__icontains'],
            max_results=10,
        )
	)

	def __init__(self, *args, **kwargs):
		super(PdrcObjetivoOpcionesForm, self).__init__(*args, **kwargs)
		self.fields['oppdrc'].widget.attrs['class'] = 'cb_pdrc'

class PdrcObjetivoForm(ModelForm):
	pdrcid = forms.IntegerField(widget=forms.HiddenInput)

	estado = ModelCheckWidget(label="Estado", textYES="Activo", textNO="Inactivo", initial=True)

	class Meta:
		model = ObjetivoPdrc
		fields = ['pdrcid', 'codigo', 'descripcion', 'estado']

	def clean(self):
		clean_data = super(PdrcObjetivoForm, self).clean()
		self.instance.pdrc = Pdrc.objects.get(pk=clean_data["pdrcid"])
		return clean_data

	def __init__(self, *args, **kwargs):
		super(PdrcObjetivoForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'
