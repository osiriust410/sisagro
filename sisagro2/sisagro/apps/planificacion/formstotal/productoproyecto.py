from django import forms
from django.utils.encoding import force_text
from django_select2.forms import ModelSelect2Widget, Select2Widget

from ...inicio.models import UnidadMedida
from ..models import ProgramaPresupuestal, ProductoProyecto, Indicador


class widgetProgPptal(ModelSelect2Widget):
    queryset=ProgramaPresupuestal.objects.all().order_by("nombre")
    search_fields=['codigo__icontains', 'nombre__icontains']
    max_results=10

    def label_from_instance(self, obj):
        return force_text(obj.codigo) + " " + force_text(obj.nombre)

class ProductoProyectoSelectForm(forms.Form):
    opprogpptal = forms.ModelChoiceField(
        queryset=ProgramaPresupuestal.objects.all(),
        label=u'Programa Presupuestal',
        widget=widgetProgPptal(
            attrs={'id': 'cb_progpptal', 'class': 'formProgPptal_load', 'data-allow-clear': 'false'},
        )
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(ProductoProyectoSelectForm, self).__init__(*args, **kwargs)

class ProductoProyectoForm(forms.ModelForm):

    programapresupuestalid = forms.IntegerField(
        widget=forms.HiddenInput
    )

    unidadmedida = forms.ModelChoiceField(
        queryset=UnidadMedida.objects.all(),
        label=u'Unidad de Medida',
        widget=ModelSelect2Widget(
            queryset=UnidadMedida.objects.order_by("nombre"),
            max_results=10,
            search_fields=["nombre__icontains"],
        )
    )

    indicador = forms.ModelChoiceField(
        queryset=Indicador.objects.all(),
        label=u'Indicador',
        widget=ModelSelect2Widget(
            queryset=Indicador.objects.order_by("nombre"),
            max_results=10,
            search_fields=["nombre__icontains"],
        )
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        self.kwargs = kwargs.pop('kwargs')
        super(ProductoProyectoForm, self).__init__(*args, **kwargs)
        for i, (fname, field) in enumerate(self.fields.iteritems()):
            field.widget.attrs['class'] = 'form-control'

    class Meta:
        model = ProductoProyecto
        fields = ['indicador', 'unidadmedida', 'codigo', 'nombre', 'descripcion']

    def clean(self):
        self.instance.programapresupuestal_id = self.cleaned_data["programapresupuestalid"]
        if self.instance.pk == None:
            self.instance.tipo = self.kwargs.get("tipo", "").upper()

