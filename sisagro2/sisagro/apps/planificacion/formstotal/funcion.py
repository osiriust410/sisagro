from django.forms import ModelForm
from ..models import Funcion

class FuncionForm(ModelForm):

	def __init__(self, *args, **kwargs):
		super(FuncionForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'

	class Meta:
		model = Funcion
		fields = ['nombre']

