# -*- coding: utf-8 -*-
from django import forms
from django.utils.encoding import force_text
from django_select2.forms import ModelSelect2Widget

from ...inicio.models import Departamento, Provincia, Distrito, CentroPoblado, Caserio
from ..models import ProductoProyecto, AeXPop, AccionObra, CadenaProductiva


class widgetOpProdProy(ModelSelect2Widget):
    search_fields=["codigo__icontains", "nombre__icontains"]
    queryset=ProductoProyecto.objects.filter(tipo=ProductoProyecto.tipoProdProy[1][0]).order_by("nombre")
    max_results=10

    def label_from_instance(self, obj):
        return force_text(obj.codigo) + " " + force_text(obj.nombre)

class widgetAccEst(ModelSelect2Widget):
    search_fields=["accionestrategica__codigo__icontains", "accionestrategica__nombre__icontains"]
    max_results=10

    def label_from_instance(self, obj):
        return force_text(obj.accionestrategica.codigo) + " " + force_text(obj.accionestrategica.nombre)

class AccionObraProdProyForm(forms.Form):
    opprodproy = forms.ChoiceField(
        required=False,
        label=u'Proyecto',
        widget=widgetOpProdProy(
            attrs={'id': 'cb_prodproy', 'class': 'formProdProy_load', 'data-allow-clear': 'false'}
        )
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(AccionObraProdProyForm, self).__init__(*args, **kwargs)

class AccionObraAccEstForm(forms.Form):
    opaccest = forms.ChoiceField(
        required=False,
        label=u'Acción Estratégica',
        widget=widgetAccEst(
            attrs={'id': 'cb_accest', 'class': 'formAccEst_load', 'data-allow-clear': 'false'}
        )
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(AccionObraAccEstForm, self).__init__(*args, **kwargs)
        self.fields["opaccest"].widget.queryset = AeXPop.objects.filter(
            productoproyecto_id=self.request.GET.get("opprodproy", 0)
        ).order_by("accionestrategica__codigo")

class AccionObraForm(forms.ModelForm):

    aexpopid = forms.IntegerField(
        widget=forms.HiddenInput
    )

    departamento = forms.ModelChoiceField(
        label=u'Departamento',
        queryset=Departamento.objects.all(),
        widget=ModelSelect2Widget(
            queryset=Departamento.objects.order_by("nombre"),
            search_fields=["nombre__icontains"],
            max_results=10
        )
    )

    provincia = forms.ModelChoiceField(
        label=u'Provincia',
        queryset=Provincia.objects.all(),
        widget=ModelSelect2Widget(
            queryset=Provincia.objects.order_by("nombre"),
            search_fields=["nombre__icontains"],
            max_results=10,
            dependent_fields={'departamento': 'departamento'},
        )
    )

    distrito = forms.ModelChoiceField(
        label=u'Distrito',
        queryset=Distrito.objects.all(),
        widget=ModelSelect2Widget(
            queryset=Distrito.objects.order_by("nombre"),
            search_fields=["nombre__icontains"],
            max_results=10,
            dependent_fields={'provincia': 'provincia'},
        )
    )

    centropoblado = forms.ModelChoiceField(
        label=u'Centro Poblado',
        queryset=CentroPoblado.objects.all(),
        widget=ModelSelect2Widget(
            queryset=CentroPoblado.objects.order_by("nombre"),
            search_fields=["nombre__icontains"],
            max_results=10,
            dependent_fields={'distrito': 'distrito'},
        )
    )

    caserio = forms.ModelChoiceField(
        label=u'Caserío',
        queryset=Caserio.objects.all(),
        widget=ModelSelect2Widget(
            queryset=Caserio.objects.order_by("nombre"),
            search_fields=["nombre__icontains"],
            max_results=10,
            dependent_fields={'centropoblado': 'centropoblado'},
        )
    )

    cadenaproductiva = forms.ModelChoiceField(
        queryset=CadenaProductiva.objects.all(),
        label=u'Cadena Productiva',
        widget=ModelSelect2Widget(
            queryset=CadenaProductiva.objects.order_by("nombre"),
            search_fields=["nombre__icontains"],
            max_results=10
        )
    )

    def __init__(self, *args, **kwargs):
        super(AccionObraForm, self).__init__(*args, **kwargs)
        for i, (fname, field) in enumerate(self.fields.iteritems()):
            if field.widget.__class__ == forms.NumberInput:
                if self.instance.pk == None:
                    field.initial = 0
                dp = 2 if type(field) == forms.DecimalField else 0
                dpe = 0.01 if type(field) == forms.DecimalField else 1
                field.widget.attrs = {"class": 'touch-spin', "data-max": "Infinity", "data-decimals": dp,
                                      "data-step": dpe, "style": "text-align:right"}
            else:
                field.widget.attrs['class'] = 'form-control'
        self.fields["km"].label = u"Kilómetros"
        self.fields["ha"].label = u"Hectáreas"
        if self.instance.pk == None:
            self.fields["departamento"].initial = Departamento.objects.get(codigo="06")
        else:
            self.fields["departamento"].initial = self.instance.caserio.centropoblado.distrito.provincia.departamento
            self.fields["provincia"].initial = self.instance.caserio.centropoblado.distrito.provincia
            self.fields["distrito"].initial = self.instance.caserio.centropoblado.distrito
            self.fields["centropoblado"].initial = self.instance.caserio.centropoblado

    class Meta:
        model = AccionObra
        fields = [
            'aexpopid', 'departamento', 'provincia', 'distrito', 'centropoblado', 'caserio',
            'cadenaproductiva', 'km', 'volumen', 'conduce', 'presupuesto',
            'ha'
        ]

    def clean(self):
        self.instance.aexpop_id = self.cleaned_data["aexpopid"]

