from django.forms import ModelForm

from ..models import SubActividadCatalogo

class SubActividadCatalogoForm(ModelForm):

	def __init__(self, *args, **kwargs):
		super(SubActividadCatalogoForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'

	class Meta:
		model = SubActividadCatalogo
		fields = ['nombre']

