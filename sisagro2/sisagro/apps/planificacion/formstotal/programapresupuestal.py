from django import forms
from django.utils.encoding import force_text
from django_select2.forms import ModelSelect2Widget, Select2Widget


from ..models import ProgramaPresupuestal, Indicador, UnidadMedida

class ProgramaPresupuestalForm(forms.ModelForm):

    indicador = forms.ModelChoiceField(
        queryset=Indicador.objects.all(),
        label=u'Indicador',
        widget=ModelSelect2Widget(
            max_results=10,
            queryset=Indicador.objects.order_by("nombre"),
            search_fields=["nombre__icontains"],
        )
    )

    unidadmedida = forms.ModelChoiceField(
        label=u'Un. de Medida',
        queryset=UnidadMedida.objects.all(),
        widget=ModelSelect2Widget(
            max_results=10,
            queryset=UnidadMedida.objects.order_by("nombre"),
            search_fields=["nombre__icontains", "abreviatura__icontains"],
        )
    )

    class Meta:
        model = ProgramaPresupuestal
        fields = ['codigo', 'nombre', 'descripcion', 'unidadmedida','indicador']


    def __init__(self, *args, **kwargs):
        super(ProgramaPresupuestalForm, self).__init__(*args, **kwargs)
        for i, (fname, field) in enumerate(self.fields.iteritems()):
            field.widget.attrs['class'] = 'form-control'
