# -*- coding: utf-8 -*-
from django.forms import ModelForm
from django import forms
from django_select2.forms import ModelSelect2Widget

from sisagro.widgets.inputcheck import ModelCheckWidget
from ..models import Pdrc, Periodo, EjeEcon


class PdrcForm(ModelForm):

	estado = ModelCheckWidget(label="Estado", textYES="Activo", textNO="Inactivo", initial=True)

	periodo = forms.ModelChoiceField(
		label=u'Periodo',
		queryset=Periodo.objects.all(),
		widget=ModelSelect2Widget(
			queryset=Periodo.objects.order_by("nombre"),
			search_fields=["nombre__icontains"],
			max_results=10
		)
	)

	ejeecon = forms.ModelChoiceField(
		label=u'Eje Económico',
		queryset=EjeEcon.objects.all(),
		widget=ModelSelect2Widget(
			queryset=EjeEcon.objects.order_by("nombre"),
			search_fields=["nombre__icontains"],
			max_results=10
		)
	)

	class Meta:
		model = Pdrc
		fields = ['periodo', 'ejeecon', 'codigo', 'descripcion', 'estado']

	def __init__(self, *args, **kwargs):
		super(PdrcForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'
		self.fields['ejeecon'].label = 'Eje economico'
	