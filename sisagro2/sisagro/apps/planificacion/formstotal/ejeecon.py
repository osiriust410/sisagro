from django.forms import ModelForm

from sisagro.widgets.inputcheck import ModelCheckWidget
from ..models import EjeEcon

class EjeEconForm(ModelForm):

	estado = ModelCheckWidget(label="Estado", textYES="Activo", textNO="Inactivo", initial=True)

	def __init__(self, *args, **kwargs):
		super(EjeEconForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'

	class Meta:
		model = EjeEcon
		fields = ['nombre', 'descripcion', 'estado']
