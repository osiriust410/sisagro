# -*- coding: utf-8 -*-
import datetime
from django import forms
from django.forms import ModelForm
from django.utils.encoding import force_text
from django_select2.forms import ModelSelect2Widget, Select2Widget

from ...inicio.models import Mes, UnidadMedida
from ..models import ProductoProyecto, AeXPop, Actividad, MetaFF, Indicador

class Media:
    css = {
        'all': ('django_select2/css/select2.min.css',)
    }


class MetaFFTipoForm(forms.Form):
    optipo = forms.ChoiceField(
        label=u'Tipo',
        choices=ProductoProyecto.tipoProdProy,
        widget=Select2Widget(
            attrs={'id': 'cb_tipo', 'class': 'formTipo_load', 'data-allow-clear': 'false', 'data-minimum-results-for-search': 'Infinity'},
        )
    )

class MetaFFProdProyForm(forms.Form):
    opprodproy = forms.ModelChoiceField(
        queryset=ProductoProyecto.objects.all(),
        widget=ModelSelect2Widget(
            attrs={'id': 'cb_prodproy', 'class': 'formProdProy_load', 'data-allow-clear': 'false'},
            model=ProductoProyecto,
            search_fields=['nombre__icontains'],
            max_results=10,
        )
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(MetaFFProdProyForm, self).__init__(*args, **kwargs)
        self.fields["opprodproy"].label = u'Producto' if self.request.GET.get("optipo", "").upper() == "PR" else u'Proyecto'
        self.fields["opprodproy"].widget.queryset = ProductoProyecto.objects.filter(
            tipo=self.request.GET.get("optipo", "").upper()
        ).order_by("nombre")

class widgetActividad(ModelSelect2Widget):
    search_fields = ["actividadcatalogo__nombre__icontains"]
    model = Actividad
    max_results = 10

    def label_from_instance(self, obj):
        return force_text(obj.actividadcatalogo.nombre) + ' - ' + force_text("%s" % obj.anio)

class widgetAccEst(ModelSelect2Widget):
    search_fields=["accionestrategica__codigo__icontains", "accionestrategica__nombre__icontains"]
    model=AeXPop
    max_results=10

    def label_from_instance(self, obj):
        return force_text(obj.accionestrategica.nombre)

class MetaFFAccEstForm(forms.Form):
    opaccest = forms.ChoiceField(
        required=False,
        label=u'Acción Estratégica',
        widget=widgetAccEst(
            attrs={'id': 'cb_accest', 'class': 'formAccEst_load', 'data-allow-clear': 'false'}
        )
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(MetaFFAccEstForm, self).__init__(*args, **kwargs)
        self.fields["opaccest"].widget.queryset = AeXPop.objects.filter(
            productoproyecto_id=self.request.GET.get("opprodproy", 0)
        ).order_by("accionestrategica__nombre")

class MetaFFActividadForm(forms.Form):
    opactividad = forms.ChoiceField(
        required=False,
        label=u'Actividad',
        widget=widgetActividad(attrs={'id': 'cb_actividad', 'class': 'formActividad_load', 'data-allow-clear': 'false'},)
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(MetaFFActividadForm, self).__init__(*args, **kwargs)
        self.fields["opactividad"].widget.queryset = Actividad.objects.filter(
            aexpop_id=self.request.GET.get("opaccest", 0)
        ).order_by("actividadcatalogo__nombre")


