# -*- coding: utf-8 -*-
import datetime
from django import forms
from django.conf import settings
from django.utils.encoding import force_text
from django_select2.forms import ModelSelect2Widget, Select2Widget

from ...administracion.models import Area
from ...inicio.funciones import ListaAnios
from ...inicio.models import Departamento, Provincia, Distrito, CentroPoblado, Caserio, UnidadMedida, Mes
from ..models import ProductoProyecto, AeXPop, AccionObra, CadenaProductiva, Actividad, Funcion, DivisionFuncional, \
    GrupoFuncional, ActividadCatalogo, Indicador, MetaFF


class widgetOpProdProy(ModelSelect2Widget):
    search_fields = ["codigo__icontains", "nombre__icontains"]
    queryset = ProductoProyecto.objects.filter(tipo=ProductoProyecto.tipoProdProy[0][0]).order_by("nombre")
    max_results = 10

    def label_from_instance(self, obj):
        return force_text(obj.codigo) + " " + force_text(obj.nombre)


class widgetAccEst(ModelSelect2Widget):
    search_fields = ["accionestrategica__codigo__icontains", "accionestrategica__nombre__icontains"]
    max_results = 10

    def label_from_instance(self, obj):
        return force_text(obj.accionestrategica.codigo) + " " + force_text(obj.accionestrategica.nombre)


class ActividadProdProyForm(forms.Form):
    opprodproy = forms.ChoiceField(
        required=False,
        label=u'Producto',
        widget=widgetOpProdProy(
            attrs={'id': 'cb_prodproy', 'class': 'formProdProy_load', 'data-allow-clear': 'false'}
        )
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(ActividadProdProyForm, self).__init__(*args, **kwargs)


class ActividadAccEstForm(forms.Form):
    opaccest = forms.ChoiceField(
        required=False,
        label=u'Acción Estratégica',
        widget=widgetAccEst(
            attrs={'id': 'cb_accest', 'class': 'formAccEst_load', 'data-allow-clear': 'false'}
        )
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(ActividadAccEstForm, self).__init__(*args, **kwargs)
        self.fields["opaccest"].widget.queryset = AeXPop.objects.filter(
            productoproyecto_id=self.request.GET.get("opprodproy", 0)
        ).order_by("accionestrategica__codigo")


class ActividadCadProForm(forms.Form):
    opcadpro = forms.ModelChoiceField(
        required=False,
        label=u'Cadena Productiva',
        initial=CadenaProductiva.objects.first(),
        queryset=CadenaProductiva.objects.all(),
        widget=ModelSelect2Widget(
            max_results = 10,
            search_fields= ["nombre__icontains"],
            attrs={'id': 'cb_cadpro', 'class': 'formCadPro_load', 'data-allow-clear': 'false'},
            queryset=CadenaProductiva.objects.order_by("nombre")
        )
    )


class ActividadForm(forms.ModelForm):
    aexpopid = forms.IntegerField(
        widget=forms.HiddenInput
    )

    cadproid = forms.IntegerField(
        widget=forms.HiddenInput
    )

    area = forms.ModelChoiceField(
        label=u'Area',
        queryset=Area.objects.all(),
        widget=ModelSelect2Widget(
            queryset=Area.objects.order_by("nombre"),
            search_fields=["nombre__icontains"],
            max_results=10
        )
    )

    funcion = forms.ModelChoiceField(
        label=u'Función',
        queryset=Funcion.objects.all(),
        widget=ModelSelect2Widget(
            queryset=Funcion.objects.order_by("nombre"),
            search_fields=["nombre__icontains"],
            max_results=10
        )
    )

    divisionfuncional = forms.ModelChoiceField(
        label=u'División Funcional',
        queryset=DivisionFuncional.objects.all(),
        widget=ModelSelect2Widget(
            queryset=DivisionFuncional.objects.order_by("nombre"),
            search_fields=["nombre__icontains"],
            max_results=10
        )
    )

    grupofuncional = forms.ModelChoiceField(
        label=u'Grupo Funcional',
        queryset=GrupoFuncional.objects.all(),
        widget=ModelSelect2Widget(
            queryset=GrupoFuncional.objects.order_by("nombre"),
            search_fields=["nombre__icontains"],
            max_results=10
        )
    )

    actividadcatalogo = forms.ModelChoiceField(
        label=u'Nombre',
        queryset=ActividadCatalogo.objects.all(),
        widget=ModelSelect2Widget(
            queryset=ActividadCatalogo.objects.order_by("nombre"),
            search_fields=["nombre__icontains"],
            max_results=10
        )
    )

    indicador = forms.ModelChoiceField(
        label=u'Indicador',
        queryset=Indicador.objects.all(),
        widget=ModelSelect2Widget(
            queryset=Indicador.objects.order_by("nombre"),
            search_fields=["nombre__icontains"],
            max_results=10
        )
    )

    unidadmedida = forms.ModelChoiceField(
        label=u'Unidad de Medida',
        queryset=UnidadMedida.objects.all(),
        widget=ModelSelect2Widget(
            queryset=UnidadMedida.objects.order_by("nombre"),
            search_fields=["nombre__icontains"],
            max_results=10
        )
    )

    anio = forms.ChoiceField(
        label=u"Año",
        choices=ListaAnios,
        widget=Select2Widget(attrs={'data-minimum-results-for-search': 'Infinity'})
    )


    def __init__(self, *args, **kwargs):
        super(ActividadForm, self).__init__(*args, **kwargs)
        for i, (fname, field) in enumerate(self.fields.iteritems()):
            field.widget.attrs['class'] = 'form-control'

    class Meta:
        model = Actividad
        fields = [
            'aexpopid', 'cadproid', 'anio', 'actividadcatalogo', 'area', 'funcion' ,
            'divisionfuncional', 'grupofuncional', 'indicador', 'unidadmedida'
        ]

    def clean(self):
        self.instance.aexpop_id = self.cleaned_data["aexpopid"]
        self.instance.cadenaproductiva_id = self.cleaned_data["cadproid"]

    def save(self, commit=True):
        obAct = super(ActividadForm, self).save(commit=True)
        metasffs = []
        for mes in Mes.objects.all():
            if MetaFF.objects.filter(mes=mes, actividad=obAct).count() == 0:
                metasffs.append(MetaFF(actividad=obAct, mes=mes, creador=self.request.user))
        if len(metasffs) > 0:
            MetaFF.objects.bulk_create(metasffs)

        return obAct



