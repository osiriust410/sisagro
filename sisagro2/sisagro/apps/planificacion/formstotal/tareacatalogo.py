from django.forms import ModelForm

from ..models import TareaCatalogo

class TareaCatalogoForm(ModelForm):

	def __init__(self, *args, **kwargs):
		super(TareaCatalogoForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'

	class Meta:
		model = TareaCatalogo
		fields = ['nombre']

