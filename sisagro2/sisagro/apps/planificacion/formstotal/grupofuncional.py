from django.forms import ModelForm
from ..models import GrupoFuncional

class GrupoFuncionalForm(ModelForm):

	def __init__(self, *args, **kwargs):
		super(GrupoFuncionalForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'

	class Meta:
		model = GrupoFuncional
		fields = ['nombre']

