from django.forms import ModelForm
from ..models import DivisionFuncional

class DivisionFuncionalForm(ModelForm):

	def __init__(self, *args, **kwargs):
		super(DivisionFuncionalForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'

	class Meta:
		model = DivisionFuncional
		fields = ['nombre']

