# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import *

# Register your models here.
class AdminFuncion(admin.ModelAdmin):
	list_display = ["id", "nombre"]
	# list_filter = ["nombre"]
	search_fields = ["nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminDivisionFuncional(admin.ModelAdmin):
	list_display = ["id", "nombre"]
	# list_filter = ["nombre"]
	search_fields = ["nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminGrupoFuncional(admin.ModelAdmin):
	list_display = ["id", "nombre"]
	# list_filter = ["nombre"]
	search_fields = ["nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminIndicador(admin.ModelAdmin):
	list_display = ["id", "nombre"]
	# list_filter = ["nombre"]
	search_fields = ["nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminPeriodo(admin.ModelAdmin):
	list_display = ["id", "nombre", "estado"]
	# list_filter = ["nombre"]
	search_fields = ["nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminEjeEcon(admin.ModelAdmin):
	list_display = ["id", "nombre", "descripcion", "estado"]
	# list_filter = ["nombre"]
	search_fields = ["nombre", "descripcion"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminPdrc(admin.ModelAdmin):
	list_display = ["id", "codigo", "descripcion", "estado", "periodo", "ejeecon"]
	# list_filter = ["nombre"]
	search_fields = ["codigo", "descripcion"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminObjetivoPdrc(admin.ModelAdmin):
	list_display = ["id", "codigo", "descripcion", "estado", "pdrc"]
	# list_filter = ["nombre"]
	search_fields = ["codigo", "descripcion"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminObjetivoEspecifico(admin.ModelAdmin):
	list_display = ["id", "codigo", "descripcion", "estado", "objetivopdrc"]
	# list_filter = ["nombre"]
	search_fields = ["codigo", "descripcion"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminAccionEstrategica(admin.ModelAdmin):
	list_display = ["id", "codigo", "nombre", "objetivoespecifico"]
	# list_filter = ["nombre"]
	search_fields = ["codigo", "nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminCadenaProductiva(admin.ModelAdmin):
	list_display = ["id", "nombre", "estado"]
	# list_filter = ["nombre"]
	search_fields = ["nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminActividadCatalogo(admin.ModelAdmin):
	list_display = ["id", "nombre"]
	# list_filter = ["nombre"]
	search_fields = ["nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminActividad(admin.ModelAdmin):
	list_display = ["id", "actividadcatalogo", "area", "funcion"]
	# list_filter = ["nombre"]
	search_fields = ["actividadcatalogo__nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminProductoProyecto(admin.ModelAdmin):
	list_display = ["id", "codigo", "nombre", "descripcion", "indicador", "unidadmedida"]
	# list_filter = ["nombre"]
	search_fields = ["codigo", "nombre", "descripcion"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminAeXPop(admin.ModelAdmin):
	list_display = ["id", "accionestrategica", "productoproyecto"]
	# list_filter = ["nombre"]
	# search_fields = ["nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminProgramaPresupuestal(admin.ModelAdmin):
	list_display = ["id", "codigo", "nombre", "descripcion", "unidadmedida"]
	# list_filter = ["nombre"]
	search_fields = ["codigo", "nombre", "descripcion"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminMetaFF(admin.ModelAdmin):
	list_display = ["id", "mes", "metafisica_programada", "metafinanciera_programada", "metafisica_ejecutada", "metafinanciera_ejecutada"]
	# list_filter = ["nombre"]
	# search_fields = ["nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminAccionObra(admin.ModelAdmin):
	list_display = ["id", "km", "volumen", "conduce", "presupuesto"]
	# list_filter = ["nombre"]
	# search_fields = ["nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminSubActividadCatalogo(admin.ModelAdmin):
	list_display = ["id", "nombre"]
	# list_filter = ["nombre"]
	search_fields = ["nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminSubActividadComponente(admin.ModelAdmin):
	list_display = ["id", "codigo", "subactividadcatalogo", "unidadmedida"]
	# list_filter = ["nombre"]
	search_fields = ["codigo", "subactividadcatalogo__nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminTareaCatalogo(admin.ModelAdmin):
	list_display = ["id", "nombre"]
	# list_filter = ["nombre"]
	search_fields = ["nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminTareaCpCu(admin.ModelAdmin):
	list_display = ["id", "referencia"]
	# list_filter = ["nombre"]
	search_fields = ["referencia"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminTareaCpCr(admin.ModelAdmin):
	list_display = ["id", "nombre"]
	# list_filter = ["nombre"]
	search_fields = ["nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

admin.site.register(Funcion, AdminFuncion)
admin.site.register(DivisionFuncional, AdminDivisionFuncional)
admin.site.register(GrupoFuncional, AdminGrupoFuncional)
admin.site.register(Indicador, AdminIndicador)
admin.site.register(Periodo, AdminPeriodo)
admin.site.register(EjeEcon, AdminEjeEcon)
admin.site.register(Pdrc, AdminPdrc)
admin.site.register(ObjetivoPdrc, AdminObjetivoPdrc)
admin.site.register(ObjetivoEspecifico, AdminObjetivoEspecifico)
admin.site.register(AccionEstrategica, AdminAccionEstrategica)
admin.site.register(CadenaProductiva, AdminCadenaProductiva)
admin.site.register(Actividad, AdminActividad)
admin.site.register(ActividadCatalogo, AdminActividadCatalogo)
admin.site.register(ProductoProyecto, AdminProductoProyecto)
admin.site.register(AeXPop, AdminAeXPop)
admin.site.register(ProgramaPresupuestal, AdminProgramaPresupuestal)
admin.site.register(MetaFF, AdminMetaFF)
admin.site.register(AccionObra, AdminAccionObra)
admin.site.register(SubActividadCatalogo, AdminSubActividadCatalogo)
admin.site.register(SubActividadComponente, AdminSubActividadComponente)
admin.site.register(TareaCatalogo, AdminTareaCatalogo)
admin.site.register(TareaCpCu, AdminTareaCpCu)
admin.site.register(TareaCpCr, AdminTareaCpCr)

