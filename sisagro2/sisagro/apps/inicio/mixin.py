
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import AccessMixin
from django.contrib.auth.views import redirect_to_login
from django.core.exceptions import PermissionDenied


class LoginRequiredMixin(AccessMixin):

	redirect_unauthenticated_users = False

	def dispatch(self, request, *args, **kwargs):

		if not request.user.is_authenticated():
			if self.raise_exception and not self.redirect_unauthenticated_users:
				raise PermissionDenied  # return a forbidden response
			else:
				return redirect_to_login('/',
										 self.get_login_url(),
										 self.get_redirect_field_name())

		return super(LoginRequiredMixin, self).dispatch(
			request, *args, **kwargs)


