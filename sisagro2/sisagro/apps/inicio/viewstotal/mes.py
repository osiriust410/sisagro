from django.shortcuts import render,HttpResponse
from django.views.generic import TemplateView
from django.views import View

from ..models import Mes
from ..formstotal.mes import MesForm
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import json
from django.utils.decorators import method_decorator

from ..mixin import LoginRequiredMixin




# @method_decorator(login_required, name='dispatch')

class MesTemplateView(LoginRequiredMixin,TemplateView):
	template_name = "inicio/mes/template.html"

	# def get_context_data(self, **kwargs):
	# 	context = super(ProfesionTemplateView, self).get_context_data(**kwargs)
	# 	# context['latest_articles'] = Article.objects.all()[:5]
	# 	return context

class MesCreateView(LoginRequiredMixin,View):
	model = Mes
	form_class = MesForm
	template_name = "inicio/mes/formulario.html"
	
	def get(self, request, *args, **kwargs):
		pk = int(kwargs['pk'])
		if pk == 0:
			form = self.form_class()
		else:
			mes = Mes.objects.get(pk = pk)
			form = self.form_class(instance=mes)
		return render(request, self.template_name, {'form': form})

	def post(self, request, *args, **kwargs):
		pk = int(kwargs['pk'])
		if pk == 0:
			form = self.form_class(request.POST)
			form.instance.creador = request.user
		else: 
			mes = Mes.objects.get(pk = pk)
			form = self.form_class(request.POST, instance=mes)
			form.instance.editor = request.user
		msg = {}
		msg['msg'] = "Error al guardar"
		msg['value'] = False
		if form.is_valid():
			msg['msg'] = "Guardado Correctamente"
			msg['value'] = True
			form.save()
			# <process form cleaned data>
			return HttpResponse(json.dumps(msg), content_type='application/json')

		return render(request, self.template_name, {'form': form})

class MesDeleteView(LoginRequiredMixin,View):
	
	def get(self, request, *args, **kwargs):
		pk = int(kwargs['pk'])
		mes = Mes.objects.get(pk = pk)
		mes.delete()
		msg = {}
		msg['msg'] = "Eliminado Correctamente"
		msg['value'] = True
		return HttpResponse(json.dumps(msg), content_type='application/json')



class ListarMesJson(LoginRequiredMixin,View):

	def get(self, request):
	
		limit = int(request.GET.get('limit', 1)) 
		offset = int(request.GET.get('offset',0)) 
		order = str(request.GET.get('order', '')) 
		search = request.GET.get('search', '')
		sort = request.GET.get('sort', '')
		# data = {}
		orderby = ""
		if order!='asc':
			orderby = "-"

		mes = Mes.objects.all().order_by('-id')

		if search != '':
			mes = Mes.objects.filter(Q(numero__startswith=search) | Q(nombre__icontains=search) )
			# profesion = Profesion.objects.filter(Q(nombre__startswith=search) | Q(apellido__startswith=search) | Q(dni__startswith=search))

		if sort!='':
			mes = mes.order_by(orderby+sort)

		paginator = Paginator(mes, limit)

		try:
			mes = paginator.page((offset/limit)+1)

		except PageNotAnInteger:
		# If page is not an integer, deliver first page.
			mes = paginator.page(1)
		except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
			mes = paginator.page(paginator.num_pages)

		return render(request, 'inicio/mes/lista.json', {'mes': mes, 'total':paginator.count})
