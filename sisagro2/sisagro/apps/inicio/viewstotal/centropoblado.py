from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView, ListView
from django.views import View

from django.http import HttpResponse
import json
from django.core.serializers import serialize

from ..models import AuditableMixin, CentroPoblado, Provincia, Distrito
from ..formstotal.centropoblado import CentroPobladoForm, CentroPobladoUbiegoForm
from ..mixin import LoginRequiredMixin
from django.utils.encoding import force_text
from django.core.serializers.json import DjangoJSONEncoder



class CentroPobladoTemplateView(LoginRequiredMixin, TemplateView):
	template_name = "inicio/centropoblado/inicio.html"
	form_class = CentroPobladoUbiegoForm
	def get_context_data(self, **kwargs):
		context = super(CentroPobladoTemplateView, self).get_context_data(**kwargs)
		context['form'] = self.form_class
		return context

class CentroPobladoListView(LoginRequiredMixin, ListView):

	def get(self, request):
		limit = int(request.GET.get('limit', 1))
		offset = int(request.GET.get('offset', 0))
		order = str(request.GET.get('order', ''))
		search = request.GET.get('search', '')
		sort = request.GET.get('sort', '')
		iddistrito = int(request.GET.get('distrito', -1))
		# data = {}
		orderby = ""
		if order != 'asc':
			orderby = "-"

		centrospoblados = CentroPoblado.objects.filter(distrito_id=iddistrito).order_by('-id')

		if len(search) > 0:
			centrospoblados = centrospoblados.filter(nombre__icontains=search)
			# profesion = Profesion.objects.filter(Q(nombre__startswith=search) | Q(apellido__startswith=search) | Q(dni__startswith=search))

		if sort != '':
			centrospoblados = centrospoblados.order_by(orderby + sort)

		paginador = Paginator(centrospoblados, limit)

		try:
			centrospoblados = paginador.page((offset / limit) + 1)
		except PageNotAnInteger:
			# If page is not an integer, deliver first page.
			centrospoblados = paginador.page(1)
		except EmptyPage:
			# If page is out of range (e.g. 9999), deliver last page of results.
			centrospoblados = paginador.page(paginador.num_pages)

		return render(
			request,
			'inicio/centropoblado/datos.json',
			{
				'centrospoblados': centrospoblados,
				'total': paginador.count
			}
		)

class CentroPobladoCreateView(LoginRequiredMixin, AuditableMixin, CreateView):
	model = CentroPoblado
	form_class = CentroPobladoForm
	template_name = "inicio/centropoblado/formulario.html"

	def get_form_kwargs(self):
		kwargs = super(CentroPobladoCreateView, self).get_form_kwargs()
		kwargs['kwargs'] = self.kwargs
		kwargs['request'] = self.request
		return kwargs

	def form_valid(self, form):
		super(CentroPobladoCreateView, self).form_valid(form)
		msg = {}
		msg['msg'] = "Creado Correctamente"
		msg['value'] = True
		return HttpResponse(json.dumps(msg), content_type='application/json')

	def form_invalid(self, form):
		super(CentroPobladoCreateView, self).form_invalid(form)
		return render(self.request, self.template_name, {'form': form})

class CentroPobladoUpdateView(LoginRequiredMixin, AuditableMixin, UpdateView):
	model = CentroPoblado
	form_class = CentroPobladoForm
	template_name = "inicio/centropoblado/formulario.html"

	def get_form_kwargs(self):
		kwargs = super(CentroPobladoUpdateView, self).get_form_kwargs()
		kwargs['kwargs'] = self.kwargs
		kwargs['request'] = self.request
		return kwargs

	def form_valid(self, form):
		super(CentroPobladoUpdateView, self).form_valid(form)
		msg = {}
		msg['msg'] = "Actualizado Correctamente"
		msg['value'] = True
		return HttpResponse(json.dumps(msg), content_type='application/json')

	def form_invalid(self, form):
		super(CentroPobladoUpdateView, self).form_invalid(form)
		return render(self.request, self.template_name, {'form': form})

class CentroPobladoDeleteView(LoginRequiredMixin, DeleteView):
	model = CentroPoblado
	form_class = CentroPobladoForm
	template_name = "inicio/centropoblado/eliminar.html"

	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		self.object.delete()
		msg = {}
		msg['msg'] = "Eliminado Correctamente"
		msg['value'] = True
		return HttpResponse(json.dumps(msg), content_type='application/json')


class ProvincaDistritoListView(View):
	def get(self, request,*args, **kwargs):

		valor = int(kwargs['valor'])
		pk = int(kwargs['pk'])
		if valor == 0:
			objeto = Provincia.objects.filter(departamento_id = pk)
		elif valor == 1:
			objeto = Distrito.objects.filter(provincia_id = pk) 
		return HttpResponse(serialize('json', objeto, ))	   




