from django.shortcuts import render, redirect
from django.conf import settings
from django.http import HttpResponseRedirect,HttpResponse
import json

def InicioView(request):
	_url = "login"
	if request.user.is_authenticated:
		_url = "inicio"

	siguiente = request.GET.get('next')
	if siguiente is not None:
		return redirect("/")

	return render(
		request,
		"inicio/" + _url + ".html",
		{}
	)

