from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView, ListView
from django.views import View

from django.http import HttpResponse
import json
from django.core.serializers import serialize

from ..models import AuditableMixin, Caserio, CentroPoblado, Provincia, Distrito
from ..formstotal.caserio import CaserioForm , CaserioUbigeoForm
from ..mixin import LoginRequiredMixin
from django.utils.encoding import force_text
from django.core.serializers.json import DjangoJSONEncoder



class CaserioTemplateView(LoginRequiredMixin, TemplateView):
	template_name = "inicio/caserio/inicio.html"
	form_class = CaserioUbigeoForm

	def get_context_data(self, **kwargs):
		context = super(CaserioTemplateView, self).get_context_data(**kwargs)
		context['form'] = self.form_class
		return context


class CaserioListView(LoginRequiredMixin, ListView):

	def get(self, request, *args, **kwargs):
		limit = int(request.GET.get('limit', 1))
		offset = int(request.GET.get('offset', 0))
		order = str(request.GET.get('order', ''))
		search = request.GET.get('search', '')
		sort = request.GET.get('sort', '')
		idCentroPoblado = int(request.GET.get('centropoblado', -1))
		# data = {}
		orderby = ""
		if order != 'asc':
			orderby = "-"

		caserios = Caserio.objects.filter(centropoblado_id = idCentroPoblado).order_by('-id')

		if len(search) > 0:
			caserios = caserios.filter(nombre__icontains=search)

		if sort != '':
			caserios = caserios.order_by(orderby + sort)

		paginador = Paginator(caserios, limit)

		try:
			caserios = paginador.page((offset / limit) + 1)
		except PageNotAnInteger:
			# If page is not an integer, deliver first page.
			caserios = paginador.page(1)
		except EmptyPage:
			# If page is out of range (e.g. 9999), deliver last page of results.
			caserios = paginador.page(paginador.num_pages)

		return render(
			request,
			'inicio/caserio/datos.json',
			{
				'caserios': caserios,
				'total': paginador.count
			}
		)

class CaserioCreateView(LoginRequiredMixin, AuditableMixin, CreateView):
	model = Caserio
	form_class = CaserioForm
	template_name = "inicio/caserio/formulario.html"

	def get_form_kwargs(self):
		kwargs = super(CaserioCreateView, self).get_form_kwargs()
		kwargs['kwargs'] = self.kwargs
		kwargs['request'] = self.request
		return kwargs

	def form_valid(self, form):
		super(CaserioCreateView, self).form_valid(form)
		msg = {}
		msg['msg'] = "Creado Correctamente"
		msg['value'] = True
		return HttpResponse(json.dumps(msg), content_type='application/json')

	def form_invalid(self, form):
		super(CaserioCreateView, self).form_invalid(form)
		return render(self.request, self.template_name, {'form': form})

class CaserioUpdateView(LoginRequiredMixin, AuditableMixin, UpdateView):
	model = Caserio
	form_class = CaserioForm
	template_name = "inicio/caserio/formulario.html"

	def get_form_kwargs(self):
		kwargs = super(CaserioUpdateView, self).get_form_kwargs()
		kwargs['kwargs'] = self.kwargs
		kwargs['request'] = self.request
		return kwargs

	def form_valid(self, form):
		super(CaserioUpdateView, self).form_valid(form)
		msg = {}
		msg['msg'] = "Actualizado Correctamente"
		msg['value'] = True
		return HttpResponse(json.dumps(msg), content_type='application/json')

	def form_invalid(self, form):
		super(CaserioUpdateView, self).form_invalid(form)
		return render(self.request, self.template_name, {'form': form})

class CaserioDeleteView(LoginRequiredMixin, DeleteView):
	model = Caserio
	form_class = CaserioForm
	template_name = "inicio/caserio/eliminar.html"

	def dispatch(self, *args, **kwargs):
		self.caserio_id = kwargs['pk']
		return super(CaserioDeleteView, self).dispatch(*args, **kwargs)

	def get_success_url(self):
		pass

	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		self.object.delete()
		msg = {}
		msg['msg'] = "Eliminado Correctamente"
		msg['value'] = True
		return HttpResponse(json.dumps(msg), content_type='application/json')

	def get_context_data(self, **kwargs):
		context = super(CaserioDeleteView, self).get_context_data(**kwargs)
		context["caserio_id"] = self.caserio_id
		return context


class ProvincaDistritoListView(View):
	def get(self, request,*args, **kwargs):

		valor = int(kwargs['valor'])
		pk = int(kwargs['pk'])
		if valor == 0:
			objeto = Provincia.objects.filter(departamento_id = pk)
		elif valor == 1:
			objeto = Distrito.objects.filter(provincia_id = pk) 
		return HttpResponse(serialize('json', objeto, ))	   




