from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView, ListView
from django.http import HttpResponse
import json

from ..models import AuditableMixin, UnidadMedida
from ..formstotal.unidadmedida import UnidadMedidaForm

class UnidadMedidaView(TemplateView):
    template_name = "inicio/unidadmedida/inicio.html"

class UnidadMedidaListView(ListView):

    def get(self, request):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        # data = {}
        orderby = ""
        if order != 'asc':
            orderby = "-"

        unidadesmedidas = UnidadMedida.objects.all().order_by('-id')

        if len(search) > 0:
            # unidadesmedidas = unidadesmedidas.filter(nombre__icontains=search)
            unidadesmedidas = unidadesmedidas.filter(Q(nombre__icontains=search) | Q(abreviatura__icontains=search))

        if sort != '':
            unidadesmedidas = unidadesmedidas.order_by(orderby + sort)

        paginador = Paginator(unidadesmedidas, limit)

        try:
            unidadesmedidas = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            unidadesmedidas = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            unidadesmedidas = paginador.page(paginador.num_pages)

        return render(
            request,
            'inicio/unidadmedida/datos.json',
            {
                'unidadesmedidas': unidadesmedidas,
                'total': paginador.count
            }
        )

class UnidadMedidaCreateView(AuditableMixin, CreateView):
    model = UnidadMedida
    form_class = UnidadMedidaForm
    template_name = "inicio/especialidad/formulario.html"

    def form_valid(self, form):
        super(UnidadMedidaCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(UnidadMedidaCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class UnidadMedidaUpdateView(AuditableMixin, UpdateView):
    model = UnidadMedida
    form_class = UnidadMedidaForm
    template_name = "inicio/unidadmedida/formulario.html"

    def dispatch(self, *args, **kwargs):
        self.unidadmedida_id = kwargs['pk']
        return super(UnidadMedidaUpdateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        super(UnidadMedidaUpdateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(UnidadMedidaUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

    def get_context_data(self, **kwargs):
        context = super(UnidadMedidaUpdateView, self).get_context_data(**kwargs)
        context["unidadmedida_id"] = self.unidadmedida_id
        return context

class UnidadMedidaDeleteView(DeleteView):
    model = UnidadMedida
    form_class = UnidadMedidaForm
    template_name = "inicio/unidadmedida/eliminar.html"

    def dispatch(self, *args, **kwargs):
        self.unidadmedida_id = kwargs['pk']
        return super(UnidadMedidaDeleteView, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def get_context_data(self, **kwargs):
        context = super(UnidadMedidaDeleteView, self).get_context_data(**kwargs)
        context["unidadmedida_id"] = self.unidadmedida_id
        return context
