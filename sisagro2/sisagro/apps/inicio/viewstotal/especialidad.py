from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView, ListView
from django.http import HttpResponse, HttpResponseRedirect
import json

from ..models import AuditableMixin, Especialidad
from ..formstotal.especialidad import EspecialidadForm
from ..mixin import LoginRequiredMixin

class EspecialidadView(LoginRequiredMixin, TemplateView):
    template_name = "inicio/especialidad/inicio.html"

class EspecialidadListView(LoginRequiredMixin, ListView):

    def get(self, request):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        # data = {}
        orderby = ""
        if order != 'asc':
            orderby = "-"

        especialidades = Especialidad.objects.all().order_by('-id')

        if len(search) > 0:
            especialidades = especialidades.filter(nombre__icontains=search)
            # profesion = Profesion.objects.filter(Q(nombre__startswith=search) | Q(apellido__startswith=search) | Q(dni__startswith=search))

        if sort != '':
            especialidades = especialidades.order_by(orderby + sort)

        paginador = Paginator(especialidades, limit)

        try:
            especialidades = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            especialidades = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            especialidades = paginador.page(paginador.num_pages)

        return render(
            request,
            'inicio/especialidad/datos.json',
            {
                'especialidades': especialidades,
                'total': paginador.count
            }
        )

class EspecialidadCreateView(AuditableMixin, CreateView):
    model = Especialidad
    form_class = EspecialidadForm
    template_name = "inicio/especialidad/formulario.html"

    def form_valid(self, form):
        super(EspecialidadCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(EspecialidadCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class EspecialidadUpdateView(AuditableMixin, UpdateView):
    model = Especialidad
    form_class = EspecialidadForm
    template_name = "inicio/especialidad/formulario.html"

    def dispatch(self, *args, **kwargs):
        self.especialidad_id = kwargs['pk']
        return super(EspecialidadUpdateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        super(EspecialidadUpdateView, self).form_valid(form)
        especialidad = Especialidad.objects.get(pk=self.especialidad_id)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(EspecialidadUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

    def get_context_data(self, **kwargs):
        context = super(EspecialidadUpdateView, self).get_context_data(**kwargs)
        context["especialidad_id"] = self.especialidad_id
        return context

class EspecialidadDeleteView(DeleteView):
    model = Especialidad
    form_class = EspecialidadForm
    template_name = "inicio/especialidad/eliminar.html"

    def dispatch(self, *args, **kwargs):
        self.especialidad_id = kwargs['pk']
        return super(EspecialidadDeleteView, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def get_context_data(self, **kwargs):
        context = super(EspecialidadDeleteView, self).get_context_data(**kwargs)
        context["especialidad_id"] = self.especialidad_id
        return context

