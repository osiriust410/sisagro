$modalFormLoaded = null;
$grillaOtrasAccionesButton = [];
$grillaOtrasAccionesAction = {}

function actionFormatter(value, row, index) {
	return [
	    '<a class="btn-action edit " href="javascript:void(0)" title="Editar" role="button">',
	    '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>',
	    '<a class="btn-action remove" href="javascript:void(0)" title="Eliminar">',
	    '<span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>',
	].concat($grillaOtrasAccionesButton).join('');
}
window.actionEvents = {
    'click .edit': function (e, value, row, index) {
        idValue = row['id'];
        opcion = "Editar";
        url_post = url_editar + idValue;
        $modalForm.modal('show');
        $modalForm.waitMe({text: "Cargando..."});
    },
    'click .remove': function (e, value, row, index) {
        opcion = "Eliminar";
        idValue = row['id'];
        url_post = url_eliminar + idValue;
        $modalForm.modal('show');
        $modalForm.waitMe({text: "Cargando..."});
    },
};
$( document ).ready(function() {
	$("#btnNuevo").click(function(event) {
		opcion = "Nuev" + $formGenero;
		idValue = 0;
		url_post = url_crear;
		$modalForm.modal('show');
		$modalForm.waitMe({text:"Cargando..."});
	});
	$modalForm.on('show.bs.modal', function (e) {
		btnSave = $modalForm.find("#btnGuardar");
		$(this).find('.modal-body').html("");
		if(opcion=="Eliminar") {
			$txtBtn = "Eliminando";
			url_load = url_eliminar + idValue;
			$(this).find('h4').text("Eliminar " + $formTitulo);
			btnSave.html("Eliminar");
		}
		else {
			btnSave.html("Guardar");
			if(opcion.indexOf("Nuev") > -1 || opcion == "Editar") {
                if (idValue == 0) {
                    $txtBtn = "Guardando";
                    url_load = url_crear;
                    $(this).find('h4').text("Nuev" + $formGenero + " " + $formTitulo);
                } else {
                    $txtBtn = "Actualizando";
                    url_load = url_editar + idValue;
                    $(this).find('h4').text("Editanto " + $formTitulo);
                }
            }
            else {
				$txtBtn = "Guardando";
				$(this).find('h4').text(opcion + " - " + $formTitulo);
			}
        }
        $(this).find('.modal-body').load(url_load, function(e) {
        	$modalForm.waitMe('hide');
        	if ($modalFormLoaded) eval($modalFormLoaded);
        	if ($('#' + $firstField)) {
        		setTimeout(function() { $('#' + $firstField).focus(); }, 500);
            }
		});
	});
	$modalForm.on('shown.bs.modal', function(e) {
    });
	$("#btnGuardar").on('click', function(event) {
		$formView.waitMe({text:$txtBtn+"..."});
		form = $formView.serialize();
		$.post(url_post, form).done(function(data) {
			if(data.value){
				toastr["success"](data.msg);
				$formView.waitMe('hide');
				$modalForm.modal('hide');
				$tablaView.bootstrapTable('refresh');
			}else{
				toastr["error"](data.msg);
				$formView.waitMe('hide');
				$modalForm.find('.modal-body').html(data);
			}
		});
	});
});
