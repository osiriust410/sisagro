
// Bootstrap Table 
function actionFormatter(value, row, index) {
	return [
	    '<a class="btn-action edit " href="javascript:void(0)" title="Editar" role="button">',
	    '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>',
	    '<a class="btn-action remove" href="javascript:void(0)" title="Eliminar">',
	    '<span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>',
	].join('');
}

var opcion = "";
var idmes = 0;

window.actionEvents = {
// 'click .like': function (e, value, row, index) {
//     alert('You click like icon, row: ' + JSON.stringify(row));
//     console.log(value, row, index);
// },
	'click .edit': function (e, value, row, index) {
	    idmes = row['id'];
	    opcion = "Editar";	    
		$("#modalMes").modal('show');
		
		// $("#modalMes").waitMe({text:"Cargando..."});
	},
	'click .remove': function (e, value, row, index) {
		id = row['id'];

		swal({
			title: 'Seguro que desea eliminar el registro',
			type: 'question',
			showCancelButton: true,
		}).then(function () {
			$.get("./"+id+"/eliminar/", function( data ) {
				if( data.value ){
					$("#tablaMes").bootstrapTable('refresh');
					toastr["success"](data.msg);
				}else{
					toastr["error"](data.msg);
				}
			});
		}, function (d) {})
	}
};

$( document ).ready(function() {
	$("#btnNuevoMes").click(function(event) {

		opcion = "Nuevo";
		idmes = 0;
		$("#modalMes").modal('show');

		// $("#modalMes").waitMe({text:"Cargando..."});

	});

	$('#modalMes').on('show.bs.modal', function (e) {
		$(this).find('.modal-body').load("./"+idmes+"/crear/");
        $(this).find('h4').text(idmes==0 ? "Nuevo Mes" : "Editanto Mes");
	});


	$('#modalMes').on('shown.bs.modal', function (e) {
		$(this).find("#id_numero").focus();

		// $("#modalMes").waitMe('hide');

	});

	$("#btnMesGuardar").on('click', function(event) {

		$("#modalMes").waitMe({text:"Guardando..."});

		form = $("#formMes").serialize();
		$.post( "./"+idmes+"/crear/", form).done(function( data ) {
			if(data.value){
				$("#modalMes").waitMe('hide');
				$("#modalMes").modal('hide');
				$("#tablaMes").bootstrapTable('refresh');

				toastr["success"](data.msg);
			}else{
				toastr["error"](data.msg);
				$("#modalMes").waitMe('hide');

				$("#modalMes").find('.modal-body').html(data);


			}
		});

	});
});
