var opcion = "";
var idprofesion = 0;

function actionFormatter(value, row, index) {
	return [
	    '<a class="btn-action edit " href="javascript:void(0)" title="Editar" role="button">',
	    '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>',
	    '<a class="btn-action remove" href="javascript:void(0)" title="Eliminar">',
	    '<span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>',
	].join('');
}

window.actionEvents = {
	'click .edit': function (e, value, row, index) {
	    idprofesion = row['id'];
	    opcion = "Editar";	    
		$("#modalProfesion").modal('show');
	},
	'click .remove': function (e, value, row, index) {
		id = row['id'];

		swal({
			title: 'Seguro que desea eliminar el registro',
			type: 'question',
			showCancelButton: true,
		}).then(function () {
			$.get("../"+id+"/eliminar/", function( data ) {
				if( data.value ){
					$("#tablaProfesion").bootstrapTable('refresh');
					toastr["success"](data.msg);
				}else{
					toastr["error"](data.msg);
				}
			});
		}, function (d) {})
	}
};

$( document ).ready(function() {
	$("#btnNuevo").click(function(event) {
		opcion = "Nuevo";
		idprofesion = 0;
		$("#modalProfesion").modal('show');
		// $("#modalProfesion").waitMe({text:"Cargando..."});
	});

	$('#modalProfesion').on('show.bs.modal', function (e) {
        $(this).find('h4').text(idprofesion==0 ? "Nueva Profesion" : "Editanto Profesion");
        $(this).find('.modal-body').load("../"+idprofesion+"/crear/");
	});


	$('#modalProfesion').on('shown.bs.modal', function (e) {
		$(this).find("#id_nombre").focus();
		// $("#modalProfesion").waitMe('hide');
	});

	$("#btnProfesionGuardar").on('click', function(event) {

		$("#modalProfesion").waitMe({text:"Guardando..."});

		form = $("#formProfesion").serialize();
		$.post( "../"+idprofesion+"/crear/", form).done(function( data ) {
			if(data.value){
				$("#modalProfesion").waitMe('hide');
				$("#modalProfesion").modal('hide');
				$("#tablaProfesion").bootstrapTable('refresh');

				toastr["success"](data.msg);
			}else{
				toastr["error"](data.msg);
				$("#modalProfesion").waitMe('hide');

				$("#modalProfesion").find('.modal-body').html(data);

			}
		});

	});
});