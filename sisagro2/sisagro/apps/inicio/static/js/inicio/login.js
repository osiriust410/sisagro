$(document).ready(function(){
    swal.setDefaults({
        padding: 10,
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-warning'
    });

    $('#btnIniciar').click(function(){
        $('.input-group').removeClass('has-error');

        if($.trim($('#tbUsuario').val())==""){
            $('#tbUsuario').parent().addClass('has-error');
            swal({title: 'El Usuario no puede quedar vacío',  type: 'info'}).then(function () {
                 $("#tbUsuario").focus();
            })
            return;
        }else if($.trim($('#tbClave').val())==""){
            $('#tbClave').parent().addClass('has-error');
            swal({ title: 'La Contraseña no puede quedar vacía', type: 'info'}).then(function () {
                 $("#tbClave").focus();
                 return;
            })
        }else{
            $(".form-signin").waitMe({text:"Iniciando Sesión..."});

            $.post('login', {
                username:$('#tbUsuario').val(),
                password:$('#tbClave').val(),
                "csrfmiddlewaretoken":csrftoken,
                },function(data){
                    console.log(data);
                    if(data.success){
                        document.location.reload();
                    }else{
                        $(".form-signin").waitMe('hide');

                        swal({title: data.msg, type:"error"}).then(function () {
                             $("#tbClave").focus();
                             return;
                        })
                    }
                }, "json");
        }
    });

    $('#tbClave').keypress(function(e){
        if (e.keyCode == 13) {
            $('#btnIniciar').click();
            return true;
        }
    });
});