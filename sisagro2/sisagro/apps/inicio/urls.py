from django.conf.urls import url
from .viewstotal import inicio
from .viewstotal import login
from .viewstotal import unidadmedida as uni
from .viewstotal import profesion as prof
from .viewstotal import especialidad as esp
from .viewstotal import tipoorganizacion as tiporg
from .viewstotal import mes
from .viewstotal import centropoblado as cenpo
from .viewstotal import caserio
from .viewstotal.profesionXLSX import *
from .viewstotal.tipoorganizacionXLSX import *

urlpatterns = [

	# LOGIN
    # url(r'^modelo/accion/$', ModeloAccion, name='modelo_accion'),
    url(r'^$', inicio.InicioView, name="inicio_view"),
    url(r'^login$', login.LoginView, name='user_login'),
    url(r'^logout$', login.LogoutView, name='user_logout'),

	# UNIDAD MEDIDA
    url(r'^unidadmedida/$', uni.UnidadMedidaView.as_view(), name='unidadmedida_inicio'),
    url(r'^unidadmedida/listar/$', uni.UnidadMedidaListView.as_view(), name='unidadmedida_listar'),
    url(r'^unidadmedida/crear/$', uni.UnidadMedidaCreateView.as_view(), name='unidadmedida_crear'),
    url(r'^unidadmedida/actualizar/(?P<pk>\d+)$', uni.UnidadMedidaUpdateView.as_view(), name='unidadmedida_actualizar'),
    url(r'^unidadmedida/eliminar/(?P<pk>\d+)$', uni.UnidadMedidaDeleteView.as_view(), name='unidadmedida_eliminar'),


    # PROFESION
    url(r'^profesion/listar/$', prof.ProfesionTemplateView.as_view(), name='profesion_inicio'),
    url(r'^profesion/listar/json$', prof.ListarProfesionJson.as_view(), name='profesion_listar'),
    url(r'^profesion/(?P<pk>\d+)/crear/$', prof.ProfesionCreateView.as_view(), name='profesion_create'),
    url(r'^profesion/(?P<pk>\d+)/eliminar/$', prof.ProfesionDeleteView.as_view(), name='profesion_delete'),
    url(r'^profesion/xlsx/$', ListaProfesionesXLS, name='profesion_xlsx'),

    # ESPECIALIDAD
    url(r'^especialidad/$', esp.EspecialidadView.as_view(), name='especialidad_inicio'),
    url(r'^especialidad/listar/$', esp.EspecialidadListView.as_view(), name='especialidad_listar'),
    url(r'^especialidad/crear/$', esp.EspecialidadCreateView.as_view(), name='especialidad_crear'),
    url(r'^especialidad/actualizar/(?P<pk>\d+)$', esp.EspecialidadUpdateView.as_view(), name='especialidad_actualizar'),
    url(r'^especialidad/eliminar/(?P<pk>\d+)$', esp.EspecialidadDeleteView.as_view(), name='especialidad_eliminar'),


    # MES
    url(r'^mes/$', mes.MesTemplateView.as_view(), name='mes_inicio'),
    url(r'^mes/listar/$', mes.ListarMesJson.as_view(), name='mes_listar'),
    url(r'^mes/(?P<pk>\d+)/crear/$', mes.MesCreateView.as_view(), name='profesion_create'),
    url(r'^mes/(?P<pk>\d+)/eliminar/$', mes.MesDeleteView.as_view(), name='profesion_eliminar'),

    # TIPO ORGANIZACION
    url(r'^tipoorganizacion/$', tiporg.TipoOrganizacionView.as_view(), name='tipoorganizacion_inicio'),
    url(r'^tipoorganizacion/listar/$', tiporg.TipoOrganizacionListView.as_view(), name='tipoorganizacion_listar'),
    url(r'^tipoorganizacion/crear/$', tiporg.TipoOrganizacionCreateView.as_view(), name='tipoorganizacion_crear'),
    url(r'^tipoorganizacion/actualizar/(?P<pk>\d+)$', tiporg.TipoOrganizacionUpdateView.as_view(), name='tipoorganizacion_actualizar'),
    url(r'^tipoorganizacion/eliminar/(?P<pk>\d+)$', tiporg.TipoOrganizacionDeleteView.as_view(), name='tipoorganizacion_eliminar'),
    url(r'^tipoorganizacion/xlsx/$', ListaTipoOrganizacionXLSX, name='tipoorganizacion_xlsx'),

    # CENTRO POBLADO
    url(r'^centropoblado/$', cenpo.CentroPobladoTemplateView.as_view(), name='centropoblado_inicio'),
    url(r'^centropoblado/listar/$', cenpo.CentroPobladoListView.as_view(), name='centropoblado_lista'),
    url(r'^centropoblado/crear/(?P<dist>\d+)$', cenpo.CentroPobladoCreateView.as_view(), name='centropoblado_crear'),
    url(r'^centropoblado/actualizar/(?P<pk>\d+)$', cenpo.CentroPobladoUpdateView.as_view(), name='centropoblado_actualizar'),
    url(r'^centropoblado/eliminar/(?P<pk>\d+)$', cenpo.CentroPobladoDeleteView.as_view(), name='centropoblado_eliminar'),
    url(r'^centropoblado/ubigeo/(?P<pk>\d+)/(?P<valor>\d+)/$', cenpo.ProvincaDistritoListView.as_view(), name='centropoblado_ubiego'),

    # CASERIO
    url(r'^caserio/$', caserio.CaserioTemplateView.as_view(), name='caserio_inicio'),
    url(r'^caserio/listar/$', caserio.CaserioListView.as_view(), name='caserio_lista'),
    url(r'^caserio/crear/(?P<cp>\d+)$', caserio.CaserioCreateView.as_view(), name='caserio_crear'),
    url(r'^caserio/actualizar/(?P<pk>\d+)$', caserio.CaserioUpdateView.as_view(), name='caserio_actualizar'),
    url(r'^caserio/eliminar/(?P<pk>\d+)$', caserio.CaserioDeleteView.as_view(), name='caserio_eliminar'),
    url(r'^caserio/ubigeo/(?P<pk>\d+)/(?P<valor>\d+)/$', caserio.ProvincaDistritoListView.as_view(), name='caserio_ubiego'),



]
