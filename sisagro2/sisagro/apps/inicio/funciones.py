import datetime
from django.conf import settings

def CalcularEdad(fechaAnterior, fechaActual):
    edad = 0
    if fechaAnterior != None and fechaActual != None:
        nacAno = fechaAnterior.year
        nacMes = fechaAnterior.month
        nacDia = fechaAnterior.day
        actAno = fechaActual.year
        actMes = fechaActual.month
        actDia = fechaActual.day
        edad = actAno - nacAno
        if (actMes - nacMes < 0 and nacAno < actAno) or (actMes == nacMes and nacDia > actDia):
            edad = edad - 1
            #
    return edad

def ListaAnios():
    return list(reversed([(anio, anio) for anio in range(settings.APP_CONFIG["ANIO"], datetime.datetime.now().year + 1)]))
