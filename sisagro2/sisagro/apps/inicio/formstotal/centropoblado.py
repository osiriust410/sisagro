# coding=utf-8
from django import forms
from django.utils.encoding import force_text

from ..models import CentroPoblado, Departamento, Provincia, Distrito
from django_select2.forms import ModelSelect2Widget


class CentroPobladoForm(forms.ModelForm):
	distritoid = forms.IntegerField()
	ubigeo = forms.CharField(max_length=6)
	codigo1 = forms.CharField(min_length=2, max_length=2)

	class Meta:
		model = CentroPoblado
		fields = ['codigo1', 'nombre', 'area', 'distritoid', 'ubigeo']

	def __init__(self, *args, **kwargs):
		self.request = kwargs.pop('request')
		self.kwargs = kwargs.pop('kwargs')
		super(CentroPobladoForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'
		self.fields['distritoid'].widget = forms.HiddenInput()
		self.fields['ubigeo'].widget = forms.HiddenInput()
		self.fields['codigo1'].label = u"Código"
		dist = self.kwargs.get("dist", 0)
		if self.instance.pk == None:
			if dist > 0:
				self.fields['ubigeo'].initial = Distrito.objects.get(pk=dist).codigo
		else:
			self.fields['ubigeo'].initial = self.instance.distrito.codigo
			self.fields['codigo1'].initial = self.instance.codigo[-2:8]

	def clean(self):
		self.instance.distrito_id = self.cleaned_data["distritoid"]
		if self.cleaned_data.get("codigo1", "") != "":
			self.instance.codigo = self.cleaned_data["ubigeo"] + self.cleaned_data["codigo1"]
			if CentroPoblado.objects.filter(codigo=self.instance.codigo).exclude(pk=self.instance.pk).count() > 0:
				self.add_error("codigo1", u"El código ya existe")


class CentroPobladoUbiegoForm(forms.Form):
	departamento = forms.ModelChoiceField(
		queryset=Departamento.objects.all(),
		widget=ModelSelect2Widget(
			search_fields=['nombre__icontains'],
			queryset=Departamento.objects.order_by("nombre"),

		)
	)

	provincia = forms.ModelChoiceField(
		queryset=Provincia.objects.all(),
		widget=ModelSelect2Widget(
			search_fields=['nombre__icontains'],
			dependent_fields={'departamento': 'departamento'},
			max_results=500,
			queryset=Provincia.objects.order_by("nombre")
		)
	)

	distritos = forms.ModelChoiceField(
		queryset=Distrito.objects.all(),
		widget=ModelSelect2Widget(
			search_fields=['nombre__icontains'],
			dependent_fields={'provincia': 'provincia'},
			max_results=500,
			queryset=Distrito.objects.order_by("nombre"),
        )
	)

	def __init__(self, *args, **kwargs):
		super(CentroPobladoUbiegoForm, self).__init__(*args, **kwargs)
