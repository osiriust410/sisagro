from django.forms import ModelForm
from ..models import Profesion

class ProfesionForm(ModelForm):
	class Meta:
		model = Profesion
		fields = ['nombre']
	
	def __init__(self, *args, **kwargs):
		super(ProfesionForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()): 
			field.widget.attrs['class'] = 'form-control'
