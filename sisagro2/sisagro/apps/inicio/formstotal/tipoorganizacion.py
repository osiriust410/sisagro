from django.forms import ModelForm

from sisagro.widgets.inputcheck import ModelCheckWidget
from ..models import TipoOrganizacion

class TipoOrganizacionForm(ModelForm):

	estado = ModelCheckWidget(label="Estado", textYES="Activo", textNO="Inactivo", initial=True)

	def __init__(self, *args, **kwargs):
		super(TipoOrganizacionForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'

	class Meta:
		model = TipoOrganizacion
		fields = ['nombre', 'estado']

