from django.forms import ModelForm
from ..models import Mes

class MesForm(ModelForm):
	class Meta:
		model = Mes
		fields = ['numero','nombre']
	
	def __init__(self, *args, **kwargs):
		super(MesForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()): 
			field.widget.attrs['class'] = 'form-control'
