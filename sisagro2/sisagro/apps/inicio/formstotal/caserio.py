# coding=utf-8
from django import forms
from ..models import Caserio, CentroPoblado, Departamento, Provincia, Distrito
from django_select2.forms import ModelSelect2Widget

class CaserioForm(forms.ModelForm):
	codigo1 = forms.CharField(min_length=2, max_length=2)
	centropobladoid = forms.IntegerField()
	ubigeo = forms.CharField(max_length=8)

	class Meta:
		model = Caserio
		fields = ['codigo1','nombre', 'centropobladoid', 'ubigeo']

	def __init__(self, *args, **kwargs):
		self.request = kwargs.pop('request')
		self.kwargs = kwargs.pop('kwargs')
		super(CaserioForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'
		self.fields['centropobladoid'].widget = forms.HiddenInput()
		self.fields['ubigeo'].widget = forms.HiddenInput()
		self.fields['codigo1'].label = u"Código"
		cp = self.kwargs.get("cp", 0)
		if self.instance.pk == None:
			if cp > 0:
				self.fields['ubigeo'].initial = CentroPoblado.objects.get(pk=cp).codigo
		else:
			self.fields['ubigeo'].initial = self.instance.centropoblado.codigo
			self.fields['codigo1'].initial = self.instance.codigo[-2:10]

	def clean(self):
		self.instance.centropoblado_id = self.cleaned_data["centropobladoid"]
		if self.cleaned_data.get("codigo1", "") != "":
			self.instance.codigo = self.cleaned_data["ubigeo"] + self.cleaned_data["codigo1"]
			if Caserio.objects.filter(codigo=self.instance.codigo).exclude(pk=self.instance.pk).count() > 0:
				self.add_error("codigo1", u"El código ya existe")


class CaserioUbigeoForm(forms.Form):
	departamento = forms.ModelChoiceField(
		queryset=Departamento.objects.all(),
		widget=ModelSelect2Widget(
			search_fields=['nombre__icontains'],
			queryset=Departamento.objects.order_by("nombre")
		)
	)

	provincia = forms.ModelChoiceField(
		queryset=Provincia.objects.all(),
			widget=ModelSelect2Widget(
			search_fields=['nombre__icontains'],
			dependent_fields={'departamento':'departamento'},
			max_results=500,
			queryset=Provincia.objects.order_by("nombre")
		)
	)

	distrito = forms.ModelChoiceField(
		queryset=Distrito.objects.all(),
		widget=ModelSelect2Widget(
			search_fields=['nombre__icontains'],
			dependent_fields={'provincia': 'provincia'},
			max_results=500,
			queryset=Distrito.objects.order_by("nombre")
		)
	)

	centropoblados = forms.ModelChoiceField(
		queryset=CentroPoblado.objects.all(),
		widget=ModelSelect2Widget(
			search_fields=['nombre__icontains'],
			dependent_fields={'distrito': 'distrito'},
			max_results=500,
			queryset=CentroPoblado.objects.order_by("nombre")
		)
	)

	def __init__(self, *args, **kwargs):
		super(CaserioUbigeoForm, self).__init__(*args, **kwargs)
		self.fields['centropoblados'].label = "Centro Poblado"
