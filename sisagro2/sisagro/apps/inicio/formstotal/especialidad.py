from django.forms import ModelForm
from ..models import Especialidad

class EspecialidadForm(ModelForm):

	def __init__(self, *args, **kwargs):
		super(EspecialidadForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'

	class Meta:
		model = Especialidad
		fields = ['nombre']

