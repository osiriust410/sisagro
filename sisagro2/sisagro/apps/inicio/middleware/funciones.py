from django.conf import settings
from django.contrib.auth.models import User
from django.urls import reverse

from ...administracion.models import Menu, Acceso

class LoadConfigMiddleware(object):

    def __init__(self, next_layer=None):
        self.get_response = next_layer

    def process_request(self, request):
        request.app_config = settings.APP_CONFIG
        request.app_menus = Menu.objects.filter(padre=None).filter(estado=True).order_by("orden")
        request.app_submenus = Menu.objects.exclude(padre=None).exclude(estado=False).order_by("orden")
        request.app_menu_actual = None
        if request.user.is_authenticated:
            if not request.path.startswith("/admin/"):
                if not request.user.is_superuser:
                    if request.user.persona != None:
                        request.app_submenus = request.app_submenus.filter(
                            pk__in=Acceso.objects.filter(
                                persona=request.user.persona
                            ).values_list("menu_id").distinct()
                        )
                        request.app_menus = request.app_menus.filter(
                            pk__in=request.app_submenus.values_list("padre_id").distinct()
                        )
                for smenu in request.app_submenus:
                    if smenu.ruta != None and len(smenu.ruta) > 0:
                        if reverse(smenu.ruta) == request.path:
                            request.app_menu_actual = smenu
                            break

        """Let's handle old-style request processing here, as usual."""
        # Do something with request
        # Probably return None
        # Or return an HttpResponse in some cases

    def process_response(self, request, response):
        """Let's handle old-style response processing here, as usual."""
        # Do something with response, possibly using request.
        return response

    def __call__(self, request):
        """Handle new-style middleware here."""
        response = self.process_request(request)
        if response is None:
            # If process_request returned None, we must call the next middleware or
            # the view. Note that here, we are sure that self.get_response is not
            # None because this method is executed only in new-style middlewares.
            response = self.get_response(request)
        response = self.process_response(request, response)
        return response


