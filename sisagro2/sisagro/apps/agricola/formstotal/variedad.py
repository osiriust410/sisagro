# -*- coding: utf-8 -*-
from datetimewidget.widgets import DateWidget
from django import forms
from ..models import Variedad, Plaga, Socio, Base, VariedadCatalogo
from django_select2.forms import ModelSelect2Widget, Select2Widget


class VariedadesBaseForm(forms.Form):
	opbase = forms.ModelChoiceField(
		required=False,
		queryset=Base.objects.all(),
		label=u'Base',
		widget=ModelSelect2Widget(
			attrs={'id': 'cb_base', 'class': 'formBase_load', 'data-allow-clear': 'false'},
			queryset=Base.objects.order_by("nombre"),
			search_fields=['nombre__icontains'],
			max_results=10,
		)
	)

	def __init__(self, *args, **kwargs):
		self.request = kwargs.pop('request')
		super(VariedadesBaseForm, self).__init__(*args, **kwargs)

class VariedadesSocioForm(forms.Form):
	opsocio = forms.ModelChoiceField(
		required=False,
		queryset=Base.objects.all(),
		label=u'Socio',
		widget=ModelSelect2Widget(
			attrs={'id': 'cb_socio', 'class': 'formSocio_load', 'data-allow-clear': 'false'},
			search_fields=['persona__nombres__icontains', 'persona__apepat__icontains', 'persona__apemat__icontains', 'persona__dni__icontains'],
			max_results=10,
		)
	)

	def __init__(self, *args, **kwargs):
		self.request = kwargs.pop('request')
		super(VariedadesSocioForm, self).__init__(*args, **kwargs)
		self.fields["opsocio"].widget.queryset = Socio.objects.filter(
			base_id=self.request.GET.get("opbase", 0)
		).order_by("persona__apepat", "persona__apemat", "persona__nombres")


class VariedadForm(forms.ModelForm):
	variedadcatalogo = forms.ModelChoiceField(
		queryset=VariedadCatalogo.objects.all(),
		label=u'Nombre',
		widget=ModelSelect2Widget(
			queryset=VariedadCatalogo.objects.all().order_by("nombre"),
			search_fields=['nombre__icontains'],
			max_results=10,
		)
	)

	plaga = forms.ModelChoiceField(
		queryset=Plaga.objects.all(),
		label=u'Plaga',
		widget=ModelSelect2Widget(
			queryset=Plaga.objects.all().order_by("nombre"),
			search_fields=['nombre__icontains', 'nombrecientifico__icontains'],
			max_results=10,
		)
	)

	tipocertificado = forms.ChoiceField(
		required=True,
		choices=Variedad.tipoCertificado,
		widget=Select2Widget(attrs={'data-minimum-results-for-search': 'Infinity'})
	)

	socioid = forms.IntegerField(widget=forms.HiddenInput())

	titulo = forms.ChoiceField(
		label=u"Título",
		choices=Variedad.tipoTitulo,
		widget = Select2Widget(attrs={'data-minimum-results-for-search': 'Infinity'})
	)

	fecha = forms.DateField(
        label=u'Fecha',
        widget=DateWidget(
            usel10n=True,
            bootstrap_version=3,
            attrs={'data-datepicker-type': "4", "data-provide": "datepicker-inline"},
            options={
                'autoclose': 'true',
				'clearBtn': 'false'
            }
        )
    )

	def __init__(self, *args, **kwargs):
		super(VariedadForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			if field.widget.__class__ == forms.NumberInput:
				dp = 2 if type(field) == forms.DecimalField else 0
				dpe = 0.01 if type(field) == forms.DecimalField else 1
				field.widget.attrs = {"class": 'touch-spin', "data-max": "Infinity", "data-decimals": dp, "data-step": dpe,
									  "style": "text-align:right", "data-forcestepdivisibility": "none"}
				if self.instance.pk == None:
					field.initial = 0
			else:
				field.widget.attrs['class'] = 'form-control'
		self.fields['area'].label = 'Area'
		self.fields['areasecano'].label = 'Area de Secano'
		self.fields['areabajoriego'].label = 'Area Bajo Riego'
		self.fields['preciochacra'].label = 'Precio de Chacra'
		self.fields['preciomercado'].label = 'Precio de Mercado'
		self.fields['volumenproduccion'].label = 'Volumen de Producción'
		self.fields['tipocertificado'].label = 'Tipo de Cert.'
		self.fields['certificadas'].label = 'Certificadas'
		self.fields['sincertificar'].label = 'Sin Certificar'
		self.fields['fecha'].label = 'Fecha'
		# self.fields['titulo'].label = 'Título'

	def clean(self):
		self.instance.socio_id = self.cleaned_data["socioid"]
		if self.cleaned_data.get("area", 0) > 0:
			self.instance.rendimiento = self.cleaned_data["volumenproduccion"]/self.cleaned_data["area"]
		else:
			self.instance.rendimiento = 0

	class Meta:
		model = Variedad
		fields = [
            'plaga', 'socioid',
            'variedadcatalogo', 'area', 'areasecano', 'areabajoriego',
            'preciochacra', 'preciomercado',
            'volumenproduccion', 'tipocertificado',
			'certificadas', 'sincertificar',
            'titulo', 'fecha', 'observaciones'
        ]

