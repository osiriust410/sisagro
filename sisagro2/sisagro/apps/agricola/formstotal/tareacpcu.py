# -*- coding: utf-8 -*-
from datetimewidget.widgets import DateWidget
from django import forms
from django.utils.encoding import force_text
from django_select2.forms import ModelSelect2Widget

from ...administracion.models import Trabajador
from ..models import Base
from ...planificacion.models import TareaCpCu, CadenaProductiva, Actividad, SubActividadComponente, TareaCatalogo


class TareaCpCuCadProdForm(forms.Form):
	opcadprod = forms.ModelChoiceField(
		required=False,
		queryset=CadenaProductiva.objects.all(),
		label=u'Cadena Productiva',
		widget=ModelSelect2Widget(
			attrs={'id': 'cb_cadprod', 'class': 'formCadProd_load', 'data-allow-clear': 'false'},
			queryset=CadenaProductiva.objects.order_by("nombre"),
			search_fields=['nombre__icontains'],
			max_results=10,
		)
	)

	def __init__(self, *args, **kwargs):
		self.request = kwargs.pop('request')
		super(TareaCpCuCadProdForm, self).__init__(*args, **kwargs)


class TareaCpCuActividadForm(forms.Form):
	opactividad = forms.ModelChoiceField(
		required=False,
		queryset=Actividad.objects.all(),
		label=u'Actividad',
		widget=ModelSelect2Widget(
			attrs={'id': 'cb_actividad', 'class': 'formActividad_load', 'data-allow-clear': 'false'},
			search_fields=['actividadcatalogo__nombre__icontains'],
			max_results=10,
		)
	)

	def __init__(self, *args, **kwargs):
		self.request = kwargs.pop('request')
		super(TareaCpCuActividadForm, self).__init__(*args, **kwargs)
		self.fields["opactividad"].widget.queryset = Actividad.objects.filter(
			cadenaproductiva_id=self.request.GET.get("opcadprod", 0)
		).order_by("actividadcatalogo__nombre")


class TareaCpCuSubActividadForm(forms.Form):
	opsubactividad = forms.ModelChoiceField(
		required=False,
		queryset=SubActividadComponente.objects.all(),
		label=u'Sub Actividad',
		widget=ModelSelect2Widget(
			attrs={'id': 'cb_subactividad', 'class': 'formSubActividad_load', 'data-allow-clear': 'false'},
			search_fields=['subactividadcatalogo__nombre__icontains'],
			max_results=10,
		)
	)

	def __init__(self, *args, **kwargs):
		self.request = kwargs.pop('request')
		super(TareaCpCuSubActividadForm, self).__init__(*args, **kwargs)
		self.fields["opsubactividad"].widget.queryset = SubActividadComponente.objects.filter(
			actividad_id=self.request.GET.get("opactividad", 0)
		).order_by("subactividadcatalogo__nombre")


class widgetTrabajador(ModelSelect2Widget):
	queryset = Trabajador.objects.order_by("persona__apepat", "persona__apemat", "persona__nombres")
	search_fields = ["persona__apepat", "persona__apemat", "persona__nombres", "persona__dni"]
	max_results = 10

	def label_from_instance(self, obj):
		return force_text(obj.persona.nombreCompleto())


class TareaCpCuForm(forms.ModelForm):
	subactid = forms.IntegerField(
		widget=forms.HiddenInput
	)

	trabajador = forms.ModelChoiceField(
		label=u'Asignado a',
		queryset=Trabajador.objects.all(),
		widget=widgetTrabajador
	)

	base = forms.ModelChoiceField(
		label=u"Base",
		queryset=Base.objects.all(),
		widget=ModelSelect2Widget(
			queryset=Base.objects.order_by("nombre"),
			search_fields=["nombre__icontains"],
			max_results=10
		)
	)

	inicio = forms.DateField(
		label=u'Inicio',
		required=True,
		widget=DateWidget(
			usel10n=True,
			bootstrap_version=3,
			attrs={'data-datepicker-type': "4", "data-provide": "datepicker-inline"},
			options={
				'autoclose': 'true',
				'clearBtn': 'false'
			},
		)
	)

	fin = forms.DateField(
		label=u'Finalización',
		required=True,
		widget=DateWidget(
			usel10n=True,
			bootstrap_version=3,
			attrs={'data-datepicker-type': "4", "data-provide": "datepicker-inline"},
			options={
				'autoclose': 'true',
				'clearBtn': 'false'
			},
		)
	)

	tareacatalogo = forms.ModelChoiceField(
		label=u"Nombre",
		queryset=TareaCatalogo.objects.all(),
		widget=ModelSelect2Widget(
			queryset=TareaCatalogo.objects.order_by("nombre"),
			search_fields=["nombre__icontains"],
			max_results=10
		)
	)

	def __init__(self, *args, **kwargs):
		super(TareaCpCuForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'
		self.fields['descripcion'].label = 'Descripción'

	class Meta:
		model = TareaCpCu
		fields = [
			'subactid', 'tareacatalogo', 'trabajador', 'base', 'descripcion',
			'inicio', 'fin'
		]

	def clean(self):
		self.instance.subactividadcomponente_id = self.cleaned_data["subactid"]
