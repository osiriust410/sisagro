# -*- coding: utf-8 -*-
from django.forms import ModelForm
from ..models import Plaga

class PlagaForm(ModelForm):

	def __init__(self, *args, **kwargs):
		super(PlagaForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'
		self.fields['nombre'].label = 'Nombre'
		self.fields['nombrecientifico'].label = 'Nombre Científico'
		self.fields['descripcion'].label = 'Descripción'

	class Meta:
		model = Plaga
		fields = [
            'nombre', 'nombrecientifico', 'descripcion'
        ]
