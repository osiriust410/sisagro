from django.forms import ModelForm

from ..models import VariedadCatalogo

class VariedadCatalogoForm(ModelForm):

	def __init__(self, *args, **kwargs):
		super(VariedadCatalogoForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'

	class Meta:
		model = VariedadCatalogo
		fields = ['nombre']

