# -*- coding: utf-8 -*-
from django import forms
from django_select2.forms import ModelSelect2Widget

from ..models import Raza, Especie

class RazaSelectForm(forms.Form):
	opespecie = forms.ChoiceField(
		label=u'Especie',
		required=False,
		widget=ModelSelect2Widget(
			attrs={'id': 'cb_especie', 'class': 'formEspecie_load', 'data-allow-clear': 'false'},
			queryset=Especie.objects.all().order_by("nombre"),
			model=Especie,
			search_fields=['nombre__icontains'],
			max_results=10,
		)
	)

	def __init__(self, *args, **kwargs):
		self.request = kwargs.pop('request')
		super(RazaSelectForm, self).__init__(*args, **kwargs)

class RazaForm(forms.ModelForm):
	especieid = forms.IntegerField(
        widget=forms.HiddenInput
    )


	def __init__(self, *args, **kwargs):
		super(RazaForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'
		self.fields['nombre'].label = 'Nombre'

	class Meta:
		model = Raza
		fields = ['especieid', 'nombre']

	def clean(self):
		self.instance.especie_id = self.cleaned_data["especieid"]



