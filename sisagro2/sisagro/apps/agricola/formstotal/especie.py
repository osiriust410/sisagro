# -*- coding: utf-8 -*-
from django import forms

from sisagro.widgets.inputcheck import ModelCheckWidget
from ..models import Especie, EnfermedadXEspecie, Enfermedad


import math
from itertools import chain
from django.utils.encoding import force_unicode
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe

class EspecieForm(forms.ModelForm):

	estado = ModelCheckWidget(label="Estado", textYES="Activo", textNO="Inactivo", initial=True)

	def __init__(self, *args, **kwargs):
		super(EspecieForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'
		self.fields['nombre'].label = 'Nombre'
		self.fields['estado'].label = 'Estado'

	class Meta:
		model = Especie
		fields = ['nombre', 'estado']

class EnfermedadXEspecieForm(forms.ModelForm):

	seleccionado = forms.ModelMultipleChoiceField(
		queryset=Enfermedad.objects.all().order_by("nombre"),
		widget=forms.CheckboxSelectMultiple(),
		label=u"",
		required=False
	)

	def __init__(self, *args, **kwargs):
		super(EnfermedadXEspecieForm, self).__init__(*args, **kwargs)
		if not 'data' in kwargs:
			checks = []
			for en_x_es in self.instance.enfermedadxespecie_set.all():
				checks.append(en_x_es.enfermedad.pk)
			self.fields["seleccionado"].initial = checks

	def save(self, commit=True):
		_result = super(EnfermedadXEspecieForm, self).save(commit=False)
		enfermedades = self.cleaned_data.get("seleccionado")
		EnfermedadXEspecie.objects.filter(
			especie=self.instance
		).exclude(
			enfermedad__in=enfermedades
		).delete()
		enf_news = []
		for enf in enfermedades:
			if EnfermedadXEspecie.objects.filter(
					especie=self.instance,
					enfermedad=enf
				).count() == 0:
				enf_news.append(EnfermedadXEspecie(especie=self.instance, enfermedad=enf, creador=self.request.user))

		if len(enf_news) > 0:
			EnfermedadXEspecie.objects.bulk_create(enf_news)

		return _result

	class Meta:
		model = Enfermedad
		fields = ['seleccionado']
