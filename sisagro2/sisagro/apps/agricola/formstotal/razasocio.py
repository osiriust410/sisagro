# -*- coding: utf-8 -*-
from django import forms
from django_select2.forms import ModelSelect2Widget
from django_select2.forms import Select2Widget

from ..models import RazaXSocio, Socio, Raza,Base


class RazaSocioForm(forms.ModelForm):

    raza = forms.ModelChoiceField(
        queryset = Raza.objects.all(),
        label=u'Raza',
        required=True,
        widget=ModelSelect2Widget(
            search_fields=['nombre__icontains'],
            queryset=Raza.objects.order_by("nombre"),
        )
    )

    class Meta:
        model = RazaXSocio
        fields = ['socio','raza','rendimiento', 'nrocabezas', 'produccion', 'seca']

    def __init__(self, *args, **kwargs):
        super(RazaSocioForm, self).__init__(*args, **kwargs)
        for i, (fname, field) in enumerate(self.fields.iteritems()):
            if field.widget.__class__ == forms.NumberInput:
                dp = 2 if type(field) == forms.DecimalField else 0
                dpe = 0.01 if type(field) == forms.DecimalField else 1
                field.widget.attrs = {"class": 'touch-spin', "data-max": "Infinity", "data-decimals": dp,
                                      "data-step": dpe,
                                      "style": "text-align:right", "data-forcestepdivisibility": "none"}
                if self.instance.pk == None:
                    field.initial = 0
            else:
                field.widget.attrs['class'] = 'form-control'
        self.fields['socio'].widget = forms.HiddenInput()



class SociosForm(forms.Form):

    base = forms.ModelChoiceField(
        queryset=Base.objects.all(),
        widget=ModelSelect2Widget(
            search_fields=['nombre__icontains'],
            queryset=Base.objects.order_by("nombre"),
        )
    )

    socios = forms.ModelChoiceField(
        queryset=Socio.objects.all(),
        widget=ModelSelect2Widget(
            search_fields=['persona__apepat__icontains','persona__apemat__icontains','persona__nombres__icontains','persona__dni__icontains'],
            queryset=Socio.objects.order_by("persona__apepat", "persona__apemat", "persona__nombres"),
            dependent_fields={'base': 'base'},
        )
    )
    def __init__(self, *args, **kwargs):
        super(SociosForm, self).__init__(*args, **kwargs)
