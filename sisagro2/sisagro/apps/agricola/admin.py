# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import *

# Register your models here.
class AdminEnfermedad(admin.ModelAdmin):
	list_display = ["id", "nombre", "descripcion"]
	# list_filter = ["nombre"]
	search_fields = ["nombre", "descripcion"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminEspecie(admin.ModelAdmin):
	list_display = ["id", "nombre", "estado"]
	# list_filter = ["nombre"]
	search_fields = ["nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminEnfermedadXEspecie(admin.ModelAdmin):
	list_display = ["id", "enfermedad", "especie"]
	# list_filter = ["nombre"]
	search_fields = ["enfermedad__nombre", "especie__nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminBase(admin.ModelAdmin):
	list_display = ["id", "nombre", "caserio", "organizacion"]
	# list_filter = ["nombre"]
	search_fields = ["nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminSocio(admin.ModelAdmin):
	list_display = ["id", "persona", "email"]
	# list_filter = ["nombre"]
	search_fields = ["persona__nombres"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminRaza(admin.ModelAdmin):
	list_display = ["id", "nombre", "especie"]
	# list_filter = ["nombre"]
	search_fields = ["nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminPlaga(admin.ModelAdmin):
	list_display = ["id", "nombre", "nombrecientifico", "descripcion"]
	# list_filter = ["nombre"]
	search_fields = ["nombre", "nombrecientifico", "descripcion"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminRazaXSocio(admin.ModelAdmin):
	list_display = ["id", "raza", "socio"]
	# list_filter = ["nombre"]
	search_fields = ["raza__nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminEnfermedadXSocio(admin.ModelAdmin):
	list_display = ["id", "enfermedadxespecie", "razaxsocio"]
	# list_filter = ["nombre"]
	search_fields = ["enfermedadxespecie__enfermedad__nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminVariedadCatalogo(admin.ModelAdmin):
	list_display = ["id", "nombre"]
	# list_filter = ["nombre"]
	search_fields = ["nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminVariedad(admin.ModelAdmin):
	list_display = ["id", "variedadcatalogo", "area"]
	# list_filter = ["nombre"]
	search_fields = ["variedadcatalogo__nombre", "area"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

admin.site.register(Enfermedad, AdminEnfermedad)
admin.site.register(Especie, AdminEspecie)
admin.site.register(EnfermedadXEspecie, AdminEnfermedadXEspecie)
admin.site.register(Base, AdminBase)
admin.site.register(Socio, AdminSocio)
admin.site.register(Plaga, AdminPlaga)
admin.site.register(Raza, AdminRaza)
admin.site.register(RazaXSocio, AdminRazaXSocio)
admin.site.register(EnfermedadXSocio, AdminEnfermedadXSocio)
admin.site.register(VariedadCatalogo, AdminVariedadCatalogo)
admin.site.register(Variedad, AdminVariedad)
