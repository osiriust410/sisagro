from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import FormView, ListView, DetailView
from django.http import HttpResponse
import json

from ..formstotal.socio import SocioForm
from ...inicio.models import AuditableMixin
from ..models import Variedad, Plaga, Socio
from ..formstotal.variedad import VariedadForm, VariedadesBaseForm, VariedadesSocioForm
from ...inicio.mixin import LoginRequiredMixin

class VariedadBaseView(LoginRequiredMixin, FormView):
    template_name = "agricola/variedad/inicio.html"
    form_class = VariedadesBaseForm

    def get_form_kwargs(self):
        kwargs = super(VariedadBaseView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(VariedadBaseView, self).get_context_data(**kwargs)
        data['formBase'] = data.get('form')
        return data

class VariedadSocioView(LoginRequiredMixin, FormView):
    template_name = "agricola/variedad/socio.html"
    form_class = VariedadesSocioForm

    def get_form_kwargs(self):
        kwargs = super(VariedadSocioView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(VariedadSocioView, self).get_context_data(**kwargs)
        data['formSocio'] = data.get('form')
        return data

class VariedadTablaView(LoginRequiredMixin, DetailView):
    template_name = "agricola/variedad/tabla.html"
    model = Socio
    context_object_name = "oSocio"

class VariedadListView(LoginRequiredMixin, ListView):

    def get(self, request, *args, **kwargs):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        socid = kwargs.get('socid', 0)
        # data = {}
        orderby = ""
        if order != 'asc':
            orderby = "-"

        variedades = Variedad.objects.filter(socio_id=socid).order_by('-id')

        if len(search) > 0:
            # socios = socios.filter(nombre__icontains=search)
            variedades = variedades.filter(
                Q(nombre__icontains=search)
                # Q(persona__apepat__icontains=search) |
                # Q(persona__apemat__icontains=search)
            )

        if sort != '':
            variedades = variedades.order_by(orderby + sort)

        paginador = Paginator(variedades, limit)

        try:
            variedades = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            variedades = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            variedades = paginador.page(paginador.num_pages)

        return render(
            request,
            'agricola/variedad/datos.json',
            {
                'variedades': variedades,
                'total': paginador.count
            }
        )

class VariedadCreateView(AuditableMixin, CreateView):
    model = Variedad
    form_class = VariedadForm
    template_name = "agricola/variedad/formulario.html"

    def form_valid(self, form):
        super(VariedadCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(VariedadCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class VariedadUpdateView(AuditableMixin, UpdateView):
    model = Variedad
    form_class = VariedadForm
    template_name = "agricola/variedad/formulario.html"

    def dispatch(self, *args, **kwargs):
        self.variedad_id = kwargs['pk']
        return super(VariedadUpdateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        super(VariedadUpdateView, self).form_valid(form)
        variedad = Variedad.objects.get(pk=self.variedad_id)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(VariedadUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

    def get_context_data(self, **kwargs):
        context = super(VariedadUpdateView, self).get_context_data(**kwargs)
        context["variedad_id"] = self.variedad_id
        return context

class VariedadDeleteView(DeleteView):
    model = Variedad
    form_class = VariedadForm
    template_name = "agricola/variedad/eliminar.html"

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        try:
            self.get_object().delete()
        except Exception as e:
            msg['value'] = False
            return render(request, self.template_name, {'object': self.get_object(), 'delete_error': str(e)})
        return HttpResponse(json.dumps(msg), content_type='application/json')

