# -*- coding: utf-8 -*-
import json

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import FormView, TemplateView, ListView, UpdateView, CreateView, DeleteView

from ..formstotal.plaga import PlagaForm
from ..models import Plaga
from ...inicio.mixin import LoginRequiredMixin
from ...inicio.models import AuditableMixin

class PlagaView(LoginRequiredMixin, TemplateView):
    template_name = "agricola/plaga/inicio.html"

class PlagaListView(LoginRequiredMixin, ListView):

    def get(self, request):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        # data = {}
        orderby = ""
        if order != 'asc':
            orderby = "-"

        plagas = Plaga.objects.all().order_by('-id')

        if len(search) > 0:
            # socios = socios.filter(nombre__icontains=search)
            plagas = plagas.filter(
                Q(nombre__icontains=search) |
                Q(nombrecientifico__icontains=search) |
                Q(descripcion__icontains=search)
            )

        if sort != '':
            plagas = plagas.order_by(orderby + sort)

        paginador = Paginator(plagas, limit)

        try:
            plagas = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            plagas = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            plagas = paginador.page(paginador.num_pages)

        return render(
            request,
            'agricola/plaga/datos.json',
            {
                'plagas': plagas,
                'total': paginador.count
            }
        )

class PlagaCreateView(AuditableMixin, CreateView):
    model = Plaga
    form_class = PlagaForm
    template_name = "agricola/plaga/formulario.html"

    def form_valid(self, form):
        super(PlagaCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(PlagaCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class PlagaUpdateView(UpdateView):
    model = Plaga
    form_class = PlagaForm
    template_name = "agricola/variedad/formulario.html"

    def form_valid(self, form):
        super(PlagaUpdateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(PlagaUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class PlagaDeleteView(DeleteView):
    model = Plaga
    form_class = PlagaForm
    template_name = "agricola/plaga/eliminar.html"

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        try:
            self.get_object().delete()
        except Exception as e:
            msg['value'] = False
            return render(request, self.template_name, {'object': self.get_object(), 'delete_error': str(e)})
        return HttpResponse(json.dumps(msg), content_type='application/json')


