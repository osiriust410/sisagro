from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from django.views.generic import TemplateView, ListView, DetailView
from django.http import HttpResponse
import json

from ...administracion.models import Persona
from ...inicio.models import AuditableMixin
from ..models import Socio, Base
from ..formstotal.socio import SocioForm, SocioSelectForm
from ...inicio.mixin import LoginRequiredMixin

class SocioSelectView(LoginRequiredMixin, FormView):
    template_name = "agricola/socio/inicio.html"
    form_class = SocioSelectForm

    def get_form_kwargs(self):
        kwargs = super(SocioSelectView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(SocioSelectView, self).get_context_data(**kwargs)
        data['formBase'] = data.get('form')
        return data

class SocioView(LoginRequiredMixin, DetailView):
    template_name = "agricola/socio/socio.html"
    model = Base
    context_object_name = "oBase"

class SocioListView(LoginRequiredMixin, ListView):

    def get(self, request, *args, **kwargs):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        baseid = kwargs.get('base', 0)
        # data = {}
        orderby = ""
        if order != 'asc':
            orderby = "-"

        socios = Socio.objects.filter(base_id=baseid).order_by('-id')

        if len(search) > 0:
            # socios = socios.filter(nombre__icontains=search)
            socios = socios.filter(
                Q(persona__nombre__icontains=search) |
                Q(persona__apepat__icontains=search) |
                Q(persona__apemat__icontains=search)
            )

        if sort != '':
            if sort == "persona":
                socios = socios.order_by(orderby + "persona__nombres", orderby + "persona__apepat", orderby + "persona__apemat")
            else:
                socios = socios.order_by(orderby + sort)

        paginador = Paginator(socios, limit)

        try:
            socios = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            socios = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            socios = paginador.page(paginador.num_pages)

        return render(
            request,
            'agricola/socio/datos.json',
            {
                'socios': socios,
                'total': paginador.count
            }
        )

class SocioCreateView(AuditableMixin, CreateView):
    model = Socio
    form_class = SocioForm
    template_name = "agricola/socio/formulario.html"

    def form_valid(self, form):
        super(SocioCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(SocioCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class SocioUpdateView(AuditableMixin, UpdateView):
    model = Socio
    form_class = SocioForm
    template_name = "agricola/socio/formulario.html"

    def form_valid(self, form):
        super(SocioUpdateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(SocioUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class SocioDeleteView(DeleteView):
    model = Socio
    form_class = SocioForm
    template_name = "agricola/socio/eliminar.html"

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        try:
            self.get_object().delete()
        except Exception as e:
            msg['value'] = False
            return render(request, self.template_name, {'object': self.get_object(), 'delete_error': str(e)})
        return HttpResponse(json.dumps(msg), content_type='application/json')

