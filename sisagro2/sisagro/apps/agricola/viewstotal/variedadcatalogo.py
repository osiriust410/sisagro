from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView, ListView
from django.http import HttpResponse
import json

from ..models import VariedadCatalogo
from ...inicio.models import AuditableMixin
from ..formstotal.variedadcatalogo import VariedadCatalogoForm
from ...inicio.mixin import LoginRequiredMixin

class VariedadCatalogoView(LoginRequiredMixin, TemplateView):
    template_name = "agricola/variedadcatalogo/inicio.html"

class VariedadCatalogoListView(LoginRequiredMixin, ListView):

    def get(self, request, *args, **kwargs):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        # data = {}
        orderby = ""
        if order != 'asc':
            orderby = "-"

        variedadescatalogos = VariedadCatalogo.objects.all().order_by('-id')

        if len(search) > 0:
            variedadescatalogos = variedadescatalogos.filter(nombre__icontains=search)

        if sort != '':
            variedadescatalogos = variedadescatalogos.order_by(orderby + sort)

        paginador = Paginator(variedadescatalogos, limit)

        try:
            variedadescatalogos = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            variedadescatalogos = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            variedadescatalogos = paginador.page(paginador.num_pages)

        return render(
            request,
            'agricola/variedadcatalogo/datos.json',
            {
                'variedadescatalogos': variedadescatalogos,
                'total': paginador.count
            }
        )

class VariedadCatalogoCreateView(AuditableMixin, CreateView):
    model = VariedadCatalogo
    form_class = VariedadCatalogoForm
    template_name = "agricola/variedadcatalogo/formulario.html"

    def form_valid(self, form):
        super(VariedadCatalogoCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(VariedadCatalogoCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class VariedadCatalogoUpdateView(AuditableMixin, UpdateView):
    model = VariedadCatalogo
    form_class = VariedadCatalogoForm
    template_name = "agricola/variedadcatalogo/formulario.html"

    def form_valid(self, form):
        super(VariedadCatalogoUpdateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(VariedadCatalogoUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class VariedadCatalogoDeleteView(DeleteView):
    model = VariedadCatalogo
    form_class = VariedadCatalogoForm
    template_name = "agricola/variedadcatalogo/eliminar.html"

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        try:
            self.get_object().delete()
        except Exception as e:
            msg['value'] = False
            return render(request, self.template_name, {'object': self.get_object(), 'delete_error': str(e)})
        return HttpResponse(json.dumps(msg), content_type='application/json')



