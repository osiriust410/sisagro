# -*- coding: utf-8 -*-
import xlwt

from django.http import HttpResponse
from ..models import Variedad, Socio
from datetime import datetime
from django.utils import formats

def ListaVariedadSocioXLS(request, idsocio):

    #Poner un try
    try:
        socio = Socio.objects.get(pk=idsocio)
        variedades = enumerate(Variedad.objects.filter(socio=socio).order_by('-id'))
    except Socio.DoesNotExist:
        socio = None
        variedades = None

    if socio != None:
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename="VariedadesSocio_' + str(formats.date_format(datetime.now(), "d-m-Y")) + '.xls"'

        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('Lista')

        # Sheet header, first row
        row_num = 0

        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        data = [
                    ('Dirección Regional de Agricultura Cajamarca',),
                    ('Reporte de Variedades por Asociado',),
                    ('',),
                    ('SOCIO', '%s' % socio,'DNI', '%s' % socio.persona.dni),
                    ('ÁREA TOTAL (Sembrada y no Sembrada)-Ha:', '¿Cuál es la formula?',),
                    ('BASE', '%s' % socio.base.nombre,),
                    ('ORGANIZACIÓN::', '%s' % socio.base.organizacion.nombre,),
                    ('CASERÍO:', '%s' % socio.base.caserio.nombre,),
                    ('Provincia/Distrito/Centro Poblado:', 
                    '%s/%s/%s' % (
                            socio.base.caserio.centropoblado.distrito.provincia.nombre,
                            socio.base.caserio.centropoblado.distrito.nombre,
                            socio.base.caserio.centropoblado.nombre,
                            ),
                    ),
                    
                ]

        for tupla in data:
            row_num = row_num + 1
            for i, col_num in enumerate(tupla):
                ws.write(row_num, i, col_num, font_style)


        #Encabezados
        row_num = row_num + 2
        columns = ['Item', 
                    'Nombre Variedad', 
                    "Área Sembrada Secano (Ha)", 
                    "Área Sembrada Bajo Riego (Ha)",
                    "Área Total Sembrada por variedad (Ha)", 
                    "Volúmen Producción (kg)", 
                    "Rendimiento Kg/Ha", 
                    "Precio Chacra (S/.- Kg)", 
                    "Precio Mercado (S/.- Kg)",
                    "Tipo Certificación", 
                    "Área Certificadas (Ha)",
                    "Área Sin Certificar (Ha)",
                    "Título Propiedad",
                    'Plaga', 
                    ]

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        #Llenando datos
        font_style = xlwt.XFStyle()

        total_areasecano = 0
        total_areabajoriego = 0
        total_area = 0
        total_volumenproduccion = 0
        total_rendimiento = 0
        total_certificadas = 0
        total_sincertificar = 0


        for i, variedad in variedades:
            row_num = row_num + 1

            columns = [
                            i + 1, 
                            '%s' % (variedad.nombre),
                            variedad.areasecano,
                            variedad.areabajoriego,
                            variedad.area,
                            variedad.volumenproduccion,
                            variedad.rendimiento,
                            variedad.preciochacra,
                            '%s' % (variedad.preciomercado),
                            '%s' % (variedad.TipoCertificado()),
                            variedad.certificadas,
                            variedad.sincertificar,
                            '%s' % (variedad.Titulo()),
                            '%s' % (variedad.plaga),
                            # '%s' % formats.date_format(variedad.fecha, "d-m-Y"),
                            # '%s' % (variedad.observaciones),
                        ]

            total_areasecano = total_areasecano + variedad.areasecano
            total_areabajoriego = total_areabajoriego + variedad.areabajoriego
            total_area = total_area + variedad.area
            total_volumenproduccion = total_volumenproduccion + variedad.volumenproduccion
            total_rendimiento = total_rendimiento + variedad.rendimiento
            total_certificadas = total_certificadas + variedad.certificadas
            total_sincertificar = total_sincertificar + variedad.sincertificar

            for col_num in range(len(columns)):
                ws.write(row_num, col_num, columns[col_num], font_style)


        data = [
                    (
                        'Total',
                        '',
                        total_areasecano,
                        total_areabajoriego,
                        total_area,
                        total_volumenproduccion,
                        total_rendimiento,
                        '',
                        '',
                        '',
                        total_certificadas,
                        total_sincertificar,
                        '',
                        '',
                    ),
                ]

        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        for tupla in data:
            row_num = row_num + 1
            for i, col_num in enumerate(tupla):
                ws.write(row_num, i, col_num, font_style)


        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()
        font_style.font.bold = False

        ws.write(row_num + 3, 0, 'Reporte generado el ' + str(formats.date_format(datetime.now(), "d-m-Y h:i:s a")), font_style)

        wb.save(response)
        return response

    else:
        return HttpResponse("No se han encontrado datos válidos")