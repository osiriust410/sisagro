# -*- coding: utf-8 -*-
import json

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import FormView, DetailView, ListView, CreateView, UpdateView, DeleteView

from ...inicio.models import AuditableMixin
from ...planificacion.models import TareaCpCu, SubActividadComponente
from ..formstotal.tareacpcu import TareaCpCuCadProdForm, TareaCpCuActividadForm, TareaCpCuSubActividadForm, \
    TareaCpCuForm
from ...inicio.mixin import LoginRequiredMixin


class TareaCpCuCadProdView(LoginRequiredMixin, FormView):
    template_name = "agricola/tareacpcu/cadprod.html"
    form_class = TareaCpCuCadProdForm

    def get_form_kwargs(self):
        kwargs = super(TareaCpCuCadProdView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(TareaCpCuCadProdView, self).get_context_data(**kwargs)
        data['formCadProd'] = data.get('form')
        return data

class TareaCpCuActividadView(LoginRequiredMixin, FormView):
    template_name = "agricola/tareacpcu/actividad.html"
    form_class = TareaCpCuActividadForm

    def get_form_kwargs(self):
        kwargs = super(TareaCpCuActividadView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(TareaCpCuActividadView, self).get_context_data(**kwargs)
        data['formActividad'] = data.get('form')
        return data

class TareaCpCuSubActividadView(LoginRequiredMixin, FormView):
    template_name = "agricola/tareacpcu/subactividad.html"
    form_class = TareaCpCuSubActividadForm

    def get_form_kwargs(self):
        kwargs = super(TareaCpCuSubActividadView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(TareaCpCuSubActividadView, self).get_context_data(**kwargs)
        data['formSubActividad'] = data.get('form')
        return data

class TareaCpCuTablaView(LoginRequiredMixin, DetailView):
    template_name = "agricola/tareacpcu/tabla.html"
    model = SubActividadComponente
    context_object_name = "oSubActividad"

class TareaCpCuListView(LoginRequiredMixin, ListView):

    def get(self, request, *args, **kwargs):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        subactid = kwargs.get('subact', 0)
        orderby = ""
        if order != 'asc':
            orderby = "-"

        tareascpcu = TareaCpCu.objects.filter(subactividadcomponente_id=subactid).order_by('-id')

        if len(search) > 0:
            tareascpcu = tareascpcu.filter(
                Q(referencia__icontains=search) |
                Q(trabajador__persona__apepat__icontains=search) |
                Q(trabajador__persona__apemat__icontains=search) |
                Q(trabajador__persona__nombres__icontains=search)
            )

        if sort != '':
            tareascpcu = tareascpcu.order_by(orderby + sort)

        paginador = Paginator(tareascpcu, limit)

        try:
            tareascpcu = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            tareascpcu = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            tareascpcu = paginador.page(paginador.num_pages)

        return render(
            request,
            'agricola/tareacpcu/datos.json',
            {
                'tareascpcu': tareascpcu,
                'total': paginador.count
            }
        )

class TareaCpCuCreateView(AuditableMixin, CreateView):
    model = TareaCpCu
    form_class = TareaCpCuForm
    template_name = "agricola/tareacpcu/formulario.html"

    def form_valid(self, form):
        super(TareaCpCuCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(TareaCpCuCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class TareaCpCuUpdateView(AuditableMixin, UpdateView):
    model = TareaCpCu
    form_class = TareaCpCuForm
    template_name = "agricola/tareacpcu/formulario.html"

    def form_valid(self, form):
        super(TareaCpCuUpdateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(TareaCpCuUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class TareaCpCuDeleteView(DeleteView):
    model = TareaCpCu
    form_class = TareaCpCuForm
    template_name = "agricola/tareacpcu/eliminar.html"

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        try:
            self.get_object().delete()
        except Exception as e:
            msg['value'] = False
            return render(request, self.template_name, {'object': self.get_object(), 'delete_error': str(e)})
        return HttpResponse(json.dumps(msg), content_type='application/json')

