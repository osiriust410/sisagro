from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView, ListView
from django.http import HttpResponse
import json

# from ...administracion.models import Persona
from ...inicio.models import AuditableMixin
from ..models import Enfermedad, Base
from ..formstotal.enfermedad import EnfermedadForm
from ...inicio.mixin import LoginRequiredMixin

class EnfermedadView(LoginRequiredMixin, TemplateView):
    template_name = "agricola/enfermedad/inicio.html"

class EnfermedadListView(LoginRequiredMixin, ListView):

    def get(self, request):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        # data = {}
        orderby = ""
        if order != 'asc':
            orderby = "-"

        enfermedades = Enfermedad.objects.all().order_by('-id')

        if len(search) > 0:
            # enfermedades = enfermedades.filter(nombre__icontains=search)
            enfermedades = enfermedades.filter(
                Q(nombre__icontains=search) |
                Q(descripcion__icontains=search)
            )

        if sort != '':
            if orderby == "nombre":
                orderby = "persona__nombres"
            enfermedades = enfermedades.order_by(orderby + sort)

        paginador = Paginator(enfermedades, limit)

        try:
            enfermedades = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            enfermedades = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            enfermedades = paginador.page(paginador.num_pages)

        return render(
            request,
            'agricola/enfermedad/datos.json',
            {
                'enfermedades': enfermedades,
                'total': paginador.count
            }
        )

class EnfermedadCreateView(AuditableMixin, CreateView):
    model = Enfermedad
    form_class = EnfermedadForm
    template_name = "agricola/enfermedad/formulario.html"

    def form_valid(self, form):
        super(EnfermedadCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(EnfermedadCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class EnfermedadUpdateView(AuditableMixin, UpdateView):
    model = Enfermedad
    form_class = EnfermedadForm
    template_name = "agricola/enfermedad/formulario.html"

    def dispatch(self, *args, **kwargs):
        self.enfermedad_id = kwargs['pk']
        return super(EnfermedadUpdateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        super(EnfermedadUpdateView, self).form_valid(form)
        enfermedad = Enfermedad.objects.get(pk=self.enfermedad_id)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(EnfermedadUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

    def get_context_data(self, **kwargs):
        context = super(EnfermedadUpdateView, self).get_context_data(**kwargs)
        context["enfermedad_id"] = self.enfermedad_id
        return context

class EnfermedadDeleteView(DeleteView):
    model = Enfermedad
    form_class = EnfermedadForm
    template_name = "agricola/enfermedad/eliminar.html"

    def dispatch(self, *args, **kwargs):
        self.enfermedad_id = kwargs['pk']
        return super(EnfermedadDeleteView, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def get_context_data(self, **kwargs):
        context = super(EnfermedadDeleteView, self).get_context_data(**kwargs)
        context["enfermedad_id"] = self.enfermedad_id
        return context