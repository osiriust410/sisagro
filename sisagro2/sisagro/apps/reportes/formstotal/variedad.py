# coding=utf-8
from django import forms
from apps.inicio.models import Caserio, CentroPoblado, Departamento, Provincia, Distrito
from apps.agricola.models import Variedad
from django_select2.forms import ModelSelect2Widget
from django_select2.forms import Select2Widget

class UbigeoForm(forms.Form):

	tipoCertificado = (
		('all', u'Todos'),
		('CO', u'Certificación Orgánica'),
		('CC', u'Certificación Comercio Justo'),
		('CR', u'Certificación Rinfort Alice')
	)

	tipocertificacion = forms.ChoiceField(
		label=u"Tipo Certificación",
		choices=tipoCertificado,
		widget=Select2Widget(
			attrs={'data-minimum-results-for-search': 'Infinity'}
		)
	)

	provincia = forms.ModelChoiceField(
		queryset=Provincia.objects.all().filter(departamento__codigo="06"),
		widget=ModelSelect2Widget(
			search_fields=['nombre__icontains'],
			queryset=Provincia.objects.all().filter(departamento__codigo="06").order_by("nombre")
		)
	)

	distrito = forms.ModelChoiceField(
		queryset=Distrito.objects.all(),
		widget=ModelSelect2Widget(
			search_fields=['nombre__icontains'],
			dependent_fields={'provincia': 'provincia'},
			max_results=500,
			queryset=Distrito.objects.order_by("nombre")
		)
	)

	centropoblado = forms.ModelChoiceField(
		queryset=CentroPoblado.objects.all(),
		widget=ModelSelect2Widget(
			search_fields=['nombre__icontains'],
			dependent_fields={'distrito': 'distrito'},
			max_results=500,
			queryset=CentroPoblado.objects.order_by("nombre")
		)
	)

	caserio = forms.ModelChoiceField(
		queryset=CentroPoblado.objects.all(),
		widget=ModelSelect2Widget(
			search_fields=['nombre__icontains'],
			dependent_fields={'centropoblado': 'centropoblado'},
			max_results=500,
			queryset=Caserio.objects.order_by("nombre")
		)
	)


	class Meta:
		fields = ['tipocertificacion',]

	def __init__(self, *args, **kwargs):
		super(UbigeoForm, self).__init__(*args, **kwargs)
		self.fields['centropoblado'].label = "Centro Poblado"
		self.fields['provincia'].required = False
		self.fields['distrito'].required = False
		self.fields['centropoblado'].required = False
		self.fields['caserio'].required = False
		# fields = ['dni', 'nombres', 'apepat', 'apemat', 'sexo', 'fechanacimiento']
