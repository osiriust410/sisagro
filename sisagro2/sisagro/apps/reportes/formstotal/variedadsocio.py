# coding=utf-8
from django import forms
# from apps.inicio.models import Caserio, CentroPoblado, Departamento, Provincia, Distrito
from apps.agricola.models import Socio
from django_select2.forms import ModelSelect2Widget
# from django_select2.forms import Select2Widget


# class widgetOpPdrc(ModelSelect2Widget):
#     search_fields=["codigo__icontains", "descripcion__icontains"]
#     queryset=Pdrc.objects.order_by("descripcion")
#     model=Pdrc
#     max_results=10

#     def label_from_instance(self, obj):
#         return force_text(obj.descripcion) + " (" + force_text(obj.periodo.nombre) + "-" + force_text(obj.ejeecon.nombre) + ")"

class VariedadSocioForm(forms.Form):

	socio = forms.ModelChoiceField(
		queryset=Socio.objects.all(),
		widget=ModelSelect2Widget(
			search_fields=['persona__apepat__icontains', 'persona__apemat__icontains', 'persona__nombres__icontains', 'persona__dni__icontains'],
			queryset=Socio.objects.all()
		)
	)

	def __init__(self, *args, **kwargs):
		super(VariedadSocioForm, self).__init__(*args, **kwargs)
		# self.fields['centropoblado'].label = "Centro Poblado"
		# self.fields['provincia'].required = False
		# self.fields['distrito'].required = False
		# self.fields['centropoblado'].required = False
		# self.fields['caserio'].required = False
		# fields = ['dni', 'nombres', 'apepat', 'apemat', 'sexo', 'fechanacimiento']
