# coding=utf-8
from django import forms
# from apps.inicio.models import Caserio, CentroPoblado, Departamento, Provincia, Distrito
# from apps.agricola.models import Variedad
# from django_select2.forms import ModelSelect2Widget
# from django_select2.forms import Select2Widget
from datetimewidget.widgets import DateWidget

class SocioForm(forms.Form):

	inicio = forms.DateField(
        label=u'Desde',
        required=True,
        widget=DateWidget(
            usel10n=True,
            bootstrap_version=3,
            attrs={'data-datepicker-type': "4", "data-provide": "datepicker-inline"},
            options={
                'autoclose': 'true',
                'clearBtn': 'false'
            },
        )
    )

	fin = forms.DateField(
        label=u'Hasta',
        required=True,
        widget=DateWidget(
            usel10n=True,
            bootstrap_version=3,
            attrs={'data-datepicker-type': "4", "data-provide": "datepicker-inline"},
            options={
                'autoclose': 'true',
                'clearBtn': 'false'
            },
        )
    )
	

	# class Meta:
	# 	fields = ['tipocertificacion',]

	def __init__(self, *args, **kwargs):
		super(SocioForm, self).__init__(*args, **kwargs)
		# self.fields['centropoblado'].label = "Centro Poblado"
		# self.fields['provincia'].required = False
		# self.fields['distrito'].required = False
		# self.fields['centropoblado'].required = False
		# self.fields['caserio'].required = False
		# fields = ['dni', 'nombres', 'apepat', 'apemat', 'sexo', 'fechanacimiento']
