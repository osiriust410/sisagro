# coding=utf-8
from django import forms
# from apps.inicio.models import Caserio, CentroPoblado, Departamento, Provincia, Distrito
# from apps.agricola.models import Variedad
# from django_select2.forms import ModelSelect2Widget
from ...inicio.funciones import ListaAnios
from django_select2.forms import Select2Widget

class PdrcObjEspecificoForm(forms.Form):

	meses = (
		('1', u'Enero'),
		('2', u'Febrero'),
		('3', u'Marzo'),
		('4', u'Abril'),
		('5', u'Mayo'),
		('6', u'Junio'),
		('7', u'Julio'),
		('8', u'Agosto'),
		('9', u'Septiembre'),
		('10', u'Octubre'),
		('11', u'Noviembre'),
		('12', u'Diciembte'),
	)

	anio = forms.ChoiceField(
        label=u"Año",
        choices=ListaAnios,
        # choices=sorted([(anio, anio) for anio in range(settings.APP_CONFIG["ANIO"], datetime.datetime.now().year+1)], key=lambda x: x.count, reverse=True),
        widget=Select2Widget(attrs={'data-minimum-results-for-search': 'Infinity'})
    )
    

	inicio = forms.ChoiceField(
		label=u"Desde",
		choices=meses,
		widget=Select2Widget(
			attrs={'data-minimum-results-for-search': 'Infinity'}
		)
	)


	fin = forms.ChoiceField(
		label=u"Hasta",
		choices=meses,
		widget=Select2Widget(
			attrs={'data-minimum-results-for-search': 'Infinity'}
		)
	)

	


	# class Meta:
	# 	fields = ['anio', 'inicio', 'fin']

	def __init__(self, *args, **kwargs):
		super(PdrcObjEspecificoForm, self).__init__(*args, **kwargs)
		# self.fields['centropoblado'].label = "Centro Poblado"
		# self.fields['provincia'].required = False
		# self.fields['distrito'].required = False
		# self.fields['centropoblado'].required = False
		# self.fields['caserio'].required = False
		# fields = ['dni', 'nombres', 'apepat', 'apemat', 'sexo', 'fechanacimiento']
