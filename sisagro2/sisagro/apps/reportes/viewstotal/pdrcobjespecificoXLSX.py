# -*- coding: utf-8 -*-
import xlwt
import time

from django.http import HttpResponse
from apps.planificacion.models import ObjetivoEspecifico, MetaFF
from apps.inicio.models import Mes
from datetime import datetime
from django.utils import formats
from decimal import Decimal

def ListaObjetivoEspecificoXLSX(request):

    anio = request.POST.get('anio', time.strftime("%Y"))
    inicio = request.POST.get('inicio', 1)
    fin = request.POST.get('fin', 12)

    obj_inicio = Mes.objects.get(numero=inicio)
    obj_fin = Mes.objects.get(numero=fin)

    metas = MetaFF.objects.all().filter(mes__numero__range=[inicio, fin], actividad__anio=anio).order_by('actividad__aexpop__accionestrategica__objetivoespecifico__id', 'actividad__id', 'mes__numero')

    # objetivoespecificos = enumerate(ObjetivoEspecifico.objects.all())

    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="ObjetivoEspecificoPdrc_' + str(formats.date_format(datetime.now(), "d-m-Y")) + '.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Lista')

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    
    #Encabezado
    # row_num = row_num + 1
    data = [
                ('DIRECCIÓN REGIONAL DE AGRICULATURA CAJAMARCA',),
                ('REPORTE DE OBJETIVOS POR MES',),
                ('',),
                ('PERIODO',),
                ('', 'AÑO', anio),
                ('', 'DESDE', obj_inicio.nombre),
                ('', 'HASTA', obj_fin.nombre),
            ]

    for tupla in data:
        for i, col_num in enumerate(tupla):
            ws.write(row_num, i, col_num, font_style)
        row_num = row_num + 1

    #Encabezados
    row_num = row_num + 2
    columns = [
                    'Item', 
                    'CADENA PRODUCTIVA', 
                    'AGENCIA AGRARIA', 
                    'OBJETIVOS ESTRATÉGICOS', 
                    'OBJETIVOS ESPECÍFICOS', 
                    'ACCIONES ESTRATÉGICAS', 
                    'ACTIVIDAD - Código', 
                    'ACTIVIDAD - Descripción', 
                    'MES', 
                    'INDICADOR', 
                    'UNIDAD MEDIDA',
                    "FÍSICO PROGRAMADO",
                    "FÍSICO EJECUTADO",
                    '% DE AVANCE FISICO',
                    '% DE AVANCE FINANCIERO',
                    '% DE AVANCE ANUAL X OBJET. ESP - Físico',
                    '% DE AVANCE ANUAL X OBJET. ESP - Financiero',
                    "AGENCIA AGRARIA",
                ]

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    #Llenando datos
    font_style = xlwt.XFStyle()
    for i, meta in enumerate(metas):
        row_num = row_num + 1

        columns = [
                        i + 1, 
                        "%s" % meta.actividad.cadenaproductiva.nombre,
                        "%s" % meta.actividad.area.nombre,
                        # "%s" % (meta.actividad.aexpop.accionestrategica.objetivoespecifico.objetivopdrc.pdrc.descripcion),
                        # "%s" % (meta.actividad.aexpop.accionestrategica.objetivoespecifico.objetivopdrc.codigo),
                        "%s" % (meta.actividad.aexpop.accionestrategica.objetivoespecifico.objetivopdrc.descripcion),
                        # "%s" % (meta.actividad.aexpop.accionestrategica.objetivoespecifico.codigo),
                        "%s" % (meta.actividad.aexpop.accionestrategica.objetivoespecifico.descripcion),
                        # "%s" % (meta.actividad.aexpop.accionestrategica.objetivoespecifico.estado),
                        # "%s" % (meta.actividad.aexpop.accionestrategica.objetivoespecifico.anio),
                        "%s" % meta.actividad.aexpop.accionestrategica.nombre,
                        "%s" % meta.actividad.actividadcatalogo.codigo,
                        "%s" % meta.actividad.actividadcatalogo.nombre,
                        "%s" % meta.mes.nombre,
                        "%s" % meta.actividad.indicador.nombre,
                        "%s" % meta.actividad.unidadmedida.abreviatura,
                        ("%s" % format(meta.metafisica_programada, '.2f') if meta.metafisica_programada != None else ''),
                        ("%s" % format(meta.metafisica_ejecutada, '.2f') if meta.metafisica_ejecutada != None else ''),
                        ("%s" % format(Decimal(meta.metafisica_ejecutada)/Decimal(meta.metafisica_programada)*100, '.2f') if meta.metafisica_programada != None and meta.metafisica_ejecutada != None else ''),
                        "De donde se saca la data",
                        "¿Cómo se calcula?",
                        "¿Cómo se calcula?",
                        "%s" % meta.actividad.area.nombre,
                    ]

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    ws.write(row_num + 3, 0, 'Reporte generado el ' + str(formats.date_format(datetime.now(), "d-m-Y h:i:s a")), font_style)

    wb.save(response)
    return response