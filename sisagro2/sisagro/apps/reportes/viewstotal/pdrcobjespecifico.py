from django.shortcuts import render
from ..formstotal.pdrcobjespecifico import PdrcObjEspecificoForm

# from .models import Question


def index(request):
	form = PdrcObjEspecificoForm()
	return render(request, 'reportes/pdrcobjespecifico/inicio.html', {'form':form } )