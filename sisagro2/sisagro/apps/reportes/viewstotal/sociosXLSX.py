# -*- coding: utf-8 -*-
import xlwt

from django.http import HttpResponse
from apps.agricola.models import Socio
from datetime import datetime
from django.utils import formats
from apps.inicio.funciones import CalcularEdad

def ListaSociosXLS(request):

    inicio = request.POST.get('inicio', '01-01-2000')
    fin = request.POST.get('fin', '31-12-2050')

    inicio1 = datetime.strptime(inicio, '%d/%m/%Y').strftime('%Y-%m-%d')
    fin1 = datetime.strptime(fin, '%d/%m/%Y').strftime('%Y-%m-%d')

    if inicio and fin:
        #Socios que han ingresado
        socios_ingresos  = Socio.objects.all().filter(ingreso__range=[inicio1, fin1]).order_by('-id')
        socios_salidas = Socio.objects.all().filter(salida__range=[inicio1, fin1]).order_by('-id')


        socios = enumerate(socios_ingresos)

        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename="Socios_' + str(formats.date_format(datetime.now(), "d-m-Y")) + '.xls"'

        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('Lista')

        # Sheet header, first row
        row_num = 0

        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        style = xlwt.XFStyle()
        pattern = xlwt.Pattern()
        pattern.pattern = xlwt.Pattern.SOLID_PATTERN
        pattern.pattern_fore_colour = xlwt.Style.colour_map['bright_green']
        style.pattern = pattern
        
        #Encabezado
        row_num = row_num + 1
        columns = ['Dirección Regional de Agricultura Cajamarca',]
        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        row_num = row_num + 1
        columns = ['SisDRAC',]
        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        row_num = row_num + 1
        columns = ['Reporte de asociados que Ingresaron y Salieron',]
        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)


        row_num = row_num + 1
        columns = ['Desde', inicio, 'Hasta', fin]
        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        #Encabezados
        row_num = row_num + 3
        columns = [
                    'Item', 
                    'Nombre Completo Socio', 
                    'DNI', 
                    'Edad Actual Años', 
                    'Fecha de Ingreso', 
                    'Fecha de Salida', 
                    'Organización/Caserío/Base',
                    'Unidad Organizacional', 
                    'Provincia', 
                    'Distrito', 
                    'Centro Poblado', 
                    'Estado'
        ]

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        #Llenando datos
        font_style = xlwt.XFStyle()
        font_style.font.bold = False
        for i, socio in socios:
            row_num = row_num + 1

            columns = [
                            i + 1, 
                            "%s" % (socio.persona.nombreCompleto()),
                            "%s" % (socio.persona.dni),
                            "%s" % CalcularEdad(socio.persona.fechanacimiento, socio.salida),
                            "%s" % (formats.date_format(socio.ingreso, "d-m-Y") if socio.ingreso != None else '') ,
                            "%s" % (formats.date_format(socio.salida, "d-m-Y") if socio.salida != None else '') ,
                            "%s/%s/%s" % (socio.base.organizacion, socio.base.caserio, socio.base),
                            '%s' % (socio.base.tareacpcu_set.first().trabajador.area if socio.base.tareacpcu_set.first() != None else '') ,
                            "%s" % (socio.base.caserio.centropoblado.distrito.provincia.nombre),
                            "%s" % (socio.base.caserio.centropoblado.distrito.nombre),
                            "%s" % (socio.base.caserio.centropoblado.nombre),
                            "%s" % ('Activo' if socio.estado else 'Inactivo'),
                        ]

            for col_num in range(len(columns)):
                ws.write(row_num, col_num, columns[col_num], font_style)

        font_style = xlwt.XFStyle()
        font_style.font.bold = True
        columns = ['Total', socios_ingresos.count()]
        for col_num in range(len(columns)):
            ws.write(row_num + 1, col_num, columns[col_num], font_style)



        #Matriz para totales
        columns = ['Total Ingresos', socios_ingresos.count()]
        for col_num in range(len(columns)):
            ws.write(row_num + 3, col_num, columns[col_num], font_style)


        columns = ['Total Salidas', socios_salidas.count()]
        for col_num in range(len(columns)):
            ws.write(row_num + 4, col_num, columns[col_num], font_style)


        style = xlwt.XFStyle()
        pattern = xlwt.Pattern()
        pattern.pattern = xlwt.Pattern.SOLID_PATTERN

        if socios_ingresos.count() - socios_salidas.count() >= 0:
            pattern.pattern_fore_colour = xlwt.Style.colour_map['bright_green']
        else:
            pattern.pattern_fore_colour = xlwt.Style.colour_map['red']
        style.pattern = pattern

        columns = ['Balance', socios_ingresos.count() - socios_salidas.count()]
        for col_num in range(len(columns)):
            ws.write(row_num + 5, col_num, columns[col_num], style)


        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()
        font_style.font.bold = False

        ws.write(row_num + 7, 0, 'Reporte generado el ' + str(formats.date_format(datetime.now(), "d-m-Y h:i:s a")), font_style)

        wb.save(response)
        return response

    else:
        return HttpResponse("No se han ingresado fechas válidas")