from django.shortcuts import render
from ..formstotal.variedad import UbigeoForm
from ..formstotal.variedadsocio import VariedadSocioForm

# from .models import Question


def index(request):
	form = UbigeoForm()
	return render(request, 'reportes/variedad/inicio.html', {'form':form } )



def VariedadSocio(request):
	form = VariedadSocioForm()
	return render(request, 'reportes/variedadsocio/inicio.html', {'form':form } )