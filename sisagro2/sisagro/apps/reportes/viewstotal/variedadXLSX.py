# -*- coding: utf-8 -*-
import xlwt

from django.http import HttpResponse
from apps.agricola.models import Variedad
from datetime import datetime
from django.utils import formats
from apps.inicio.models import Provincia, Caserio, Distrito, CentroPoblado
from decimal import Decimal


def ListaVariedadesXLS(request):

    tipocertificacion = request.POST.get('tipocertificacion', 'all')
    provincia = request.POST.get('provincia', 0)
    distrito = request.POST.get('distrito', 0)
    centropoblado = request.POST.get('centropoblado', 0)
    caserio = request.POST.get('caserio', 0)

    if tipocertificacion == 'all':
        text_tipocertificacion = 'TODOS'
        variedades = Variedad.objects.all()
    else:

        try:
            variedades = Variedad.objects.all().filter(tipocertificado=tipocertificacion)
        except Variedad.DoesNotExist:
            variedades = None

        if variedades.count() > 0:
            obj_tipocertificacion = variedades.first()
            text_tipocertificacion = obj_tipocertificacion.TipoCertificado()
        else:
            text_tipocertificacion = tipocertificacion

    if provincia == 0:
        text_provincia = 'TODAS'
        text_distrito = 'TODOS'
        text_centropoblado = 'TODOS'
        text_caserio = 'TODOS'
        variedades = variedades

    elif provincia > 0 and distrito == 0:
        try:
            obj_provincia = Provincia.objects.get(pk=provincia)
            text_provincia = obj_provincia.nombre
        except Provincia.DoesNotExist:
            text_provincia = 'TODAS'

        text_distrito = 'TODOS'
        text_centropoblado = 'TODOS'
        text_caserio = 'TODOS'
        variedades = variedades.filter(socio__base__caserio__centropoblado__distrito__provincia__id=provincia)
        
    elif provincia > 0 and distrito > 0 and centropoblado == 0:
        try:
            obj_provincia = Provincia.objects.get(pk=provincia)
            text_provincia = obj_provincia.nombre
        except Provincia.DoesNotExist:
            text_provincia = 'TODAS'

        try:
            obj_distrito = Distrito.objects.get(pk=distrito)
            text_distrito = obj_distrito.nombre
        except Distrito.DoesNotExist:
            text_distrito = 'TODOS'

        text_centropoblado = 'TODOS'
        text_caserio = 'TODOS'

        variedades = variedades.filter(
                                        socio__base__caserio__centropoblado__distrito__provincia__id=provincia, 
                                        socio__base__caserio__centropoblado__distrito__id=distrito
                                        )

    elif provincia > 0 and distrito > 0 and centropoblado > 0 and caserio ==0 :
        try:
            obj_provincia = Provincia.objects.get(pk=provincia)
            text_provincia = obj_provincia.nombre
        except Provincia.DoesNotExist:
            text_provincia = 'TODAS'

        try:
            obj_distrito = Distrito.objects.get(pk=distrito)
            text_distrito = obj_distrito.nombre
        except Distrito.DoesNotExist:
            text_distrito = 'TODOS'

        try:
            obj_centropoblado = CentroPoblado.objects.get(pk=centropoblado)
            text_centropoblado = obj_centropoblado.nombre
        except CentroPoblado.DoesNotExist:
            text_centropoblado = 'TODOS'

        text_caserio = 'TODOS'

        
        variedades = variedades.filter(
                                        socio__base__caserio__centropoblado__distrito__provincia__id=provincia, 
                                        socio__base__caserio__centropoblado__distrito__id=distrito,
                                        socio__base__caserio__centropoblado__id=centropoblado
                                        )
    else:
        try:
            obj_provincia = Provincia.objects.get(pk=provincia)
            text_provincia = obj_provincia.nombre
        except Provincia.DoesNotExist:
            text_provincia = 'TODAS'

        try:
            obj_distrito = Distrito.objects.get(pk=distrito)
            text_distrito = obj_distrito.nombre
        except Distrito.DoesNotExist:
            text_distrito = 'TODOS'

        try:
            obj_centropoblado = CentroPoblado.objects.get(pk=centropoblado)
            text_centropoblado = obj_centropoblado.nombre
        except CentroPoblado.DoesNotExist:
            text_centropoblado = 'TODOS'

        try:
            obj_caserio = Caserio.objects.get(pk=caserio)
            text_caserio = obj_caserio.nombre
        except Caserio.DoesNotExist:
            text_caserio = 'TODOS'

        variedades = variedades.filter(
                                        socio__base__caserio__centropoblado__distrito__provincia__id=provincia, 
                                        socio__base__caserio__centropoblado__distrito__id=distrito,
                                        socio__base__caserio__centropoblado__id=centropoblado,
                                        socio__base__caserio__id=caserio
                                        )


    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="Variedades_' + str(formats.date_format(datetime.now(), "d-m-Y")) + '.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Lista')

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    data = [
                ('DIRECCIÓN REGIONAL DE AGRICULATURA CAJAMARCA',),
                ('REPORTE DE VARIEDADES',),
                ('',),
                ('TIPO CERTIFICADO:', text_tipocertificacion),
                ('PROVINCIA:', '%s' % text_provincia,),
                ('DISTRITO:', '%s' % text_distrito,),
                ('CENTRO POBLADO:', '%s' % text_centropoblado,),
                ('CASERIO:', '%s' % text_caserio,),
            ]

    for tupla in data:
        for i, col_num in enumerate(tupla):
            ws.write(row_num, i, col_num, font_style)
        row_num = row_num + 1

    #Encabezados
    row_num = row_num + 3
    columns = [
                'Item', 
                'Nombre Socio', 
                'DNI Socio', 
                "Nombre Variedad",
                "Área Total (Sembrada y no )",
                "Área Sembrada Secano (Ha)",
                "Área Sembrada Bajo Riego (Ha)",
                "Área Total Sembrada (Ha)",
                "Volúmen Producción (kg)",
                "Rendimiento Kg/Ha",
                "Precio Chacra (S/.- Kg )",
                "Precio Mercado (S/.- Kg )",
                "Tipo Certificación",
                "Área Certificadas (Ha)",
                "Área Sin Certificar  (Ha)",
                "Título Propiedad",
                "Plagas",
                "Organización/Caserio/Base",
                "Organización",
                "Provincia",
                "Distrito",
                "Centro Poblado",
                "Caserio",
                "Base",
                'Unidad Organizacional',
                ]

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    #Llenando datos
    font_style = xlwt.XFStyle()


    total = 0
    area_total = 0
    area_secano = 0
    area_bajoriego = 0
    area_certificada = 0
    area_sincertificar = 0

    for i, variedad in enumerate(variedades):
        row_num = row_num + 1

        columns = [
                        i + 1, 
                        '%s' % (variedad.socio),
                        '%s' % (variedad.socio.persona.dni),
                        '%s' % (variedad.variedadcatalogo.nombre),
                        format(variedad.area,'.2f'),
                        format(variedad.areasecano,'.2f'),
                        format(variedad.areabajoriego, '.2f'),
                        format(Decimal(variedad.areasecano) + Decimal(variedad.areabajoriego), '.2f'),
                        format(variedad.volumenproduccion,'.2f'),
                        format(variedad.rendimiento,'.2f'),
                        format(variedad.preciochacra,'.2f'),
                        format(variedad.preciomercado,'.2f'),
                        '%s' % (variedad.TipoCertificado()),
                        format(variedad.certificadas,'.2f'),
                        format(variedad.sincertificar,'.2f'),
                        '%s' % (variedad.Titulo()),
                        '%s' % (variedad.plaga.nombre),
                        '%s/%s/%s' % (variedad.socio.base.organizacion.nombre, variedad.socio.base.caserio.nombre, variedad.socio.base.nombre),
                        '%s' % (variedad.socio.base.organizacion),
                        "%s" % (variedad.socio.base.caserio.centropoblado.distrito.provincia.nombre),
                        "%s" % (variedad.socio.base.caserio.centropoblado.distrito.nombre),
                        "%s" % (variedad.socio.base.caserio.centropoblado.nombre),
                        "%s" % (variedad.socio.base.caserio.nombre),
                        '%s' % (variedad.socio.base),
                        "¿De dónde se obtiene esta información????? - Estamos en Variedad",
                    ]

        total = i + 1
        area_total = area_total + variedad.area
        area_secano = area_secano + variedad.areasecano
        area_bajoriego = area_bajoriego + variedad.areabajoriego
        area_certificada = area_certificada + variedad.certificadas
        area_sincertificar = area_sincertificar + variedad.sincertificar

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)


    data = [
            (
                'Total',
                total,
                '',
                total,
                area_total,
                area_secano,
                area_bajoriego,
                '',
                '',
                '',
                '',
                '',
                '',
                area_certificada,
                area_sincertificar,
            ),
        ]

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    for tupla in data:
        row_num = row_num + 1
        for i, col_num in enumerate(tupla):
            ws.write(row_num, i, col_num, font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    ws.write(row_num + 4, 0, 'Reporte generado el ' + str(formats.date_format(datetime.now(), "d-m-Y h:i:s a")), font_style)

    wb.save(response)
    return response