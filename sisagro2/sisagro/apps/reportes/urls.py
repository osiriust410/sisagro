from django.conf.urls import url
# from .viewstotal.variedadsocioXLSX import *
from .viewstotal import variedad
from .viewstotal import pdrcobjespecifico
from .viewstotal import organizacion
from .viewstotal import socios
from .viewstotal import variedadXLSX
from .viewstotal import variedadsocioXLSX
from .viewstotal import pdrcobjespecificoXLSX
from .viewstotal import organizacionXLSX
from .viewstotal import sociosXLSX



urlpatterns = [
	
    # VARIEDAD
    url(r'^variedadtipocertificacion/$', variedad.index, name='variedadtipocertificacion_inicio'),
    url(r'^variedadtipocertificacion/xlsx/$', variedadXLSX.ListaVariedadesXLS, name='variedadtipocertificacion_xlsxl'),

    url(r'^variedad/socio/$', variedad.VariedadSocio, name='variedadsocio_inicio'),
    url(r'^variedad/socio/xlsx/$', variedadsocioXLSX.ListaVariedadSocioXLS, name='variedadsocio_xlsx'),

    url(r'^pdrcobjespecifico/$', pdrcobjespecifico.index, name='pdrcobjespecifico_inicio'),
    url(r'^pdrcobjespecifico/xlsx/$', pdrcobjespecificoXLSX.ListaObjetivoEspecificoXLSX, name='pdrcobjespecifico_xlsx'),

    url(r'^organizacion/$', organizacion.index, name='organizacion_inicio'),
    url(r'^organizacion/xlsx/$', organizacionXLSX.ListaOrganizacionesXLSX, name='organizacion_xlsx'),

    url(r'^socios/$', socios.index, name='socios_inicio'),
    url(r'^socios/xlsx/$', sociosXLSX.ListaSociosXLS, name='socios_xlsx'),
]


