/*
Navicat MySQL Data Transfer

Source Server         : Servidor MySQL Xampp
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : sisagro

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2017-07-30 12:39:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for administracion_acceso
-- ----------------------------
DROP TABLE IF EXISTS `administracion_acceso`;
CREATE TABLE `administracion_acceso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `menu_id` int(11) NOT NULL,
  `persona_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `administracion_acceso_creador_id_da6e439f_fk_auth_user_id` (`creador_id`),
  KEY `administracion_acceso_editor_id_e7469d94_fk_auth_user_id` (`editor_id`),
  KEY `administracion_acceso_menu_id_a00f5992_fk_administracion_menu_id` (`menu_id`),
  KEY `administracion_acces_persona_id_d8981518_fk_administr` (`persona_id`),
  CONSTRAINT `administracion_acces_persona_id_d8981518_fk_administr` FOREIGN KEY (`persona_id`) REFERENCES `administracion_persona` (`id`),
  CONSTRAINT `administracion_acceso_creador_id_da6e439f_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `administracion_acceso_editor_id_e7469d94_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `administracion_acceso_menu_id_a00f5992_fk_administracion_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `administracion_menu` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of administracion_acceso
-- ----------------------------
INSERT INTO `administracion_acceso` VALUES ('3', '2017-07-24 22:38:22.504914', '2017-07-24 22:38:22.504954', '1', null, '34', '6');
INSERT INTO `administracion_acceso` VALUES ('4', '2017-07-24 22:38:22.514129', '2017-07-24 22:38:22.514164', '1', null, '24', '6');
INSERT INTO `administracion_acceso` VALUES ('5', '2017-07-24 22:38:22.522480', '2017-07-24 22:38:22.522514', '1', null, '31', '6');
INSERT INTO `administracion_acceso` VALUES ('6', '2017-07-24 22:38:22.534101', '2017-07-24 22:38:22.534141', '1', null, '23', '6');
INSERT INTO `administracion_acceso` VALUES ('7', '2017-07-24 22:38:22.542868', '2017-07-24 22:38:22.542904', '1', null, '20', '6');
INSERT INTO `administracion_acceso` VALUES ('8', '2017-07-24 22:38:22.554177', '2017-07-24 22:38:22.554212', '1', null, '21', '6');
INSERT INTO `administracion_acceso` VALUES ('9', '2017-07-24 22:38:22.564638', '2017-07-24 22:38:22.564707', '1', null, '14', '6');
INSERT INTO `administracion_acceso` VALUES ('10', '2017-07-24 22:38:22.573488', '2017-07-24 22:38:22.573523', '1', null, '6', '6');
INSERT INTO `administracion_acceso` VALUES ('11', '2017-07-24 22:38:22.584943', '2017-07-24 22:38:22.584977', '1', null, '46', '6');
INSERT INTO `administracion_acceso` VALUES ('12', '2017-07-24 22:38:22.603360', '2017-07-24 22:38:22.603396', '1', null, '42', '6');

-- ----------------------------
-- Table structure for administracion_area
-- ----------------------------
DROP TABLE IF EXISTS `administracion_area`;
CREATE TABLE `administracion_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `abreviatura` varchar(10) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `esagencia` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  UNIQUE KEY `abreviatura` (`abreviatura`),
  KEY `administracion_area_creador_id_7f36441c_fk_auth_user_id` (`creador_id`),
  KEY `administracion_area_editor_id_503ec25a_fk_auth_user_id` (`editor_id`),
  CONSTRAINT `administracion_area_creador_id_7f36441c_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `administracion_area_editor_id_503ec25a_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of administracion_area
-- ----------------------------
INSERT INTO `administracion_area` VALUES ('1', '2017-06-16 12:11:30.000000', '2017-07-25 16:26:10.788161', 'Dirección  de Competividad Agraria', 'DCA', '1', '1', '1', '0');
INSERT INTO `administracion_area` VALUES ('2', '2017-07-02 18:54:49.000000', '2017-07-25 16:27:24.140362', 'Dirección de Estadística e Informática', 'DEI', '1', '2', '1', '0');
INSERT INTO `administracion_area` VALUES ('3', '2017-07-22 23:11:58.643764', '2017-07-25 17:06:43.057069', 'Dirección de Titulación de Tierras y Catastro Rural', 'DPC', '1', '1', '1', '0');
INSERT INTO `administracion_area` VALUES ('5', '2017-07-22 23:12:35.143855', '2017-07-25 17:06:51.460554', 'Direccion de Recursos Forestales y de Fauna Silvestre', 'DREFS', '1', '1', '1', '0');
INSERT INTO `administracion_area` VALUES ('6', '2017-07-24 22:32:50.915109', '2017-07-25 17:14:36.808651', 'Agencia Agraria Chota', 'AACH', '1', '1', '1', '1');
INSERT INTO `administracion_area` VALUES ('7', '2017-07-24 22:33:05.869532', '2017-07-25 17:14:32.130986', 'Agencia Agraria Cajabamba', 'AAC', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for administracion_funcioncargo
-- ----------------------------
DROP TABLE IF EXISTS `administracion_funcioncargo`;
CREATE TABLE `administracion_funcioncargo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `administracion_funcioncargo_creador_id_d124d4b3_fk_auth_user_id` (`creador_id`),
  KEY `administracion_funcioncargo_editor_id_24fc3283_fk_auth_user_id` (`editor_id`),
  CONSTRAINT `administracion_funcioncargo_creador_id_d124d4b3_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `administracion_funcioncargo_editor_id_24fc3283_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of administracion_funcioncargo
-- ----------------------------
INSERT INTO `administracion_funcioncargo` VALUES ('1', '2017-06-16 12:11:59.000000', '2017-06-16 12:11:59.000000', 'Administrador 1', '3', null);
INSERT INTO `administracion_funcioncargo` VALUES ('2', '2017-06-16 12:12:01.000000', '2017-06-16 12:12:01.000000', 'Administrador 2', '3', null);
INSERT INTO `administracion_funcioncargo` VALUES ('3', '2017-07-22 23:13:11.350985', '2017-07-22 23:13:11.351021', 'Nuevo Función Cargo', '1', null);
INSERT INTO `administracion_funcioncargo` VALUES ('4', '2017-07-22 23:13:29.122718', '2017-07-22 23:13:29.122754', 'Other Función/Cargo', '1', null);
INSERT INTO `administracion_funcioncargo` VALUES ('5', '2017-07-24 22:33:18.823817', '2017-07-24 22:33:18.823856', 'Nuevo cargo', '1', null);
INSERT INTO `administracion_funcioncargo` VALUES ('6', '2017-07-24 22:33:25.528043', '2017-07-24 22:33:31.963810', 'Otro cargo 2', '1', '1');

-- ----------------------------
-- Table structure for administracion_menu
-- ----------------------------
DROP TABLE IF EXISTS `administracion_menu`;
CREATE TABLE `administracion_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `ruta` varchar(100) DEFAULT NULL,
  `orden` int(11) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `padre_id` int(11) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL,
  `separador` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `administracion_menu_creador_id_a6a8455e_fk_auth_user_id` (`creador_id`),
  KEY `administracion_menu_editor_id_0b506b05_fk_auth_user_id` (`editor_id`),
  KEY `administracion_menu_padre_id_cc62b731_fk_administracion_menu_id` (`padre_id`),
  CONSTRAINT `administracion_menu_creador_id_a6a8455e_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `administracion_menu_editor_id_0b506b05_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `administracion_menu_padre_id_cc62b731_fk_administracion_menu_id` FOREIGN KEY (`padre_id`) REFERENCES `administracion_menu` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of administracion_menu
-- ----------------------------
INSERT INTO `administracion_menu` VALUES ('1', '2017-06-16 08:43:08.000000', '2017-06-16 08:43:08.000000', 'Maestras', null, null, '1', '1', null, null, '1', '0');
INSERT INTO `administracion_menu` VALUES ('2', '2017-06-16 08:43:29.000000', '2017-06-16 08:43:29.000000', 'Unidades de Medida', null, 'inicio:unidadmedida_inicio', '1', '1', null, '1', '1', '0');
INSERT INTO `administracion_menu` VALUES ('3', '2017-06-16 08:43:53.000000', '2017-07-30 16:07:04.950000', 'Meses', null, 'inicio:mes_inicio', '-1', '1', null, '1', '0', '0');
INSERT INTO `administracion_menu` VALUES ('4', '2017-06-16 08:44:18.000000', '2017-07-30 16:07:04.884000', 'Departamentos', null, null, '-1', '1', null, '1', '0', '0');
INSERT INTO `administracion_menu` VALUES ('5', '2017-06-16 08:46:15.000000', '2017-07-30 16:07:04.817000', 'Provincias', null, null, '-1', '1', null, '1', '0', '0');
INSERT INTO `administracion_menu` VALUES ('6', '2017-06-16 08:46:34.000000', '2017-07-30 16:07:04.751000', 'Distritos', null, null, '-1', '1', null, '1', '0', '0');
INSERT INTO `administracion_menu` VALUES ('7', '2017-06-16 08:47:01.000000', '2017-07-30 16:07:04.674000', 'Centros Poblados', null, 'inicio:centropoblado_inicio', '6', '1', null, '1', '1', '0');
INSERT INTO `administracion_menu` VALUES ('8', '2017-06-16 08:47:27.000000', '2017-07-30 16:07:04.583000', 'Caseríos', null, 'inicio:caserio_inicio', '7', '1', null, '1', '1', '0');
INSERT INTO `administracion_menu` VALUES ('9', '2017-06-16 08:47:47.000000', '2017-07-30 16:07:04.506000', 'Profesiones', null, 'inicio:profesion_inicio', '3', '1', null, '1', '1', '0');
INSERT INTO `administracion_menu` VALUES ('10', '2017-06-16 08:48:12.000000', '2017-07-30 16:07:04.401000', 'Especialidades', null, 'inicio:especialidad_inicio', '4', '1', null, '1', '1', '0');
INSERT INTO `administracion_menu` VALUES ('11', '2017-06-16 10:50:05.000000', '2017-07-27 15:51:06.575000', 'Administración', null, null, '3', '1', null, null, '1', '0');
INSERT INTO `administracion_menu` VALUES ('12', '2017-06-16 10:50:34.000000', '2017-06-16 10:50:34.000000', 'Funciones/Cargos', null, 'administracion:funcioncargo_inicio', '2', '1', null, '11', '1', '0');
INSERT INTO `administracion_menu` VALUES ('13', '2017-06-21 18:11:23.000000', '2017-07-27 15:51:06.966000', 'Agricola', null, null, '8', '1', null, null, '1', '0');
INSERT INTO `administracion_menu` VALUES ('14', '2017-06-21 18:11:50.000000', '2017-07-24 05:09:27.694426', 'Socios', null, 'agricola:socio_inicio', '2', '1', null, '13', '1', '0');
INSERT INTO `administracion_menu` VALUES ('15', '2017-06-21 18:59:58.000000', '2017-07-30 16:07:04.031000', 'Tipo de Organización', null, 'inicio:tipoorganizacion_inicio', '5', '1', null, '1', '1', '0');
INSERT INTO `administracion_menu` VALUES ('16', '2017-06-21 19:30:12.000000', '2017-07-27 15:51:06.849000', 'Planificación', null, null, '5', '1', null, null, '1', '0');
INSERT INTO `administracion_menu` VALUES ('17', '2017-06-21 19:30:41.000000', '2017-07-27 16:11:35.563000', 'Cadenas Productivas', null, 'planificacion:cadenaproductiva_inicio', '2', '1', null, '52', '1', '0');
INSERT INTO `administracion_menu` VALUES ('18', '2017-06-22 08:15:03.000000', '2017-07-24 04:33:57.747045', 'Periodos', null, 'planificacion:periodo_inicio', '1', '1', null, '47', '1', '0');
INSERT INTO `administracion_menu` VALUES ('19', '2017-06-22 08:16:51.000000', '2017-07-27 15:41:50.553000', 'Grupos Funcionales', null, 'planificacion:grupofuncional_inicio', '3', '1', null, '16', '1', '0');
INSERT INTO `administracion_menu` VALUES ('20', '2017-06-22 08:17:50.000000', '2017-07-24 05:03:04.155077', 'Indicadores', null, 'planificacion:indicador_inicio', '2', '1', null, '1', '1', '0');
INSERT INTO `administracion_menu` VALUES ('21', '2017-06-22 08:59:26.000000', '2017-07-24 04:34:49.264243', 'PDRC Cajamarca', 'Plan de Desarrollo Regional Concertado', 'planificacion:pdrc_inicio', '3', '1', null, '47', '1', '0');
INSERT INTO `administracion_menu` VALUES ('22', '2017-06-22 09:00:26.000000', '2017-07-27 15:41:50.491000', 'Función', null, 'planificacion:funcion_inicio', '2', '1', null, '16', '1', '0');
INSERT INTO `administracion_menu` VALUES ('23', '2017-06-22 09:01:16.000000', '2017-06-22 09:01:16.000000', 'Areas', null, 'administracion:area_inicio', '1', '1', null, '11', '1', '0');
INSERT INTO `administracion_menu` VALUES ('24', '2017-06-22 15:11:57.000000', '2017-07-27 15:41:50.402000', 'División Funcional', null, 'planificacion:divisionfuncional_inicio', '1', '1', null, '16', '1', '0');
INSERT INTO `administracion_menu` VALUES ('25', '2017-06-22 15:11:57.000000', '2017-07-24 05:09:27.710514', 'Plagas', null, 'agricola:plaga_inicio', '4', '1', null, '13', '1', '0');
INSERT INTO `administracion_menu` VALUES ('26', '2017-06-22 17:01:08.000000', '2017-07-24 04:34:12.353247', 'Ejes', null, 'planificacion:ejeecon_inicio', '2', '1', null, '47', '1', '0');
INSERT INTO `administracion_menu` VALUES ('27', '2017-06-26 16:03:13.000000', '2017-06-26 16:03:18.000000', 'Variedades', null, 'agricola:variedad_inicio', '3', '1', null, '13', '1', '0');
INSERT INTO `administracion_menu` VALUES ('28', '2017-06-27 09:28:39.000000', '2017-07-25 14:32:03.390059', 'Objetivos Generales', null, 'planificacion:pdrcobjetivo_inicio', '4', '1', null, '47', '1', '0');
INSERT INTO `administracion_menu` VALUES ('29', '2017-06-28 17:10:41.000000', '2017-06-28 17:10:44.000000', 'Usuarios', null, 'administracion:usuario_inicio', '5', '1', null, '11', '1', '0');
INSERT INTO `administracion_menu` VALUES ('30', '2017-06-28 08:43:21.000000', '2017-07-30 16:24:39.732000', 'Programación Física Financiera', null, 'planificacion:metaffprogramado_inicio', '6', '1', null, '16', '1', '1');
INSERT INTO `administracion_menu` VALUES ('31', '2017-06-28 09:38:00.000000', '2017-07-27 16:11:35.638000', 'Actividades', null, 'planificacion:actividad_inicio', '3', '1', null, '52', '1', '0');
INSERT INTO `administracion_menu` VALUES ('32', '2017-06-28 09:38:44.000000', '2017-07-24 04:37:26.662411', 'Acciones Estratégicas', null, 'planificacion:accionestrategica_inicio', '6', '1', null, '47', '1', '0');
INSERT INTO `administracion_menu` VALUES ('34', '2017-06-28 17:09:51.000000', '2017-06-28 17:09:56.000000', 'Trabajadores', null, 'administracion:trabajador_inicio', '4', '1', null, '11', '1', '0');
INSERT INTO `administracion_menu` VALUES ('35', '2017-06-28 17:11:07.000000', '2017-07-24 21:15:11.590265', 'Personas', null, 'administracion:persona_inicio', '3', '1', null, '11', '1', '0');
INSERT INTO `administracion_menu` VALUES ('36', '2017-07-12 03:26:04.239000', '2017-07-24 05:09:27.730039', 'Bases', null, 'agricola:base_inicio', '1', '1', null, '13', '1', '0');
INSERT INTO `administracion_menu` VALUES ('37', '2017-07-12 16:46:48.646000', '2017-07-12 16:46:48.647000', 'Especies', null, 'agricola:especie_inicio', '5', '1', null, '13', '1', '0');
INSERT INTO `administracion_menu` VALUES ('38', '2017-07-12 17:45:38.751000', '2017-07-15 13:40:25.442000', 'Razas', null, 'agricola:raza_inicio', '6', '1', null, '13', '1', '0');
INSERT INTO `administracion_menu` VALUES ('39', '2017-07-15 13:40:51.240000', '2017-07-24 05:15:27.480654', 'Enfermedades', null, 'agricola:enfermedad_inicio', '8', '1', null, '13', '1', '0');
INSERT INTO `administracion_menu` VALUES ('40', '2017-07-20 14:57:34.881000', '2017-07-20 14:57:34.881000', 'Organizaciones', null, 'administracion:organizacion_inicio', '8', '1', null, '11', '1', '0');
INSERT INTO `administracion_menu` VALUES ('41', '2017-07-22 15:41:04.674812', '2017-07-25 14:33:03.824270', 'Objetivos Específicos', null, 'planificacion:objetivoespecifico_inicio', '5', '1', null, '47', '1', '0');
INSERT INTO `administracion_menu` VALUES ('42', '2017-07-22 22:37:38.813309', '2017-07-24 05:15:27.508149', 'Razas por Socio', null, 'agricola:razasocio_inicio', '7', '1', null, '13', '1', '0');
INSERT INTO `administracion_menu` VALUES ('43', '2017-07-24 03:58:39.968027', '2017-07-27 16:01:13.395000', 'Programas Presupuestales', null, 'planificacion:programapresupuestal_inicio', '4', '1', null, '16', '1', '0');
INSERT INTO `administracion_menu` VALUES ('44', '2017-07-24 04:03:38.446272', '2017-07-27 16:00:02.872000', 'Productos', null, 'planificacion:producto_inicio', '1', '1', null, '52', '1', '0');
INSERT INTO `administracion_menu` VALUES ('45', '2017-07-24 04:09:56.589416', '2017-07-27 16:03:37.166000', 'Alineación al PDRC', 'Acciones Estratégicas por Producto / Proyecto', 'planificacion:aexpop_inicio', '5', '1', null, '16', '1', '0');
INSERT INTO `administracion_menu` VALUES ('46', '2017-07-24 04:23:44.158611', '2017-07-27 16:11:35.759000', 'Sub Actividades', null, 'planificacion:subactividadcomponente_inicio', '4', '1', null, '52', '1', '0');
INSERT INTO `administracion_menu` VALUES ('47', '2017-07-24 04:29:22.806508', '2017-07-27 15:51:06.794000', 'PDRC', null, null, '4', '1', null, null, '1', '0');
INSERT INTO `administracion_menu` VALUES ('48', '2017-07-24 04:48:08.645165', '2017-07-30 16:54:22.110000', 'Tareas', null, 'agricola:tareacpcr_inicio', '3', '1', null, '53', '1', '0');
INSERT INTO `administracion_menu` VALUES ('49', '2017-07-24 04:57:53.951938', '2017-07-27 16:11:35.416000', 'Acciones', null, 'planificacion:accionobra_inicio', '2', '1', null, '53', '1', '0');
INSERT INTO `administracion_menu` VALUES ('50', '2017-07-24 05:26:05.238959', '2017-07-24 05:26:05.239040', 'Empresas', null, null, '10', '1', null, '13', '1', '0');
INSERT INTO `administracion_menu` VALUES ('51', '2017-07-24 05:26:35.258835', '2017-07-24 05:26:35.258885', 'Ventas', null, null, '11', '1', null, '13', '1', '0');
INSERT INTO `administracion_menu` VALUES ('52', '2017-07-27 15:47:08.743000', '2017-07-27 15:47:08.743000', 'Producto', null, null, '6', '1', null, null, '1', '0');
INSERT INTO `administracion_menu` VALUES ('53', '2017-07-27 15:47:25.469000', '2017-07-27 15:47:25.469000', 'Proyecto', null, null, '7', '1', null, null, '1', '0');
INSERT INTO `administracion_menu` VALUES ('54', '2017-07-27 15:56:41.746000', '2017-07-27 15:56:41.746000', 'Ejecución Física Financiera', null, 'planificacion:metaffejecutado_inicio', '7', '1', null, '16', '1', '0');
INSERT INTO `administracion_menu` VALUES ('55', '2017-07-27 15:58:26.982000', '2017-07-27 16:14:05.921000', 'Proyectos', null, 'planificacion:proyecto_inicio', '1', '1', null, '53', '1', '0');
INSERT INTO `administracion_menu` VALUES ('56', '2017-07-27 15:59:21.927000', '2017-07-27 16:17:01.699000', 'Tareas', null, 'agricola:tareacpcu_inicio', '5', '1', null, '52', '1', '0');
INSERT INTO `administracion_menu` VALUES ('58', '2017-07-29 13:36:28.665000', '2017-07-30 16:21:51.714000', 'Catálogo de Actividades', null, 'planificacion:actividadcatalogo_inicio', '8', '1', null, '1', '1', '1');
INSERT INTO `administracion_menu` VALUES ('59', '2017-07-29 13:38:02.473000', '2017-07-30 16:07:44.912000', 'Catálogo de Sub Actividades', null, 'planificacion:subactividadcatalogo_inicio', '9', '1', null, '1', '1', '0');
INSERT INTO `administracion_menu` VALUES ('60', '2017-07-29 13:38:25.923000', '2017-07-30 16:07:44.811000', 'Catálogo de Tareas', null, 'planificacion:tareacatalogo_inicio', '10', '1', null, '1', '1', '0');
INSERT INTO `administracion_menu` VALUES ('61', '2017-07-30 16:08:44.616000', '2017-07-30 16:08:44.616000', 'Reportes', null, null, '8', '1', null, null, '1', '0');
INSERT INTO `administracion_menu` VALUES ('62', '2017-07-30 16:11:50.038000', '2017-07-30 16:13:19.177000', 'Variedades por Asociado', null, 'reportes:variedadsocio_inicio', '1', '1', null, '61', '1', '0');
INSERT INTO `administracion_menu` VALUES ('63', '2017-07-30 16:12:08.504000', '2017-07-30 16:13:19.056000', 'Variedades por Certificación y Lugar', null, 'reportes:variedadtipocertificacion_inicio', '2', '1', null, '61', '1', '0');
INSERT INTO `administracion_menu` VALUES ('64', '2017-07-30 16:12:20.014000', '2017-07-30 16:13:18.875000', 'Organizaciones Area, Rendimiento y Volumen', null, 'reportes:organizacion_inicio', '3', '1', null, '61', '1', '0');
INSERT INTO `administracion_menu` VALUES ('65', '2017-07-30 16:12:31.269000', '2017-07-30 16:13:18.777000', 'Socios Ingresos y Salidas', null, 'reportes:socios_inicio', '4', '1', null, '61', '1', '0');
INSERT INTO `administracion_menu` VALUES ('66', '2017-07-30 16:12:44.239000', '2017-07-30 16:13:18.656000', 'Objetivos por Mes', null, 'reportes:pdrcobjespecifico_inicio', '5', '1', null, '61', '1', '0');
INSERT INTO `administracion_menu` VALUES ('67', '2017-07-30 17:29:08.127000', '2017-07-30 17:29:43.832000', 'Catálogo de Variedades', null, 'agricola:variedadcatalogo_inicio', '11', '1', null, '1', '1', '0');

-- ----------------------------
-- Table structure for administracion_organizacion
-- ----------------------------
DROP TABLE IF EXISTS `administracion_organizacion`;
CREATE TABLE `administracion_organizacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `direccion` varchar(45) NOT NULL,
  `ruc` varchar(45) NOT NULL,
  `area` decimal(9,2) NOT NULL,
  `rendimiento` decimal(9,2) NOT NULL,
  `volumen` decimal(9,2) NOT NULL,
  `caserio_id` int(11) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `tipoorganizacion_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ruc` (`ruc`),
  KEY `administracion_organ_caserio_id_731a82e9_fk_inicio_ca` (`caserio_id`),
  KEY `administracion_organizacion_creador_id_95d208b2_fk_auth_user_id` (`creador_id`),
  KEY `administracion_organizacion_editor_id_6a491570_fk_auth_user_id` (`editor_id`),
  KEY `administracion_organ_tipoorganizacion_id_d4f07f2c_fk_inicio_ti` (`tipoorganizacion_id`),
  CONSTRAINT `administracion_organ_caserio_id_731a82e9_fk_inicio_ca` FOREIGN KEY (`caserio_id`) REFERENCES `inicio_caserio` (`id`),
  CONSTRAINT `administracion_organ_tipoorganizacion_id_d4f07f2c_fk_inicio_ti` FOREIGN KEY (`tipoorganizacion_id`) REFERENCES `inicio_tipoorganizacion` (`id`),
  CONSTRAINT `administracion_organizacion_creador_id_95d208b2_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `administracion_organizacion_editor_id_6a491570_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of administracion_organizacion
-- ----------------------------
INSERT INTO `administracion_organizacion` VALUES ('1', '2017-06-21 18:34:32.000000', '2017-07-20 15:39:49.430000', 'Organización 1', 'Dir 1', '20453744165', '125.00', '10.00', '39.00', '1', '1', '1', '1');
INSERT INTO `administracion_organizacion` VALUES ('3', '2017-07-24 22:39:50.251704', '2017-07-24 22:39:50.251748', 'Organización 01', 'Su casa', '20908990101', '1230.00', '20.00', '234.00', '2', '1', null, '5');

-- ----------------------------
-- Table structure for administracion_persona
-- ----------------------------
DROP TABLE IF EXISTS `administracion_persona`;
CREATE TABLE `administracion_persona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `dni` varchar(8) NOT NULL,
  `nombres` varchar(50) NOT NULL,
  `apepat` varchar(50) NOT NULL,
  `apemat` varchar(50) NOT NULL,
  `sexo` varchar(1) NOT NULL,
  `fechanacimiento` date NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dni` (`dni`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `administracion_persona_creador_id_8132ff20_fk_auth_user_id` (`creador_id`),
  KEY `administracion_persona_editor_id_0729719a_fk_auth_user_id` (`editor_id`),
  CONSTRAINT `administracion_persona_creador_id_8132ff20_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `administracion_persona_editor_id_0729719a_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `administracion_persona_user_id_b0865423_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of administracion_persona
-- ----------------------------
INSERT INTO `administracion_persona` VALUES ('1', '2017-06-16 08:12:52.000000', '2017-07-22 14:50:57.506686', '56256321', 'JOSE', 'PAREDES', 'CALLA', 'M', '1989-01-04', '1', null, '2');
INSERT INTO `administracion_persona` VALUES ('2', '2017-06-16 08:22:16.000000', '2017-06-16 08:22:16.000000', '12345678', 'CHARLY', 'PEREZ', 'GARCÍA', 'M', '1980-02-10', '1', null, '3');
INSERT INTO `administracion_persona` VALUES ('3', '2017-06-16 11:54:22.000000', '2017-07-22 14:50:19.628637', '25635416', 'Cleopatro', 'Calderon', 'Tacilla', 'M', '2017-06-16', '1', null, '4');
INSERT INTO `administracion_persona` VALUES ('4', '2017-06-16 12:06:48.000000', '2017-06-16 12:06:48.000000', '12345672', 'Juan', 'Perez', 'Perez', 'M', '2017-06-16', '1', null, '5');
INSERT INTO `administracion_persona` VALUES ('6', '2017-07-24 22:34:30.059099', '2017-07-24 22:37:53.119798', '90909000', 'Una Flaca', 'De Jaén 2017', 'Cajamarca', 'F', '2017-07-24', '1', '1', '6');
INSERT INTO `administracion_persona` VALUES ('8', '2017-07-26 15:31:42.091568', '2017-07-26 15:31:42.091638', '12345686', 'AAA', 'BB', 'CC', 'M', '2017-07-13', '1', null, null);
INSERT INTO `administracion_persona` VALUES ('9', '2017-07-28 14:40:35.594655', '2017-07-28 14:49:06.401749', '76985421', 'Mia', 'Kalifa', 'Kalifa', 'F', '1999-07-11', '1', null, '7');

-- ----------------------------
-- Table structure for administracion_trabajador
-- ----------------------------
DROP TABLE IF EXISTS `administracion_trabajador`;
CREATE TABLE `administracion_trabajador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `direccion` varchar(45) NOT NULL,
  `modalidad` varchar(45) NOT NULL,
  `fechainicio` date NOT NULL,
  `fechafin` date DEFAULT NULL,
  `estado` tinyint(1) NOT NULL,
  `area_id` int(11) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `especialidad_id` int(11) NOT NULL,
  `funcioncargo_id` int(11) NOT NULL,
  `persona_id` int(11) NOT NULL,
  `profesion_id` int(11) NOT NULL,
  `provincia_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `administracion_traba_area_id_5b9c38d9_fk_administr` (`area_id`),
  KEY `administracion_trabajador_creador_id_db47f427_fk_auth_user_id` (`creador_id`),
  KEY `administracion_trabajador_editor_id_3c0c0aa7_fk_auth_user_id` (`editor_id`),
  KEY `administracion_traba_especialidad_id_11f83ebc_fk_inicio_es` (`especialidad_id`),
  KEY `administracion_traba_funcioncargo_id_3c0cf9e9_fk_administr` (`funcioncargo_id`),
  KEY `administracion_traba_persona_id_d4cdb76f_fk_administr` (`persona_id`),
  KEY `administracion_traba_profesion_id_2443f0a8_fk_inicio_pr` (`profesion_id`),
  KEY `administracion_traba_provincia_id_a951cf50_fk_inicio_pr` (`provincia_id`),
  CONSTRAINT `administracion_traba_area_id_5b9c38d9_fk_administr` FOREIGN KEY (`area_id`) REFERENCES `administracion_area` (`id`),
  CONSTRAINT `administracion_traba_especialidad_id_11f83ebc_fk_inicio_es` FOREIGN KEY (`especialidad_id`) REFERENCES `inicio_especialidad` (`id`),
  CONSTRAINT `administracion_traba_funcioncargo_id_3c0cf9e9_fk_administr` FOREIGN KEY (`funcioncargo_id`) REFERENCES `administracion_funcioncargo` (`id`),
  CONSTRAINT `administracion_traba_persona_id_d4cdb76f_fk_administr` FOREIGN KEY (`persona_id`) REFERENCES `administracion_persona` (`id`),
  CONSTRAINT `administracion_traba_profesion_id_2443f0a8_fk_inicio_pr` FOREIGN KEY (`profesion_id`) REFERENCES `inicio_profesion` (`id`),
  CONSTRAINT `administracion_traba_provincia_id_a951cf50_fk_inicio_pr` FOREIGN KEY (`provincia_id`) REFERENCES `inicio_provincia` (`id`),
  CONSTRAINT `administracion_trabajador_creador_id_db47f427_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `administracion_trabajador_editor_id_3c0c0aa7_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of administracion_trabajador
-- ----------------------------
INSERT INTO `administracion_trabajador` VALUES ('3', '2017-06-16 12:16:44.000000', '2017-06-16 12:16:44.000000', '', '', '1989-06-16', null, '1', '1', '1', null, '1', '2', '4', '3', '1');
INSERT INTO `administracion_trabajador` VALUES ('4', '2017-07-02 18:59:37.000000', '2017-07-24 21:17:52.318072', '', '', '2017-01-01', '2017-11-15', '1', '2', '2', '1', '1', '2', '2', '2', '1');
INSERT INTO `administracion_trabajador` VALUES ('6', '2017-07-24 22:35:45.789378', '2017-07-28 13:56:17.806238', 'Su casa', 'CAS', '2017-08-18', '2017-07-18', '1', '7', '1', '1', '3', '3', '6', '2', '1');
INSERT INTO `administracion_trabajador` VALUES ('7', '2017-07-28 14:45:20.521036', '2017-07-28 14:45:20.521084', 'direccion', 'Manualidades', '2017-07-12', '2017-07-27', '1', '1', '1', null, '1', '1', '9', '1', '1');

-- ----------------------------
-- Table structure for agricola_base
-- ----------------------------
DROP TABLE IF EXISTS `agricola_base`;
CREATE TABLE `agricola_base` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `caserio_id` int(11) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `organizacion_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `agricola_base_caserio_id_1e9bb4ce_fk_inicio_caserio_id` (`caserio_id`),
  KEY `agricola_base_creador_id_61532cfd_fk_auth_user_id` (`creador_id`),
  KEY `agricola_base_editor_id_0dc263b9_fk_auth_user_id` (`editor_id`),
  KEY `agricola_base_organizacion_id_0f2eea86_fk_administr` (`organizacion_id`),
  CONSTRAINT `agricola_base_caserio_id_1e9bb4ce_fk_inicio_caserio_id` FOREIGN KEY (`caserio_id`) REFERENCES `inicio_caserio` (`id`),
  CONSTRAINT `agricola_base_creador_id_61532cfd_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `agricola_base_editor_id_0dc263b9_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `agricola_base_organizacion_id_0f2eea86_fk_administr` FOREIGN KEY (`organizacion_id`) REFERENCES `administracion_organizacion` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of agricola_base
-- ----------------------------
INSERT INTO `agricola_base` VALUES ('1', '2017-06-21 18:34:46.000000', '2017-06-21 18:34:46.000000', 'Base 01', '1', '1', null, '1');
INSERT INTO `agricola_base` VALUES ('2', '2017-07-12 14:52:11.211000', '2017-07-12 15:01:22.115000', 'Base 02', '1', '2', '2', '1');
INSERT INTO `agricola_base` VALUES ('3', '2017-07-24 22:56:39.163041', '2017-07-24 22:56:39.163080', 'Cinco Bases', '2', '1', null, '3');

-- ----------------------------
-- Table structure for agricola_detalleventa
-- ----------------------------
DROP TABLE IF EXISTS `agricola_detalleventa`;
CREATE TABLE `agricola_detalleventa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `precio` decimal(11,2) NOT NULL,
  `volumen` decimal(11,2) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `variedad_id` int(11) NOT NULL,
  `venta_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `agricola_detalleventa_creador_id_27ea3203_fk_auth_user_id` (`creador_id`),
  KEY `agricola_detalleventa_editor_id_33315601_fk_auth_user_id` (`editor_id`),
  KEY `agricola_detallevent_variedad_id_c88a9722_fk_agricola_` (`variedad_id`),
  KEY `agricola_detalleventa_venta_id_00eb67c9_fk_agricola_venta_id` (`venta_id`),
  CONSTRAINT `agricola_detallevent_variedad_id_c88a9722_fk_agricola_` FOREIGN KEY (`variedad_id`) REFERENCES `agricola_variedad` (`id`),
  CONSTRAINT `agricola_detalleventa_creador_id_27ea3203_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `agricola_detalleventa_editor_id_33315601_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `agricola_detalleventa_venta_id_00eb67c9_fk_agricola_venta_id` FOREIGN KEY (`venta_id`) REFERENCES `agricola_venta` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of agricola_detalleventa
-- ----------------------------

-- ----------------------------
-- Table structure for agricola_empresa
-- ----------------------------
DROP TABLE IF EXISTS `agricola_empresa`;
CREATE TABLE `agricola_empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `razon_social` varchar(300) NOT NULL,
  `ruc` varchar(11) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `agricola_empresa_creador_id_b6783e2d_fk_auth_user_id` (`creador_id`),
  KEY `agricola_empresa_editor_id_1cc39f2e_fk_auth_user_id` (`editor_id`),
  CONSTRAINT `agricola_empresa_creador_id_b6783e2d_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `agricola_empresa_editor_id_1cc39f2e_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of agricola_empresa
-- ----------------------------

-- ----------------------------
-- Table structure for agricola_enfermedad
-- ----------------------------
DROP TABLE IF EXISTS `agricola_enfermedad`;
CREATE TABLE `agricola_enfermedad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `descripcion` longtext NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `agricola_enfermedad_creador_id_b19d2edf_fk_auth_user_id` (`creador_id`),
  KEY `agricola_enfermedad_editor_id_81e4c3db_fk_auth_user_id` (`editor_id`),
  CONSTRAINT `agricola_enfermedad_creador_id_b19d2edf_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `agricola_enfermedad_editor_id_81e4c3db_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of agricola_enfermedad
-- ----------------------------
INSERT INTO `agricola_enfermedad` VALUES ('1', '2017-07-15 13:42:20.512000', '2017-07-15 13:42:20.512000', 'Enfermedad 01', 'Prueba 01', '2', null);
INSERT INTO `agricola_enfermedad` VALUES ('3', '2017-07-15 13:42:38.565000', '2017-07-15 13:42:38.566000', 'Enfermedad 03', 'dsads das', '2', null);
INSERT INTO `agricola_enfermedad` VALUES ('4', '2017-07-16 18:28:43.814000', '2017-07-16 18:28:43.814000', 'Enfermedad 04', 'sds', '2', null);
INSERT INTO `agricola_enfermedad` VALUES ('5', '2017-07-16 18:28:50.234000', '2017-07-16 18:28:50.234000', 'Enfermedad 05', 'dsdsa', '2', null);
INSERT INTO `agricola_enfermedad` VALUES ('6', '2017-07-16 18:28:56.887000', '2017-07-16 18:28:56.887000', 'Enfermedad 06', 'fdss', '2', null);
INSERT INTO `agricola_enfermedad` VALUES ('7', '2017-07-16 18:29:06.111000', '2017-07-16 18:29:06.111000', 'Enfermedad 07', 'dsdsfmsdl', '2', null);
INSERT INTO `agricola_enfermedad` VALUES ('9', '2017-07-16 18:29:22.446000', '2017-07-16 18:29:22.446000', 'Enfermedad 09', 'dada', '2', null);

-- ----------------------------
-- Table structure for agricola_enfermedadxespecie
-- ----------------------------
DROP TABLE IF EXISTS `agricola_enfermedadxespecie`;
CREATE TABLE `agricola_enfermedadxespecie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `enfermedad_id` int(11) NOT NULL,
  `especie_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `agricola_enfermedadxespecie_creador_id_05082f0c_fk_auth_user_id` (`creador_id`),
  KEY `agricola_enfermedadxespecie_editor_id_72cab15b_fk_auth_user_id` (`editor_id`),
  KEY `agricola_enfermedadx_enfermedad_id_262da369_fk_agricola_` (`enfermedad_id`),
  KEY `agricola_enfermedadx_especie_id_e66a628b_fk_agricola_` (`especie_id`),
  CONSTRAINT `agricola_enfermedadx_enfermedad_id_262da369_fk_agricola_` FOREIGN KEY (`enfermedad_id`) REFERENCES `agricola_enfermedad` (`id`),
  CONSTRAINT `agricola_enfermedadx_especie_id_e66a628b_fk_agricola_` FOREIGN KEY (`especie_id`) REFERENCES `agricola_especie` (`id`),
  CONSTRAINT `agricola_enfermedadxespecie_creador_id_05082f0c_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `agricola_enfermedadxespecie_editor_id_72cab15b_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of agricola_enfermedadxespecie
-- ----------------------------
INSERT INTO `agricola_enfermedadxespecie` VALUES ('5', '2017-07-16 18:31:43.340000', '2017-07-16 18:31:43.341000', '2', null, '5', '1');
INSERT INTO `agricola_enfermedadxespecie` VALUES ('8', '2017-07-16 18:31:43.343000', '2017-07-16 18:31:43.343000', '2', null, '9', '1');
INSERT INTO `agricola_enfermedadxespecie` VALUES ('9', '2017-07-16 18:31:51.559000', '2017-07-16 18:31:51.559000', '2', null, '7', '1');
INSERT INTO `agricola_enfermedadxespecie` VALUES ('11', '2017-07-16 20:07:41.741000', '2017-07-16 20:07:41.741000', '2', null, '1', '1');
INSERT INTO `agricola_enfermedadxespecie` VALUES ('12', '2017-07-16 20:07:41.742000', '2017-07-16 20:07:41.742000', '2', null, '3', '1');

-- ----------------------------
-- Table structure for agricola_enfermedadxsocio
-- ----------------------------
DROP TABLE IF EXISTS `agricola_enfermedadxsocio`;
CREATE TABLE `agricola_enfermedadxsocio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `enfermedadxespecie_id` int(11) NOT NULL,
  `razaxsocio_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `agricola_enfermedadxsocio_creador_id_57008a3e_fk_auth_user_id` (`creador_id`),
  KEY `agricola_enfermedadxsocio_editor_id_e1532882_fk_auth_user_id` (`editor_id`),
  KEY `agricola_enfermedadx_enfermedadxespecie_i_aa68f0a6_fk_agricola_` (`enfermedadxespecie_id`),
  KEY `agricola_enfermedadx_razaxsocio_id_43cf8e27_fk_agricola_` (`razaxsocio_id`),
  CONSTRAINT `agricola_enfermedadx_enfermedadxespecie_i_aa68f0a6_fk_agricola_` FOREIGN KEY (`enfermedadxespecie_id`) REFERENCES `agricola_enfermedadxespecie` (`id`),
  CONSTRAINT `agricola_enfermedadx_razaxsocio_id_43cf8e27_fk_agricola_` FOREIGN KEY (`razaxsocio_id`) REFERENCES `agricola_razaxsocio` (`id`),
  CONSTRAINT `agricola_enfermedadxsocio_creador_id_57008a3e_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `agricola_enfermedadxsocio_editor_id_e1532882_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of agricola_enfermedadxsocio
-- ----------------------------

-- ----------------------------
-- Table structure for agricola_especie
-- ----------------------------
DROP TABLE IF EXISTS `agricola_especie`;
CREATE TABLE `agricola_especie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `agricola_especie_creador_id_ff64f1ba_fk_auth_user_id` (`creador_id`),
  KEY `agricola_especie_editor_id_5451daec_fk_auth_user_id` (`editor_id`),
  CONSTRAINT `agricola_especie_creador_id_ff64f1ba_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `agricola_especie_editor_id_5451daec_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of agricola_especie
-- ----------------------------
INSERT INTO `agricola_especie` VALUES ('1', '2017-07-12 16:47:05.364000', '2017-07-16 17:28:28.455000', 'Especie 01', '1', '2', '2');
INSERT INTO `agricola_especie` VALUES ('2', '2017-07-24 23:01:02.876913', '2017-07-24 23:01:02.876950', 'Nueva', '1', '1', null);

-- ----------------------------
-- Table structure for agricola_plaga
-- ----------------------------
DROP TABLE IF EXISTS `agricola_plaga`;
CREATE TABLE `agricola_plaga` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `nombrecientifico` varchar(45) NOT NULL,
  `descripcion` longtext NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  UNIQUE KEY `nombrecientifico` (`nombrecientifico`),
  KEY `agricola_plaga_creador_id_df8c48b0_fk_auth_user_id` (`creador_id`),
  KEY `agricola_plaga_editor_id_e0601602_fk_auth_user_id` (`editor_id`),
  CONSTRAINT `agricola_plaga_creador_id_df8c48b0_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `agricola_plaga_editor_id_e0601602_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of agricola_plaga
-- ----------------------------
INSERT INTO `agricola_plaga` VALUES ('1', '2017-07-01 04:29:52.000000', '2017-07-01 04:29:52.000000', 'Plaga 01', 'Plaga Cientifica 01', 'Descripcion 01', '2', null);
INSERT INTO `agricola_plaga` VALUES ('2', '2017-07-01 04:30:08.000000', '2017-07-01 04:30:08.000000', 'Plaga 02', 'Plaga Cientifica 02', 'Descripcion 02', '2', null);

-- ----------------------------
-- Table structure for agricola_raza
-- ----------------------------
DROP TABLE IF EXISTS `agricola_raza`;
CREATE TABLE `agricola_raza` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `nombre` varchar(75) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `especie_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `agricola_raza_creador_id_5f9c3525_fk_auth_user_id` (`creador_id`),
  KEY `agricola_raza_editor_id_739b9fa7_fk_auth_user_id` (`editor_id`),
  KEY `agricola_raza_especie_id_f1179b23_fk_agricola_especie_id` (`especie_id`),
  CONSTRAINT `agricola_raza_creador_id_5f9c3525_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `agricola_raza_editor_id_739b9fa7_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `agricola_raza_especie_id_f1179b23_fk_agricola_especie_id` FOREIGN KEY (`especie_id`) REFERENCES `agricola_especie` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of agricola_raza
-- ----------------------------
INSERT INTO `agricola_raza` VALUES ('1', '2017-07-12 17:45:59.875000', '2017-07-12 17:45:59.876000', 'Raza 01', '2', null, '1');
INSERT INTO `agricola_raza` VALUES ('2', '2017-07-24 23:01:15.602060', '2017-07-24 23:01:15.602098', 'Nueva raza', '1', null, '1');
INSERT INTO `agricola_raza` VALUES ('3', '2017-07-24 23:01:21.057074', '2017-07-24 23:01:21.057114', 'otra raza', '1', null, '1');

-- ----------------------------
-- Table structure for agricola_razaxsocio
-- ----------------------------
DROP TABLE IF EXISTS `agricola_razaxsocio`;
CREATE TABLE `agricola_razaxsocio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `raza_id` int(11) NOT NULL,
  `socio_id` int(11) NOT NULL,
  `nrocabezas` decimal(11,2) DEFAULT NULL,
  `produccion` decimal(11,2) DEFAULT NULL,
  `rendimiento` decimal(11,2) DEFAULT NULL,
  `seca` decimal(11,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `agricola_razaxsocio_creador_id_ac56a7d7_fk_auth_user_id` (`creador_id`),
  KEY `agricola_razaxsocio_editor_id_bff73311_fk_auth_user_id` (`editor_id`),
  KEY `agricola_razaxsocio_raza_id_22e24e72_fk_agricola_raza_id` (`raza_id`),
  KEY `agricola_razaxsocio_socio_id_a7d093ed_fk_agricola_socio_id` (`socio_id`),
  CONSTRAINT `agricola_razaxsocio_creador_id_ac56a7d7_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `agricola_razaxsocio_editor_id_bff73311_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `agricola_razaxsocio_raza_id_22e24e72_fk_agricola_raza_id` FOREIGN KEY (`raza_id`) REFERENCES `agricola_raza` (`id`),
  CONSTRAINT `agricola_razaxsocio_socio_id_a7d093ed_fk_agricola_socio_id` FOREIGN KEY (`socio_id`) REFERENCES `agricola_socio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of agricola_razaxsocio
-- ----------------------------
INSERT INTO `agricola_razaxsocio` VALUES ('1', '2017-07-22 22:38:05.917784', '2017-07-22 22:38:05.917834', '1', null, '1', '4', '121.00', '1212.00', '1212.00', '12.00');
INSERT INTO `agricola_razaxsocio` VALUES ('2', '2017-07-22 22:39:49.069404', '2017-07-24 14:32:49.023690', '1', '1', '1', '5', '1231.00', '123213.00', '12312.00', '123.00');
INSERT INTO `agricola_razaxsocio` VALUES ('3', '2017-07-22 23:51:41.346781', '2017-07-22 23:55:30.138151', '1', '1', '1', '4', '12.20', '12.00', '1212.10', '12.00');
INSERT INTO `agricola_razaxsocio` VALUES ('4', '2017-07-22 23:57:09.035770', '2017-07-22 23:57:09.035838', '1', null, '1', '7', '12121.00', '1212.00', '12121.00', '1212.00');
INSERT INTO `agricola_razaxsocio` VALUES ('5', '2017-07-24 14:33:10.363815', '2017-07-24 14:33:41.345364', '1', '1', '1', '10', '12312.00', '21312.11', '123213.00', '123.10');
INSERT INTO `agricola_razaxsocio` VALUES ('6', '2017-07-24 20:04:40.236215', '2017-07-24 20:04:40.236264', '1', null, '1', '4', null, null, null, null);
INSERT INTO `agricola_razaxsocio` VALUES ('7', '2017-07-24 23:02:21.246393', '2017-07-24 23:02:21.246435', '1', null, '1', '7', '9089.00', '9089.00', '123.00', '1234.00');
INSERT INTO `agricola_razaxsocio` VALUES ('8', '2017-07-28 14:01:50.401006', '2017-07-28 14:01:50.401081', '1', null, '2', '4', null, null, null, null);

-- ----------------------------
-- Table structure for agricola_socio
-- ----------------------------
DROP TABLE IF EXISTS `agricola_socio`;
CREATE TABLE `agricola_socio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `email` varchar(75) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `base_id` int(11) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `persona_id` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `ingreso` date NOT NULL,
  `salida` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `agricola_socio_base_id_9f7d0b7e_fk_agricola_base_id` (`base_id`),
  KEY `agricola_socio_creador_id_ce378524_fk_auth_user_id` (`creador_id`),
  KEY `agricola_socio_editor_id_89129772_fk_auth_user_id` (`editor_id`),
  KEY `agricola_socio_persona_id_4550431d_fk_administracion_persona_id` (`persona_id`),
  CONSTRAINT `agricola_socio_base_id_9f7d0b7e_fk_agricola_base_id` FOREIGN KEY (`base_id`) REFERENCES `agricola_base` (`id`),
  CONSTRAINT `agricola_socio_creador_id_ce378524_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `agricola_socio_editor_id_89129772_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `agricola_socio_persona_id_4550431d_fk_administracion_persona_id` FOREIGN KEY (`persona_id`) REFERENCES `administracion_persona` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of agricola_socio
-- ----------------------------
INSERT INTO `agricola_socio` VALUES ('4', '2017-06-21 18:40:13.000000', '2017-07-20 13:44:29.595000', 'jperez@gmail.com', '999777111', '1', '2', '1', '4', '1', '2017-02-14', null);
INSERT INTO `agricola_socio` VALUES ('5', '2017-07-01 04:26:06.000000', '2017-07-28 16:42:38.379296', null, null, '1', '2', '1', '2', '1', '2017-01-11', null);
INSERT INTO `agricola_socio` VALUES ('6', '2017-07-22 21:35:48.578632', '2017-07-22 21:35:48.578678', null, null, '2', '1', null, '4', '1', '2017-07-09', '2017-07-26');
INSERT INTO `agricola_socio` VALUES ('7', '2017-07-22 21:36:24.531814', '2017-07-28 16:42:59.198641', 'pepeparades@gmail.com', '123456789', '1', '1', '1', '1', '1', '2017-07-28', '2017-07-24');
INSERT INTO `agricola_socio` VALUES ('10', '2017-07-24 05:16:32.380326', '2017-07-24 05:16:32.380372', 'correito@gmail.com', '362262', '2', '1', null, '2', '1', '2017-07-11', '2017-07-24');
INSERT INTO `agricola_socio` VALUES ('11', '2017-07-24 22:57:38.500176', '2017-07-24 22:57:38.500220', 'hola@hotmail.com', '98765123123', '3', '1', null, '3', '1', '2017-07-17', '2017-07-25');
INSERT INTO `agricola_socio` VALUES ('12', '2017-07-24 22:58:13.012137', '2017-07-24 22:58:13.012180', 'jprez@hola.com', '123456789', '3', '1', null, '4', '1', '2017-07-03', '2017-07-25');

-- ----------------------------
-- Table structure for agricola_variedad
-- ----------------------------
DROP TABLE IF EXISTS `agricola_variedad`;
CREATE TABLE `agricola_variedad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `area` decimal(9,2) NOT NULL,
  `areasecano` decimal(11,2) NOT NULL,
  `areabajoriego` decimal(11,2) NOT NULL,
  `preciochacra` decimal(11,2) NOT NULL,
  `preciomercado` decimal(11,2) NOT NULL,
  `rendimiento` decimal(9,2) NOT NULL,
  `volumenproduccion` decimal(9,2) NOT NULL,
  `certificadas` decimal(11,2) NOT NULL,
  `sincertificar` decimal(11,2) NOT NULL,
  `titulo` varchar(2) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `plaga_id` int(11) NOT NULL,
  `socio_id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `observaciones` longtext,
  `tipocertificado` varchar(2) NOT NULL,
  `variedadcatalogo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `agricola_variedad_creador_id_a39b76e5_fk_auth_user_id` (`creador_id`),
  KEY `agricola_variedad_editor_id_20e2124b_fk_auth_user_id` (`editor_id`),
  KEY `agricola_variedad_plaga_id_b85dc361_fk_agricola_plaga_id` (`plaga_id`),
  KEY `agricola_variedad_socio_id_c3071740_fk_agricola_socio_id` (`socio_id`),
  KEY `agricola_variedad_variedadcatalogo_id_54b77eee_fk_agricola_` (`variedadcatalogo_id`),
  CONSTRAINT `agricola_variedad_creador_id_a39b76e5_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `agricola_variedad_editor_id_20e2124b_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `agricola_variedad_plaga_id_b85dc361_fk_agricola_plaga_id` FOREIGN KEY (`plaga_id`) REFERENCES `agricola_plaga` (`id`),
  CONSTRAINT `agricola_variedad_socio_id_c3071740_fk_agricola_socio_id` FOREIGN KEY (`socio_id`) REFERENCES `agricola_socio` (`id`),
  CONSTRAINT `agricola_variedad_variedadcatalogo_id_54b77eee_fk_agricola_` FOREIGN KEY (`variedadcatalogo_id`) REFERENCES `agricola_variedadcatalogo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of agricola_variedad
-- ----------------------------
INSERT INTO `agricola_variedad` VALUES ('1', '2017-07-01 04:30:41.000000', '2017-07-20 17:12:04.114000', '1.50', '0.00', '0.00', '0.00', '0.00', '5333.33', '8000.00', '0.00', '0.00', 'CT', '2', '1', '1', '4', '2017-07-20', '', 'CO', '1');
INSERT INTO `agricola_variedad` VALUES ('2', '2017-07-01 04:31:01.000000', '2017-07-20 17:11:43.177000', '1029.00', '0.00', '0.00', '0.00', '0.00', '5.05', '5200.00', '0.00', '0.00', 'CT', '2', '1', '2', '5', '2017-07-20', '', 'CO', '2');
INSERT INTO `agricola_variedad` VALUES ('3', '2017-07-20 16:32:35.729000', '2017-07-20 17:11:36.445000', '100.00', '0.00', '0.00', '0.00', '0.00', '85.00', '8500.00', '0.00', '0.00', 'CT', '1', '1', '1', '5', '2017-07-20', '', 'CO', '3');
INSERT INTO `agricola_variedad` VALUES ('4', '2017-07-30 17:23:28.947000', '2017-07-30 17:23:28.947000', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'CT', '1', null, '1', '7', '2017-07-03', '', 'CO', '3');

-- ----------------------------
-- Table structure for agricola_variedadcatalogo
-- ----------------------------
DROP TABLE IF EXISTS `agricola_variedadcatalogo`;
CREATE TABLE `agricola_variedadcatalogo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `agricola_variedadcatalogo_creador_id_4d809f05_fk_auth_user_id` (`creador_id`),
  KEY `agricola_variedadcatalogo_editor_id_42500c2a_fk_auth_user_id` (`editor_id`),
  CONSTRAINT `agricola_variedadcatalogo_creador_id_4d809f05_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `agricola_variedadcatalogo_editor_id_42500c2a_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of agricola_variedadcatalogo
-- ----------------------------
INSERT INTO `agricola_variedadcatalogo` VALUES ('1', '2017-07-01 04:30:41.000000', '2017-07-20 17:12:04.114000', 'Variedad 01', '1', null);
INSERT INTO `agricola_variedadcatalogo` VALUES ('2', '2017-07-01 04:31:01.000000', '2017-07-20 17:11:43.177000', 'Variedad 02', '1', null);
INSERT INTO `agricola_variedadcatalogo` VALUES ('3', '2017-07-20 16:32:35.729000', '2017-07-20 17:11:36.445000', 'Variedad 15', '1', null);

-- ----------------------------
-- Table structure for agricola_venta
-- ----------------------------
DROP TABLE IF EXISTS `agricola_venta`;
CREATE TABLE `agricola_venta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `fecha` date DEFAULT NULL,
  `estado` tinyint(1) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `empresa_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `agricola_venta_creador_id_e92222be_fk_auth_user_id` (`creador_id`),
  KEY `agricola_venta_editor_id_eb36d2ea_fk_auth_user_id` (`editor_id`),
  KEY `agricola_venta_empresa_id_3b739759_fk_agricola_empresa_id` (`empresa_id`),
  CONSTRAINT `agricola_venta_creador_id_e92222be_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `agricola_venta_editor_id_eb36d2ea_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `agricola_venta_empresa_id_3b739759_fk_agricola_empresa_id` FOREIGN KEY (`empresa_id`) REFERENCES `agricola_empresa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of agricola_venta
-- ----------------------------

-- ----------------------------
-- Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of auth_group
-- ----------------------------

-- ----------------------------
-- Table structure for auth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_permissions`;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of auth_group_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO `auth_permission` VALUES ('1', 'Can add log entry', '1', 'add_logentry');
INSERT INTO `auth_permission` VALUES ('2', 'Can change log entry', '1', 'change_logentry');
INSERT INTO `auth_permission` VALUES ('3', 'Can delete log entry', '1', 'delete_logentry');
INSERT INTO `auth_permission` VALUES ('4', 'Can add group', '2', 'add_group');
INSERT INTO `auth_permission` VALUES ('5', 'Can change group', '2', 'change_group');
INSERT INTO `auth_permission` VALUES ('6', 'Can delete group', '2', 'delete_group');
INSERT INTO `auth_permission` VALUES ('7', 'Can add permission', '3', 'add_permission');
INSERT INTO `auth_permission` VALUES ('8', 'Can change permission', '3', 'change_permission');
INSERT INTO `auth_permission` VALUES ('9', 'Can delete permission', '3', 'delete_permission');
INSERT INTO `auth_permission` VALUES ('10', 'Can add user', '4', 'add_user');
INSERT INTO `auth_permission` VALUES ('11', 'Can change user', '4', 'change_user');
INSERT INTO `auth_permission` VALUES ('12', 'Can delete user', '4', 'delete_user');
INSERT INTO `auth_permission` VALUES ('13', 'Can add content type', '5', 'add_contenttype');
INSERT INTO `auth_permission` VALUES ('14', 'Can change content type', '5', 'change_contenttype');
INSERT INTO `auth_permission` VALUES ('15', 'Can delete content type', '5', 'delete_contenttype');
INSERT INTO `auth_permission` VALUES ('16', 'Can add session', '6', 'add_session');
INSERT INTO `auth_permission` VALUES ('17', 'Can change session', '6', 'change_session');
INSERT INTO `auth_permission` VALUES ('18', 'Can delete session', '6', 'delete_session');
INSERT INTO `auth_permission` VALUES ('19', 'Can add Provincia', '7', 'add_provincia');
INSERT INTO `auth_permission` VALUES ('20', 'Can change Provincia', '7', 'change_provincia');
INSERT INTO `auth_permission` VALUES ('21', 'Can delete Provincia', '7', 'delete_provincia');
INSERT INTO `auth_permission` VALUES ('22', 'Can add Unidad de Medida', '8', 'add_unidadmedida');
INSERT INTO `auth_permission` VALUES ('23', 'Can change Unidad de Medida', '8', 'change_unidadmedida');
INSERT INTO `auth_permission` VALUES ('24', 'Can delete Unidad de Medida', '8', 'delete_unidadmedida');
INSERT INTO `auth_permission` VALUES ('25', 'Can add Departamento', '9', 'add_departamento');
INSERT INTO `auth_permission` VALUES ('26', 'Can change Departamento', '9', 'change_departamento');
INSERT INTO `auth_permission` VALUES ('27', 'Can delete Departamento', '9', 'delete_departamento');
INSERT INTO `auth_permission` VALUES ('28', 'Can add Centro Poblado', '10', 'add_centropoblado');
INSERT INTO `auth_permission` VALUES ('29', 'Can change Centro Poblado', '10', 'change_centropoblado');
INSERT INTO `auth_permission` VALUES ('30', 'Can delete Centro Poblado', '10', 'delete_centropoblado');
INSERT INTO `auth_permission` VALUES ('31', 'Can add Profesión', '11', 'add_profesion');
INSERT INTO `auth_permission` VALUES ('32', 'Can change Profesión', '11', 'change_profesion');
INSERT INTO `auth_permission` VALUES ('33', 'Can delete Profesión', '11', 'delete_profesion');
INSERT INTO `auth_permission` VALUES ('34', 'Can add Tipo de Organización', '12', 'add_tipoorganizacion');
INSERT INTO `auth_permission` VALUES ('35', 'Can change Tipo de Organización', '12', 'change_tipoorganizacion');
INSERT INTO `auth_permission` VALUES ('36', 'Can delete Tipo de Organización', '12', 'delete_tipoorganizacion');
INSERT INTO `auth_permission` VALUES ('37', 'Can add Caserío', '13', 'add_caserio');
INSERT INTO `auth_permission` VALUES ('38', 'Can change Caserío', '13', 'change_caserio');
INSERT INTO `auth_permission` VALUES ('39', 'Can delete Caserío', '13', 'delete_caserio');
INSERT INTO `auth_permission` VALUES ('40', 'Can add Distrito', '14', 'add_distrito');
INSERT INTO `auth_permission` VALUES ('41', 'Can change Distrito', '14', 'change_distrito');
INSERT INTO `auth_permission` VALUES ('42', 'Can delete Distrito', '14', 'delete_distrito');
INSERT INTO `auth_permission` VALUES ('43', 'Can add Mes', '15', 'add_mes');
INSERT INTO `auth_permission` VALUES ('44', 'Can change Mes', '15', 'change_mes');
INSERT INTO `auth_permission` VALUES ('45', 'Can delete Mes', '15', 'delete_mes');
INSERT INTO `auth_permission` VALUES ('46', 'Can add Especialidad', '16', 'add_especialidad');
INSERT INTO `auth_permission` VALUES ('47', 'Can change Especialidad', '16', 'change_especialidad');
INSERT INTO `auth_permission` VALUES ('48', 'Can delete Especialidad', '16', 'delete_especialidad');
INSERT INTO `auth_permission` VALUES ('49', 'Can add Menú', '17', 'add_menu');
INSERT INTO `auth_permission` VALUES ('50', 'Can change Menú', '17', 'change_menu');
INSERT INTO `auth_permission` VALUES ('51', 'Can delete Menú', '17', 'delete_menu');
INSERT INTO `auth_permission` VALUES ('52', 'Can add Trabajador', '18', 'add_trabajador');
INSERT INTO `auth_permission` VALUES ('53', 'Can change Trabajador', '18', 'change_trabajador');
INSERT INTO `auth_permission` VALUES ('54', 'Can delete Trabajador', '18', 'delete_trabajador');
INSERT INTO `auth_permission` VALUES ('55', 'Can add Persona', '19', 'add_persona');
INSERT INTO `auth_permission` VALUES ('56', 'Can change Persona', '19', 'change_persona');
INSERT INTO `auth_permission` VALUES ('57', 'Can delete Persona', '19', 'delete_persona');
INSERT INTO `auth_permission` VALUES ('58', 'Can add Acceso', '20', 'add_acceso');
INSERT INTO `auth_permission` VALUES ('59', 'Can change Acceso', '20', 'change_acceso');
INSERT INTO `auth_permission` VALUES ('60', 'Can delete Acceso', '20', 'delete_acceso');
INSERT INTO `auth_permission` VALUES ('61', 'Can add Organización', '21', 'add_organizacion');
INSERT INTO `auth_permission` VALUES ('62', 'Can change Organización', '21', 'change_organizacion');
INSERT INTO `auth_permission` VALUES ('63', 'Can delete Organización', '21', 'delete_organizacion');
INSERT INTO `auth_permission` VALUES ('64', 'Can add Area', '22', 'add_area');
INSERT INTO `auth_permission` VALUES ('65', 'Can change Area', '22', 'change_area');
INSERT INTO `auth_permission` VALUES ('66', 'Can delete Area', '22', 'delete_area');
INSERT INTO `auth_permission` VALUES ('67', 'Can add Función - Cargo', '23', 'add_funcioncargo');
INSERT INTO `auth_permission` VALUES ('68', 'Can change Función - Cargo', '23', 'change_funcioncargo');
INSERT INTO `auth_permission` VALUES ('69', 'Can delete Función - Cargo', '23', 'delete_funcioncargo');
INSERT INTO `auth_permission` VALUES ('70', 'Can add Acción Obra', '24', 'add_accionobra');
INSERT INTO `auth_permission` VALUES ('71', 'Can change Acción Obra', '24', 'change_accionobra');
INSERT INTO `auth_permission` VALUES ('72', 'Can delete Acción Obra', '24', 'delete_accionobra');
INSERT INTO `auth_permission` VALUES ('73', 'Can add Acción Estratégica', '25', 'add_accionestrategica');
INSERT INTO `auth_permission` VALUES ('74', 'Can change Acción Estratégica', '25', 'change_accionestrategica');
INSERT INTO `auth_permission` VALUES ('75', 'Can delete Acción Estratégica', '25', 'delete_accionestrategica');
INSERT INTO `auth_permission` VALUES ('76', 'Can add Grupo Funcional', '26', 'add_grupofuncional');
INSERT INTO `auth_permission` VALUES ('77', 'Can change Grupo Funcional', '26', 'change_grupofuncional');
INSERT INTO `auth_permission` VALUES ('78', 'Can delete Grupo Funcional', '26', 'delete_grupofuncional');
INSERT INTO `auth_permission` VALUES ('79', 'Can add Sub Actividad de Componente', '27', 'add_subactividadcomponente');
INSERT INTO `auth_permission` VALUES ('80', 'Can change Sub Actividad de Componente', '27', 'change_subactividadcomponente');
INSERT INTO `auth_permission` VALUES ('81', 'Can delete Sub Actividad de Componente', '27', 'delete_subactividadcomponente');
INSERT INTO `auth_permission` VALUES ('82', 'Can add Objetivo del PDRC', '28', 'add_objetivopdrc');
INSERT INTO `auth_permission` VALUES ('83', 'Can change Objetivo del PDRC', '28', 'change_objetivopdrc');
INSERT INTO `auth_permission` VALUES ('84', 'Can delete Objetivo del PDRC', '28', 'delete_objetivopdrc');
INSERT INTO `auth_permission` VALUES ('85', 'Can add Programa Presupuestal', '29', 'add_programapresupuestal');
INSERT INTO `auth_permission` VALUES ('86', 'Can change Programa Presupuestal', '29', 'change_programapresupuestal');
INSERT INTO `auth_permission` VALUES ('87', 'Can delete Programa Presupuestal', '29', 'delete_programapresupuestal');
INSERT INTO `auth_permission` VALUES ('88', 'Can add División Funcional', '30', 'add_divisionfuncional');
INSERT INTO `auth_permission` VALUES ('89', 'Can change División Funcional', '30', 'change_divisionfuncional');
INSERT INTO `auth_permission` VALUES ('90', 'Can delete División Funcional', '30', 'delete_divisionfuncional');
INSERT INTO `auth_permission` VALUES ('91', 'Can add Objetivo del PDRC', '31', 'add_objetivoespecifico');
INSERT INTO `auth_permission` VALUES ('92', 'Can change Objetivo del PDRC', '31', 'change_objetivoespecifico');
INSERT INTO `auth_permission` VALUES ('93', 'Can delete Objetivo del PDRC', '31', 'delete_objetivoespecifico');
INSERT INTO `auth_permission` VALUES ('94', 'Can add Periodo', '32', 'add_periodo');
INSERT INTO `auth_permission` VALUES ('95', 'Can change Periodo', '32', 'change_periodo');
INSERT INTO `auth_permission` VALUES ('96', 'Can delete Periodo', '32', 'delete_periodo');
INSERT INTO `auth_permission` VALUES ('97', 'Can add Producto de Proyecto', '33', 'add_productoproyecto');
INSERT INTO `auth_permission` VALUES ('98', 'Can change Producto de Proyecto', '33', 'change_productoproyecto');
INSERT INTO `auth_permission` VALUES ('99', 'Can delete Producto de Proyecto', '33', 'delete_productoproyecto');
INSERT INTO `auth_permission` VALUES ('100', 'Can add Indicador', '34', 'add_indicador');
INSERT INTO `auth_permission` VALUES ('101', 'Can change Indicador', '34', 'change_indicador');
INSERT INTO `auth_permission` VALUES ('102', 'Can delete Indicador', '34', 'delete_indicador');
INSERT INTO `auth_permission` VALUES ('103', 'Can add Tarea CpCr', '35', 'add_tareacpcr');
INSERT INTO `auth_permission` VALUES ('104', 'Can change Tarea CpCr', '35', 'change_tareacpcr');
INSERT INTO `auth_permission` VALUES ('105', 'Can delete Tarea CpCr', '35', 'delete_tareacpcr');
INSERT INTO `auth_permission` VALUES ('106', 'Can add Cadena Productiva', '36', 'add_cadenaproductiva');
INSERT INTO `auth_permission` VALUES ('107', 'Can change Cadena Productiva', '36', 'change_cadenaproductiva');
INSERT INTO `auth_permission` VALUES ('108', 'Can delete Cadena Productiva', '36', 'delete_cadenaproductiva');
INSERT INTO `auth_permission` VALUES ('109', 'Can add Plan de Desarrollo', '37', 'add_pdrc');
INSERT INTO `auth_permission` VALUES ('110', 'Can change Plan de Desarrollo', '37', 'change_pdrc');
INSERT INTO `auth_permission` VALUES ('111', 'Can delete Plan de Desarrollo', '37', 'delete_pdrc');
INSERT INTO `auth_permission` VALUES ('112', 'Can add Eje Económico', '38', 'add_ejeecon');
INSERT INTO `auth_permission` VALUES ('113', 'Can change Eje Económico', '38', 'change_ejeecon');
INSERT INTO `auth_permission` VALUES ('114', 'Can delete Eje Económico', '38', 'delete_ejeecon');
INSERT INTO `auth_permission` VALUES ('115', 'Can add Aexpop', '39', 'add_aexpop');
INSERT INTO `auth_permission` VALUES ('116', 'Can change Aexpop', '39', 'change_aexpop');
INSERT INTO `auth_permission` VALUES ('117', 'Can delete Aexpop', '39', 'delete_aexpop');
INSERT INTO `auth_permission` VALUES ('118', 'Can add Función', '40', 'add_funcion');
INSERT INTO `auth_permission` VALUES ('119', 'Can change Función', '40', 'change_funcion');
INSERT INTO `auth_permission` VALUES ('120', 'Can delete Función', '40', 'delete_funcion');
INSERT INTO `auth_permission` VALUES ('121', 'Can add Tarea CpCu', '41', 'add_tareacpcu');
INSERT INTO `auth_permission` VALUES ('122', 'Can change Tarea CpCu', '41', 'change_tareacpcu');
INSERT INTO `auth_permission` VALUES ('123', 'Can delete Tarea CpCu', '41', 'delete_tareacpcu');
INSERT INTO `auth_permission` VALUES ('124', 'Can add Actividad', '42', 'add_actividad');
INSERT INTO `auth_permission` VALUES ('125', 'Can change Actividad', '42', 'change_actividad');
INSERT INTO `auth_permission` VALUES ('126', 'Can delete Actividad', '42', 'delete_actividad');
INSERT INTO `auth_permission` VALUES ('127', 'Can add Meta Física Financiera', '43', 'add_metaff');
INSERT INTO `auth_permission` VALUES ('128', 'Can change Meta Física Financiera', '43', 'change_metaff');
INSERT INTO `auth_permission` VALUES ('129', 'Can delete Meta Física Financiera', '43', 'delete_metaff');
INSERT INTO `auth_permission` VALUES ('130', 'Can add Raza', '44', 'add_raza');
INSERT INTO `auth_permission` VALUES ('131', 'Can change Raza', '44', 'change_raza');
INSERT INTO `auth_permission` VALUES ('132', 'Can delete Raza', '44', 'delete_raza');
INSERT INTO `auth_permission` VALUES ('133', 'Can add Socio', '45', 'add_socio');
INSERT INTO `auth_permission` VALUES ('134', 'Can change Socio', '45', 'change_socio');
INSERT INTO `auth_permission` VALUES ('135', 'Can delete Socio', '45', 'delete_socio');
INSERT INTO `auth_permission` VALUES ('136', 'Can add Especie', '46', 'add_especie');
INSERT INTO `auth_permission` VALUES ('137', 'Can change Especie', '46', 'change_especie');
INSERT INTO `auth_permission` VALUES ('138', 'Can delete Especie', '46', 'delete_especie');
INSERT INTO `auth_permission` VALUES ('139', 'Can add Base', '47', 'add_base');
INSERT INTO `auth_permission` VALUES ('140', 'Can change Base', '47', 'change_base');
INSERT INTO `auth_permission` VALUES ('141', 'Can delete Base', '47', 'delete_base');
INSERT INTO `auth_permission` VALUES ('142', 'Can add Enfermedad', '48', 'add_enfermedad');
INSERT INTO `auth_permission` VALUES ('143', 'Can change Enfermedad', '48', 'change_enfermedad');
INSERT INTO `auth_permission` VALUES ('144', 'Can delete Enfermedad', '48', 'delete_enfermedad');
INSERT INTO `auth_permission` VALUES ('145', 'Can add Raza por Socio', '49', 'add_razaxsocio');
INSERT INTO `auth_permission` VALUES ('146', 'Can change Raza por Socio', '49', 'change_razaxsocio');
INSERT INTO `auth_permission` VALUES ('147', 'Can delete Raza por Socio', '49', 'delete_razaxsocio');
INSERT INTO `auth_permission` VALUES ('148', 'Can add Enfermedad por Especie', '50', 'add_enfermedadxespecie');
INSERT INTO `auth_permission` VALUES ('149', 'Can change Enfermedad por Especie', '50', 'change_enfermedadxespecie');
INSERT INTO `auth_permission` VALUES ('150', 'Can delete Enfermedad por Especie', '50', 'delete_enfermedadxespecie');
INSERT INTO `auth_permission` VALUES ('151', 'Can add Plaga', '51', 'add_plaga');
INSERT INTO `auth_permission` VALUES ('152', 'Can change Plaga', '51', 'change_plaga');
INSERT INTO `auth_permission` VALUES ('153', 'Can delete Plaga', '51', 'delete_plaga');
INSERT INTO `auth_permission` VALUES ('154', 'Can add Variedad', '52', 'add_variedad');
INSERT INTO `auth_permission` VALUES ('155', 'Can change Variedad', '52', 'change_variedad');
INSERT INTO `auth_permission` VALUES ('156', 'Can delete Variedad', '52', 'delete_variedad');
INSERT INTO `auth_permission` VALUES ('157', 'Can add Enfermedad por Socio', '53', 'add_enfermedadxsocio');
INSERT INTO `auth_permission` VALUES ('158', 'Can change Enfermedad por Socio', '53', 'change_enfermedadxsocio');
INSERT INTO `auth_permission` VALUES ('159', 'Can delete Enfermedad por Socio', '53', 'delete_enfermedadxsocio');
INSERT INTO `auth_permission` VALUES ('160', 'Can add detalle venta', '54', 'add_detalleventa');
INSERT INTO `auth_permission` VALUES ('161', 'Can change detalle venta', '54', 'change_detalleventa');
INSERT INTO `auth_permission` VALUES ('162', 'Can delete detalle venta', '54', 'delete_detalleventa');
INSERT INTO `auth_permission` VALUES ('163', 'Can add empresa', '55', 'add_empresa');
INSERT INTO `auth_permission` VALUES ('164', 'Can change empresa', '55', 'change_empresa');
INSERT INTO `auth_permission` VALUES ('165', 'Can delete empresa', '55', 'delete_empresa');
INSERT INTO `auth_permission` VALUES ('166', 'Can add venta', '56', 'add_venta');
INSERT INTO `auth_permission` VALUES ('167', 'Can change venta', '56', 'change_venta');
INSERT INTO `auth_permission` VALUES ('168', 'Can delete venta', '56', 'delete_venta');
INSERT INTO `auth_permission` VALUES ('169', 'Can add Catálogo de Actividad', '57', 'add_actividadcatalogo');
INSERT INTO `auth_permission` VALUES ('170', 'Can change Catálogo de Actividad', '57', 'change_actividadcatalogo');
INSERT INTO `auth_permission` VALUES ('171', 'Can delete Catálogo de Actividad', '57', 'delete_actividadcatalogo');
INSERT INTO `auth_permission` VALUES ('172', 'Can add Catálogo de Sub Actividad', '58', 'add_subactividadcatalogo');
INSERT INTO `auth_permission` VALUES ('173', 'Can change Catálogo de Sub Actividad', '58', 'change_subactividadcatalogo');
INSERT INTO `auth_permission` VALUES ('174', 'Can delete Catálogo de Sub Actividad', '58', 'delete_subactividadcatalogo');
INSERT INTO `auth_permission` VALUES ('175', 'Can add Catálogo de Tarea', '59', 'add_tareacatalogo');
INSERT INTO `auth_permission` VALUES ('176', 'Can change Catálogo de Tarea', '59', 'change_tareacatalogo');
INSERT INTO `auth_permission` VALUES ('177', 'Can delete Catálogo de Tarea', '59', 'delete_tareacatalogo');
INSERT INTO `auth_permission` VALUES ('178', 'Can add Catálogo de Variedad', '60', 'add_variedadcatalogo');
INSERT INTO `auth_permission` VALUES ('179', 'Can change Catálogo de Variedad', '60', 'change_variedadcatalogo');
INSERT INTO `auth_permission` VALUES ('180', 'Can delete Catálogo de Variedad', '60', 'delete_variedadcatalogo');

-- ----------------------------
-- Table structure for auth_user
-- ----------------------------
DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of auth_user
-- ----------------------------
INSERT INTO `auth_user` VALUES ('1', 'pbkdf2_sha256$36000$lbGXM13UuG9l$7UQixWkA3bzwjuMsfwyhLoBbVkScys/8Ubjd97/UdqY=', '2017-07-29 22:44:23.561866', '1', 'admin', '', '', 'drac@drac.com', '1', '1', '2017-07-12 03:06:18.564000');
INSERT INTO `auth_user` VALUES ('2', 'pbkdf2_sha256$36000$hlecGPmZV6Os$pKT0ZKqBTLPN97/F/QLnwehzfvf9ARlx7O9mDE1HltA=', '2017-07-15 12:44:26.000000', '1', 'jparedes', '', '', '', '0', '1', '2017-07-12 03:12:33.000000');
INSERT INTO `auth_user` VALUES ('3', 'pbkdf2_sha256$36000$wKOPd5jaHL6K$258LXc5NyxHW6EHEUcOWtrWQJWGVfAwWXUlwgQlYO9w=', null, '1', 'charly', '', '', '', '0', '1', '2017-07-12 03:12:44.470000');
INSERT INTO `auth_user` VALUES ('4', 'pbkdf2_sha256$36000$zPuCZNlfLpfL$c68E5MxTdzosd+8z/Pw6ticLT9cVZ5LMmBhtR5vOnFY=', null, '1', 'ccalderon', '', '', '', '0', '1', '2017-07-12 03:12:53.000000');
INSERT INTO `auth_user` VALUES ('5', 'pbkdf2_sha256$36000$OPabnpx35Vto$B2BSE+rTMAytd4uPPyuCDj2a8EjahR45csBV2Hm7RIc=', null, '0', 'jperez', '', '', '', '0', '1', '2017-07-12 03:13:01.162000');
INSERT INTO `auth_user` VALUES ('6', 'pbkdf2_sha256$36000$vGHwfXjpC9KW$zen0FEbzTd1aR1AAKUO+BWyxGPHO07CqlnA5SQvZUWg=', null, '0', 'yoshi', '', '', '', '0', '1', '2017-07-24 22:37:52.959452');
INSERT INTO `auth_user` VALUES ('7', 'pbkdf2_sha256$36000$nJrhqCuMgTgd$L5p9vQYfNVhRzkLKRBcTdm+3/aqM7ere5U9J5g4F4b0=', null, '0', 'MKalifa', '', '', '', '0', '1', '2017-07-28 14:49:06.199545');

-- ----------------------------
-- Table structure for auth_user_groups
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_groups`;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of auth_user_groups
-- ----------------------------

-- ----------------------------
-- Table structure for auth_user_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_user_permissions`;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of auth_user_user_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for django_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `django_admin_log`;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of django_admin_log
-- ----------------------------
INSERT INTO `django_admin_log` VALUES ('1', '2017-07-12 03:12:33.384000', '2', 'jparejao', '1', '[{\"added\": {}}]', '4', '1');
INSERT INTO `django_admin_log` VALUES ('2', '2017-07-12 03:12:44.754000', '3', 'charly', '1', '[{\"added\": {}}]', '4', '1');
INSERT INTO `django_admin_log` VALUES ('3', '2017-07-12 03:12:53.610000', '4', 'cruiz', '1', '[{\"added\": {}}]', '4', '1');
INSERT INTO `django_admin_log` VALUES ('4', '2017-07-12 03:13:01.419000', '5', 'jperez', '1', '[{\"added\": {}}]', '4', '1');
INSERT INTO `django_admin_log` VALUES ('5', '2017-07-12 03:26:04.299000', '36', 'Bases', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('6', '2017-07-12 16:46:48.669000', '37', 'Especies', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('7', '2017-07-12 17:45:38.788000', '38', 'Razas', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('8', '2017-07-15 13:40:25.549000', '38', 'Razas', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('9', '2017-07-15 13:40:51.265000', '39', 'Enfermedades', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('10', '2017-07-16 14:52:47.938000', '1', 'Enfermedad 01 - Especie 01', '1', '[{\"added\": {}}]', '50', '1');
INSERT INTO `django_admin_log` VALUES ('11', '2017-07-16 14:52:56.360000', '2', 'Enfermedad 03 - Especie 01', '1', '[{\"added\": {}}]', '50', '1');
INSERT INTO `django_admin_log` VALUES ('12', '2017-07-16 17:05:18.621000', '2', 'Enfermedad 03 - Especie 01', '3', '', '50', '1');
INSERT INTO `django_admin_log` VALUES ('13', '2017-07-16 17:05:18.690000', '1', 'Enfermedad 01 - Especie 01', '3', '', '50', '1');
INSERT INTO `django_admin_log` VALUES ('14', '2017-07-16 17:24:44.514000', '3', 'Enfermedad 01 - Especie 01', '1', '[{\"added\": {}}]', '50', '1');
INSERT INTO `django_admin_log` VALUES ('15', '2017-07-20 14:57:34.912000', '40', 'Organizaciones', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('16', '2017-07-21 13:52:26.955000', '32', 'Acciones Estratégicas', '2', '[{\"changed\": {\"fields\": [\"ruta\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('17', '2017-07-21 15:58:39.787000', '1', 'Objetivo Específico 001', '1', '[{\"added\": {}}]', '31', '1');
INSERT INTO `django_admin_log` VALUES ('18', '2017-07-21 17:43:24.257000', '1', 'Acción Estratégica 001', '1', '[{\"added\": {}}]', '25', '1');
INSERT INTO `django_admin_log` VALUES ('19', '2017-07-22 14:46:45.666582', '2', 'jparedes', '2', '[{\"changed\": {\"fields\": [\"username\"]}}]', '4', '1');
INSERT INTO `django_admin_log` VALUES ('20', '2017-07-22 14:48:21.161881', '4', 'ccalderon', '2', '[{\"changed\": {\"fields\": [\"username\"]}}]', '4', '1');
INSERT INTO `django_admin_log` VALUES ('21', '2017-07-22 14:50:19.630406', '3', 'Cleopatro Calderon Tacilla', '2', '[{\"changed\": {\"fields\": [\"dni\", \"nombres\", \"apepat\", \"apemat\"]}}]', '19', '1');
INSERT INTO `django_admin_log` VALUES ('22', '2017-07-22 14:50:57.508412', '1', 'JOSE PAREDES CALLA', '2', '[{\"changed\": {\"fields\": [\"dni\", \"nombres\", \"apepat\", \"apemat\", \"fechanacimiento\"]}}]', '19', '1');
INSERT INTO `django_admin_log` VALUES ('23', '2017-07-22 15:20:07.989109', '26', 'Ejes', '2', '[{\"changed\": {\"fields\": [\"nombre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('24', '2017-07-22 15:20:29.484078', '21', 'PDRC', '2', '[{\"changed\": {\"fields\": [\"nombre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('25', '2017-07-22 15:26:29.877604', '21', 'PDRC', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('26', '2017-07-22 15:26:29.889663', '26', 'Ejes', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('27', '2017-07-22 15:39:47.168565', '28', 'Objetivos PDRC', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('28', '2017-07-22 15:41:04.675756', '41', 'Objetivos Específicos', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('29', '2017-07-22 15:44:02.188509', '20', 'Indicadores', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('30', '2017-07-22 15:44:02.204860', '22', 'Función', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('31', '2017-07-22 15:44:02.221811', '24', 'División Funcional', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('32', '2017-07-22 15:44:02.237350', '32', 'Acciones Estratégicas', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('33', '2017-07-22 15:44:22.946250', '17', 'Cadenas Productivas', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('34', '2017-07-22 22:37:38.814268', '42', 'Raza Socio', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('35', '2017-07-24 02:50:56.214778', '1', 'Desarrollo competitivo de cadenas de valor en la actividad agropecuaria, turismo y mineria', '2', '[{\"changed\": {\"fields\": [\"codigo\", \"descripcion\"]}}]', '31', '1');
INSERT INTO `django_admin_log` VALUES ('36', '2017-07-24 02:52:09.636042', '2', 'Desarrollo de la conectividad territorial e infraestructura productiva', '1', '[{\"added\": {}}]', '31', '1');
INSERT INTO `django_admin_log` VALUES ('37', '2017-07-24 02:53:34.557817', '3', 'Desarrollo de ámbitos rurales con poblaciones pobres', '1', '[{\"added\": {}}]', '31', '1');
INSERT INTO `django_admin_log` VALUES ('38', '2017-07-24 03:25:43.334304', '19', 'Grupos Funcionales', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('39', '2017-07-24 03:25:43.351478', '32', 'Acciones Estratégicas', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('40', '2017-07-24 03:47:31.127062', '19', 'Grupos Funcionales', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('41', '2017-07-24 03:47:31.141009', '20', 'Indicadores', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('42', '2017-07-24 03:47:31.154517', '22', 'Función', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('43', '2017-07-24 03:47:31.170452', '24', 'División Funcional', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('44', '2017-07-24 03:47:31.186438', '30', 'Metas Fisicas/Financieras', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('45', '2017-07-24 03:47:31.201571', '31', 'Actividades', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('46', '2017-07-24 03:47:31.222193', '17', 'Cadenas Productivas', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('47', '2017-07-24 03:47:58.677105', '32', 'Acciones Estratégicas', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('48', '2017-07-24 03:58:15.153929', '20', 'Indicadores', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('49', '2017-07-24 03:58:39.969275', '43', 'Programa Presupuestal', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('50', '2017-07-24 04:03:38.447152', '44', 'Producto Proyecto', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('51', '2017-07-24 04:09:56.590700', '45', 'Acción Estratégica Producto Proyecto', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('52', '2017-07-24 04:13:51.728171', '31', 'Actividades', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('53', '2017-07-24 04:23:14.086093', '30', 'Metas Fisicas/Financieras', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('54', '2017-07-24 04:23:44.159831', '46', 'Sub Actividades', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('55', '2017-07-24 04:25:42.954243', '46', 'Sub Actividades/Componentes', '2', '[{\"changed\": {\"fields\": [\"nombre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('56', '2017-07-24 04:28:19.260128', '30', 'Metas Fisicas/Financieras', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('57', '2017-07-24 04:28:19.276637', '19', 'Grupos Funcionales', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('58', '2017-07-24 04:28:19.290717', '22', 'Función', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('59', '2017-07-24 04:28:19.304344', '24', 'División Funcional', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('60', '2017-07-24 04:28:19.320092', '17', 'Cadenas Productivas', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('61', '2017-07-24 04:29:22.807628', '47', 'PDRC', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('62', '2017-07-24 04:30:00.362800', '16', 'Planificación', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('63', '2017-07-24 04:30:00.377933', '13', 'Agricola', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('64', '2017-07-24 04:31:35.556794', '31', 'Actividades', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('65', '2017-07-24 04:31:35.570058', '46', 'Sub Actividades/Componentes', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('66', '2017-07-24 04:31:35.591650', '24', 'División Funcional', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('67', '2017-07-24 04:31:35.605897', '22', 'Función', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('68', '2017-07-24 04:31:35.625280', '19', 'Grupos Funcionales', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('69', '2017-07-24 04:31:35.640212', '17', 'Cadenas Productivas', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('70', '2017-07-24 04:32:43.550962', '21', 'Plan', '2', '[{\"changed\": {\"fields\": [\"nombre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('71', '2017-07-24 04:33:04.817988', '21', 'Plan', '2', '[{\"changed\": {\"fields\": [\"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('72', '2017-07-24 04:33:57.748494', '18', 'Periodos', '2', '[{\"changed\": {\"fields\": [\"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('73', '2017-07-24 04:34:12.355165', '26', 'Ejes', '2', '[{\"changed\": {\"fields\": [\"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('74', '2017-07-24 04:34:49.265724', '21', 'PDRC Cajamarca', '2', '[{\"changed\": {\"fields\": [\"nombre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('75', '2017-07-24 04:35:37.596982', '28', 'Objetivos PDRC', '2', '[{\"changed\": {\"fields\": [\"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('76', '2017-07-24 04:36:39.960824', '28', 'Objetivos PDRC', '2', '[{\"changed\": {\"fields\": [\"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('77', '2017-07-24 04:36:59.422107', '41', 'Objetivos Específicos', '2', '[{\"changed\": {\"fields\": [\"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('78', '2017-07-24 04:37:26.664081', '32', 'Acciones Estratégicas', '2', '[{\"changed\": {\"fields\": [\"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('79', '2017-07-24 04:48:08.646091', '48', 'Tareas', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('80', '2017-07-24 04:50:01.610945', '14', 'Socios', '2', '[{\"changed\": {\"fields\": [\"nombre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('81', '2017-07-24 04:56:03.517671', '31', 'Actividades', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('82', '2017-07-24 04:56:03.532918', '46', 'Sub Actividades/Componentes', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('83', '2017-07-24 04:56:03.550278', '30', 'Metas Fisicas/Financieras', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('84', '2017-07-24 04:57:53.952918', '49', 'Acción Obra', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('85', '2017-07-24 04:58:50.744706', '24', 'División Funcional', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('86', '2017-07-24 04:58:50.759304', '22', 'Función', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('87', '2017-07-24 04:58:50.780494', '19', 'Grupos Funcionales', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('88', '2017-07-24 04:58:50.794870', '17', 'Cadenas Productivas', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('89', '2017-07-24 04:58:50.808774', '46', 'Sub Actividades/Componentes', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('90', '2017-07-24 05:02:32.112876', '11', 'Administración', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('91', '2017-07-24 05:02:32.129292', '47', 'PDRC', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('92', '2017-07-24 05:02:32.145212', '16', 'Planificación', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('93', '2017-07-24 05:02:32.170424', '13', 'Agricola', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('94', '2017-07-24 05:02:32.209696', '20', 'Indicadores', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('95', '2017-07-24 05:02:32.271901', '24', 'División Funcional', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('96', '2017-07-24 05:02:32.295228', '22', 'Función', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('97', '2017-07-24 05:02:32.329065', '19', 'Grupos Funcionales', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('98', '2017-07-24 05:02:32.344693', '17', 'Cadenas Productivas', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('99', '2017-07-24 05:02:32.377757', '46', 'Sub Actividades/Componentes', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('100', '2017-07-24 05:03:04.157316', '20', 'Indicadores', '2', '[{\"changed\": {\"fields\": [\"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('101', '2017-07-24 05:04:09.809301', '3', 'Meses', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('102', '2017-07-24 05:04:09.824154', '4', 'Departamentos', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('103', '2017-07-24 05:04:09.839845', '5', 'Provincias', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('104', '2017-07-24 05:04:09.855028', '6', 'Distritos', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('105', '2017-07-24 05:04:09.869071', '7', 'Centros Poblados', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('106', '2017-07-24 05:04:09.883069', '8', 'Caseríos', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('107', '2017-07-24 05:04:09.900514', '9', 'Profesiones', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('108', '2017-07-24 05:04:09.914464', '10', 'Especialidades', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('109', '2017-07-24 05:04:09.928941', '15', 'Tipo de Organización', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('110', '2017-07-24 05:09:27.702259', '14', 'Socios', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('111', '2017-07-24 05:09:27.721523', '25', 'Plagas', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('112', '2017-07-24 05:09:27.738976', '36', 'Bases', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('113', '2017-07-24 05:13:43.718788', '42', 'Razas por Socio', '2', '[{\"changed\": {\"fields\": [\"nombre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('114', '2017-07-24 05:15:27.499825', '39', 'Enfermedades', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('115', '2017-07-24 05:15:27.515099', '42', 'Razas por Socio', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('116', '2017-07-24 05:26:05.240322', '50', 'Empresas', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('117', '2017-07-24 05:26:35.259726', '51', 'Ventas', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('118', '2017-07-24 14:44:25.902902', '3', 'Meses', '2', '[{\"changed\": {\"fields\": [\"estado\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('119', '2017-07-24 14:45:13.785407', '4', 'Departamentos', '2', '[{\"changed\": {\"fields\": [\"estado\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('120', '2017-07-24 14:47:59.584551', '5', 'Provincias', '2', '[{\"changed\": {\"fields\": [\"estado\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('121', '2017-07-24 14:48:10.466288', '6', 'Distritos', '2', '[{\"changed\": {\"fields\": [\"estado\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('122', '2017-07-24 16:24:07.887878', '28', 'Objetivos Generales y Específicos', '2', '[{\"changed\": {\"fields\": [\"nombre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('123', '2017-07-24 16:24:07.902244', '41', 'Objetivos Específicos', '2', '[{\"changed\": {\"fields\": [\"estado\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('124', '2017-07-24 17:31:20.624848', '44', 'Producto Proyecto', '2', '[{\"changed\": {\"fields\": [\"ruta\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('125', '2017-07-24 20:28:33.650176', '45', 'Acción Estratégica Producto Proyecto', '2', '[{\"changed\": {\"fields\": [\"ruta\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('126', '2017-07-24 20:32:38.254159', '5', 'Organización', '1', '[{\"added\": {}}]', '8', '1');
INSERT INTO `django_admin_log` VALUES ('127', '2017-07-24 20:32:53.910782', '1', 'Fortalecimiento oraganizacional y mejoramiento de la gestión empresarial', '1', '[{\"added\": {}}]', '29', '1');
INSERT INTO `django_admin_log` VALUES ('128', '2017-07-24 20:33:26.645719', '1', 'Fortalecimiento organizacional y mejoramiento de la gestión empresarial', '2', '[{\"changed\": {\"fields\": [\"nombre\"]}}]', '29', '1');
INSERT INTO `django_admin_log` VALUES ('129', '2017-07-24 21:15:11.596391', '35', 'Personas', '2', '[{\"changed\": {\"fields\": [\"ruta\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('130', '2017-07-24 22:05:48.654793', '31', 'Actividades', '2', '[{\"changed\": {\"fields\": [\"descripcion\", \"ruta\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('131', '2017-07-24 22:54:31.133136', '1', 'Actividad 01', '1', '[{\"added\": {}}]', '42', '1');
INSERT INTO `django_admin_log` VALUES ('132', '2017-07-25 12:54:35.582107', '49', 'Acción Obra', '2', '[{\"changed\": {\"fields\": [\"ruta\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('133', '2017-07-25 14:32:03.405943', '28', 'Objetivos Generales', '2', '[{\"changed\": {\"fields\": [\"nombre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('134', '2017-07-25 14:32:03.431696', '41', 'Objetivos Específicos', '2', '[{\"changed\": {\"fields\": [\"ruta\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('135', '2017-07-25 14:33:03.839490', '41', 'Objetivos Específicos', '2', '[{\"changed\": {\"fields\": [\"estado\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('136', '2017-07-25 16:14:19.285287', '43', 'Programa Presupuestal', '2', '[{\"changed\": {\"fields\": [\"ruta\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('137', '2017-07-25 17:12:39.060308', '48', 'Tareas Cultivos', '2', '[{\"changed\": {\"fields\": [\"nombre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('138', '2017-07-25 22:19:18.427369', '48', 'Tareas', '2', '[{\"changed\": {\"fields\": [\"nombre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('139', '2017-07-25 22:19:18.444690', '43', 'Programa Presupuestal', '2', '[{\"changed\": {\"fields\": [\"ruta\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('140', '2017-07-25 22:19:18.457087', '31', 'Actividades', '2', '[{\"changed\": {\"fields\": [\"ruta\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('141', '2017-07-25 22:23:54.706560', '48', 'Tareas Cultivos', '2', '[{\"changed\": {\"fields\": [\"nombre\", \"ruta\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('142', '2017-07-25 22:34:13.398929', '43', 'Programa Presupuestal', '2', '[{\"changed\": {\"fields\": [\"ruta\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('143', '2017-07-27 15:28:43.470000', '3', 'Meses', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('144', '2017-07-27 15:28:43.544000', '4', 'Departamentos', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('145', '2017-07-27 15:28:43.611000', '5', 'Provincias', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('146', '2017-07-27 15:28:43.667000', '6', 'Distritos', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('147', '2017-07-27 15:28:43.721000', '7', 'Centros Poblados', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('148', '2017-07-27 15:28:43.777000', '8', 'Caseríos', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('149', '2017-07-27 15:28:43.850000', '9', 'Profesiones', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('150', '2017-07-27 15:28:43.905000', '10', 'Especialidades', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('151', '2017-07-27 15:28:43.961000', '15', 'Tipo de Organización', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('152', '2017-07-27 15:28:44.016000', '24', 'División Funcional', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('153', '2017-07-27 15:28:44.083000', '22', 'Función', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('154', '2017-07-27 15:28:44.154000', '19', 'Grupos Funcionales', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('155', '2017-07-27 15:29:14.226000', '24', 'División Funcional', '2', '[{\"changed\": {\"fields\": [\"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('156', '2017-07-27 15:29:38.465000', '22', 'Función', '2', '[{\"changed\": {\"fields\": [\"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('157', '2017-07-27 15:30:05.719000', '19', 'Grupos Funcionales', '2', '[{\"changed\": {\"fields\": [\"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('158', '2017-07-27 15:32:05.002000', '4', 'Departamentos', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('159', '2017-07-27 15:32:05.063000', '5', 'Provincias', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('160', '2017-07-27 15:32:05.202000', '6', 'Distritos', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('161', '2017-07-27 15:32:05.273000', '7', 'Centros Poblados', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('162', '2017-07-27 15:32:05.340000', '8', 'Caseríos', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('163', '2017-07-27 15:33:42.908000', '24', 'División Funcional', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('164', '2017-07-27 15:33:42.976000', '22', 'Función', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('165', '2017-07-27 15:33:43.047000', '19', 'Grupos Funcionales', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('166', '2017-07-27 15:33:43.113000', '43', 'Programa Presupuestal', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('167', '2017-07-27 15:34:03.764000', '24', 'División Funcional', '2', '[{\"changed\": {\"fields\": [\"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('168', '2017-07-27 15:34:20.840000', '22', 'Función', '2', '[{\"changed\": {\"fields\": [\"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('169', '2017-07-27 15:34:44.559000', '19', 'Grupos Funcionales', '2', '[{\"changed\": {\"fields\": [\"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('170', '2017-07-27 15:41:50.315000', '43', 'Programa Presupuestal', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('171', '2017-07-27 15:41:50.452000', '24', 'División Funcional', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('172', '2017-07-27 15:41:50.523000', '22', 'Función', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('173', '2017-07-27 15:41:50.591000', '19', 'Grupos Funcionales', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('174', '2017-07-27 15:47:08.932000', '52', 'Producto', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('175', '2017-07-27 15:47:25.570000', '53', 'Proyecto', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('176', '2017-07-27 15:49:36.337000', '44', 'Administrar', '2', '[{\"changed\": {\"fields\": [\"nombre\", \"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('177', '2017-07-27 15:49:36.420000', '49', 'Acción Obra', '2', '[{\"changed\": {\"fields\": [\"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('178', '2017-07-27 15:50:37.348000', '44', 'Productos', '2', '[{\"changed\": {\"fields\": [\"nombre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('179', '2017-07-27 15:50:37.497000', '49', 'Acciones', '2', '[{\"changed\": {\"fields\": [\"nombre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('180', '2017-07-27 15:51:06.715000', '11', 'Administración', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('181', '2017-07-27 15:51:06.821000', '47', 'PDRC', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('182', '2017-07-27 15:51:06.876000', '16', 'Planificación', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('183', '2017-07-27 15:51:07.020000', '13', 'Agricola', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('184', '2017-07-27 15:54:33.647000', '17', 'Cadenas Productivas', '2', '[{\"changed\": {\"fields\": [\"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('185', '2017-07-27 15:54:33.731000', '31', 'Actividades', '2', '[{\"changed\": {\"fields\": [\"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('186', '2017-07-27 15:54:33.787000', '46', 'Sub Actividades', '2', '[{\"changed\": {\"fields\": [\"nombre\", \"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('187', '2017-07-27 15:54:33.842000', '30', 'Programación Física Financiera', '2', '[{\"changed\": {\"fields\": [\"nombre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('188', '2017-07-27 15:56:41.896000', '54', 'Ejecución Física Financiera', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('189', '2017-07-27 15:57:12.509000', '45', 'Acción Estratégica Producto Proyecto', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('190', '2017-07-27 15:57:12.609000', '30', 'Programación Física Financiera', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('191', '2017-07-27 15:57:26.944000', '17', 'Cadenas Productivas', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('192', '2017-07-27 15:57:27.033000', '31', 'Actividades', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('193', '2017-07-27 15:57:27.127000', '46', 'Sub Actividades', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('194', '2017-07-27 15:58:27.121000', '55', 'Proyectos', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('195', '2017-07-27 15:58:57.627000', '48', 'Tareas', '2', '[{\"changed\": {\"fields\": [\"nombre\", \"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('196', '2017-07-27 15:59:22.054000', '56', 'Tareas', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('197', '2017-07-27 16:00:02.827000', '48', 'Tareas', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('198', '2017-07-27 16:00:02.900000', '44', 'Productos', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('199', '2017-07-27 16:00:02.989000', '49', 'Acciones', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('200', '2017-07-27 16:00:03.077000', '56', 'Tareas', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('201', '2017-07-27 16:01:13.506000', '43', 'Programas Presupuestales', '2', '[{\"changed\": {\"fields\": [\"nombre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('202', '2017-07-27 16:02:53.428000', '45', 'Alineación al PDRC', '2', '[{\"changed\": {\"fields\": [\"nombre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('203', '2017-07-27 16:03:37.167000', '45', 'Alineación al PDRC', '2', '[{\"changed\": {\"fields\": [\"descripcion\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('204', '2017-07-27 16:11:35.442000', '49', 'Acciones', '2', '[{\"changed\": {\"fields\": [\"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('205', '2017-07-27 16:11:35.514000', '48', 'Tareas', '2', '[{\"changed\": {\"fields\": [\"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('206', '2017-07-27 16:11:35.609000', '17', 'Cadenas Productivas', '2', '[{\"changed\": {\"fields\": [\"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('207', '2017-07-27 16:11:35.704000', '31', 'Actividades', '2', '[{\"changed\": {\"fields\": [\"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('208', '2017-07-27 16:11:35.804000', '46', 'Sub Actividades', '2', '[{\"changed\": {\"fields\": [\"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('209', '2017-07-27 16:11:35.869000', '56', 'Tareas', '2', '[{\"changed\": {\"fields\": [\"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('210', '2017-07-27 16:14:05.980000', '55', 'Proyectos', '2', '[{\"changed\": {\"fields\": [\"ruta\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('211', '2017-07-27 16:16:29.322000', '48', 'Tareas', '2', '[{\"changed\": {\"fields\": [\"padre\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('212', '2017-07-27 16:17:01.922000', '56', 'Tareas', '2', '[{\"changed\": {\"fields\": [\"ruta\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('213', '2017-07-27 16:17:02.027000', '48', 'Tareas', '2', '[{\"changed\": {\"fields\": [\"ruta\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('214', '2017-07-29 13:34:10.476000', '57', 'Catálogos', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('215', '2017-07-29 13:36:28.747000', '58', 'Actividades', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('216', '2017-07-29 13:37:11.594000', '58', 'Actividades', '2', '[{\"changed\": {\"fields\": [\"ruta\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('217', '2017-07-29 13:38:02.511000', '59', 'Sub Actividades', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('218', '2017-07-29 13:38:05.797000', '59', 'Sub Actividades', '2', '[]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('219', '2017-07-29 13:38:25.939000', '60', 'Tareas', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('220', '2017-07-30 16:07:04.184000', '15', 'Tipo de Organización', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('221', '2017-07-30 16:07:04.475000', '10', 'Especialidades', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('222', '2017-07-30 16:07:04.551000', '9', 'Profesiones', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('223', '2017-07-30 16:07:04.629000', '8', 'Caseríos', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('224', '2017-07-30 16:07:04.705000', '7', 'Centros Poblados', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('225', '2017-07-30 16:07:04.782000', '6', 'Distritos', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('226', '2017-07-30 16:07:04.848000', '5', 'Provincias', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('227', '2017-07-30 16:07:04.915000', '4', 'Departamentos', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('228', '2017-07-30 16:07:04.981000', '3', 'Meses', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('229', '2017-07-30 16:07:44.867000', '60', 'Catálogo de Tareas', '2', '[{\"changed\": {\"fields\": [\"nombre\", \"padre\", \"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('230', '2017-07-30 16:07:44.967000', '59', 'Catálogo de Sub Actividades', '2', '[{\"changed\": {\"fields\": [\"nombre\", \"padre\", \"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('231', '2017-07-30 16:07:45.056000', '58', 'Catálogo de Actividades', '2', '[{\"changed\": {\"fields\": [\"nombre\", \"padre\", \"orden\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('232', '2017-07-30 16:08:04.081000', '57', 'Catálogos', '3', '', '17', '1');
INSERT INTO `django_admin_log` VALUES ('233', '2017-07-30 16:08:44.659000', '61', 'Reportes', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('234', '2017-07-30 16:11:50.058000', '62', 'Variedades por Asociado', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('235', '2017-07-30 16:12:08.533000', '63', 'Variedades por Certificación y Lugar', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('236', '2017-07-30 16:12:20.024000', '64', 'Organizaciones Area, Rendimiento y Volumen', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('237', '2017-07-30 16:12:31.315000', '65', 'Socios Ingresos y Salidas', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('238', '2017-07-30 16:12:44.256000', '66', 'Objetivos por Mes', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('239', '2017-07-30 16:13:18.722000', '66', 'Objetivos por Mes', '2', '[{\"changed\": {\"fields\": [\"ruta\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('240', '2017-07-30 16:13:18.843000', '65', 'Socios Ingresos y Salidas', '2', '[{\"changed\": {\"fields\": [\"ruta\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('241', '2017-07-30 16:13:18.934000', '64', 'Organizaciones Area, Rendimiento y Volumen', '2', '[{\"changed\": {\"fields\": [\"ruta\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('242', '2017-07-30 16:13:19.111000', '63', 'Variedades por Certificación y Lugar', '2', '[{\"changed\": {\"fields\": [\"ruta\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('243', '2017-07-30 16:13:19.230000', '62', 'Variedades por Asociado', '2', '[{\"changed\": {\"fields\": [\"ruta\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('244', '2017-07-30 16:21:51.756000', '58', 'Catálogo de Actividades', '2', '[{\"changed\": {\"fields\": [\"separador\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('245', '2017-07-30 16:24:39.852000', '30', 'Programación Física Financiera', '2', '[{\"changed\": {\"fields\": [\"separador\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('246', '2017-07-30 16:54:22.176000', '48', 'Tareas', '2', '[{\"changed\": {\"fields\": [\"ruta\"]}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('247', '2017-07-30 17:29:08.157000', '67', 'Catálogo de Variedades', '1', '[{\"added\": {}}]', '17', '1');
INSERT INTO `django_admin_log` VALUES ('248', '2017-07-30 17:29:43.892000', '67', 'Catálogo de Variedades', '2', '[{\"changed\": {\"fields\": [\"orden\"]}}]', '17', '1');

-- ----------------------------
-- Table structure for django_content_type
-- ----------------------------
DROP TABLE IF EXISTS `django_content_type`;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of django_content_type
-- ----------------------------
INSERT INTO `django_content_type` VALUES ('1', 'admin', 'logentry');
INSERT INTO `django_content_type` VALUES ('20', 'administracion', 'acceso');
INSERT INTO `django_content_type` VALUES ('22', 'administracion', 'area');
INSERT INTO `django_content_type` VALUES ('23', 'administracion', 'funcioncargo');
INSERT INTO `django_content_type` VALUES ('17', 'administracion', 'menu');
INSERT INTO `django_content_type` VALUES ('21', 'administracion', 'organizacion');
INSERT INTO `django_content_type` VALUES ('19', 'administracion', 'persona');
INSERT INTO `django_content_type` VALUES ('18', 'administracion', 'trabajador');
INSERT INTO `django_content_type` VALUES ('47', 'agricola', 'base');
INSERT INTO `django_content_type` VALUES ('54', 'agricola', 'detalleventa');
INSERT INTO `django_content_type` VALUES ('55', 'agricola', 'empresa');
INSERT INTO `django_content_type` VALUES ('48', 'agricola', 'enfermedad');
INSERT INTO `django_content_type` VALUES ('50', 'agricola', 'enfermedadxespecie');
INSERT INTO `django_content_type` VALUES ('53', 'agricola', 'enfermedadxsocio');
INSERT INTO `django_content_type` VALUES ('46', 'agricola', 'especie');
INSERT INTO `django_content_type` VALUES ('51', 'agricola', 'plaga');
INSERT INTO `django_content_type` VALUES ('44', 'agricola', 'raza');
INSERT INTO `django_content_type` VALUES ('49', 'agricola', 'razaxsocio');
INSERT INTO `django_content_type` VALUES ('45', 'agricola', 'socio');
INSERT INTO `django_content_type` VALUES ('52', 'agricola', 'variedad');
INSERT INTO `django_content_type` VALUES ('60', 'agricola', 'variedadcatalogo');
INSERT INTO `django_content_type` VALUES ('56', 'agricola', 'venta');
INSERT INTO `django_content_type` VALUES ('2', 'auth', 'group');
INSERT INTO `django_content_type` VALUES ('3', 'auth', 'permission');
INSERT INTO `django_content_type` VALUES ('4', 'auth', 'user');
INSERT INTO `django_content_type` VALUES ('5', 'contenttypes', 'contenttype');
INSERT INTO `django_content_type` VALUES ('13', 'inicio', 'caserio');
INSERT INTO `django_content_type` VALUES ('10', 'inicio', 'centropoblado');
INSERT INTO `django_content_type` VALUES ('9', 'inicio', 'departamento');
INSERT INTO `django_content_type` VALUES ('14', 'inicio', 'distrito');
INSERT INTO `django_content_type` VALUES ('16', 'inicio', 'especialidad');
INSERT INTO `django_content_type` VALUES ('15', 'inicio', 'mes');
INSERT INTO `django_content_type` VALUES ('11', 'inicio', 'profesion');
INSERT INTO `django_content_type` VALUES ('7', 'inicio', 'provincia');
INSERT INTO `django_content_type` VALUES ('12', 'inicio', 'tipoorganizacion');
INSERT INTO `django_content_type` VALUES ('8', 'inicio', 'unidadmedida');
INSERT INTO `django_content_type` VALUES ('25', 'planificacion', 'accionestrategica');
INSERT INTO `django_content_type` VALUES ('24', 'planificacion', 'accionobra');
INSERT INTO `django_content_type` VALUES ('42', 'planificacion', 'actividad');
INSERT INTO `django_content_type` VALUES ('57', 'planificacion', 'actividadcatalogo');
INSERT INTO `django_content_type` VALUES ('39', 'planificacion', 'aexpop');
INSERT INTO `django_content_type` VALUES ('36', 'planificacion', 'cadenaproductiva');
INSERT INTO `django_content_type` VALUES ('30', 'planificacion', 'divisionfuncional');
INSERT INTO `django_content_type` VALUES ('38', 'planificacion', 'ejeecon');
INSERT INTO `django_content_type` VALUES ('40', 'planificacion', 'funcion');
INSERT INTO `django_content_type` VALUES ('26', 'planificacion', 'grupofuncional');
INSERT INTO `django_content_type` VALUES ('34', 'planificacion', 'indicador');
INSERT INTO `django_content_type` VALUES ('43', 'planificacion', 'metaff');
INSERT INTO `django_content_type` VALUES ('31', 'planificacion', 'objetivoespecifico');
INSERT INTO `django_content_type` VALUES ('28', 'planificacion', 'objetivopdrc');
INSERT INTO `django_content_type` VALUES ('37', 'planificacion', 'pdrc');
INSERT INTO `django_content_type` VALUES ('32', 'planificacion', 'periodo');
INSERT INTO `django_content_type` VALUES ('33', 'planificacion', 'productoproyecto');
INSERT INTO `django_content_type` VALUES ('29', 'planificacion', 'programapresupuestal');
INSERT INTO `django_content_type` VALUES ('58', 'planificacion', 'subactividadcatalogo');
INSERT INTO `django_content_type` VALUES ('27', 'planificacion', 'subactividadcomponente');
INSERT INTO `django_content_type` VALUES ('59', 'planificacion', 'tareacatalogo');
INSERT INTO `django_content_type` VALUES ('35', 'planificacion', 'tareacpcr');
INSERT INTO `django_content_type` VALUES ('41', 'planificacion', 'tareacpcu');
INSERT INTO `django_content_type` VALUES ('6', 'sessions', 'session');

-- ----------------------------
-- Table structure for django_migrations
-- ----------------------------
DROP TABLE IF EXISTS `django_migrations`;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of django_migrations
-- ----------------------------
INSERT INTO `django_migrations` VALUES ('1', 'contenttypes', '0001_initial', '2017-07-12 02:54:37.378000');
INSERT INTO `django_migrations` VALUES ('2', 'auth', '0001_initial', '2017-07-12 02:54:48.153000');
INSERT INTO `django_migrations` VALUES ('3', 'admin', '0001_initial', '2017-07-12 02:54:50.175000');
INSERT INTO `django_migrations` VALUES ('4', 'admin', '0002_logentry_remove_auto_add', '2017-07-12 02:54:50.258000');
INSERT INTO `django_migrations` VALUES ('5', 'inicio', '0001_initial', '2017-07-12 02:55:26.283000');
INSERT INTO `django_migrations` VALUES ('6', 'administracion', '0001_initial', '2017-07-12 02:56:01.877000');
INSERT INTO `django_migrations` VALUES ('7', 'agricola', '0001_initial', '2017-07-12 02:56:50.299000');
INSERT INTO `django_migrations` VALUES ('8', 'contenttypes', '0002_remove_content_type_name', '2017-07-12 02:56:52.107000');
INSERT INTO `django_migrations` VALUES ('9', 'auth', '0002_alter_permission_name_max_length', '2017-07-12 02:56:53.150000');
INSERT INTO `django_migrations` VALUES ('10', 'auth', '0003_alter_user_email_max_length', '2017-07-12 02:56:55.016000');
INSERT INTO `django_migrations` VALUES ('11', 'auth', '0004_alter_user_username_opts', '2017-07-12 02:56:55.442000');
INSERT INTO `django_migrations` VALUES ('12', 'auth', '0005_alter_user_last_login_null', '2017-07-12 02:56:56.526000');
INSERT INTO `django_migrations` VALUES ('13', 'auth', '0006_require_contenttypes_0002', '2017-07-12 02:56:56.598000');
INSERT INTO `django_migrations` VALUES ('14', 'auth', '0007_alter_validators_add_error_messages', '2017-07-12 02:56:56.888000');
INSERT INTO `django_migrations` VALUES ('15', 'auth', '0008_alter_user_username_max_length', '2017-07-12 02:56:57.885000');
INSERT INTO `django_migrations` VALUES ('16', 'planificacion', '0001_initial', '2017-07-12 02:59:31.173000');
INSERT INTO `django_migrations` VALUES ('17', 'sessions', '0001_initial', '2017-07-12 02:59:32.659000');
INSERT INTO `django_migrations` VALUES ('18', 'agricola', '0002_auto_20170712_1130', '2017-07-12 16:31:20.305000');
INSERT INTO `django_migrations` VALUES ('19', 'agricola', '0003_auto_20170719_1713', '2017-07-19 22:13:46.801000');
INSERT INTO `django_migrations` VALUES ('20', 'planificacion', '0002_auto_20170719_1713', '2017-07-19 22:14:02.356000');
INSERT INTO `django_migrations` VALUES ('21', 'planificacion', '0003_cadenaproductiva_tipo', '2017-07-20 13:16:12.140000');
INSERT INTO `django_migrations` VALUES ('22', 'agricola', '0004_auto_20170720_0834', '2017-07-20 13:34:48.163000');
INSERT INTO `django_migrations` VALUES ('23', 'administracion', '0002_menu_estado', '2017-07-20 16:45:25.789000');
INSERT INTO `django_migrations` VALUES ('24', 'agricola', '0005_auto_20170720_1145', '2017-07-20 16:45:30.951000');
INSERT INTO `django_migrations` VALUES ('25', 'agricola', '0006_auto_20170720_1206', '2017-07-20 17:07:07.601000');
INSERT INTO `django_migrations` VALUES ('26', 'agricola', '0007_remove_variedad_posesion', '2017-07-20 17:07:09.554000');
INSERT INTO `django_migrations` VALUES ('27', 'administracion', '0003_auto_20170725_1128', '2017-07-25 16:37:57.986000');
INSERT INTO `django_migrations` VALUES ('28', 'administracion', '0004_area_esagencia', '2017-07-25 16:37:58.946000');
INSERT INTO `django_migrations` VALUES ('29', 'agricola', '0008_auto_20170725_1128', '2017-07-25 16:37:59.438000');
INSERT INTO `django_migrations` VALUES ('30', 'planificacion', '0004_auto_20170725_1128', '2017-07-25 16:38:00.458000');
INSERT INTO `django_migrations` VALUES ('31', 'planificacion', '0005_auto_20170725_1613', '2017-07-25 22:09:07.457000');
INSERT INTO `django_migrations` VALUES ('32', 'administracion', '0005_remove_trabajador_fechacaducidad', '2017-07-27 17:50:03.162000');
INSERT INTO `django_migrations` VALUES ('33', 'administracion', '0006_remove_organizacion_codigo', '2017-07-27 17:50:04.577000');
INSERT INTO `django_migrations` VALUES ('34', 'planificacion', '0006_auto_20170726_2236', '2017-07-27 17:50:08.291000');
INSERT INTO `django_migrations` VALUES ('35', 'planificacion', '0007_auto_20170727_1019', '2017-07-27 17:50:31.924000');
INSERT INTO `django_migrations` VALUES ('36', 'planificacion', '0008_auto_20170728_2202', '2017-07-29 04:18:44.775000');
INSERT INTO `django_migrations` VALUES ('37', 'planificacion', '0009_actividadcatalogo_subactividadcatalogo_tareacatalogo', '2017-07-29 04:18:53.444000');
INSERT INTO `django_migrations` VALUES ('38', 'planificacion', '0010_auto_20170728_2226', '2017-07-29 04:18:53.444000');
INSERT INTO `django_migrations` VALUES ('39', 'planificacion', '0011_auto_20170730_1012', '2017-07-30 15:12:58.417000');
INSERT INTO `django_migrations` VALUES ('40', 'planificacion', '0012_actividad_anio', '2017-07-30 15:32:06.139000');
INSERT INTO `django_migrations` VALUES ('41', 'planificacion', '0013_actividadcatalogo_codigo', '2017-07-30 15:35:20.167000');
INSERT INTO `django_migrations` VALUES ('42', 'planificacion', '0014_auto_20170730_1035', '2017-07-30 15:36:00.996000');
INSERT INTO `django_migrations` VALUES ('43', 'planificacion', '0015_auto_20170730_1050', '2017-07-30 15:50:18.703000');
INSERT INTO `django_migrations` VALUES ('44', 'administracion', '0007_menu_separador', '2017-07-30 16:20:43.324000');
INSERT INTO `django_migrations` VALUES ('45', 'planificacion', '0016_auto_20170730_1144', '2017-07-30 16:44:32.019000');
INSERT INTO `django_migrations` VALUES ('46', 'agricola', '0009_variedadcatalogo', '2017-07-30 17:14:58.634000');
INSERT INTO `django_migrations` VALUES ('47', 'agricola', '0010_auto_20170730_1220', '2017-07-30 17:20:47.502000');

-- ----------------------------
-- Table structure for django_session
-- ----------------------------
DROP TABLE IF EXISTS `django_session`;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of django_session
-- ----------------------------
INSERT INTO `django_session` VALUES ('0642cx1h853nwb4z2dnvzbze4y43jn0s', 'ZTI0ODYzZjEzZDFlNmRiYmFiZjVjMGY2MWNiY2IwZWE2M2ViNjI0Zjp7Il9hdXRoX3VzZXJfaGFzaCI6ImMyYjM4YjlkYWZjZmNjYzQwYjRkNTFiZmM2ZWJmZDI2ZmQzYWZmMGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-08-03 12:58:05.231000');
INSERT INTO `django_session` VALUES ('5vl4leaece48tw8vmfiw8oj1ma2c2dbr', 'ZTI0ODYzZjEzZDFlNmRiYmFiZjVjMGY2MWNiY2IwZWE2M2ViNjI0Zjp7Il9hdXRoX3VzZXJfaGFzaCI6ImMyYjM4YjlkYWZjZmNjYzQwYjRkNTFiZmM2ZWJmZDI2ZmQzYWZmMGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-08-05 21:39:26.674355');
INSERT INTO `django_session` VALUES ('7kmjc6vy2rgcc9umvr9ipd22po5zx38f', 'ZTI0ODYzZjEzZDFlNmRiYmFiZjVjMGY2MWNiY2IwZWE2M2ViNjI0Zjp7Il9hdXRoX3VzZXJfaGFzaCI6ImMyYjM4YjlkYWZjZmNjYzQwYjRkNTFiZmM2ZWJmZDI2ZmQzYWZmMGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-08-12 18:38:40.602161');
INSERT INTO `django_session` VALUES ('8e6e90qqz4ur6327nnf4xce4x5p0eke6', 'ZTI0ODYzZjEzZDFlNmRiYmFiZjVjMGY2MWNiY2IwZWE2M2ViNjI0Zjp7Il9hdXRoX3VzZXJfaGFzaCI6ImMyYjM4YjlkYWZjZmNjYzQwYjRkNTFiZmM2ZWJmZDI2ZmQzYWZmMGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-08-11 16:19:56.818580');
INSERT INTO `django_session` VALUES ('8mtuof1uflken5m0pp6ai8x6j7fj93rv', 'MDI0OTFlYzA5YTkyM2Y2OTZiYmRhOGVjYTA5NGM0NDYzOWNjNzM1OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjdlNzlhM2M0NWJlNDVhZmZhMTZmMTA3OGY0MTJlMjJjMWJmYTUyNmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=', '2017-07-26 03:13:55.729000');
INSERT INTO `django_session` VALUES ('8uw7t7ua8p2ut8ub75rcqcdervswlwb5', 'ZTI0ODYzZjEzZDFlNmRiYmFiZjVjMGY2MWNiY2IwZWE2M2ViNjI0Zjp7Il9hdXRoX3VzZXJfaGFzaCI6ImMyYjM4YjlkYWZjZmNjYzQwYjRkNTFiZmM2ZWJmZDI2ZmQzYWZmMGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-08-05 14:41:46.501251');
INSERT INTO `django_session` VALUES ('9h6ayfoo8a42mbjbe0wcsbl932smk22y', 'ZTI0ODYzZjEzZDFlNmRiYmFiZjVjMGY2MWNiY2IwZWE2M2ViNjI0Zjp7Il9hdXRoX3VzZXJfaGFzaCI6ImMyYjM4YjlkYWZjZmNjYzQwYjRkNTFiZmM2ZWJmZDI2ZmQzYWZmMGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-08-07 23:28:52.794247');
INSERT INTO `django_session` VALUES ('a9gxxhft8g4nwyrt49py4uc74cbd0jf7', 'ZTI0ODYzZjEzZDFlNmRiYmFiZjVjMGY2MWNiY2IwZWE2M2ViNjI0Zjp7Il9hdXRoX3VzZXJfaGFzaCI6ImMyYjM4YjlkYWZjZmNjYzQwYjRkNTFiZmM2ZWJmZDI2ZmQzYWZmMGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-08-10 14:22:55.581652');
INSERT INTO `django_session` VALUES ('di49adrz79dublklvxxsx32mrdt2n6oy', 'ZTI0ODYzZjEzZDFlNmRiYmFiZjVjMGY2MWNiY2IwZWE2M2ViNjI0Zjp7Il9hdXRoX3VzZXJfaGFzaCI6ImMyYjM4YjlkYWZjZmNjYzQwYjRkNTFiZmM2ZWJmZDI2ZmQzYWZmMGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-08-05 21:34:56.751243');
INSERT INTO `django_session` VALUES ('dtz94wa40e1710bt2kelwlnv5a3s26ol', 'ZTI0ODYzZjEzZDFlNmRiYmFiZjVjMGY2MWNiY2IwZWE2M2ViNjI0Zjp7Il9hdXRoX3VzZXJfaGFzaCI6ImMyYjM4YjlkYWZjZmNjYzQwYjRkNTFiZmM2ZWJmZDI2ZmQzYWZmMGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-08-03 12:49:16.723000');
INSERT INTO `django_session` VALUES ('hl7ydxk9l9zihptofxjlye9zw48304kp', 'ZTI0ODYzZjEzZDFlNmRiYmFiZjVjMGY2MWNiY2IwZWE2M2ViNjI0Zjp7Il9hdXRoX3VzZXJfaGFzaCI6ImMyYjM4YjlkYWZjZmNjYzQwYjRkNTFiZmM2ZWJmZDI2ZmQzYWZmMGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-08-10 16:21:52.198000');
INSERT INTO `django_session` VALUES ('is0efapaoda3hn3o5fhgew39yw6kb8ys', 'ZTI0ODYzZjEzZDFlNmRiYmFiZjVjMGY2MWNiY2IwZWE2M2ViNjI0Zjp7Il9hdXRoX3VzZXJfaGFzaCI6ImMyYjM4YjlkYWZjZmNjYzQwYjRkNTFiZmM2ZWJmZDI2ZmQzYWZmMGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-08-08 19:56:21.210855');
INSERT INTO `django_session` VALUES ('kub94l3deyyawvccdbnjl83bzxx1avs6', 'ZTI0ODYzZjEzZDFlNmRiYmFiZjVjMGY2MWNiY2IwZWE2M2ViNjI0Zjp7Il9hdXRoX3VzZXJfaGFzaCI6ImMyYjM4YjlkYWZjZmNjYzQwYjRkNTFiZmM2ZWJmZDI2ZmQzYWZmMGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-08-05 21:45:11.162047');
INSERT INTO `django_session` VALUES ('m66nt4gizo148kqpihd4to1q0b6r799n', 'ZTI0ODYzZjEzZDFlNmRiYmFiZjVjMGY2MWNiY2IwZWE2M2ViNjI0Zjp7Il9hdXRoX3VzZXJfaGFzaCI6ImMyYjM4YjlkYWZjZmNjYzQwYjRkNTFiZmM2ZWJmZDI2ZmQzYWZmMGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-08-10 15:26:12.428000');
INSERT INTO `django_session` VALUES ('nb6rtmkqpvgv290g2rfmc9yqq8v6f760', 'ZTI0ODYzZjEzZDFlNmRiYmFiZjVjMGY2MWNiY2IwZWE2M2ViNjI0Zjp7Il9hdXRoX3VzZXJfaGFzaCI6ImMyYjM4YjlkYWZjZmNjYzQwYjRkNTFiZmM2ZWJmZDI2ZmQzYWZmMGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-08-09 18:47:37.831964');
INSERT INTO `django_session` VALUES ('q2zd2k5zbah6y840n5bm00x2uopswkbu', 'ZTI0ODYzZjEzZDFlNmRiYmFiZjVjMGY2MWNiY2IwZWE2M2ViNjI0Zjp7Il9hdXRoX3VzZXJfaGFzaCI6ImMyYjM4YjlkYWZjZmNjYzQwYjRkNTFiZmM2ZWJmZDI2ZmQzYWZmMGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-08-07 14:28:07.176585');
INSERT INTO `django_session` VALUES ('rokjrwdvm5pi80jz17nia0yrsmb2v1eu', 'MDI0OTFlYzA5YTkyM2Y2OTZiYmRhOGVjYTA5NGM0NDYzOWNjNzM1OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjdlNzlhM2M0NWJlNDVhZmZhMTZmMTA3OGY0MTJlMjJjMWJmYTUyNmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=', '2017-07-29 12:44:26.621000');
INSERT INTO `django_session` VALUES ('rv891kv2u5w2rmtb1jkrsq17slk2e1qr', 'ZTI0ODYzZjEzZDFlNmRiYmFiZjVjMGY2MWNiY2IwZWE2M2ViNjI0Zjp7Il9hdXRoX3VzZXJfaGFzaCI6ImMyYjM4YjlkYWZjZmNjYzQwYjRkNTFiZmM2ZWJmZDI2ZmQzYWZmMGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-08-11 00:25:39.406209');
INSERT INTO `django_session` VALUES ('slbp5b931rvjpqzbt36wpe48b0ttshm4', 'ZTI0ODYzZjEzZDFlNmRiYmFiZjVjMGY2MWNiY2IwZWE2M2ViNjI0Zjp7Il9hdXRoX3VzZXJfaGFzaCI6ImMyYjM4YjlkYWZjZmNjYzQwYjRkNTFiZmM2ZWJmZDI2ZmQzYWZmMGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-07-26 03:12:18.790000');
INSERT INTO `django_session` VALUES ('tv1drr50ddma8wx0noi5pg8ueq395d2m', 'ZTI0ODYzZjEzZDFlNmRiYmFiZjVjMGY2MWNiY2IwZWE2M2ViNjI0Zjp7Il9hdXRoX3VzZXJfaGFzaCI6ImMyYjM4YjlkYWZjZmNjYzQwYjRkNTFiZmM2ZWJmZDI2ZmQzYWZmMGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-08-12 22:44:23.568712');
INSERT INTO `django_session` VALUES ('zkfu616ntvageivuawrlmb1jlv0bxuki', 'ZTI0ODYzZjEzZDFlNmRiYmFiZjVjMGY2MWNiY2IwZWE2M2ViNjI0Zjp7Il9hdXRoX3VzZXJfaGFzaCI6ImMyYjM4YjlkYWZjZmNjYzQwYjRkNTFiZmM2ZWJmZDI2ZmQzYWZmMGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-08-04 23:01:26.896661');

-- ----------------------------
-- Table structure for inicio_caserio
-- ----------------------------
DROP TABLE IF EXISTS `inicio_caserio`;
CREATE TABLE `inicio_caserio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `codigo` varchar(12) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `centropoblado_id` int(11) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo` (`codigo`),
  KEY `inicio_caserio_centropoblado_id_01a1bac4_fk_inicio_ce` (`centropoblado_id`),
  KEY `inicio_caserio_creador_id_ba2842ed_fk_auth_user_id` (`creador_id`),
  KEY `inicio_caserio_editor_id_67487a4a_fk_auth_user_id` (`editor_id`),
  CONSTRAINT `inicio_caserio_centropoblado_id_01a1bac4_fk_inicio_ce` FOREIGN KEY (`centropoblado_id`) REFERENCES `inicio_centropoblado` (`id`),
  CONSTRAINT `inicio_caserio_creador_id_ba2842ed_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `inicio_caserio_editor_id_67487a4a_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of inicio_caserio
-- ----------------------------
INSERT INTO `inicio_caserio` VALUES ('1', '2017-06-21 18:34:00.000000', '2017-07-28 14:20:02.289357', '0601010105', 'Caserío 01', '1', '1', '1');
INSERT INTO `inicio_caserio` VALUES ('2', '2017-07-24 22:30:25.710481', '2017-07-24 22:30:25.710530', '06011201', 'Nuevo Caserío', '2', '1', null);
INSERT INTO `inicio_caserio` VALUES ('3', '2017-07-24 22:30:38.575158', '2017-07-24 22:30:38.575203', '06011202', 'Otro Caserío', '2', '1', null);

-- ----------------------------
-- Table structure for inicio_centropoblado
-- ----------------------------
DROP TABLE IF EXISTS `inicio_centropoblado`;
CREATE TABLE `inicio_centropoblado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `area` varchar(15) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `distrito_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo` (`codigo`),
  KEY `inicio_centropoblado_creador_id_92b97afb_fk_auth_user_id` (`creador_id`),
  KEY `inicio_centropoblado_distrito_id_09605b84_fk_inicio_distrito_id` (`distrito_id`),
  KEY `inicio_centropoblado_editor_id_7ebe1232_fk_auth_user_id` (`editor_id`),
  CONSTRAINT `inicio_centropoblado_creador_id_92b97afb_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `inicio_centropoblado_distrito_id_09605b84_fk_inicio_distrito_id` FOREIGN KEY (`distrito_id`) REFERENCES `inicio_distrito` (`id`),
  CONSTRAINT `inicio_centropoblado_editor_id_7ebe1232_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of inicio_centropoblado
-- ----------------------------
INSERT INTO `inicio_centropoblado` VALUES ('1', '2017-06-21 18:33:40.000000', '2017-06-27 15:16:26.000000', '06010101', 'Cajamarca', 'Urbano', '1', '1', '1');
INSERT INTO `inicio_centropoblado` VALUES ('2', '2017-07-24 22:27:03.535758', '2017-07-24 22:27:03.535812', '06011201', 'Fundo los Milagros', '1234', '1', '554', null);
INSERT INTO `inicio_centropoblado` VALUES ('3', '2017-07-24 22:28:08.584680', '2017-07-24 22:28:08.584726', '06011202', 'F&M', '900', '1', '554', null);
INSERT INTO `inicio_centropoblado` VALUES ('4', '2017-07-24 22:28:23.323687', '2017-07-24 22:28:23.323727', '06011205', 'Nuevo CP', '789', '1', '554', null);
INSERT INTO `inicio_centropoblado` VALUES ('9', '2017-07-26 15:53:59.681481', '2017-07-26 15:55:54.730135', '01010101', 'q', 'q', '1', '1', '1');
INSERT INTO `inicio_centropoblado` VALUES ('10', '2017-07-28 14:15:03.643846', '2017-07-28 14:15:03.643887', '06010103', 'Nuevo Centro Poblado', '125', '1', '1', null);
INSERT INTO `inicio_centropoblado` VALUES ('11', '2017-07-28 14:15:29.960654', '2017-07-28 14:15:29.960689', '06010102', 'otro centro', '1254', '1', '1', null);

-- ----------------------------
-- Table structure for inicio_departamento
-- ----------------------------
DROP TABLE IF EXISTS `inicio_departamento`;
CREATE TABLE `inicio_departamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `codigo` varchar(2) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo` (`codigo`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `inicio_departamento_creador_id_02249440_fk_auth_user_id` (`creador_id`),
  KEY `inicio_departamento_editor_id_1acdd39b_fk_auth_user_id` (`editor_id`),
  CONSTRAINT `inicio_departamento_creador_id_02249440_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `inicio_departamento_editor_id_1acdd39b_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of inicio_departamento
-- ----------------------------
INSERT INTO `inicio_departamento` VALUES ('1', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '06', 'Cajamarca', '1', null);
INSERT INTO `inicio_departamento` VALUES ('2', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '01', 'Amazonas', '1', null);
INSERT INTO `inicio_departamento` VALUES ('3', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '02', 'Ancash', '1', null);
INSERT INTO `inicio_departamento` VALUES ('4', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '03', 'Apurimac', '1', null);
INSERT INTO `inicio_departamento` VALUES ('5', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '04', 'Arequipa', '1', null);
INSERT INTO `inicio_departamento` VALUES ('6', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '05', 'Ayacucho', '1', null);
INSERT INTO `inicio_departamento` VALUES ('7', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '07', 'Callao', '1', null);
INSERT INTO `inicio_departamento` VALUES ('8', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '08', 'Cusco', '1', null);
INSERT INTO `inicio_departamento` VALUES ('9', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '09', 'Huancavelica', '1', null);
INSERT INTO `inicio_departamento` VALUES ('10', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '10', 'Huanuco', '1', null);
INSERT INTO `inicio_departamento` VALUES ('11', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '11', 'Ica', '1', null);
INSERT INTO `inicio_departamento` VALUES ('12', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '12', 'Junin', '1', null);
INSERT INTO `inicio_departamento` VALUES ('13', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '13', 'La Libertad', '1', null);
INSERT INTO `inicio_departamento` VALUES ('14', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '14', 'Lambayeque', '1', null);
INSERT INTO `inicio_departamento` VALUES ('15', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '15', 'Lima', '1', null);
INSERT INTO `inicio_departamento` VALUES ('16', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '16', 'Loreto', '1', null);
INSERT INTO `inicio_departamento` VALUES ('17', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '17', 'Madre de Dios', '1', null);
INSERT INTO `inicio_departamento` VALUES ('18', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '18', 'Moquegua', '1', null);
INSERT INTO `inicio_departamento` VALUES ('19', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '19', 'Pasco', '1', null);
INSERT INTO `inicio_departamento` VALUES ('20', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '20', 'Piura', '1', null);
INSERT INTO `inicio_departamento` VALUES ('21', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '21', 'Puno', '1', null);
INSERT INTO `inicio_departamento` VALUES ('22', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '22', 'San Martin', '1', null);
INSERT INTO `inicio_departamento` VALUES ('23', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '23', 'Tacna', '1', null);
INSERT INTO `inicio_departamento` VALUES ('24', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '24', 'Tumbes', '1', null);
INSERT INTO `inicio_departamento` VALUES ('25', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '25', 'Ucayali', '1', null);

-- ----------------------------
-- Table structure for inicio_distrito
-- ----------------------------
DROP TABLE IF EXISTS `inicio_distrito`;
CREATE TABLE `inicio_distrito` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `codigo` varchar(6) NOT NULL,
  `nombre` varchar(75) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `provincia_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo` (`codigo`),
  KEY `inicio_distrito_creador_id_4a0317a9_fk_auth_user_id` (`creador_id`),
  KEY `inicio_distrito_editor_id_41ac45d8_fk_auth_user_id` (`editor_id`),
  KEY `inicio_distrito_provincia_id_0e500876_fk_inicio_provincia_id` (`provincia_id`),
  CONSTRAINT `inicio_distrito_creador_id_4a0317a9_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `inicio_distrito_editor_id_41ac45d8_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `inicio_distrito_provincia_id_0e500876_fk_inicio_provincia_id` FOREIGN KEY (`provincia_id`) REFERENCES `inicio_provincia` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1848 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of inicio_distrito
-- ----------------------------
INSERT INTO `inicio_distrito` VALUES ('1', '2017-06-21 18:33:22.000000', '2017-06-21 18:33:22.000000', '060101', 'Cajamarca', '1', null, '1');
INSERT INTO `inicio_distrito` VALUES ('2', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010101', 'Chachapoyas', '1', null, '2');
INSERT INTO `inicio_distrito` VALUES ('3', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010102', 'Asuncion', '1', null, '2');
INSERT INTO `inicio_distrito` VALUES ('4', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010103', 'Balsas', '1', null, '2');
INSERT INTO `inicio_distrito` VALUES ('5', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010104', 'Cheto', '1', null, '2');
INSERT INTO `inicio_distrito` VALUES ('6', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010105', 'Chiliquin', '1', null, '2');
INSERT INTO `inicio_distrito` VALUES ('7', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010106', 'Chuquibamba', '1', null, '2');
INSERT INTO `inicio_distrito` VALUES ('8', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010107', 'Granada', '1', null, '2');
INSERT INTO `inicio_distrito` VALUES ('9', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010108', 'Huancas', '1', null, '2');
INSERT INTO `inicio_distrito` VALUES ('10', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010109', 'La Jalca', '1', null, '2');
INSERT INTO `inicio_distrito` VALUES ('11', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010110', 'Leimebamba', '1', null, '2');
INSERT INTO `inicio_distrito` VALUES ('12', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010111', 'Levanto', '1', null, '2');
INSERT INTO `inicio_distrito` VALUES ('13', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010112', 'Magdalena', '1', null, '2');
INSERT INTO `inicio_distrito` VALUES ('14', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010113', 'Mariscal Castilla', '1', null, '2');
INSERT INTO `inicio_distrito` VALUES ('15', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010114', 'Molinopampa', '1', null, '2');
INSERT INTO `inicio_distrito` VALUES ('16', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010115', 'Montevideo', '1', null, '2');
INSERT INTO `inicio_distrito` VALUES ('17', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010116', 'Olleros', '1', null, '2');
INSERT INTO `inicio_distrito` VALUES ('18', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010117', 'Quinjalca', '1', null, '2');
INSERT INTO `inicio_distrito` VALUES ('19', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010118', 'San Francisco de Daguas', '1', null, '2');
INSERT INTO `inicio_distrito` VALUES ('20', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010119', 'San Isidro de Maino', '1', null, '2');
INSERT INTO `inicio_distrito` VALUES ('21', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010120', 'Soloco', '1', null, '2');
INSERT INTO `inicio_distrito` VALUES ('22', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010121', 'Sonche', '1', null, '2');
INSERT INTO `inicio_distrito` VALUES ('23', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010201', 'La Peca', '1', null, '3');
INSERT INTO `inicio_distrito` VALUES ('24', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010202', 'Aramango', '1', null, '3');
INSERT INTO `inicio_distrito` VALUES ('25', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010203', 'Copallin', '1', null, '3');
INSERT INTO `inicio_distrito` VALUES ('26', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010204', 'El Parco', '1', null, '3');
INSERT INTO `inicio_distrito` VALUES ('27', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010205', 'Imaza', '1', null, '3');
INSERT INTO `inicio_distrito` VALUES ('28', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010301', 'Jumbilla', '1', null, '4');
INSERT INTO `inicio_distrito` VALUES ('29', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010302', 'Corosha', '1', null, '4');
INSERT INTO `inicio_distrito` VALUES ('30', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010303', 'Cuispes', '1', null, '4');
INSERT INTO `inicio_distrito` VALUES ('31', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010304', 'Chisquilla', '1', null, '4');
INSERT INTO `inicio_distrito` VALUES ('32', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010305', 'Churuja', '1', null, '4');
INSERT INTO `inicio_distrito` VALUES ('33', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010306', 'Florida', '1', null, '4');
INSERT INTO `inicio_distrito` VALUES ('34', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010307', 'Recta', '1', null, '4');
INSERT INTO `inicio_distrito` VALUES ('35', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010308', 'San Carlos', '1', null, '4');
INSERT INTO `inicio_distrito` VALUES ('36', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010309', 'Shipasbamba', '1', null, '4');
INSERT INTO `inicio_distrito` VALUES ('37', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010310', 'Valera', '1', null, '4');
INSERT INTO `inicio_distrito` VALUES ('38', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010311', 'Yambrasbamba', '1', null, '4');
INSERT INTO `inicio_distrito` VALUES ('39', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010312', 'Jazan', '1', null, '4');
INSERT INTO `inicio_distrito` VALUES ('40', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010401', 'Nieva', '1', null, '5');
INSERT INTO `inicio_distrito` VALUES ('41', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010402', 'El Cenepa', '1', null, '5');
INSERT INTO `inicio_distrito` VALUES ('42', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010403', 'Rio Santiago', '1', null, '5');
INSERT INTO `inicio_distrito` VALUES ('43', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010501', 'Lamud', '1', null, '6');
INSERT INTO `inicio_distrito` VALUES ('44', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010502', 'Camporredondo', '1', null, '6');
INSERT INTO `inicio_distrito` VALUES ('45', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010503', 'Cocabamba', '1', null, '6');
INSERT INTO `inicio_distrito` VALUES ('46', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010504', 'Colcamar', '1', null, '6');
INSERT INTO `inicio_distrito` VALUES ('47', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010505', 'Conila', '1', null, '6');
INSERT INTO `inicio_distrito` VALUES ('48', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010506', 'Inguilpata', '1', null, '6');
INSERT INTO `inicio_distrito` VALUES ('49', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010507', 'Longuita', '1', null, '6');
INSERT INTO `inicio_distrito` VALUES ('50', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010508', 'Lonya Chico', '1', null, '6');
INSERT INTO `inicio_distrito` VALUES ('51', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010509', 'Luya', '1', null, '6');
INSERT INTO `inicio_distrito` VALUES ('52', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010510', 'Luya Viejo', '1', null, '6');
INSERT INTO `inicio_distrito` VALUES ('53', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010511', 'Maria', '1', null, '6');
INSERT INTO `inicio_distrito` VALUES ('54', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010512', 'Ocalli', '1', null, '6');
INSERT INTO `inicio_distrito` VALUES ('55', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010513', 'Ocumal', '1', null, '6');
INSERT INTO `inicio_distrito` VALUES ('56', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010514', 'Pisuquia', '1', null, '6');
INSERT INTO `inicio_distrito` VALUES ('57', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010515', 'Providencia', '1', null, '6');
INSERT INTO `inicio_distrito` VALUES ('58', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010516', 'San Cristobal', '1', null, '6');
INSERT INTO `inicio_distrito` VALUES ('59', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010517', 'San Francisco del Yeso', '1', null, '6');
INSERT INTO `inicio_distrito` VALUES ('60', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010518', 'San Jeronimo', '1', null, '6');
INSERT INTO `inicio_distrito` VALUES ('61', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010519', 'San Juan de Lopecancha', '1', null, '6');
INSERT INTO `inicio_distrito` VALUES ('62', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010520', 'Santa Catalina', '1', null, '6');
INSERT INTO `inicio_distrito` VALUES ('63', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010521', 'Santo Tomas', '1', null, '6');
INSERT INTO `inicio_distrito` VALUES ('64', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010522', 'Tingo', '1', null, '6');
INSERT INTO `inicio_distrito` VALUES ('65', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010523', 'Trita', '1', null, '6');
INSERT INTO `inicio_distrito` VALUES ('66', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010601', 'San Nicolas', '1', null, '7');
INSERT INTO `inicio_distrito` VALUES ('67', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010602', 'Chirimoto', '1', null, '7');
INSERT INTO `inicio_distrito` VALUES ('68', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010603', 'Cochamal', '1', null, '7');
INSERT INTO `inicio_distrito` VALUES ('69', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010604', 'Huambo', '1', null, '7');
INSERT INTO `inicio_distrito` VALUES ('70', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010605', 'Limabamba', '1', null, '7');
INSERT INTO `inicio_distrito` VALUES ('71', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010606', 'Longar', '1', null, '7');
INSERT INTO `inicio_distrito` VALUES ('72', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010607', 'Mariscal Benavides', '1', null, '7');
INSERT INTO `inicio_distrito` VALUES ('73', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010608', 'Milpuc', '1', null, '7');
INSERT INTO `inicio_distrito` VALUES ('74', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010609', 'Omia', '1', null, '7');
INSERT INTO `inicio_distrito` VALUES ('75', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010610', 'Santa Rosa', '1', null, '7');
INSERT INTO `inicio_distrito` VALUES ('76', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010611', 'Totora', '1', null, '7');
INSERT INTO `inicio_distrito` VALUES ('77', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010612', 'Vista Alegre', '1', null, '7');
INSERT INTO `inicio_distrito` VALUES ('78', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010701', 'Bagua Grande', '1', null, '8');
INSERT INTO `inicio_distrito` VALUES ('79', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010702', 'Cajaruro', '1', null, '8');
INSERT INTO `inicio_distrito` VALUES ('80', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010703', 'Cumba', '1', null, '8');
INSERT INTO `inicio_distrito` VALUES ('81', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010704', 'El Milagro', '1', null, '8');
INSERT INTO `inicio_distrito` VALUES ('82', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010705', 'Jamalca', '1', null, '8');
INSERT INTO `inicio_distrito` VALUES ('83', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010706', 'Lonya Grande', '1', null, '8');
INSERT INTO `inicio_distrito` VALUES ('84', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '010707', 'Yamon', '1', null, '8');
INSERT INTO `inicio_distrito` VALUES ('85', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020101', 'Huaraz', '1', null, '9');
INSERT INTO `inicio_distrito` VALUES ('86', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020102', 'Cochabamba', '1', null, '9');
INSERT INTO `inicio_distrito` VALUES ('87', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020103', 'Colcabamba', '1', null, '9');
INSERT INTO `inicio_distrito` VALUES ('88', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020104', 'Huanchay', '1', null, '9');
INSERT INTO `inicio_distrito` VALUES ('89', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020105', 'Independencia', '1', null, '9');
INSERT INTO `inicio_distrito` VALUES ('90', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020106', 'Jangas', '1', null, '9');
INSERT INTO `inicio_distrito` VALUES ('91', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020107', 'La Libertad', '1', null, '9');
INSERT INTO `inicio_distrito` VALUES ('92', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020108', 'Olleros', '1', null, '9');
INSERT INTO `inicio_distrito` VALUES ('93', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020109', 'Pampas', '1', null, '9');
INSERT INTO `inicio_distrito` VALUES ('94', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020110', 'Pariacoto', '1', null, '9');
INSERT INTO `inicio_distrito` VALUES ('95', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020111', 'Pira', '1', null, '9');
INSERT INTO `inicio_distrito` VALUES ('96', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020112', 'Tarica', '1', null, '9');
INSERT INTO `inicio_distrito` VALUES ('97', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020201', 'Aija', '1', null, '10');
INSERT INTO `inicio_distrito` VALUES ('98', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020202', 'Coris', '1', null, '10');
INSERT INTO `inicio_distrito` VALUES ('99', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020203', 'Huacllan', '1', null, '10');
INSERT INTO `inicio_distrito` VALUES ('100', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020204', 'La Merced', '1', null, '10');
INSERT INTO `inicio_distrito` VALUES ('101', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020205', 'Succha', '1', null, '10');
INSERT INTO `inicio_distrito` VALUES ('102', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020301', 'Llamellin', '1', null, '11');
INSERT INTO `inicio_distrito` VALUES ('103', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020302', 'Aczo', '1', null, '11');
INSERT INTO `inicio_distrito` VALUES ('104', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020303', 'Chaccho', '1', null, '11');
INSERT INTO `inicio_distrito` VALUES ('105', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020304', 'Chingas', '1', null, '11');
INSERT INTO `inicio_distrito` VALUES ('106', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020305', 'Mirgas', '1', null, '11');
INSERT INTO `inicio_distrito` VALUES ('107', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020306', 'San Juan de Rontoy', '1', null, '11');
INSERT INTO `inicio_distrito` VALUES ('108', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020401', 'Chacas', '1', null, '12');
INSERT INTO `inicio_distrito` VALUES ('109', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020402', 'Acochaca', '1', null, '12');
INSERT INTO `inicio_distrito` VALUES ('110', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020501', 'Chiquian', '1', null, '13');
INSERT INTO `inicio_distrito` VALUES ('111', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020502', 'Abelardo Pardo Lezameta', '1', null, '13');
INSERT INTO `inicio_distrito` VALUES ('112', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020503', 'Antonio Raymondi', '1', null, '13');
INSERT INTO `inicio_distrito` VALUES ('113', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020504', 'Aquia', '1', null, '13');
INSERT INTO `inicio_distrito` VALUES ('114', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020505', 'Cajacay', '1', null, '13');
INSERT INTO `inicio_distrito` VALUES ('115', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020506', 'Canis', '1', null, '13');
INSERT INTO `inicio_distrito` VALUES ('116', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020507', 'Colquioc', '1', null, '13');
INSERT INTO `inicio_distrito` VALUES ('117', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020508', 'Huallanca', '1', null, '13');
INSERT INTO `inicio_distrito` VALUES ('118', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020509', 'Huasta', '1', null, '13');
INSERT INTO `inicio_distrito` VALUES ('119', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020510', 'Huayllacayan', '1', null, '13');
INSERT INTO `inicio_distrito` VALUES ('120', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020511', 'La Primavera', '1', null, '13');
INSERT INTO `inicio_distrito` VALUES ('121', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020512', 'Mangas', '1', null, '13');
INSERT INTO `inicio_distrito` VALUES ('122', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020513', 'Pacllon', '1', null, '13');
INSERT INTO `inicio_distrito` VALUES ('123', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020514', 'San Miguel de Corpanqui', '1', null, '13');
INSERT INTO `inicio_distrito` VALUES ('124', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020515', 'Ticllos', '1', null, '13');
INSERT INTO `inicio_distrito` VALUES ('125', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020601', 'Carhuaz', '1', null, '14');
INSERT INTO `inicio_distrito` VALUES ('126', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020602', 'Acopampa', '1', null, '14');
INSERT INTO `inicio_distrito` VALUES ('127', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020603', 'Amashca', '1', null, '14');
INSERT INTO `inicio_distrito` VALUES ('128', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020604', 'Anta', '1', null, '14');
INSERT INTO `inicio_distrito` VALUES ('129', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020605', 'Ataquero', '1', null, '14');
INSERT INTO `inicio_distrito` VALUES ('130', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020606', 'Marcara', '1', null, '14');
INSERT INTO `inicio_distrito` VALUES ('131', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020607', 'Pariahuanca', '1', null, '14');
INSERT INTO `inicio_distrito` VALUES ('132', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020608', 'San Miguel de Aco', '1', null, '14');
INSERT INTO `inicio_distrito` VALUES ('133', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020609', 'Shilla', '1', null, '14');
INSERT INTO `inicio_distrito` VALUES ('134', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020610', 'Tinco', '1', null, '14');
INSERT INTO `inicio_distrito` VALUES ('135', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020611', 'Yungar', '1', null, '14');
INSERT INTO `inicio_distrito` VALUES ('136', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020701', 'San Luis', '1', null, '15');
INSERT INTO `inicio_distrito` VALUES ('137', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020702', 'San Nicolas', '1', null, '15');
INSERT INTO `inicio_distrito` VALUES ('138', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020703', 'Yauya', '1', null, '15');
INSERT INTO `inicio_distrito` VALUES ('139', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020801', 'Casma', '1', null, '16');
INSERT INTO `inicio_distrito` VALUES ('140', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020802', 'Buena Vista Alta', '1', null, '16');
INSERT INTO `inicio_distrito` VALUES ('141', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020803', 'Comandante Noel', '1', null, '16');
INSERT INTO `inicio_distrito` VALUES ('142', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020804', 'Yautan', '1', null, '16');
INSERT INTO `inicio_distrito` VALUES ('143', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020901', 'Corongo', '1', null, '7');
INSERT INTO `inicio_distrito` VALUES ('144', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020902', 'Aco', '1', null, '7');
INSERT INTO `inicio_distrito` VALUES ('145', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020903', 'Bambas', '1', null, '7');
INSERT INTO `inicio_distrito` VALUES ('146', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020904', 'Cusca', '1', null, '7');
INSERT INTO `inicio_distrito` VALUES ('147', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020905', 'La Pampa', '1', null, '7');
INSERT INTO `inicio_distrito` VALUES ('148', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020906', 'Yanac', '1', null, '7');
INSERT INTO `inicio_distrito` VALUES ('149', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '020907', 'Yupan', '1', null, '7');
INSERT INTO `inicio_distrito` VALUES ('150', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021001', 'Huari', '1', null, '18');
INSERT INTO `inicio_distrito` VALUES ('151', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021002', 'Anra', '1', null, '18');
INSERT INTO `inicio_distrito` VALUES ('152', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021003', 'Cajay', '1', null, '18');
INSERT INTO `inicio_distrito` VALUES ('153', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021004', 'Chavin de Huantar', '1', null, '18');
INSERT INTO `inicio_distrito` VALUES ('154', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021005', 'Huacachi', '1', null, '18');
INSERT INTO `inicio_distrito` VALUES ('155', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021006', 'Huacchis', '1', null, '18');
INSERT INTO `inicio_distrito` VALUES ('156', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021007', 'Huachis', '1', null, '18');
INSERT INTO `inicio_distrito` VALUES ('157', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021008', 'Huantar', '1', null, '18');
INSERT INTO `inicio_distrito` VALUES ('158', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021009', 'Masin', '1', null, '18');
INSERT INTO `inicio_distrito` VALUES ('159', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021010', 'Paucas', '1', null, '18');
INSERT INTO `inicio_distrito` VALUES ('160', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021011', 'Ponto', '1', null, '18');
INSERT INTO `inicio_distrito` VALUES ('161', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021012', 'Rahuapampa', '1', null, '18');
INSERT INTO `inicio_distrito` VALUES ('162', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021013', 'Rapayan', '1', null, '18');
INSERT INTO `inicio_distrito` VALUES ('163', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021014', 'San Marcos', '1', null, '18');
INSERT INTO `inicio_distrito` VALUES ('164', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021015', 'San Pedro de Chana', '1', null, '18');
INSERT INTO `inicio_distrito` VALUES ('165', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021016', 'Uco', '1', null, '18');
INSERT INTO `inicio_distrito` VALUES ('166', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021101', 'Huarmey', '1', null, '19');
INSERT INTO `inicio_distrito` VALUES ('167', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021102', 'Cochapeti', '1', null, '19');
INSERT INTO `inicio_distrito` VALUES ('168', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021103', 'Culebras', '1', null, '19');
INSERT INTO `inicio_distrito` VALUES ('169', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021104', 'Huayan', '1', null, '19');
INSERT INTO `inicio_distrito` VALUES ('170', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021105', 'Malvas', '1', null, '19');
INSERT INTO `inicio_distrito` VALUES ('171', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021201', 'Caraz', '1', null, '20');
INSERT INTO `inicio_distrito` VALUES ('172', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021202', 'Huallanca', '1', null, '20');
INSERT INTO `inicio_distrito` VALUES ('173', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021203', 'Huata', '1', null, '20');
INSERT INTO `inicio_distrito` VALUES ('174', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021204', 'Huaylas', '1', null, '20');
INSERT INTO `inicio_distrito` VALUES ('175', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021205', 'Mato', '1', null, '20');
INSERT INTO `inicio_distrito` VALUES ('176', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021206', 'Pamparomas', '1', null, '20');
INSERT INTO `inicio_distrito` VALUES ('177', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021207', 'Pueblo Libre', '1', null, '20');
INSERT INTO `inicio_distrito` VALUES ('178', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021208', 'Santa Cruz', '1', null, '20');
INSERT INTO `inicio_distrito` VALUES ('179', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021209', 'Santo Toribio', '1', null, '20');
INSERT INTO `inicio_distrito` VALUES ('180', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021210', 'Yuracmarca', '1', null, '20');
INSERT INTO `inicio_distrito` VALUES ('181', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021301', 'Piscobamba', '1', null, '21');
INSERT INTO `inicio_distrito` VALUES ('182', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021302', 'Casca', '1', null, '21');
INSERT INTO `inicio_distrito` VALUES ('183', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021303', 'Eleazar Guzman Barron', '1', null, '21');
INSERT INTO `inicio_distrito` VALUES ('184', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021304', 'Fidel Olivas Escudero', '1', null, '21');
INSERT INTO `inicio_distrito` VALUES ('185', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021305', 'Llama', '1', null, '21');
INSERT INTO `inicio_distrito` VALUES ('186', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021306', 'Llumpa', '1', null, '21');
INSERT INTO `inicio_distrito` VALUES ('187', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021307', 'Lucma', '1', null, '21');
INSERT INTO `inicio_distrito` VALUES ('188', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021308', 'Musga', '1', null, '21');
INSERT INTO `inicio_distrito` VALUES ('189', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021401', 'Ocros', '1', null, '22');
INSERT INTO `inicio_distrito` VALUES ('190', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021402', 'Acas', '1', null, '22');
INSERT INTO `inicio_distrito` VALUES ('191', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021403', 'Cajamarquilla', '1', null, '22');
INSERT INTO `inicio_distrito` VALUES ('192', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021404', 'Carhuapampa', '1', null, '22');
INSERT INTO `inicio_distrito` VALUES ('193', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021405', 'Cochas', '1', null, '22');
INSERT INTO `inicio_distrito` VALUES ('194', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021406', 'Congas', '1', null, '22');
INSERT INTO `inicio_distrito` VALUES ('195', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021407', 'Llipa', '1', null, '22');
INSERT INTO `inicio_distrito` VALUES ('196', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021408', 'San Cristobal de Rajan', '1', null, '22');
INSERT INTO `inicio_distrito` VALUES ('197', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021409', 'San Pedro', '1', null, '22');
INSERT INTO `inicio_distrito` VALUES ('198', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021410', 'Santiago de Chilcas', '1', null, '22');
INSERT INTO `inicio_distrito` VALUES ('199', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021501', 'Cabana', '1', null, '23');
INSERT INTO `inicio_distrito` VALUES ('200', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021502', 'Bolognesi', '1', null, '23');
INSERT INTO `inicio_distrito` VALUES ('201', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021503', 'Conchucos', '1', null, '23');
INSERT INTO `inicio_distrito` VALUES ('202', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021504', 'Huacaschuque', '1', null, '23');
INSERT INTO `inicio_distrito` VALUES ('203', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021505', 'Huandoval', '1', null, '23');
INSERT INTO `inicio_distrito` VALUES ('204', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021506', 'Lacabamba', '1', null, '23');
INSERT INTO `inicio_distrito` VALUES ('205', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021507', 'Llapo', '1', null, '23');
INSERT INTO `inicio_distrito` VALUES ('206', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021508', 'Pallasca', '1', null, '23');
INSERT INTO `inicio_distrito` VALUES ('207', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021509', 'Pampas', '1', null, '23');
INSERT INTO `inicio_distrito` VALUES ('208', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021510', 'Santa Rosa', '1', null, '23');
INSERT INTO `inicio_distrito` VALUES ('209', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021511', 'Tauca', '1', null, '23');
INSERT INTO `inicio_distrito` VALUES ('210', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021601', 'Pomabamba', '1', null, '24');
INSERT INTO `inicio_distrito` VALUES ('211', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021602', 'Huayllan', '1', null, '24');
INSERT INTO `inicio_distrito` VALUES ('212', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021603', 'Parobamba', '1', null, '24');
INSERT INTO `inicio_distrito` VALUES ('213', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021604', 'Quinuabamba', '1', null, '24');
INSERT INTO `inicio_distrito` VALUES ('214', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021701', 'Recuay', '1', null, '25');
INSERT INTO `inicio_distrito` VALUES ('215', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021702', 'Catac', '1', null, '25');
INSERT INTO `inicio_distrito` VALUES ('216', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021703', 'Cotaparaco', '1', null, '25');
INSERT INTO `inicio_distrito` VALUES ('217', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021704', 'Huayllapampa', '1', null, '25');
INSERT INTO `inicio_distrito` VALUES ('218', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021705', 'Llacllin', '1', null, '25');
INSERT INTO `inicio_distrito` VALUES ('219', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021706', 'Marca', '1', null, '25');
INSERT INTO `inicio_distrito` VALUES ('220', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021707', 'Pampas Chico', '1', null, '25');
INSERT INTO `inicio_distrito` VALUES ('221', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021708', 'Pararin', '1', null, '25');
INSERT INTO `inicio_distrito` VALUES ('222', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021709', 'Tapacocha', '1', null, '25');
INSERT INTO `inicio_distrito` VALUES ('223', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021710', 'Ticapampa', '1', null, '25');
INSERT INTO `inicio_distrito` VALUES ('224', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021801', 'Chimbote', '1', null, '26');
INSERT INTO `inicio_distrito` VALUES ('225', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021802', 'Caceres del Peru', '1', null, '26');
INSERT INTO `inicio_distrito` VALUES ('226', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021803', 'Coishco', '1', null, '26');
INSERT INTO `inicio_distrito` VALUES ('227', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021804', 'Macate', '1', null, '26');
INSERT INTO `inicio_distrito` VALUES ('228', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021805', 'Moro', '1', null, '26');
INSERT INTO `inicio_distrito` VALUES ('229', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021806', 'Nepeqa', '1', null, '26');
INSERT INTO `inicio_distrito` VALUES ('230', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021807', 'Samanco', '1', null, '26');
INSERT INTO `inicio_distrito` VALUES ('231', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021808', 'Santa', '1', null, '26');
INSERT INTO `inicio_distrito` VALUES ('232', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021809', 'Nuevo Chimbote', '1', null, '26');
INSERT INTO `inicio_distrito` VALUES ('233', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021901', 'Sihuas', '1', null, '27');
INSERT INTO `inicio_distrito` VALUES ('234', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021902', 'Acobamba', '1', null, '27');
INSERT INTO `inicio_distrito` VALUES ('235', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021903', 'Alfonso Ugarte', '1', null, '27');
INSERT INTO `inicio_distrito` VALUES ('236', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021904', 'Cashapampa', '1', null, '27');
INSERT INTO `inicio_distrito` VALUES ('237', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021905', 'Chingalpo', '1', null, '27');
INSERT INTO `inicio_distrito` VALUES ('238', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021906', 'Huayllabamba', '1', null, '27');
INSERT INTO `inicio_distrito` VALUES ('239', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021907', 'Quiches', '1', null, '27');
INSERT INTO `inicio_distrito` VALUES ('240', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021908', 'Ragash', '1', null, '27');
INSERT INTO `inicio_distrito` VALUES ('241', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021909', 'San Juan', '1', null, '27');
INSERT INTO `inicio_distrito` VALUES ('242', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '021910', 'Sicsibamba', '1', null, '27');
INSERT INTO `inicio_distrito` VALUES ('243', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '022001', 'Yungay', '1', null, '28');
INSERT INTO `inicio_distrito` VALUES ('244', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '022002', 'Cascapara', '1', null, '28');
INSERT INTO `inicio_distrito` VALUES ('245', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '022003', 'Mancos', '1', null, '28');
INSERT INTO `inicio_distrito` VALUES ('246', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '022004', 'Matacoto', '1', null, '28');
INSERT INTO `inicio_distrito` VALUES ('247', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '022005', 'Quillo', '1', null, '28');
INSERT INTO `inicio_distrito` VALUES ('248', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '022006', 'Ranrahirca', '1', null, '28');
INSERT INTO `inicio_distrito` VALUES ('249', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '022007', 'Shupluy', '1', null, '28');
INSERT INTO `inicio_distrito` VALUES ('250', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '022008', 'Yanama', '1', null, '28');
INSERT INTO `inicio_distrito` VALUES ('251', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030101', 'Abancay', '1', null, '29');
INSERT INTO `inicio_distrito` VALUES ('252', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030102', 'Chacoche', '1', null, '29');
INSERT INTO `inicio_distrito` VALUES ('253', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030103', 'Circa', '1', null, '29');
INSERT INTO `inicio_distrito` VALUES ('254', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030104', 'Curahuasi', '1', null, '29');
INSERT INTO `inicio_distrito` VALUES ('255', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030105', 'Huanipaca', '1', null, '29');
INSERT INTO `inicio_distrito` VALUES ('256', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030106', 'Lambrama', '1', null, '29');
INSERT INTO `inicio_distrito` VALUES ('257', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030107', 'Pichirhua', '1', null, '29');
INSERT INTO `inicio_distrito` VALUES ('258', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030108', 'San Pedro de Cachora', '1', null, '29');
INSERT INTO `inicio_distrito` VALUES ('259', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030109', 'Tamburco', '1', null, '29');
INSERT INTO `inicio_distrito` VALUES ('260', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030201', 'Andahuaylas', '1', null, '30');
INSERT INTO `inicio_distrito` VALUES ('261', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030202', 'Andarapa', '1', null, '30');
INSERT INTO `inicio_distrito` VALUES ('262', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030203', 'Chiara', '1', null, '30');
INSERT INTO `inicio_distrito` VALUES ('263', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030204', 'Huancarama', '1', null, '30');
INSERT INTO `inicio_distrito` VALUES ('264', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030205', 'Huancaray', '1', null, '30');
INSERT INTO `inicio_distrito` VALUES ('265', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030206', 'Huayana', '1', null, '30');
INSERT INTO `inicio_distrito` VALUES ('266', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030207', 'Kishuara', '1', null, '30');
INSERT INTO `inicio_distrito` VALUES ('267', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030208', 'Pacobamba', '1', null, '30');
INSERT INTO `inicio_distrito` VALUES ('268', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030209', 'Pacucha', '1', null, '30');
INSERT INTO `inicio_distrito` VALUES ('269', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030210', 'Pampachiri', '1', null, '30');
INSERT INTO `inicio_distrito` VALUES ('270', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030211', 'Pomacocha', '1', null, '30');
INSERT INTO `inicio_distrito` VALUES ('271', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030212', 'San Antonio de Cachi', '1', null, '30');
INSERT INTO `inicio_distrito` VALUES ('272', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030213', 'San Jeronimo', '1', null, '30');
INSERT INTO `inicio_distrito` VALUES ('273', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030214', 'San Miguel de Chaccrampa', '1', null, '30');
INSERT INTO `inicio_distrito` VALUES ('274', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030215', 'Santa Maria de Chicmo', '1', null, '30');
INSERT INTO `inicio_distrito` VALUES ('275', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030216', 'Talavera', '1', null, '30');
INSERT INTO `inicio_distrito` VALUES ('276', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030217', 'Tumay Huaraca', '1', null, '30');
INSERT INTO `inicio_distrito` VALUES ('277', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030218', 'Turpo', '1', null, '30');
INSERT INTO `inicio_distrito` VALUES ('278', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030219', 'Kaquiabamba', '1', null, '30');
INSERT INTO `inicio_distrito` VALUES ('279', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030301', 'Antabamba', '1', null, '31');
INSERT INTO `inicio_distrito` VALUES ('280', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030302', 'El Oro', '1', null, '31');
INSERT INTO `inicio_distrito` VALUES ('281', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030303', 'Huaquirca', '1', null, '31');
INSERT INTO `inicio_distrito` VALUES ('282', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030304', 'Juan Espinoza Medrano', '1', null, '31');
INSERT INTO `inicio_distrito` VALUES ('283', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030305', 'Oropesa', '1', null, '31');
INSERT INTO `inicio_distrito` VALUES ('284', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030306', 'Pachaconas', '1', null, '31');
INSERT INTO `inicio_distrito` VALUES ('285', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030307', 'Sabaino', '1', null, '31');
INSERT INTO `inicio_distrito` VALUES ('286', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030401', 'Chalhuanca', '1', null, '32');
INSERT INTO `inicio_distrito` VALUES ('287', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030402', 'Capaya', '1', null, '32');
INSERT INTO `inicio_distrito` VALUES ('288', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030403', 'Caraybamba', '1', null, '32');
INSERT INTO `inicio_distrito` VALUES ('289', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030404', 'Chapimarca', '1', null, '32');
INSERT INTO `inicio_distrito` VALUES ('290', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030405', 'Colcabamba', '1', null, '32');
INSERT INTO `inicio_distrito` VALUES ('291', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030406', 'Cotaruse', '1', null, '32');
INSERT INTO `inicio_distrito` VALUES ('292', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030407', 'Huayllo', '1', null, '32');
INSERT INTO `inicio_distrito` VALUES ('293', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030408', 'Justo Apu Sahuaraura', '1', null, '32');
INSERT INTO `inicio_distrito` VALUES ('294', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030409', 'Lucre', '1', null, '32');
INSERT INTO `inicio_distrito` VALUES ('295', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030410', 'Pocohuanca', '1', null, '32');
INSERT INTO `inicio_distrito` VALUES ('296', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030411', 'San Juan de Chacqa', '1', null, '32');
INSERT INTO `inicio_distrito` VALUES ('297', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030412', 'Saqayca', '1', null, '32');
INSERT INTO `inicio_distrito` VALUES ('298', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030413', 'Soraya', '1', null, '32');
INSERT INTO `inicio_distrito` VALUES ('299', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030414', 'Tapairihua', '1', null, '32');
INSERT INTO `inicio_distrito` VALUES ('300', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030415', 'Tintay', '1', null, '32');
INSERT INTO `inicio_distrito` VALUES ('301', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030416', 'Toraya', '1', null, '32');
INSERT INTO `inicio_distrito` VALUES ('302', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030417', 'Yanaca', '1', null, '32');
INSERT INTO `inicio_distrito` VALUES ('303', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030501', 'Tambobamba', '1', null, '33');
INSERT INTO `inicio_distrito` VALUES ('304', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030502', 'Cotabambas', '1', null, '33');
INSERT INTO `inicio_distrito` VALUES ('305', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030503', 'Coyllurqui', '1', null, '33');
INSERT INTO `inicio_distrito` VALUES ('306', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030504', 'Haquira', '1', null, '33');
INSERT INTO `inicio_distrito` VALUES ('307', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030505', 'Mara', '1', null, '33');
INSERT INTO `inicio_distrito` VALUES ('308', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030506', 'Challhuahuacho', '1', null, '33');
INSERT INTO `inicio_distrito` VALUES ('309', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030601', 'Chincheros', '1', null, '34');
INSERT INTO `inicio_distrito` VALUES ('310', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030602', 'Anco-Huallo', '1', null, '34');
INSERT INTO `inicio_distrito` VALUES ('311', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030603', 'Cocharcas', '1', null, '34');
INSERT INTO `inicio_distrito` VALUES ('312', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030604', 'Huaccana', '1', null, '34');
INSERT INTO `inicio_distrito` VALUES ('313', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030605', 'Ocobamba', '1', null, '34');
INSERT INTO `inicio_distrito` VALUES ('314', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030606', 'Ongoy', '1', null, '34');
INSERT INTO `inicio_distrito` VALUES ('315', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030607', 'Uranmarca', '1', null, '34');
INSERT INTO `inicio_distrito` VALUES ('316', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030608', 'Ranracancha', '1', null, '34');
INSERT INTO `inicio_distrito` VALUES ('317', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030701', 'Chuquibambilla', '1', null, '35');
INSERT INTO `inicio_distrito` VALUES ('318', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030702', 'Curpahuasi', '1', null, '35');
INSERT INTO `inicio_distrito` VALUES ('319', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030703', 'Gamarra', '1', null, '35');
INSERT INTO `inicio_distrito` VALUES ('320', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030704', 'Huayllati', '1', null, '35');
INSERT INTO `inicio_distrito` VALUES ('321', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030705', 'Mamara', '1', null, '35');
INSERT INTO `inicio_distrito` VALUES ('322', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030706', 'Micaela Bastidas', '1', null, '35');
INSERT INTO `inicio_distrito` VALUES ('323', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030707', 'Pataypampa', '1', null, '35');
INSERT INTO `inicio_distrito` VALUES ('324', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030708', 'Progreso', '1', null, '35');
INSERT INTO `inicio_distrito` VALUES ('325', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030709', 'San Antonio', '1', null, '35');
INSERT INTO `inicio_distrito` VALUES ('326', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030710', 'Santa Rosa', '1', null, '35');
INSERT INTO `inicio_distrito` VALUES ('327', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030711', 'Turpay', '1', null, '35');
INSERT INTO `inicio_distrito` VALUES ('328', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030712', 'Vilcabamba', '1', null, '35');
INSERT INTO `inicio_distrito` VALUES ('329', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030713', 'Virundo', '1', null, '35');
INSERT INTO `inicio_distrito` VALUES ('330', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '030714', 'Curasco', '1', null, '35');
INSERT INTO `inicio_distrito` VALUES ('331', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040101', 'Arequipa', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('332', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040102', 'Alto Selva Alegre', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('333', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040103', 'Cayma', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('334', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040104', 'Cerro Colorado', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('335', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040105', 'Characato', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('336', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040106', 'Chiguata', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('337', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040107', 'Jacobo Hunter', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('338', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040108', 'La Joya', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('339', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040109', 'Mariano Melgar', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('340', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040110', 'Miraflores', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('341', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040111', 'Mollebaya', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('342', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040112', 'Paucarpata', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('343', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040113', 'Pocsi', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('344', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040114', 'Polobaya', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('345', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040115', 'Quequeqa', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('346', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040116', 'Sabandia', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('347', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040117', 'Sachaca', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('348', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040118', 'San Juan de Siguas', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('349', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040119', 'San Juan de Tarucani', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('350', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040120', 'Santa Isabel de Siguas', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('351', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040121', 'Santa Rita de Siguas', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('352', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040122', 'Socabaya', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('353', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040123', 'Tiabaya', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('354', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040124', 'Uchumayo', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('355', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040125', 'Vitor', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('356', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040126', 'Yanahuara', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('357', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040127', 'Yarabamba', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('358', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040128', 'Yura', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('359', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040129', 'Jose Luis Bustamante y Rivero', '1', null, '36');
INSERT INTO `inicio_distrito` VALUES ('360', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040201', 'Camana', '1', null, '37');
INSERT INTO `inicio_distrito` VALUES ('361', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040202', 'Jose Maria Quimper', '1', null, '37');
INSERT INTO `inicio_distrito` VALUES ('362', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040203', 'Mariano Nicolas Valcarcel', '1', null, '37');
INSERT INTO `inicio_distrito` VALUES ('363', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040204', 'Mariscal Caceres', '1', null, '37');
INSERT INTO `inicio_distrito` VALUES ('364', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040205', 'Nicolas de Pierola', '1', null, '37');
INSERT INTO `inicio_distrito` VALUES ('365', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040206', 'Ocoqa', '1', null, '37');
INSERT INTO `inicio_distrito` VALUES ('366', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040207', 'Quilca', '1', null, '37');
INSERT INTO `inicio_distrito` VALUES ('367', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040208', 'Samuel Pastor', '1', null, '37');
INSERT INTO `inicio_distrito` VALUES ('368', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040301', 'Caraveli', '1', null, '38');
INSERT INTO `inicio_distrito` VALUES ('369', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040302', 'Acari', '1', null, '38');
INSERT INTO `inicio_distrito` VALUES ('370', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040303', 'Atico', '1', null, '38');
INSERT INTO `inicio_distrito` VALUES ('371', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040304', 'Atiquipa', '1', null, '38');
INSERT INTO `inicio_distrito` VALUES ('372', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040305', 'Bella Union', '1', null, '38');
INSERT INTO `inicio_distrito` VALUES ('373', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040306', 'Cahuacho', '1', null, '38');
INSERT INTO `inicio_distrito` VALUES ('374', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040307', 'Chala', '1', null, '38');
INSERT INTO `inicio_distrito` VALUES ('375', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040308', 'Chaparra', '1', null, '38');
INSERT INTO `inicio_distrito` VALUES ('376', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040309', 'Huanuhuanu', '1', null, '38');
INSERT INTO `inicio_distrito` VALUES ('377', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040310', 'Jaqui', '1', null, '38');
INSERT INTO `inicio_distrito` VALUES ('378', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040311', 'Lomas', '1', null, '38');
INSERT INTO `inicio_distrito` VALUES ('379', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040312', 'Quicacha', '1', null, '38');
INSERT INTO `inicio_distrito` VALUES ('380', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040313', 'Yauca', '1', null, '38');
INSERT INTO `inicio_distrito` VALUES ('381', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040401', 'Aplao', '1', null, '39');
INSERT INTO `inicio_distrito` VALUES ('382', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040402', 'Andagua', '1', null, '39');
INSERT INTO `inicio_distrito` VALUES ('383', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040403', 'Ayo', '1', null, '39');
INSERT INTO `inicio_distrito` VALUES ('384', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040404', 'Chachas', '1', null, '39');
INSERT INTO `inicio_distrito` VALUES ('385', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040405', 'Chilcaymarca', '1', null, '39');
INSERT INTO `inicio_distrito` VALUES ('386', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040406', 'Choco', '1', null, '39');
INSERT INTO `inicio_distrito` VALUES ('387', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040407', 'Huancarqui', '1', null, '39');
INSERT INTO `inicio_distrito` VALUES ('388', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040408', 'Machaguay', '1', null, '39');
INSERT INTO `inicio_distrito` VALUES ('389', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040409', 'Orcopampa', '1', null, '39');
INSERT INTO `inicio_distrito` VALUES ('390', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040410', 'Pampacolca', '1', null, '39');
INSERT INTO `inicio_distrito` VALUES ('391', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040411', 'Tipan', '1', null, '39');
INSERT INTO `inicio_distrito` VALUES ('392', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040412', 'Uqon', '1', null, '39');
INSERT INTO `inicio_distrito` VALUES ('393', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040413', 'Uraca', '1', null, '39');
INSERT INTO `inicio_distrito` VALUES ('394', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040414', 'Viraco', '1', null, '39');
INSERT INTO `inicio_distrito` VALUES ('395', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040419', 'Yanque', '1', null, '39');
INSERT INTO `inicio_distrito` VALUES ('396', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040420', 'Majes', '1', null, '39');
INSERT INTO `inicio_distrito` VALUES ('397', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040501', 'Chivay', '1', null, '40');
INSERT INTO `inicio_distrito` VALUES ('398', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040502', 'Achoma', '1', null, '40');
INSERT INTO `inicio_distrito` VALUES ('399', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040503', 'Cabanaconde', '1', null, '40');
INSERT INTO `inicio_distrito` VALUES ('400', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040504', 'Callalli', '1', null, '40');
INSERT INTO `inicio_distrito` VALUES ('401', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040505', 'Caylloma', '1', null, '40');
INSERT INTO `inicio_distrito` VALUES ('402', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040506', 'Coporaque', '1', null, '40');
INSERT INTO `inicio_distrito` VALUES ('403', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040507', 'Huambo', '1', null, '40');
INSERT INTO `inicio_distrito` VALUES ('404', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040508', 'Huanca', '1', null, '40');
INSERT INTO `inicio_distrito` VALUES ('405', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040509', 'Ichupampa', '1', null, '40');
INSERT INTO `inicio_distrito` VALUES ('406', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040510', 'Lari', '1', null, '40');
INSERT INTO `inicio_distrito` VALUES ('407', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040511', 'Lluta', '1', null, '40');
INSERT INTO `inicio_distrito` VALUES ('408', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040512', 'Maca', '1', null, '40');
INSERT INTO `inicio_distrito` VALUES ('409', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040513', 'Madrigal', '1', null, '40');
INSERT INTO `inicio_distrito` VALUES ('410', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040514', 'San Antonio de Chuca', '1', null, '40');
INSERT INTO `inicio_distrito` VALUES ('411', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040515', 'Sibayo', '1', null, '40');
INSERT INTO `inicio_distrito` VALUES ('412', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040516', 'Tapay', '1', null, '40');
INSERT INTO `inicio_distrito` VALUES ('413', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040517', 'Tisco', '1', null, '40');
INSERT INTO `inicio_distrito` VALUES ('414', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040518', 'Tuti', '1', null, '40');
INSERT INTO `inicio_distrito` VALUES ('415', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040519', 'Yanque', '1', null, '40');
INSERT INTO `inicio_distrito` VALUES ('416', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040520', 'Majes', '1', null, '40');
INSERT INTO `inicio_distrito` VALUES ('417', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040601', 'Chuquibamba', '1', null, '41');
INSERT INTO `inicio_distrito` VALUES ('418', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040602', 'Andaray', '1', null, '41');
INSERT INTO `inicio_distrito` VALUES ('419', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040603', 'Cayarani', '1', null, '41');
INSERT INTO `inicio_distrito` VALUES ('420', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040604', 'Chichas', '1', null, '41');
INSERT INTO `inicio_distrito` VALUES ('421', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040605', 'Iray', '1', null, '41');
INSERT INTO `inicio_distrito` VALUES ('422', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040606', 'Rio Grande', '1', null, '41');
INSERT INTO `inicio_distrito` VALUES ('423', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040607', 'Salamanca', '1', null, '41');
INSERT INTO `inicio_distrito` VALUES ('424', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040608', 'Yanaquihua', '1', null, '41');
INSERT INTO `inicio_distrito` VALUES ('425', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040701', 'Mollendo', '1', null, '42');
INSERT INTO `inicio_distrito` VALUES ('426', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040702', 'Cocachacra', '1', null, '42');
INSERT INTO `inicio_distrito` VALUES ('427', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040703', 'Dean Valdivia', '1', null, '42');
INSERT INTO `inicio_distrito` VALUES ('428', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040704', 'Islay', '1', null, '42');
INSERT INTO `inicio_distrito` VALUES ('429', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040705', 'Mejia', '1', null, '42');
INSERT INTO `inicio_distrito` VALUES ('430', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040706', 'Punta de Bombon', '1', null, '42');
INSERT INTO `inicio_distrito` VALUES ('431', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040801', 'Cotahuasi', '1', null, '43');
INSERT INTO `inicio_distrito` VALUES ('432', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040802', 'Alca', '1', null, '43');
INSERT INTO `inicio_distrito` VALUES ('433', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040803', 'Charcana', '1', null, '43');
INSERT INTO `inicio_distrito` VALUES ('434', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040804', 'Huaynacotas', '1', null, '43');
INSERT INTO `inicio_distrito` VALUES ('435', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040805', 'Pampamarca', '1', null, '43');
INSERT INTO `inicio_distrito` VALUES ('436', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040806', 'Puyca', '1', null, '43');
INSERT INTO `inicio_distrito` VALUES ('437', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040807', 'Quechualla', '1', null, '43');
INSERT INTO `inicio_distrito` VALUES ('438', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040808', 'Sayla', '1', null, '43');
INSERT INTO `inicio_distrito` VALUES ('439', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040809', 'Tauria', '1', null, '43');
INSERT INTO `inicio_distrito` VALUES ('440', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040810', 'Tomepampa', '1', null, '43');
INSERT INTO `inicio_distrito` VALUES ('441', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '040811', 'Toro', '1', null, '43');
INSERT INTO `inicio_distrito` VALUES ('442', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050101', 'Ayacucho', '1', null, '44');
INSERT INTO `inicio_distrito` VALUES ('443', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050102', 'Acocro', '1', null, '44');
INSERT INTO `inicio_distrito` VALUES ('444', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050103', 'Acos Vinchos', '1', null, '44');
INSERT INTO `inicio_distrito` VALUES ('445', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050104', 'Carmen Alto', '1', null, '44');
INSERT INTO `inicio_distrito` VALUES ('446', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050105', 'Chiara', '1', null, '44');
INSERT INTO `inicio_distrito` VALUES ('447', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050106', 'Ocros', '1', null, '44');
INSERT INTO `inicio_distrito` VALUES ('448', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050107', 'Pacaycasa', '1', null, '44');
INSERT INTO `inicio_distrito` VALUES ('449', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050108', 'Quinua', '1', null, '44');
INSERT INTO `inicio_distrito` VALUES ('450', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050109', 'San Jose de Ticllas', '1', null, '44');
INSERT INTO `inicio_distrito` VALUES ('451', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050110', 'San Juan Bautista', '1', null, '44');
INSERT INTO `inicio_distrito` VALUES ('452', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050111', 'Santiago de Pischa', '1', null, '44');
INSERT INTO `inicio_distrito` VALUES ('453', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050112', 'Socos', '1', null, '44');
INSERT INTO `inicio_distrito` VALUES ('454', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050113', 'Tambillo', '1', null, '44');
INSERT INTO `inicio_distrito` VALUES ('455', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050114', 'Vinchos', '1', null, '44');
INSERT INTO `inicio_distrito` VALUES ('456', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050115', 'Jesús Nazareno', '1', null, '44');
INSERT INTO `inicio_distrito` VALUES ('457', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050201', 'Cangallo', '1', null, '45');
INSERT INTO `inicio_distrito` VALUES ('458', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050202', 'Chuschi', '1', null, '45');
INSERT INTO `inicio_distrito` VALUES ('459', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050203', 'Los Morochucos', '1', null, '45');
INSERT INTO `inicio_distrito` VALUES ('460', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050204', 'Maria Parado de Bellido', '1', null, '45');
INSERT INTO `inicio_distrito` VALUES ('461', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050205', 'Paras', '1', null, '45');
INSERT INTO `inicio_distrito` VALUES ('462', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050206', 'Totos', '1', null, '45');
INSERT INTO `inicio_distrito` VALUES ('463', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050301', 'Sancos', '1', null, '46');
INSERT INTO `inicio_distrito` VALUES ('464', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050302', 'Carapo', '1', null, '46');
INSERT INTO `inicio_distrito` VALUES ('465', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050303', 'Sacsamarca', '1', null, '46');
INSERT INTO `inicio_distrito` VALUES ('466', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050304', 'Santiago de Lucanamarca', '1', null, '46');
INSERT INTO `inicio_distrito` VALUES ('467', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050401', 'Huanta', '1', null, '47');
INSERT INTO `inicio_distrito` VALUES ('468', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050402', 'Ayahuanco', '1', null, '47');
INSERT INTO `inicio_distrito` VALUES ('469', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050403', 'Huamanguilla', '1', null, '47');
INSERT INTO `inicio_distrito` VALUES ('470', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050404', 'Iguain', '1', null, '47');
INSERT INTO `inicio_distrito` VALUES ('471', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050405', 'Luricocha', '1', null, '47');
INSERT INTO `inicio_distrito` VALUES ('472', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050406', 'Santillana', '1', null, '47');
INSERT INTO `inicio_distrito` VALUES ('473', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050407', 'Sivia', '1', null, '47');
INSERT INTO `inicio_distrito` VALUES ('474', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050408', 'Llochegua', '1', null, '47');
INSERT INTO `inicio_distrito` VALUES ('475', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050501', 'San Miguel', '1', null, '48');
INSERT INTO `inicio_distrito` VALUES ('476', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050502', 'Anco', '1', null, '48');
INSERT INTO `inicio_distrito` VALUES ('477', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050503', 'Ayna', '1', null, '48');
INSERT INTO `inicio_distrito` VALUES ('478', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050504', 'Chilcas', '1', null, '48');
INSERT INTO `inicio_distrito` VALUES ('479', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050505', 'Chungui', '1', null, '48');
INSERT INTO `inicio_distrito` VALUES ('480', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050506', 'Luis Carranza', '1', null, '48');
INSERT INTO `inicio_distrito` VALUES ('481', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050507', 'Santa Rosa', '1', null, '48');
INSERT INTO `inicio_distrito` VALUES ('482', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050508', 'Tambo', '1', null, '48');
INSERT INTO `inicio_distrito` VALUES ('483', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050601', 'Puquio', '1', null, '49');
INSERT INTO `inicio_distrito` VALUES ('484', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050602', 'Aucara', '1', null, '49');
INSERT INTO `inicio_distrito` VALUES ('485', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050603', 'Cabana', '1', null, '49');
INSERT INTO `inicio_distrito` VALUES ('486', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050604', 'Carmen Salcedo', '1', null, '49');
INSERT INTO `inicio_distrito` VALUES ('487', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050605', 'Chaviqa', '1', null, '49');
INSERT INTO `inicio_distrito` VALUES ('488', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050606', 'Chipao', '1', null, '49');
INSERT INTO `inicio_distrito` VALUES ('489', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050607', 'Huac-Huas', '1', null, '49');
INSERT INTO `inicio_distrito` VALUES ('490', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050608', 'Laramate', '1', null, '49');
INSERT INTO `inicio_distrito` VALUES ('491', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050609', 'Leoncio Prado', '1', null, '49');
INSERT INTO `inicio_distrito` VALUES ('492', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050610', 'Llauta', '1', null, '49');
INSERT INTO `inicio_distrito` VALUES ('493', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050611', 'Lucanas', '1', null, '49');
INSERT INTO `inicio_distrito` VALUES ('494', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050612', 'Ocaqa', '1', null, '49');
INSERT INTO `inicio_distrito` VALUES ('495', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050613', 'Otoca', '1', null, '49');
INSERT INTO `inicio_distrito` VALUES ('496', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050614', 'Saisa', '1', null, '49');
INSERT INTO `inicio_distrito` VALUES ('497', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050615', 'San Cristobal', '1', null, '49');
INSERT INTO `inicio_distrito` VALUES ('498', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050616', 'San Juan', '1', null, '49');
INSERT INTO `inicio_distrito` VALUES ('499', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050617', 'San Pedro', '1', null, '49');
INSERT INTO `inicio_distrito` VALUES ('500', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050618', 'San Pedro de Palco', '1', null, '49');
INSERT INTO `inicio_distrito` VALUES ('501', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050619', 'Sancos', '1', null, '49');
INSERT INTO `inicio_distrito` VALUES ('502', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050620', 'Santa Ana de Huaycahuacho', '1', null, '49');
INSERT INTO `inicio_distrito` VALUES ('503', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050621', 'Santa Lucia', '1', null, '49');
INSERT INTO `inicio_distrito` VALUES ('504', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050701', 'Coracora', '1', null, '50');
INSERT INTO `inicio_distrito` VALUES ('505', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050702', 'Chumpi', '1', null, '50');
INSERT INTO `inicio_distrito` VALUES ('506', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050703', 'Coronel Castaqeda', '1', null, '50');
INSERT INTO `inicio_distrito` VALUES ('507', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050704', 'Pacapausa', '1', null, '50');
INSERT INTO `inicio_distrito` VALUES ('508', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050705', 'Pullo', '1', null, '50');
INSERT INTO `inicio_distrito` VALUES ('509', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050706', 'Puyusca', '1', null, '50');
INSERT INTO `inicio_distrito` VALUES ('510', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050707', 'San Francisco de Ravacayco', '1', null, '50');
INSERT INTO `inicio_distrito` VALUES ('511', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050708', 'Upahuacho', '1', null, '50');
INSERT INTO `inicio_distrito` VALUES ('512', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050801', 'Pausa', '1', null, '51');
INSERT INTO `inicio_distrito` VALUES ('513', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050802', 'Colta', '1', null, '51');
INSERT INTO `inicio_distrito` VALUES ('514', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050803', 'Corculla', '1', null, '51');
INSERT INTO `inicio_distrito` VALUES ('515', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050804', 'Lampa', '1', null, '51');
INSERT INTO `inicio_distrito` VALUES ('516', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050805', 'Marcabamba', '1', null, '51');
INSERT INTO `inicio_distrito` VALUES ('517', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050806', 'Oyolo', '1', null, '51');
INSERT INTO `inicio_distrito` VALUES ('518', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050807', 'Pararca', '1', null, '51');
INSERT INTO `inicio_distrito` VALUES ('519', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050808', 'San Javier de Alpabamba', '1', null, '51');
INSERT INTO `inicio_distrito` VALUES ('520', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050809', 'San Jose de Ushua', '1', null, '51');
INSERT INTO `inicio_distrito` VALUES ('521', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050810', 'Sara Sara', '1', null, '51');
INSERT INTO `inicio_distrito` VALUES ('522', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050901', 'Querobamba', '1', null, '52');
INSERT INTO `inicio_distrito` VALUES ('523', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050902', 'Belen', '1', null, '52');
INSERT INTO `inicio_distrito` VALUES ('524', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050903', 'Chalcos', '1', null, '52');
INSERT INTO `inicio_distrito` VALUES ('525', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050904', 'Chilcayoc', '1', null, '52');
INSERT INTO `inicio_distrito` VALUES ('526', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050905', 'Huacaqa', '1', null, '52');
INSERT INTO `inicio_distrito` VALUES ('527', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050906', 'Morcolla', '1', null, '52');
INSERT INTO `inicio_distrito` VALUES ('528', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050907', 'Paico', '1', null, '52');
INSERT INTO `inicio_distrito` VALUES ('529', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050908', 'San Pedro de Larcay', '1', null, '52');
INSERT INTO `inicio_distrito` VALUES ('530', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050909', 'San Salvador de Quije', '1', null, '52');
INSERT INTO `inicio_distrito` VALUES ('531', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050910', 'Santiago de Paucaray', '1', null, '52');
INSERT INTO `inicio_distrito` VALUES ('532', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '050911', 'Soras', '1', null, '52');
INSERT INTO `inicio_distrito` VALUES ('533', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '051001', 'Huancapi', '1', null, '53');
INSERT INTO `inicio_distrito` VALUES ('534', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '051002', 'Alcamenca', '1', null, '53');
INSERT INTO `inicio_distrito` VALUES ('535', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '051003', 'Apongo', '1', null, '53');
INSERT INTO `inicio_distrito` VALUES ('536', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '051004', 'Asquipata', '1', null, '53');
INSERT INTO `inicio_distrito` VALUES ('537', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '051005', 'Canaria', '1', null, '53');
INSERT INTO `inicio_distrito` VALUES ('538', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '051006', 'Cayara', '1', null, '53');
INSERT INTO `inicio_distrito` VALUES ('539', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '051007', 'Colca', '1', null, '53');
INSERT INTO `inicio_distrito` VALUES ('540', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '051008', 'Huamanquiquia', '1', null, '53');
INSERT INTO `inicio_distrito` VALUES ('541', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '051009', 'Huancaraylla', '1', null, '53');
INSERT INTO `inicio_distrito` VALUES ('542', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '051010', 'Huaya', '1', null, '53');
INSERT INTO `inicio_distrito` VALUES ('543', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '051011', 'Sarhua', '1', null, '53');
INSERT INTO `inicio_distrito` VALUES ('544', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '051012', 'Vilcanchos', '1', null, '53');
INSERT INTO `inicio_distrito` VALUES ('545', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '051101', 'Vilcas Huaman', '1', null, '54');
INSERT INTO `inicio_distrito` VALUES ('546', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '051102', 'Accomarca', '1', null, '54');
INSERT INTO `inicio_distrito` VALUES ('547', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '051103', 'Carhuanca', '1', null, '54');
INSERT INTO `inicio_distrito` VALUES ('548', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '051104', 'Concepcion', '1', null, '54');
INSERT INTO `inicio_distrito` VALUES ('549', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '051105', 'Huambalpa', '1', null, '54');
INSERT INTO `inicio_distrito` VALUES ('550', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '051106', 'Independencia', '1', null, '54');
INSERT INTO `inicio_distrito` VALUES ('551', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '051107', 'Saurama', '1', null, '54');
INSERT INTO `inicio_distrito` VALUES ('552', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '051108', 'Vischongo', '1', null, '54');
INSERT INTO `inicio_distrito` VALUES ('554', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060102', 'Asuncion', '1', null, '1');
INSERT INTO `inicio_distrito` VALUES ('555', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060103', 'Chetilla', '1', null, '1');
INSERT INTO `inicio_distrito` VALUES ('556', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060104', 'Cospan', '1', null, '1');
INSERT INTO `inicio_distrito` VALUES ('557', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060105', 'Encañada', '1', null, '1');
INSERT INTO `inicio_distrito` VALUES ('558', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060106', 'Jesus', '1', null, '1');
INSERT INTO `inicio_distrito` VALUES ('559', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060107', 'Llacanora', '1', null, '1');
INSERT INTO `inicio_distrito` VALUES ('560', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060108', 'Los Baños del Inca', '1', null, '1');
INSERT INTO `inicio_distrito` VALUES ('561', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060109', 'Magdalena', '1', null, '1');
INSERT INTO `inicio_distrito` VALUES ('562', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060110', 'Matara', '1', null, '1');
INSERT INTO `inicio_distrito` VALUES ('563', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060111', 'Namora', '1', null, '1');
INSERT INTO `inicio_distrito` VALUES ('564', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060112', 'San Juan', '1', null, '1');
INSERT INTO `inicio_distrito` VALUES ('565', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060199', 'Multidistrital', '1', null, '1');
INSERT INTO `inicio_distrito` VALUES ('566', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060201', 'Cajabamba', '1', null, '56');
INSERT INTO `inicio_distrito` VALUES ('567', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060202', 'Cachachi', '1', null, '56');
INSERT INTO `inicio_distrito` VALUES ('568', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060203', 'Condebamba', '1', null, '56');
INSERT INTO `inicio_distrito` VALUES ('569', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060204', 'Sitacocha', '1', null, '56');
INSERT INTO `inicio_distrito` VALUES ('570', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060299', 'Multidistrital', '1', null, '56');
INSERT INTO `inicio_distrito` VALUES ('571', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060301', 'Celendin', '1', null, '57');
INSERT INTO `inicio_distrito` VALUES ('572', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060302', 'Chumuch', '1', null, '57');
INSERT INTO `inicio_distrito` VALUES ('573', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060303', 'Cortegana', '1', null, '57');
INSERT INTO `inicio_distrito` VALUES ('574', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060304', 'Huasmin', '1', null, '57');
INSERT INTO `inicio_distrito` VALUES ('575', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060305', 'Jorge Chavez', '1', null, '57');
INSERT INTO `inicio_distrito` VALUES ('576', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060306', 'Jose Galvez', '1', null, '57');
INSERT INTO `inicio_distrito` VALUES ('577', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060307', 'Miguel Iglesias', '1', null, '57');
INSERT INTO `inicio_distrito` VALUES ('578', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060308', 'Oxamarca', '1', null, '57');
INSERT INTO `inicio_distrito` VALUES ('579', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060309', 'Sorochuco', '1', null, '57');
INSERT INTO `inicio_distrito` VALUES ('580', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060310', 'Sucre', '1', null, '57');
INSERT INTO `inicio_distrito` VALUES ('581', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060311', 'Utco', '1', null, '57');
INSERT INTO `inicio_distrito` VALUES ('582', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060312', 'La Libertad de Pallan', '1', null, '57');
INSERT INTO `inicio_distrito` VALUES ('583', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060399', 'Multidistrital', '1', null, '57');
INSERT INTO `inicio_distrito` VALUES ('584', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060401', 'Chota', '1', null, '58');
INSERT INTO `inicio_distrito` VALUES ('585', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060402', 'Anguia', '1', null, '58');
INSERT INTO `inicio_distrito` VALUES ('586', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060403', 'Chadin', '1', null, '58');
INSERT INTO `inicio_distrito` VALUES ('587', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060404', 'Chiguirip', '1', null, '58');
INSERT INTO `inicio_distrito` VALUES ('588', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060405', 'Chimban', '1', null, '58');
INSERT INTO `inicio_distrito` VALUES ('589', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060406', 'Choropampa', '1', null, '58');
INSERT INTO `inicio_distrito` VALUES ('590', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060407', 'Cochabamba', '1', null, '58');
INSERT INTO `inicio_distrito` VALUES ('591', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060408', 'Conchan', '1', null, '58');
INSERT INTO `inicio_distrito` VALUES ('592', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060409', 'Huambos', '1', null, '58');
INSERT INTO `inicio_distrito` VALUES ('593', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060410', 'Lajas', '1', null, '58');
INSERT INTO `inicio_distrito` VALUES ('594', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060411', 'Llama', '1', null, '58');
INSERT INTO `inicio_distrito` VALUES ('595', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060412', 'Miracosta', '1', null, '58');
INSERT INTO `inicio_distrito` VALUES ('596', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060413', 'Paccha', '1', null, '58');
INSERT INTO `inicio_distrito` VALUES ('597', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060414', 'Pion', '1', null, '58');
INSERT INTO `inicio_distrito` VALUES ('598', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060415', 'Querocoto', '1', null, '58');
INSERT INTO `inicio_distrito` VALUES ('599', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060416', 'San Juan de Licupis', '1', null, '58');
INSERT INTO `inicio_distrito` VALUES ('600', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060417', 'Tacabamba', '1', null, '58');
INSERT INTO `inicio_distrito` VALUES ('601', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060418', 'Tocmoche', '1', null, '58');
INSERT INTO `inicio_distrito` VALUES ('602', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060419', 'Chalamarca', '1', null, '58');
INSERT INTO `inicio_distrito` VALUES ('603', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060499', 'Multidistrital', '1', null, '58');
INSERT INTO `inicio_distrito` VALUES ('604', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060501', 'Contumaza', '1', null, '59');
INSERT INTO `inicio_distrito` VALUES ('605', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060502', 'Chilete', '1', null, '59');
INSERT INTO `inicio_distrito` VALUES ('606', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060503', 'Cupisnique', '1', null, '59');
INSERT INTO `inicio_distrito` VALUES ('607', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060504', 'Guzmango', '1', null, '59');
INSERT INTO `inicio_distrito` VALUES ('608', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060505', 'San Benito', '1', null, '59');
INSERT INTO `inicio_distrito` VALUES ('609', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060506', 'Santa Cruz de Toled', '1', null, '59');
INSERT INTO `inicio_distrito` VALUES ('610', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060507', 'Tantarica', '1', null, '59');
INSERT INTO `inicio_distrito` VALUES ('611', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060508', 'Yonan', '1', null, '59');
INSERT INTO `inicio_distrito` VALUES ('612', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060599', 'Multidistrital', '1', null, '59');
INSERT INTO `inicio_distrito` VALUES ('613', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060601', 'Cutervo', '1', null, '60');
INSERT INTO `inicio_distrito` VALUES ('614', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060602', 'Callayuc', '1', null, '60');
INSERT INTO `inicio_distrito` VALUES ('615', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060603', 'Choros', '1', null, '60');
INSERT INTO `inicio_distrito` VALUES ('616', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060604', 'Cujillo', '1', null, '60');
INSERT INTO `inicio_distrito` VALUES ('617', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060605', 'La Ramada', '1', null, '60');
INSERT INTO `inicio_distrito` VALUES ('618', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060606', 'Pimpingos', '1', null, '60');
INSERT INTO `inicio_distrito` VALUES ('619', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060607', 'Querocotillo', '1', null, '60');
INSERT INTO `inicio_distrito` VALUES ('620', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060608', 'San Andres de Cutervo', '1', null, '60');
INSERT INTO `inicio_distrito` VALUES ('621', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060609', 'San Juan de Cutervo', '1', null, '60');
INSERT INTO `inicio_distrito` VALUES ('622', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060610', 'San Luis de Lucma', '1', null, '60');
INSERT INTO `inicio_distrito` VALUES ('623', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060611', 'Santa Cruz', '1', null, '60');
INSERT INTO `inicio_distrito` VALUES ('624', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060612', 'Santo Domingo de la Capilla', '1', null, '60');
INSERT INTO `inicio_distrito` VALUES ('625', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060613', 'Santo Tomas', '1', null, '60');
INSERT INTO `inicio_distrito` VALUES ('626', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060614', 'Socota', '1', null, '60');
INSERT INTO `inicio_distrito` VALUES ('627', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060615', 'Toribio Casanova', '1', null, '60');
INSERT INTO `inicio_distrito` VALUES ('628', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060699', 'Multidistrital', '1', null, '60');
INSERT INTO `inicio_distrito` VALUES ('629', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060701', 'Bambamarca', '1', null, '61');
INSERT INTO `inicio_distrito` VALUES ('630', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060702', 'Chugur', '1', null, '61');
INSERT INTO `inicio_distrito` VALUES ('631', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060703', 'Hualgayoc', '1', null, '61');
INSERT INTO `inicio_distrito` VALUES ('632', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060799', 'Multidistrital', '1', null, '61');
INSERT INTO `inicio_distrito` VALUES ('633', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060801', 'Jaen', '1', null, '62');
INSERT INTO `inicio_distrito` VALUES ('634', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060802', 'Bellavista', '1', null, '62');
INSERT INTO `inicio_distrito` VALUES ('635', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060803', 'Chontali', '1', null, '62');
INSERT INTO `inicio_distrito` VALUES ('636', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060804', 'Colasay', '1', null, '62');
INSERT INTO `inicio_distrito` VALUES ('637', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060805', 'Huabal', '1', null, '62');
INSERT INTO `inicio_distrito` VALUES ('638', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060806', 'Las Pirias', '1', null, '62');
INSERT INTO `inicio_distrito` VALUES ('639', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060807', 'Pomahuaca', '1', null, '62');
INSERT INTO `inicio_distrito` VALUES ('640', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060808', 'Pucara', '1', null, '62');
INSERT INTO `inicio_distrito` VALUES ('641', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060809', 'Sallique', '1', null, '62');
INSERT INTO `inicio_distrito` VALUES ('642', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060810', 'San Felipe', '1', null, '62');
INSERT INTO `inicio_distrito` VALUES ('643', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060811', 'San Jose del Alto', '1', null, '62');
INSERT INTO `inicio_distrito` VALUES ('644', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060812', 'Santa Rosa', '1', null, '62');
INSERT INTO `inicio_distrito` VALUES ('645', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060899', 'Multidistrital', '1', null, '62');
INSERT INTO `inicio_distrito` VALUES ('646', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060901', 'San Ignacio', '1', null, '63');
INSERT INTO `inicio_distrito` VALUES ('647', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060902', 'Chirinos', '1', null, '63');
INSERT INTO `inicio_distrito` VALUES ('648', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060903', 'Huarango', '1', null, '63');
INSERT INTO `inicio_distrito` VALUES ('649', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060904', 'La Coipa', '1', null, '63');
INSERT INTO `inicio_distrito` VALUES ('650', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060905', 'Namballe', '1', null, '63');
INSERT INTO `inicio_distrito` VALUES ('651', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060906', 'San Jose de Lourdes', '1', null, '63');
INSERT INTO `inicio_distrito` VALUES ('652', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060907', 'Tabaconas', '1', null, '63');
INSERT INTO `inicio_distrito` VALUES ('653', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '060999', 'Multidistrital', '1', null, '63');
INSERT INTO `inicio_distrito` VALUES ('654', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061001', 'Pedro Galvez', '1', null, '64');
INSERT INTO `inicio_distrito` VALUES ('655', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061002', 'Chancay', '1', null, '64');
INSERT INTO `inicio_distrito` VALUES ('656', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061003', 'Eduardo Villanueva', '1', null, '64');
INSERT INTO `inicio_distrito` VALUES ('657', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061004', 'Gregorio Pita', '1', null, '64');
INSERT INTO `inicio_distrito` VALUES ('658', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061005', 'Ichocan', '1', null, '64');
INSERT INTO `inicio_distrito` VALUES ('659', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061006', 'Jose Manuel Quiroz', '1', null, '64');
INSERT INTO `inicio_distrito` VALUES ('660', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061007', 'Jose Sabogal', '1', null, '64');
INSERT INTO `inicio_distrito` VALUES ('661', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061099', 'Multidistrital', '1', null, '64');
INSERT INTO `inicio_distrito` VALUES ('662', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061101', 'San Miguel', '1', null, '65');
INSERT INTO `inicio_distrito` VALUES ('663', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061102', 'Bolivar', '1', null, '65');
INSERT INTO `inicio_distrito` VALUES ('664', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061103', 'Calquis', '1', null, '65');
INSERT INTO `inicio_distrito` VALUES ('665', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061104', 'Catilluc', '1', null, '65');
INSERT INTO `inicio_distrito` VALUES ('666', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061105', 'El Prado', '1', null, '65');
INSERT INTO `inicio_distrito` VALUES ('667', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061106', 'La Florida', '1', null, '65');
INSERT INTO `inicio_distrito` VALUES ('668', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061107', 'Llapa', '1', null, '65');
INSERT INTO `inicio_distrito` VALUES ('669', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061108', 'Nanchoc', '1', null, '65');
INSERT INTO `inicio_distrito` VALUES ('670', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061109', 'Niepos', '1', null, '65');
INSERT INTO `inicio_distrito` VALUES ('671', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061110', 'San Gregorio', '1', null, '65');
INSERT INTO `inicio_distrito` VALUES ('672', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061111', 'San Silvestre de Cochan', '1', null, '65');
INSERT INTO `inicio_distrito` VALUES ('673', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061112', 'Tongod', '1', null, '65');
INSERT INTO `inicio_distrito` VALUES ('674', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061113', 'Union Agua Blanca', '1', null, '65');
INSERT INTO `inicio_distrito` VALUES ('675', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061199', 'Multidistrital', '1', null, '65');
INSERT INTO `inicio_distrito` VALUES ('676', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061201', 'San Pablo', '1', null, '66');
INSERT INTO `inicio_distrito` VALUES ('677', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061202', 'San Bernardino', '1', null, '66');
INSERT INTO `inicio_distrito` VALUES ('678', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061203', 'San Luis', '1', null, '66');
INSERT INTO `inicio_distrito` VALUES ('679', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061204', 'Tumbaden', '1', null, '66');
INSERT INTO `inicio_distrito` VALUES ('680', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061299', 'Multidistrital', '1', null, '66');
INSERT INTO `inicio_distrito` VALUES ('681', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061301', 'Santa Cruz', '1', null, '67');
INSERT INTO `inicio_distrito` VALUES ('682', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061302', 'Andabamba', '1', null, '67');
INSERT INTO `inicio_distrito` VALUES ('683', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061303', 'Catache', '1', null, '67');
INSERT INTO `inicio_distrito` VALUES ('684', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061304', 'Chancaybaqos', '1', null, '67');
INSERT INTO `inicio_distrito` VALUES ('685', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061305', 'La Esperanza', '1', null, '67');
INSERT INTO `inicio_distrito` VALUES ('686', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061306', 'Ninabamba', '1', null, '67');
INSERT INTO `inicio_distrito` VALUES ('687', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061307', 'Pulan', '1', null, '67');
INSERT INTO `inicio_distrito` VALUES ('688', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061308', 'Saucepampa', '1', null, '67');
INSERT INTO `inicio_distrito` VALUES ('689', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061309', 'Sexi', '1', null, '67');
INSERT INTO `inicio_distrito` VALUES ('690', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061310', 'Uticyacu', '1', null, '67');
INSERT INTO `inicio_distrito` VALUES ('691', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061311', 'Yauyucan', '1', null, '67');
INSERT INTO `inicio_distrito` VALUES ('692', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '061399', 'Multidistrital', '1', null, '67');
INSERT INTO `inicio_distrito` VALUES ('693', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '070101', 'Callao', '1', null, '68');
INSERT INTO `inicio_distrito` VALUES ('694', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '070102', 'Bellavista', '1', null, '68');
INSERT INTO `inicio_distrito` VALUES ('695', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '070103', 'Carmen de la Legua Reynoso', '1', null, '68');
INSERT INTO `inicio_distrito` VALUES ('696', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '070104', 'La Perla', '1', null, '68');
INSERT INTO `inicio_distrito` VALUES ('697', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '070105', 'La Punta', '1', null, '68');
INSERT INTO `inicio_distrito` VALUES ('698', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '070106', 'Ventanilla', '1', null, '68');
INSERT INTO `inicio_distrito` VALUES ('699', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080101', 'Cusco', '1', null, '69');
INSERT INTO `inicio_distrito` VALUES ('700', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080102', 'Ccorca', '1', null, '69');
INSERT INTO `inicio_distrito` VALUES ('701', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080103', 'Poroy', '1', null, '69');
INSERT INTO `inicio_distrito` VALUES ('702', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080104', 'San Jeronimo', '1', null, '69');
INSERT INTO `inicio_distrito` VALUES ('703', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080105', 'San Sebastian', '1', null, '69');
INSERT INTO `inicio_distrito` VALUES ('704', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080106', 'Santiago', '1', null, '69');
INSERT INTO `inicio_distrito` VALUES ('705', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080107', 'Saylla', '1', null, '69');
INSERT INTO `inicio_distrito` VALUES ('706', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080108', 'Wanchaq', '1', null, '69');
INSERT INTO `inicio_distrito` VALUES ('707', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080201', 'Acomayo', '1', null, '70');
INSERT INTO `inicio_distrito` VALUES ('708', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080202', 'Acopia', '1', null, '70');
INSERT INTO `inicio_distrito` VALUES ('709', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080203', 'Acos', '1', null, '70');
INSERT INTO `inicio_distrito` VALUES ('710', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080204', 'Mosoc Llacta', '1', null, '70');
INSERT INTO `inicio_distrito` VALUES ('711', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080205', 'Pomacanchi', '1', null, '70');
INSERT INTO `inicio_distrito` VALUES ('712', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080206', 'Rondocan', '1', null, '70');
INSERT INTO `inicio_distrito` VALUES ('713', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080207', 'Sangarara', '1', null, '70');
INSERT INTO `inicio_distrito` VALUES ('714', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080301', 'Anta', '1', null, '71');
INSERT INTO `inicio_distrito` VALUES ('715', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080302', 'Ancahuasi', '1', null, '71');
INSERT INTO `inicio_distrito` VALUES ('716', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080303', 'Cachimayo', '1', null, '71');
INSERT INTO `inicio_distrito` VALUES ('717', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080304', 'Chinchaypujio', '1', null, '71');
INSERT INTO `inicio_distrito` VALUES ('718', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080305', 'Huarocondo', '1', null, '71');
INSERT INTO `inicio_distrito` VALUES ('719', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080306', 'Limatambo', '1', null, '71');
INSERT INTO `inicio_distrito` VALUES ('720', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080307', 'Mollepata', '1', null, '71');
INSERT INTO `inicio_distrito` VALUES ('721', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080308', 'Pucyura', '1', null, '71');
INSERT INTO `inicio_distrito` VALUES ('722', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080309', 'Zurite', '1', null, '71');
INSERT INTO `inicio_distrito` VALUES ('723', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080401', 'Calca', '1', null, '72');
INSERT INTO `inicio_distrito` VALUES ('724', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080402', 'Coya', '1', null, '72');
INSERT INTO `inicio_distrito` VALUES ('725', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080403', 'Lamay', '1', null, '72');
INSERT INTO `inicio_distrito` VALUES ('726', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080404', 'Lares', '1', null, '72');
INSERT INTO `inicio_distrito` VALUES ('727', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080405', 'Pisac', '1', null, '72');
INSERT INTO `inicio_distrito` VALUES ('728', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080406', 'San Salvador', '1', null, '72');
INSERT INTO `inicio_distrito` VALUES ('729', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080407', 'Taray', '1', null, '72');
INSERT INTO `inicio_distrito` VALUES ('730', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080408', 'Yanatile', '1', null, '72');
INSERT INTO `inicio_distrito` VALUES ('731', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080501', 'Yanaoca', '1', null, '73');
INSERT INTO `inicio_distrito` VALUES ('732', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080502', 'Checca', '1', null, '73');
INSERT INTO `inicio_distrito` VALUES ('733', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080503', 'Kunturkanki', '1', null, '73');
INSERT INTO `inicio_distrito` VALUES ('734', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080504', 'Langui', '1', null, '73');
INSERT INTO `inicio_distrito` VALUES ('735', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080505', 'Layo', '1', null, '73');
INSERT INTO `inicio_distrito` VALUES ('736', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080506', 'Pampamarca', '1', null, '73');
INSERT INTO `inicio_distrito` VALUES ('737', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080507', 'Quehue', '1', null, '73');
INSERT INTO `inicio_distrito` VALUES ('738', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080508', 'Tupac Amaru', '1', null, '73');
INSERT INTO `inicio_distrito` VALUES ('739', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080601', 'Sicuani', '1', null, '74');
INSERT INTO `inicio_distrito` VALUES ('740', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080602', 'Checacupe', '1', null, '74');
INSERT INTO `inicio_distrito` VALUES ('741', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080603', 'Combapata', '1', null, '74');
INSERT INTO `inicio_distrito` VALUES ('742', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080604', 'Marangani', '1', null, '74');
INSERT INTO `inicio_distrito` VALUES ('743', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080605', 'Pitumarca', '1', null, '74');
INSERT INTO `inicio_distrito` VALUES ('744', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080606', 'San Pablo', '1', null, '74');
INSERT INTO `inicio_distrito` VALUES ('745', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080607', 'San Pedro', '1', null, '74');
INSERT INTO `inicio_distrito` VALUES ('746', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080608', 'Tinta', '1', null, '74');
INSERT INTO `inicio_distrito` VALUES ('747', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080701', 'Santo Tomas', '1', null, '75');
INSERT INTO `inicio_distrito` VALUES ('748', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080702', 'Capacmarca', '1', null, '75');
INSERT INTO `inicio_distrito` VALUES ('749', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080703', 'Chamaca', '1', null, '75');
INSERT INTO `inicio_distrito` VALUES ('750', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080704', 'Colquemarca', '1', null, '75');
INSERT INTO `inicio_distrito` VALUES ('751', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080705', 'Livitaca', '1', null, '75');
INSERT INTO `inicio_distrito` VALUES ('752', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080706', 'Llusco', '1', null, '75');
INSERT INTO `inicio_distrito` VALUES ('753', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080707', 'Quiqota', '1', null, '75');
INSERT INTO `inicio_distrito` VALUES ('754', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080708', 'Velille', '1', null, '75');
INSERT INTO `inicio_distrito` VALUES ('755', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080801', 'Espinar', '1', null, '76');
INSERT INTO `inicio_distrito` VALUES ('756', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080802', 'Condoroma', '1', null, '76');
INSERT INTO `inicio_distrito` VALUES ('757', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080803', 'Coporaque', '1', null, '76');
INSERT INTO `inicio_distrito` VALUES ('758', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080804', 'Ocoruro', '1', null, '76');
INSERT INTO `inicio_distrito` VALUES ('759', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080805', 'Pallpata', '1', null, '76');
INSERT INTO `inicio_distrito` VALUES ('760', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080806', 'Pichigua', '1', null, '76');
INSERT INTO `inicio_distrito` VALUES ('761', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080807', 'Suyckutambo', '1', null, '76');
INSERT INTO `inicio_distrito` VALUES ('762', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080808', 'Alto Pichigua', '1', null, '76');
INSERT INTO `inicio_distrito` VALUES ('763', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080901', 'Santa Ana', '1', null, '77');
INSERT INTO `inicio_distrito` VALUES ('764', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080902', 'Echarate', '1', null, '77');
INSERT INTO `inicio_distrito` VALUES ('765', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080903', 'Huayopata', '1', null, '77');
INSERT INTO `inicio_distrito` VALUES ('766', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080904', 'Maranura', '1', null, '77');
INSERT INTO `inicio_distrito` VALUES ('767', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080905', 'Ocobamba', '1', null, '77');
INSERT INTO `inicio_distrito` VALUES ('768', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080906', 'Quellouno', '1', null, '77');
INSERT INTO `inicio_distrito` VALUES ('769', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080907', 'Quimbiri', '1', null, '77');
INSERT INTO `inicio_distrito` VALUES ('770', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080908', 'Santa Teresa', '1', null, '77');
INSERT INTO `inicio_distrito` VALUES ('771', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080909', 'Vilcabamba', '1', null, '77');
INSERT INTO `inicio_distrito` VALUES ('772', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '080910', 'Pichari', '1', null, '77');
INSERT INTO `inicio_distrito` VALUES ('773', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081001', 'Paruro', '1', null, '78');
INSERT INTO `inicio_distrito` VALUES ('774', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081002', 'Accha', '1', null, '78');
INSERT INTO `inicio_distrito` VALUES ('775', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081003', 'Ccapi', '1', null, '78');
INSERT INTO `inicio_distrito` VALUES ('776', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081004', 'Colcha', '1', null, '78');
INSERT INTO `inicio_distrito` VALUES ('777', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081005', 'Huanoquite', '1', null, '78');
INSERT INTO `inicio_distrito` VALUES ('778', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081006', 'Omacha', '1', null, '78');
INSERT INTO `inicio_distrito` VALUES ('779', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081007', 'Paccaritambo', '1', null, '78');
INSERT INTO `inicio_distrito` VALUES ('780', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081008', 'Pillpinto', '1', null, '78');
INSERT INTO `inicio_distrito` VALUES ('781', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081009', 'Yaurisque', '1', null, '78');
INSERT INTO `inicio_distrito` VALUES ('782', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081101', 'Paucartambo', '1', null, '79');
INSERT INTO `inicio_distrito` VALUES ('783', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081102', 'Caicay', '1', null, '79');
INSERT INTO `inicio_distrito` VALUES ('784', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081103', 'Challabamba', '1', null, '79');
INSERT INTO `inicio_distrito` VALUES ('785', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081104', 'Colquepata', '1', null, '79');
INSERT INTO `inicio_distrito` VALUES ('786', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081105', 'Huancarani', '1', null, '79');
INSERT INTO `inicio_distrito` VALUES ('787', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081106', 'Kosqipata', '1', null, '79');
INSERT INTO `inicio_distrito` VALUES ('788', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081201', 'Urcos', '1', null, '80');
INSERT INTO `inicio_distrito` VALUES ('789', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081202', 'Andahuaylillas', '1', null, '80');
INSERT INTO `inicio_distrito` VALUES ('790', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081203', 'Camanti', '1', null, '80');
INSERT INTO `inicio_distrito` VALUES ('791', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081204', 'Ccarhuayo', '1', null, '80');
INSERT INTO `inicio_distrito` VALUES ('792', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081205', 'Ccatca', '1', null, '80');
INSERT INTO `inicio_distrito` VALUES ('793', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081206', 'Cusipata', '1', null, '80');
INSERT INTO `inicio_distrito` VALUES ('794', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081207', 'Huaro', '1', null, '80');
INSERT INTO `inicio_distrito` VALUES ('795', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081208', 'Lucre', '1', null, '80');
INSERT INTO `inicio_distrito` VALUES ('796', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081209', 'Marcapata', '1', null, '80');
INSERT INTO `inicio_distrito` VALUES ('797', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081210', 'Ocongate', '1', null, '80');
INSERT INTO `inicio_distrito` VALUES ('798', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081211', 'Oropesa', '1', null, '80');
INSERT INTO `inicio_distrito` VALUES ('799', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081212', 'Quiquijana', '1', null, '80');
INSERT INTO `inicio_distrito` VALUES ('800', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081301', 'Urubamba', '1', null, '81');
INSERT INTO `inicio_distrito` VALUES ('801', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081302', 'Chinchero', '1', null, '81');
INSERT INTO `inicio_distrito` VALUES ('802', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081303', 'Huayllabamba', '1', null, '81');
INSERT INTO `inicio_distrito` VALUES ('803', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081304', 'Machupicchu', '1', null, '81');
INSERT INTO `inicio_distrito` VALUES ('804', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081305', 'Maras', '1', null, '81');
INSERT INTO `inicio_distrito` VALUES ('805', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081306', 'Ollantaytambo', '1', null, '81');
INSERT INTO `inicio_distrito` VALUES ('806', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '081307', 'Yucay', '1', null, '81');
INSERT INTO `inicio_distrito` VALUES ('807', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090101', 'Huancavelica', '1', null, '82');
INSERT INTO `inicio_distrito` VALUES ('808', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090102', 'Acobambilla', '1', null, '82');
INSERT INTO `inicio_distrito` VALUES ('809', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090103', 'Acoria', '1', null, '82');
INSERT INTO `inicio_distrito` VALUES ('810', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090104', 'Conayca', '1', null, '82');
INSERT INTO `inicio_distrito` VALUES ('811', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090105', 'Cuenca', '1', null, '82');
INSERT INTO `inicio_distrito` VALUES ('812', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090106', 'Huachocolpa', '1', null, '82');
INSERT INTO `inicio_distrito` VALUES ('813', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090107', 'Huayllahuara', '1', null, '82');
INSERT INTO `inicio_distrito` VALUES ('814', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090108', 'Izcuchaca', '1', null, '82');
INSERT INTO `inicio_distrito` VALUES ('815', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090109', 'Laria', '1', null, '82');
INSERT INTO `inicio_distrito` VALUES ('816', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090110', 'Manta', '1', null, '82');
INSERT INTO `inicio_distrito` VALUES ('817', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090111', 'Mariscal Caceres', '1', null, '82');
INSERT INTO `inicio_distrito` VALUES ('818', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090112', 'Moya', '1', null, '82');
INSERT INTO `inicio_distrito` VALUES ('819', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090113', 'Nuevo Occoro', '1', null, '82');
INSERT INTO `inicio_distrito` VALUES ('820', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090114', 'Palca', '1', null, '82');
INSERT INTO `inicio_distrito` VALUES ('821', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090115', 'Pilchaca', '1', null, '82');
INSERT INTO `inicio_distrito` VALUES ('822', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090116', 'Vilca', '1', null, '82');
INSERT INTO `inicio_distrito` VALUES ('823', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090117', 'Yauli', '1', null, '82');
INSERT INTO `inicio_distrito` VALUES ('824', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090118', 'Ascensión', '1', null, '82');
INSERT INTO `inicio_distrito` VALUES ('825', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090119', 'Huando', '1', null, '82');
INSERT INTO `inicio_distrito` VALUES ('826', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090201', 'Acobamba', '1', null, '83');
INSERT INTO `inicio_distrito` VALUES ('827', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090202', 'Andabamba', '1', null, '83');
INSERT INTO `inicio_distrito` VALUES ('828', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090203', 'Anta', '1', null, '83');
INSERT INTO `inicio_distrito` VALUES ('829', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090204', 'Caja', '1', null, '83');
INSERT INTO `inicio_distrito` VALUES ('830', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090205', 'Marcas', '1', null, '83');
INSERT INTO `inicio_distrito` VALUES ('831', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090206', 'Paucara', '1', null, '83');
INSERT INTO `inicio_distrito` VALUES ('832', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090207', 'Pomacocha', '1', null, '83');
INSERT INTO `inicio_distrito` VALUES ('833', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090208', 'Rosario', '1', null, '83');
INSERT INTO `inicio_distrito` VALUES ('834', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090301', 'Lircay', '1', null, '84');
INSERT INTO `inicio_distrito` VALUES ('835', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090302', 'Anchonga', '1', null, '84');
INSERT INTO `inicio_distrito` VALUES ('836', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090303', 'Callanmarca', '1', null, '84');
INSERT INTO `inicio_distrito` VALUES ('837', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090304', 'Ccochaccasa', '1', null, '84');
INSERT INTO `inicio_distrito` VALUES ('838', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090305', 'Chincho', '1', null, '84');
INSERT INTO `inicio_distrito` VALUES ('839', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090306', 'Congalla', '1', null, '84');
INSERT INTO `inicio_distrito` VALUES ('840', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090307', 'Huanca-Huanca', '1', null, '84');
INSERT INTO `inicio_distrito` VALUES ('841', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090308', 'Huayllay Grande', '1', null, '84');
INSERT INTO `inicio_distrito` VALUES ('842', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090309', 'Julcamarca', '1', null, '84');
INSERT INTO `inicio_distrito` VALUES ('843', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090310', 'San Antonio de Antaparco', '1', null, '84');
INSERT INTO `inicio_distrito` VALUES ('844', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090311', 'Santo Tomas de Pata', '1', null, '84');
INSERT INTO `inicio_distrito` VALUES ('845', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090312', 'Secclla', '1', null, '84');
INSERT INTO `inicio_distrito` VALUES ('846', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090401', 'Castrovirreyna', '1', null, '85');
INSERT INTO `inicio_distrito` VALUES ('847', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090402', 'Arma', '1', null, '85');
INSERT INTO `inicio_distrito` VALUES ('848', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090403', 'Aurahua', '1', null, '85');
INSERT INTO `inicio_distrito` VALUES ('849', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090404', 'Capillas', '1', null, '85');
INSERT INTO `inicio_distrito` VALUES ('850', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090405', 'Chupamarca', '1', null, '85');
INSERT INTO `inicio_distrito` VALUES ('851', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090406', 'Cocas', '1', null, '85');
INSERT INTO `inicio_distrito` VALUES ('852', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090407', 'Huachos', '1', null, '85');
INSERT INTO `inicio_distrito` VALUES ('853', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090408', 'Huamatambo', '1', null, '85');
INSERT INTO `inicio_distrito` VALUES ('854', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090409', 'Mollepampa', '1', null, '85');
INSERT INTO `inicio_distrito` VALUES ('855', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090410', 'San Juan', '1', null, '85');
INSERT INTO `inicio_distrito` VALUES ('856', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090411', 'Santa Ana', '1', null, '85');
INSERT INTO `inicio_distrito` VALUES ('857', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090412', 'Tantara', '1', null, '85');
INSERT INTO `inicio_distrito` VALUES ('858', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090413', 'Ticrapo', '1', null, '85');
INSERT INTO `inicio_distrito` VALUES ('859', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090501', 'Churcampa', '1', null, '86');
INSERT INTO `inicio_distrito` VALUES ('860', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090502', 'Anco', '1', null, '86');
INSERT INTO `inicio_distrito` VALUES ('861', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090503', 'Chinchihuasi', '1', null, '86');
INSERT INTO `inicio_distrito` VALUES ('862', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090504', 'El Carmen', '1', null, '86');
INSERT INTO `inicio_distrito` VALUES ('863', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090505', 'La Merced', '1', null, '86');
INSERT INTO `inicio_distrito` VALUES ('864', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090506', 'Locroja', '1', null, '86');
INSERT INTO `inicio_distrito` VALUES ('865', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090507', 'Paucarbamba', '1', null, '86');
INSERT INTO `inicio_distrito` VALUES ('866', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090508', 'San Miguel de Mayocc', '1', null, '86');
INSERT INTO `inicio_distrito` VALUES ('867', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090509', 'San Pedro de Coris', '1', null, '86');
INSERT INTO `inicio_distrito` VALUES ('868', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090510', 'Pachamarca', '1', null, '86');
INSERT INTO `inicio_distrito` VALUES ('869', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090601', 'Huaytara', '1', null, '87');
INSERT INTO `inicio_distrito` VALUES ('870', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090602', 'Ayavi', '1', null, '87');
INSERT INTO `inicio_distrito` VALUES ('871', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090603', 'Cordova', '1', null, '87');
INSERT INTO `inicio_distrito` VALUES ('872', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090604', 'Huayacundo Arma', '1', null, '87');
INSERT INTO `inicio_distrito` VALUES ('873', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090605', 'Laramarca', '1', null, '87');
INSERT INTO `inicio_distrito` VALUES ('874', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090606', 'Ocoyo', '1', null, '87');
INSERT INTO `inicio_distrito` VALUES ('875', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090607', 'Pilpichaca', '1', null, '87');
INSERT INTO `inicio_distrito` VALUES ('876', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090608', 'Querco', '1', null, '87');
INSERT INTO `inicio_distrito` VALUES ('877', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090609', 'Quito-Arma', '1', null, '87');
INSERT INTO `inicio_distrito` VALUES ('878', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090610', 'San Antonio de Cusicancha', '1', null, '87');
INSERT INTO `inicio_distrito` VALUES ('879', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090611', 'San Francisco de Sangayaico', '1', null, '87');
INSERT INTO `inicio_distrito` VALUES ('880', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090612', 'San Isidro', '1', null, '87');
INSERT INTO `inicio_distrito` VALUES ('881', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090613', 'Santiago de Chocorvos', '1', null, '87');
INSERT INTO `inicio_distrito` VALUES ('882', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090614', 'Santiago de Quirahuara', '1', null, '87');
INSERT INTO `inicio_distrito` VALUES ('883', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090615', 'Santo Domingo de Capillas', '1', null, '87');
INSERT INTO `inicio_distrito` VALUES ('884', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090616', 'Tambo', '1', null, '87');
INSERT INTO `inicio_distrito` VALUES ('885', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090701', 'Pampas', '1', null, '88');
INSERT INTO `inicio_distrito` VALUES ('886', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090702', 'Acostambo', '1', null, '88');
INSERT INTO `inicio_distrito` VALUES ('887', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090703', 'Acraquia', '1', null, '88');
INSERT INTO `inicio_distrito` VALUES ('888', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090704', 'Ahuaycha', '1', null, '88');
INSERT INTO `inicio_distrito` VALUES ('889', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090705', 'Colcabamba', '1', null, '88');
INSERT INTO `inicio_distrito` VALUES ('890', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090706', 'Daniel Hernandez', '1', null, '88');
INSERT INTO `inicio_distrito` VALUES ('891', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090707', 'Huachocolpa', '1', null, '88');
INSERT INTO `inicio_distrito` VALUES ('892', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090708', 'Huando', '1', null, '88');
INSERT INTO `inicio_distrito` VALUES ('893', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090709', 'Huaribamba', '1', null, '88');
INSERT INTO `inicio_distrito` VALUES ('894', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090710', 'Qahuimpuquio', '1', null, '88');
INSERT INTO `inicio_distrito` VALUES ('895', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090711', 'Pazos', '1', null, '88');
INSERT INTO `inicio_distrito` VALUES ('896', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090712', 'Pachamarca', '1', null, '88');
INSERT INTO `inicio_distrito` VALUES ('897', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090713', 'Quishuar', '1', null, '88');
INSERT INTO `inicio_distrito` VALUES ('898', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090714', 'Salcabamba', '1', null, '88');
INSERT INTO `inicio_distrito` VALUES ('899', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090715', 'Salcahuasi', '1', null, '88');
INSERT INTO `inicio_distrito` VALUES ('900', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090716', 'San Marcos de Rocchac', '1', null, '88');
INSERT INTO `inicio_distrito` VALUES ('901', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090717', 'Surcubamba', '1', null, '88');
INSERT INTO `inicio_distrito` VALUES ('902', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '090718', 'Tintay Puncu', '1', null, '88');
INSERT INTO `inicio_distrito` VALUES ('903', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100101', 'Huanuco', '1', null, '89');
INSERT INTO `inicio_distrito` VALUES ('904', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100102', 'Amarilis', '1', null, '89');
INSERT INTO `inicio_distrito` VALUES ('905', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100103', 'Chinchao', '1', null, '89');
INSERT INTO `inicio_distrito` VALUES ('906', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100104', 'Churubamba', '1', null, '89');
INSERT INTO `inicio_distrito` VALUES ('907', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100105', 'Margos', '1', null, '89');
INSERT INTO `inicio_distrito` VALUES ('908', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100106', 'Quisqui', '1', null, '89');
INSERT INTO `inicio_distrito` VALUES ('909', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100107', 'San Francisco de Cayran', '1', null, '89');
INSERT INTO `inicio_distrito` VALUES ('910', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100108', 'San Pedro de Chaulan', '1', null, '89');
INSERT INTO `inicio_distrito` VALUES ('911', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100109', 'Santa Maria del Valle', '1', null, '89');
INSERT INTO `inicio_distrito` VALUES ('912', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100110', 'Yarumayo', '1', null, '89');
INSERT INTO `inicio_distrito` VALUES ('913', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100111', 'Pillcomarca', '1', null, '89');
INSERT INTO `inicio_distrito` VALUES ('914', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100201', 'Ambo', '1', null, '90');
INSERT INTO `inicio_distrito` VALUES ('915', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100202', 'Cayna', '1', null, '90');
INSERT INTO `inicio_distrito` VALUES ('916', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100203', 'Colpas', '1', null, '90');
INSERT INTO `inicio_distrito` VALUES ('917', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100204', 'Conchamarca', '1', null, '90');
INSERT INTO `inicio_distrito` VALUES ('918', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100205', 'Huacar', '1', null, '90');
INSERT INTO `inicio_distrito` VALUES ('919', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100206', 'San Francisco', '1', null, '90');
INSERT INTO `inicio_distrito` VALUES ('920', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100207', 'San Rafael', '1', null, '90');
INSERT INTO `inicio_distrito` VALUES ('921', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100208', 'Tomay Kichwa', '1', null, '90');
INSERT INTO `inicio_distrito` VALUES ('922', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100301', 'La Union', '1', null, '91');
INSERT INTO `inicio_distrito` VALUES ('923', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100307', 'Chuquis', '1', null, '91');
INSERT INTO `inicio_distrito` VALUES ('924', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100311', 'Marias', '1', null, '91');
INSERT INTO `inicio_distrito` VALUES ('925', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100313', 'Pachas', '1', null, '91');
INSERT INTO `inicio_distrito` VALUES ('926', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100316', 'Quivilla', '1', null, '91');
INSERT INTO `inicio_distrito` VALUES ('927', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100317', 'Ripan', '1', null, '91');
INSERT INTO `inicio_distrito` VALUES ('928', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100321', 'Shunqui', '1', null, '91');
INSERT INTO `inicio_distrito` VALUES ('929', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100322', 'Sillapata', '1', null, '91');
INSERT INTO `inicio_distrito` VALUES ('930', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100323', 'Yanas', '1', null, '91');
INSERT INTO `inicio_distrito` VALUES ('931', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100401', 'Huacaybamba', '1', null, '92');
INSERT INTO `inicio_distrito` VALUES ('932', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100402', 'Canchabamba', '1', null, '92');
INSERT INTO `inicio_distrito` VALUES ('933', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100403', 'Cochabamba', '1', null, '92');
INSERT INTO `inicio_distrito` VALUES ('934', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100404', 'Pinra', '1', null, '92');
INSERT INTO `inicio_distrito` VALUES ('935', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100501', 'Llata', '1', null, '93');
INSERT INTO `inicio_distrito` VALUES ('936', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100502', 'Arancay', '1', null, '93');
INSERT INTO `inicio_distrito` VALUES ('937', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100503', 'Chavin de Pariarca', '1', null, '93');
INSERT INTO `inicio_distrito` VALUES ('938', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100504', 'Jacas Grande', '1', null, '93');
INSERT INTO `inicio_distrito` VALUES ('939', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100505', 'Jircan', '1', null, '93');
INSERT INTO `inicio_distrito` VALUES ('940', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100506', 'Miraflores', '1', null, '93');
INSERT INTO `inicio_distrito` VALUES ('941', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100507', 'Monzon', '1', null, '93');
INSERT INTO `inicio_distrito` VALUES ('942', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100508', 'Punchao', '1', null, '93');
INSERT INTO `inicio_distrito` VALUES ('943', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100509', 'Puqos', '1', null, '93');
INSERT INTO `inicio_distrito` VALUES ('944', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100510', 'Singa', '1', null, '93');
INSERT INTO `inicio_distrito` VALUES ('945', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100511', 'Tantamayo', '1', null, '93');
INSERT INTO `inicio_distrito` VALUES ('946', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100601', 'Rupa-Rupa', '1', null, '94');
INSERT INTO `inicio_distrito` VALUES ('947', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100602', 'Daniel Alomias Robles', '1', null, '94');
INSERT INTO `inicio_distrito` VALUES ('948', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100603', 'Hermilio Valdizan', '1', null, '94');
INSERT INTO `inicio_distrito` VALUES ('949', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100604', 'Jose Crespo y Castillo', '1', null, '94');
INSERT INTO `inicio_distrito` VALUES ('950', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100605', 'Luyando', '1', null, '94');
INSERT INTO `inicio_distrito` VALUES ('951', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100606', 'Mariano Damaso Beraun', '1', null, '94');
INSERT INTO `inicio_distrito` VALUES ('952', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100701', 'Huacrachuco', '1', null, '95');
INSERT INTO `inicio_distrito` VALUES ('953', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100702', 'Cholon', '1', null, '95');
INSERT INTO `inicio_distrito` VALUES ('954', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100703', 'San Buenaventura', '1', null, '95');
INSERT INTO `inicio_distrito` VALUES ('955', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100801', 'Panao', '1', null, '96');
INSERT INTO `inicio_distrito` VALUES ('956', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100802', 'Chaglla', '1', null, '96');
INSERT INTO `inicio_distrito` VALUES ('957', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100803', 'Molino', '1', null, '96');
INSERT INTO `inicio_distrito` VALUES ('958', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100804', 'Umari', '1', null, '96');
INSERT INTO `inicio_distrito` VALUES ('959', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100901', 'Puerto Inca', '1', null, '97');
INSERT INTO `inicio_distrito` VALUES ('960', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100902', 'Codo del Pozuzo', '1', null, '97');
INSERT INTO `inicio_distrito` VALUES ('961', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100903', 'Honoria', '1', null, '97');
INSERT INTO `inicio_distrito` VALUES ('962', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100904', 'Tournavista', '1', null, '97');
INSERT INTO `inicio_distrito` VALUES ('963', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '100905', 'Yuyapichis', '1', null, '97');
INSERT INTO `inicio_distrito` VALUES ('964', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '101001', 'Jesus', '1', null, '98');
INSERT INTO `inicio_distrito` VALUES ('965', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '101002', 'Baqos', '1', null, '98');
INSERT INTO `inicio_distrito` VALUES ('966', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '101003', 'Jivia', '1', null, '98');
INSERT INTO `inicio_distrito` VALUES ('967', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '101004', 'Queropalca', '1', null, '98');
INSERT INTO `inicio_distrito` VALUES ('968', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '101005', 'Rondos', '1', null, '98');
INSERT INTO `inicio_distrito` VALUES ('969', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '101006', 'San Francisco de Asis', '1', null, '98');
INSERT INTO `inicio_distrito` VALUES ('970', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '101007', 'San Miguel de Cauri', '1', null, '98');
INSERT INTO `inicio_distrito` VALUES ('971', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '101101', 'Chavinillo', '1', null, '99');
INSERT INTO `inicio_distrito` VALUES ('972', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '101102', 'Cahuac', '1', null, '99');
INSERT INTO `inicio_distrito` VALUES ('973', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '101103', 'Chacabamba', '1', null, '99');
INSERT INTO `inicio_distrito` VALUES ('974', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '101104', 'Chupan', '1', null, '99');
INSERT INTO `inicio_distrito` VALUES ('975', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '101105', 'Jacas Chico', '1', null, '99');
INSERT INTO `inicio_distrito` VALUES ('976', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '101106', 'Obas', '1', null, '99');
INSERT INTO `inicio_distrito` VALUES ('977', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '101107', 'Pampamarca', '1', null, '99');
INSERT INTO `inicio_distrito` VALUES ('978', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '101108', 'Choras', '1', null, '99');
INSERT INTO `inicio_distrito` VALUES ('979', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110101', 'Ica', '1', null, '100');
INSERT INTO `inicio_distrito` VALUES ('980', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110102', 'La Tinguiqa', '1', null, '100');
INSERT INTO `inicio_distrito` VALUES ('981', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110103', 'Los Aquijes', '1', null, '100');
INSERT INTO `inicio_distrito` VALUES ('982', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110104', 'Ocucaje', '1', null, '100');
INSERT INTO `inicio_distrito` VALUES ('983', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110105', 'Pachacutec', '1', null, '100');
INSERT INTO `inicio_distrito` VALUES ('984', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110106', 'Parcona', '1', null, '100');
INSERT INTO `inicio_distrito` VALUES ('985', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110107', 'Pueblo Nuevo', '1', null, '100');
INSERT INTO `inicio_distrito` VALUES ('986', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110108', 'Salas', '1', null, '100');
INSERT INTO `inicio_distrito` VALUES ('987', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110109', 'San Jose de los Molinos', '1', null, '100');
INSERT INTO `inicio_distrito` VALUES ('988', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110110', 'San Juan Bautista', '1', null, '100');
INSERT INTO `inicio_distrito` VALUES ('989', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110111', 'Santiago', '1', null, '100');
INSERT INTO `inicio_distrito` VALUES ('990', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110112', 'Subtanjalla', '1', null, '100');
INSERT INTO `inicio_distrito` VALUES ('991', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110113', 'Tate', '1', null, '100');
INSERT INTO `inicio_distrito` VALUES ('992', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110114', 'Yauca del Rosario  1/', '1', null, '100');
INSERT INTO `inicio_distrito` VALUES ('993', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110201', 'Chincha Alta', '1', null, '101');
INSERT INTO `inicio_distrito` VALUES ('994', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110202', 'Alto Laran', '1', null, '101');
INSERT INTO `inicio_distrito` VALUES ('995', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110203', 'Chavin', '1', null, '101');
INSERT INTO `inicio_distrito` VALUES ('996', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110204', 'Chincha Baja', '1', null, '101');
INSERT INTO `inicio_distrito` VALUES ('997', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110205', 'El Carmen', '1', null, '101');
INSERT INTO `inicio_distrito` VALUES ('998', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110206', 'Grocio Prado', '1', null, '101');
INSERT INTO `inicio_distrito` VALUES ('999', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110207', 'Pueblo Nuevo', '1', null, '101');
INSERT INTO `inicio_distrito` VALUES ('1000', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110208', 'San Juan de Yanac', '1', null, '101');
INSERT INTO `inicio_distrito` VALUES ('1001', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110209', 'San Pedro de Huacarpana', '1', null, '101');
INSERT INTO `inicio_distrito` VALUES ('1002', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110210', 'Sunampe', '1', null, '101');
INSERT INTO `inicio_distrito` VALUES ('1003', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110211', 'Tambo de Mora', '1', null, '101');
INSERT INTO `inicio_distrito` VALUES ('1004', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110301', 'Nazca', '1', null, '102');
INSERT INTO `inicio_distrito` VALUES ('1005', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110302', 'Changuillo', '1', null, '102');
INSERT INTO `inicio_distrito` VALUES ('1006', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110303', 'El Ingenio', '1', null, '102');
INSERT INTO `inicio_distrito` VALUES ('1007', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110304', 'Marcona', '1', null, '102');
INSERT INTO `inicio_distrito` VALUES ('1008', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110305', 'Vista Alegre', '1', null, '102');
INSERT INTO `inicio_distrito` VALUES ('1009', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110401', 'Palpa', '1', null, '103');
INSERT INTO `inicio_distrito` VALUES ('1010', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110402', 'Llipata', '1', null, '103');
INSERT INTO `inicio_distrito` VALUES ('1011', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110403', 'Rio Grande', '1', null, '103');
INSERT INTO `inicio_distrito` VALUES ('1012', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110404', 'Santa Cruz', '1', null, '103');
INSERT INTO `inicio_distrito` VALUES ('1013', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110405', 'Tibillo', '1', null, '103');
INSERT INTO `inicio_distrito` VALUES ('1014', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110501', 'Pisco', '1', null, '104');
INSERT INTO `inicio_distrito` VALUES ('1015', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110502', 'Huancano', '1', null, '104');
INSERT INTO `inicio_distrito` VALUES ('1016', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110503', 'Humay', '1', null, '104');
INSERT INTO `inicio_distrito` VALUES ('1017', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110504', 'Independencia', '1', null, '104');
INSERT INTO `inicio_distrito` VALUES ('1018', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110505', 'Paracas', '1', null, '104');
INSERT INTO `inicio_distrito` VALUES ('1019', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110506', 'San Andres', '1', null, '104');
INSERT INTO `inicio_distrito` VALUES ('1020', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110507', 'San Clemente', '1', null, '104');
INSERT INTO `inicio_distrito` VALUES ('1021', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '110508', 'Tupac Amaru Inca', '1', null, '104');
INSERT INTO `inicio_distrito` VALUES ('1022', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120101', 'Huancayo', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1023', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120104', 'Carhuacallanga', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1024', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120105', 'Chacapampa', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1025', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120106', 'Chicche', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1026', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120107', 'Chilca', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1027', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120108', 'Chongos Alto', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1028', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120111', 'Chupuro', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1029', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120112', 'Colca', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1030', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120113', 'Cullhuas', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1031', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120114', 'El Tambo', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1032', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120116', 'Huacrapuquio', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1033', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120117', 'Hualhuas', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1034', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120119', 'Huancan', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1035', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120120', 'Huasicancha', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1036', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120121', 'Huayucachi', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1037', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120122', 'Ingenio', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1038', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120124', 'Pariahuanca', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1039', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120125', 'Pilcomayo', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1040', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120126', 'Pucara', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1041', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120127', 'Quichuay', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1042', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120128', 'Quilcas', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1043', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120129', 'San Agustin', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1044', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120130', 'San Jeronimo de Tunan', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1045', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120132', 'Saqo', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1046', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120133', 'Sapallanga', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1047', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120134', 'Sicaya', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1048', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120135', 'Santo Domingo de Acobamba', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1049', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120136', 'Viques', '1', null, '105');
INSERT INTO `inicio_distrito` VALUES ('1050', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120201', 'Concepcion', '1', null, '106');
INSERT INTO `inicio_distrito` VALUES ('1051', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120202', 'Aco', '1', null, '106');
INSERT INTO `inicio_distrito` VALUES ('1052', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120203', 'Andamarca', '1', null, '106');
INSERT INTO `inicio_distrito` VALUES ('1053', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120204', 'Chambara', '1', null, '106');
INSERT INTO `inicio_distrito` VALUES ('1054', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120205', 'Cochas', '1', null, '106');
INSERT INTO `inicio_distrito` VALUES ('1055', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120206', 'Comas', '1', null, '106');
INSERT INTO `inicio_distrito` VALUES ('1056', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120207', 'Heroinas Toledo', '1', null, '106');
INSERT INTO `inicio_distrito` VALUES ('1057', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120208', 'Manzanares', '1', null, '106');
INSERT INTO `inicio_distrito` VALUES ('1058', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120209', 'Mariscal Castilla', '1', null, '106');
INSERT INTO `inicio_distrito` VALUES ('1059', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120210', 'Matahuasi', '1', null, '106');
INSERT INTO `inicio_distrito` VALUES ('1060', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120211', 'Mito', '1', null, '106');
INSERT INTO `inicio_distrito` VALUES ('1061', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120212', 'Nueve de Julio', '1', null, '106');
INSERT INTO `inicio_distrito` VALUES ('1062', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120213', 'Orcotuna', '1', null, '106');
INSERT INTO `inicio_distrito` VALUES ('1063', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120214', 'San Jose de Quero', '1', null, '106');
INSERT INTO `inicio_distrito` VALUES ('1064', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120215', 'Santa Rosa de Ocopa', '1', null, '106');
INSERT INTO `inicio_distrito` VALUES ('1065', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120301', 'Chanchamayo', '1', null, '107');
INSERT INTO `inicio_distrito` VALUES ('1066', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120302', 'Perene', '1', null, '107');
INSERT INTO `inicio_distrito` VALUES ('1067', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120303', 'Pichanaqui', '1', null, '107');
INSERT INTO `inicio_distrito` VALUES ('1068', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120304', 'San Luis de Shuaro', '1', null, '107');
INSERT INTO `inicio_distrito` VALUES ('1069', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120305', 'San Ramon', '1', null, '107');
INSERT INTO `inicio_distrito` VALUES ('1070', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120306', 'Vitoc', '1', null, '107');
INSERT INTO `inicio_distrito` VALUES ('1071', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120401', 'Jauja', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1072', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120402', 'Acolla', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1073', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120403', 'Apata', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1074', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120404', 'Ataura', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1075', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120405', 'Canchayllo', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1076', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120406', 'Curicaca', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1077', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120407', 'El Mantaro', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1078', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120408', 'Huamali', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1079', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120409', 'Huaripampa', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1080', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120410', 'Huertas', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1081', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120411', 'Janjaillo', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1082', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120412', 'Julcan', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1083', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120413', 'Leonor Ordoqez', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1084', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120414', 'Llocllapampa', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1085', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120415', 'Marco', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1086', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120416', 'Masma', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1087', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120417', 'Masma Chicche', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1088', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120418', 'Molinos', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1089', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120419', 'Monobamba', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1090', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120420', 'Muqui', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1091', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120421', 'Muquiyauyo', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1092', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120422', 'Paca', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1093', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120423', 'Paccha', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1094', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120424', 'Pancan', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1095', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120425', 'Parco', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1096', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120426', 'Pomacancha', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1097', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120427', 'Ricran', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1098', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120428', 'San Lorenzo', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1099', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120429', 'San Pedro de Chunan', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1100', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120430', 'Sausa', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1101', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120431', 'Sincos', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1102', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120432', 'Tunan Marca', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1103', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120433', 'Yauli', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1104', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120434', 'Yauyos', '1', null, '108');
INSERT INTO `inicio_distrito` VALUES ('1105', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120501', 'Junin', '1', null, '109');
INSERT INTO `inicio_distrito` VALUES ('1106', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120502', 'Carhuamayo', '1', null, '109');
INSERT INTO `inicio_distrito` VALUES ('1107', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120503', 'Ondores', '1', null, '109');
INSERT INTO `inicio_distrito` VALUES ('1108', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120504', 'Ulcumayo', '1', null, '109');
INSERT INTO `inicio_distrito` VALUES ('1109', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120601', 'Satipo', '1', null, '110');
INSERT INTO `inicio_distrito` VALUES ('1110', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120602', 'Coviriali', '1', null, '110');
INSERT INTO `inicio_distrito` VALUES ('1111', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120603', 'Llaylla', '1', null, '110');
INSERT INTO `inicio_distrito` VALUES ('1112', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120604', 'Mazamari', '1', null, '110');
INSERT INTO `inicio_distrito` VALUES ('1113', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120605', 'Pampa Hermosa', '1', null, '110');
INSERT INTO `inicio_distrito` VALUES ('1114', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120606', 'Pangoa', '1', null, '110');
INSERT INTO `inicio_distrito` VALUES ('1115', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120607', 'Rio Negro', '1', null, '110');
INSERT INTO `inicio_distrito` VALUES ('1116', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120608', 'Rio Tambo', '1', null, '110');
INSERT INTO `inicio_distrito` VALUES ('1117', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120701', 'Tarma', '1', null, '111');
INSERT INTO `inicio_distrito` VALUES ('1118', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120702', 'Acobamba', '1', null, '111');
INSERT INTO `inicio_distrito` VALUES ('1119', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120703', 'Huaricolca', '1', null, '111');
INSERT INTO `inicio_distrito` VALUES ('1120', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120704', 'Huasahuasi', '1', null, '111');
INSERT INTO `inicio_distrito` VALUES ('1121', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120705', 'La Union', '1', null, '111');
INSERT INTO `inicio_distrito` VALUES ('1122', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120706', 'Palca', '1', null, '111');
INSERT INTO `inicio_distrito` VALUES ('1123', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120707', 'Palcamayo', '1', null, '111');
INSERT INTO `inicio_distrito` VALUES ('1124', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120708', 'San Pedro de Cajas', '1', null, '111');
INSERT INTO `inicio_distrito` VALUES ('1125', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120709', 'Tapo', '1', null, '111');
INSERT INTO `inicio_distrito` VALUES ('1126', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120801', 'La Oroya', '1', null, '112');
INSERT INTO `inicio_distrito` VALUES ('1127', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120802', 'Chacapalpa', '1', null, '112');
INSERT INTO `inicio_distrito` VALUES ('1128', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120803', 'Huay-Huay', '1', null, '112');
INSERT INTO `inicio_distrito` VALUES ('1129', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120804', 'Marcapomacocha', '1', null, '112');
INSERT INTO `inicio_distrito` VALUES ('1130', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120805', 'Morococha', '1', null, '112');
INSERT INTO `inicio_distrito` VALUES ('1131', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120806', 'Paccha', '1', null, '112');
INSERT INTO `inicio_distrito` VALUES ('1132', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120807', 'Santa Barbara de Carhuacayan', '1', null, '112');
INSERT INTO `inicio_distrito` VALUES ('1133', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120808', 'Santa Rosa de Sacco', '1', null, '112');
INSERT INTO `inicio_distrito` VALUES ('1134', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120809', 'Suitucancha', '1', null, '112');
INSERT INTO `inicio_distrito` VALUES ('1135', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120810', 'Yauli', '1', null, '112');
INSERT INTO `inicio_distrito` VALUES ('1136', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120901', 'Chupaca', '1', null, '113');
INSERT INTO `inicio_distrito` VALUES ('1137', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120902', 'Ahuac', '1', null, '113');
INSERT INTO `inicio_distrito` VALUES ('1138', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120903', 'Chongos Bajo', '1', null, '113');
INSERT INTO `inicio_distrito` VALUES ('1139', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120904', 'Huachac', '1', null, '113');
INSERT INTO `inicio_distrito` VALUES ('1140', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120905', 'Huamancaca Chico', '1', null, '113');
INSERT INTO `inicio_distrito` VALUES ('1141', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120906', 'San Juan de Iscos', '1', null, '113');
INSERT INTO `inicio_distrito` VALUES ('1142', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120907', 'San Juan de Jarpa', '1', null, '113');
INSERT INTO `inicio_distrito` VALUES ('1143', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120908', 'Tres de Diciembre', '1', null, '113');
INSERT INTO `inicio_distrito` VALUES ('1144', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '120909', 'Yanacancha', '1', null, '113');
INSERT INTO `inicio_distrito` VALUES ('1145', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130101', 'Trujillo', '1', null, '114');
INSERT INTO `inicio_distrito` VALUES ('1146', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130102', 'El Porvenir', '1', null, '114');
INSERT INTO `inicio_distrito` VALUES ('1147', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130103', 'Florencia de Mora', '1', null, '114');
INSERT INTO `inicio_distrito` VALUES ('1148', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130104', 'Huanchaco', '1', null, '114');
INSERT INTO `inicio_distrito` VALUES ('1149', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130105', 'La Esperanza', '1', null, '114');
INSERT INTO `inicio_distrito` VALUES ('1150', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130106', 'Laredo', '1', null, '114');
INSERT INTO `inicio_distrito` VALUES ('1151', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130107', 'Moche', '1', null, '114');
INSERT INTO `inicio_distrito` VALUES ('1152', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130108', 'Poroto', '1', null, '114');
INSERT INTO `inicio_distrito` VALUES ('1153', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130109', 'Salaverry', '1', null, '114');
INSERT INTO `inicio_distrito` VALUES ('1154', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130110', 'Simbal', '1', null, '114');
INSERT INTO `inicio_distrito` VALUES ('1155', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130111', 'Victor Larco Herrera', '1', null, '114');
INSERT INTO `inicio_distrito` VALUES ('1156', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130201', 'Ascope', '1', null, '115');
INSERT INTO `inicio_distrito` VALUES ('1157', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130202', 'Chicama', '1', null, '115');
INSERT INTO `inicio_distrito` VALUES ('1158', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130203', 'Chocope', '1', null, '115');
INSERT INTO `inicio_distrito` VALUES ('1159', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130204', 'Magdalena de Cao', '1', null, '115');
INSERT INTO `inicio_distrito` VALUES ('1160', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130205', 'Paijan', '1', null, '115');
INSERT INTO `inicio_distrito` VALUES ('1161', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130206', 'Razuri', '1', null, '115');
INSERT INTO `inicio_distrito` VALUES ('1162', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130207', 'Santiago de Cao', '1', null, '115');
INSERT INTO `inicio_distrito` VALUES ('1163', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130208', 'Casa Grande', '1', null, '115');
INSERT INTO `inicio_distrito` VALUES ('1164', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130301', 'Bolivar', '1', null, '116');
INSERT INTO `inicio_distrito` VALUES ('1165', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130302', 'Bambamarca', '1', null, '116');
INSERT INTO `inicio_distrito` VALUES ('1166', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130303', 'Condormarca', '1', null, '116');
INSERT INTO `inicio_distrito` VALUES ('1167', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130304', 'Longotea', '1', null, '116');
INSERT INTO `inicio_distrito` VALUES ('1168', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130305', 'Uchumarca', '1', null, '116');
INSERT INTO `inicio_distrito` VALUES ('1169', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130306', 'Ucuncha', '1', null, '116');
INSERT INTO `inicio_distrito` VALUES ('1170', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130401', 'Chepen', '1', null, '117');
INSERT INTO `inicio_distrito` VALUES ('1171', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130402', 'Pacanga', '1', null, '117');
INSERT INTO `inicio_distrito` VALUES ('1172', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130403', 'Pueblo Nuevo', '1', null, '117');
INSERT INTO `inicio_distrito` VALUES ('1173', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130501', 'Julcan', '1', null, '118');
INSERT INTO `inicio_distrito` VALUES ('1174', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130502', 'Calamarca', '1', null, '118');
INSERT INTO `inicio_distrito` VALUES ('1175', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130503', 'Carabamba', '1', null, '118');
INSERT INTO `inicio_distrito` VALUES ('1176', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130504', 'Huaso', '1', null, '118');
INSERT INTO `inicio_distrito` VALUES ('1177', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130601', 'Otuzco', '1', null, '119');
INSERT INTO `inicio_distrito` VALUES ('1178', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130602', 'Agallpampa', '1', null, '119');
INSERT INTO `inicio_distrito` VALUES ('1179', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130604', 'Charat', '1', null, '119');
INSERT INTO `inicio_distrito` VALUES ('1180', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130605', 'Huaranchal', '1', null, '119');
INSERT INTO `inicio_distrito` VALUES ('1181', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130606', 'La Cuesta', '1', null, '119');
INSERT INTO `inicio_distrito` VALUES ('1182', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130608', 'Mache', '1', null, '119');
INSERT INTO `inicio_distrito` VALUES ('1183', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130610', 'Paranday', '1', null, '119');
INSERT INTO `inicio_distrito` VALUES ('1184', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130611', 'Salpo', '1', null, '119');
INSERT INTO `inicio_distrito` VALUES ('1185', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130613', 'Sinsicap', '1', null, '119');
INSERT INTO `inicio_distrito` VALUES ('1186', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130614', 'Usquil', '1', null, '119');
INSERT INTO `inicio_distrito` VALUES ('1187', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130701', 'San Pedro de Lloc', '1', null, '120');
INSERT INTO `inicio_distrito` VALUES ('1188', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130702', 'Guadalupe', '1', null, '120');
INSERT INTO `inicio_distrito` VALUES ('1189', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130703', 'Jequetepeque', '1', null, '120');
INSERT INTO `inicio_distrito` VALUES ('1190', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130704', 'Pacasmayo', '1', null, '120');
INSERT INTO `inicio_distrito` VALUES ('1191', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130705', 'San Jose', '1', null, '120');
INSERT INTO `inicio_distrito` VALUES ('1192', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130801', 'Tayabamba', '1', null, '121');
INSERT INTO `inicio_distrito` VALUES ('1193', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130802', 'Buldibuyo', '1', null, '121');
INSERT INTO `inicio_distrito` VALUES ('1194', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130803', 'Chillia', '1', null, '121');
INSERT INTO `inicio_distrito` VALUES ('1195', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130804', 'Huancaspata', '1', null, '121');
INSERT INTO `inicio_distrito` VALUES ('1196', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130805', 'Huaylillas', '1', null, '121');
INSERT INTO `inicio_distrito` VALUES ('1197', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130806', 'Huayo', '1', null, '121');
INSERT INTO `inicio_distrito` VALUES ('1198', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130807', 'Ongon', '1', null, '121');
INSERT INTO `inicio_distrito` VALUES ('1199', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130808', 'Parcoy', '1', null, '121');
INSERT INTO `inicio_distrito` VALUES ('1200', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130809', 'Pataz', '1', null, '121');
INSERT INTO `inicio_distrito` VALUES ('1201', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130810', 'Pias', '1', null, '121');
INSERT INTO `inicio_distrito` VALUES ('1202', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130811', 'Santiago de Challas', '1', null, '121');
INSERT INTO `inicio_distrito` VALUES ('1203', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130812', 'Taurija', '1', null, '121');
INSERT INTO `inicio_distrito` VALUES ('1204', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130813', 'Urpay', '1', null, '121');
INSERT INTO `inicio_distrito` VALUES ('1205', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130901', 'Huamachuco', '1', null, '122');
INSERT INTO `inicio_distrito` VALUES ('1206', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130902', 'Chugay', '1', null, '122');
INSERT INTO `inicio_distrito` VALUES ('1207', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130903', 'Cochorco', '1', null, '122');
INSERT INTO `inicio_distrito` VALUES ('1208', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130904', 'Curgos', '1', null, '122');
INSERT INTO `inicio_distrito` VALUES ('1209', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130905', 'Marcabal', '1', null, '122');
INSERT INTO `inicio_distrito` VALUES ('1210', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130906', 'Sanagoran', '1', null, '122');
INSERT INTO `inicio_distrito` VALUES ('1211', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130907', 'Sarin', '1', null, '122');
INSERT INTO `inicio_distrito` VALUES ('1212', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '130908', 'Sartimbamba', '1', null, '122');
INSERT INTO `inicio_distrito` VALUES ('1213', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '131001', 'Santiago de Chuco', '1', null, '123');
INSERT INTO `inicio_distrito` VALUES ('1214', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '131002', 'Angasmarca', '1', null, '123');
INSERT INTO `inicio_distrito` VALUES ('1215', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '131003', 'Cachicadan', '1', null, '123');
INSERT INTO `inicio_distrito` VALUES ('1216', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '131004', 'Mollebamba', '1', null, '123');
INSERT INTO `inicio_distrito` VALUES ('1217', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '131005', 'Mollepata', '1', null, '123');
INSERT INTO `inicio_distrito` VALUES ('1218', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '131006', 'Quiruvilca', '1', null, '123');
INSERT INTO `inicio_distrito` VALUES ('1219', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '131007', 'Santa Cruz de Chuca', '1', null, '123');
INSERT INTO `inicio_distrito` VALUES ('1220', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '131008', 'Sitabamba', '1', null, '123');
INSERT INTO `inicio_distrito` VALUES ('1221', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '131101', 'Cascas', '1', null, '124');
INSERT INTO `inicio_distrito` VALUES ('1222', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '131102', 'Lucma', '1', null, '124');
INSERT INTO `inicio_distrito` VALUES ('1223', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '131103', 'Marmot', '1', null, '124');
INSERT INTO `inicio_distrito` VALUES ('1224', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '131104', 'Sayapullo', '1', null, '124');
INSERT INTO `inicio_distrito` VALUES ('1225', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '131201', 'Viru', '1', null, '125');
INSERT INTO `inicio_distrito` VALUES ('1226', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '131202', 'Chao', '1', null, '125');
INSERT INTO `inicio_distrito` VALUES ('1227', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '131203', 'Guadalupito', '1', null, '125');
INSERT INTO `inicio_distrito` VALUES ('1228', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140101', 'Chiclayo', '1', null, '126');
INSERT INTO `inicio_distrito` VALUES ('1229', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140102', 'Chongoyape', '1', null, '126');
INSERT INTO `inicio_distrito` VALUES ('1230', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140103', 'Eten', '1', null, '126');
INSERT INTO `inicio_distrito` VALUES ('1231', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140104', 'Eten Puerto', '1', null, '126');
INSERT INTO `inicio_distrito` VALUES ('1232', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140105', 'Jose Leonardo Ortiz', '1', null, '126');
INSERT INTO `inicio_distrito` VALUES ('1233', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140106', 'La Victoria', '1', null, '126');
INSERT INTO `inicio_distrito` VALUES ('1234', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140107', 'Lagunas', '1', null, '126');
INSERT INTO `inicio_distrito` VALUES ('1235', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140108', 'Monsefu', '1', null, '126');
INSERT INTO `inicio_distrito` VALUES ('1236', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140109', 'Nueva Arica', '1', null, '126');
INSERT INTO `inicio_distrito` VALUES ('1237', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140110', 'Oyotun', '1', null, '126');
INSERT INTO `inicio_distrito` VALUES ('1238', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140111', 'Picsi', '1', null, '126');
INSERT INTO `inicio_distrito` VALUES ('1239', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140112', 'Pimentel', '1', null, '126');
INSERT INTO `inicio_distrito` VALUES ('1240', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140113', 'Reque', '1', null, '126');
INSERT INTO `inicio_distrito` VALUES ('1241', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140114', 'Santa Rosa', '1', null, '126');
INSERT INTO `inicio_distrito` VALUES ('1242', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140115', 'Saqa', '1', null, '126');
INSERT INTO `inicio_distrito` VALUES ('1243', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140116', 'Cayaltí', '1', null, '126');
INSERT INTO `inicio_distrito` VALUES ('1244', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140117', 'Patapo', '1', null, '126');
INSERT INTO `inicio_distrito` VALUES ('1245', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140118', 'Pomalca', '1', null, '126');
INSERT INTO `inicio_distrito` VALUES ('1246', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140119', 'Pucalá', '1', null, '126');
INSERT INTO `inicio_distrito` VALUES ('1247', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140120', 'Tumán', '1', null, '126');
INSERT INTO `inicio_distrito` VALUES ('1248', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140201', 'Ferreqafe', '1', null, '127');
INSERT INTO `inicio_distrito` VALUES ('1249', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140202', 'Caqaris', '1', null, '127');
INSERT INTO `inicio_distrito` VALUES ('1250', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140203', 'Incahuasi', '1', null, '127');
INSERT INTO `inicio_distrito` VALUES ('1251', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140204', 'Manuel Antonio Mesones Muro', '1', null, '127');
INSERT INTO `inicio_distrito` VALUES ('1252', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140205', 'Pitipo', '1', null, '127');
INSERT INTO `inicio_distrito` VALUES ('1253', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140206', 'Pueblo Nuevo', '1', null, '127');
INSERT INTO `inicio_distrito` VALUES ('1254', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140301', 'Lambayeque', '1', null, '128');
INSERT INTO `inicio_distrito` VALUES ('1255', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140302', 'Chochope', '1', null, '128');
INSERT INTO `inicio_distrito` VALUES ('1256', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140303', 'Illimo', '1', null, '128');
INSERT INTO `inicio_distrito` VALUES ('1257', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140304', 'Jayanca', '1', null, '128');
INSERT INTO `inicio_distrito` VALUES ('1258', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140305', 'Mochumi', '1', null, '128');
INSERT INTO `inicio_distrito` VALUES ('1259', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140306', 'Morrope', '1', null, '128');
INSERT INTO `inicio_distrito` VALUES ('1260', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140307', 'Motupe', '1', null, '128');
INSERT INTO `inicio_distrito` VALUES ('1261', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140308', 'Olmos', '1', null, '128');
INSERT INTO `inicio_distrito` VALUES ('1262', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140309', 'Pacora', '1', null, '128');
INSERT INTO `inicio_distrito` VALUES ('1263', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140310', 'Salas', '1', null, '128');
INSERT INTO `inicio_distrito` VALUES ('1264', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140311', 'San Jose', '1', null, '128');
INSERT INTO `inicio_distrito` VALUES ('1265', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '140312', 'Tucume', '1', null, '128');
INSERT INTO `inicio_distrito` VALUES ('1266', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150101', 'Lima', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1267', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150102', 'Ancon', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1268', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150103', 'Ate', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1269', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150104', 'Barranco', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1270', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150105', 'Breqa', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1271', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150106', 'Carabayllo', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1272', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150107', 'Chaclacayo', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1273', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150108', 'Chorrillos', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1274', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150109', 'Cieneguilla', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1275', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150110', 'Comas', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1276', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150111', 'El Agustino', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1277', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150112', 'Independencia', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1278', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150113', 'Jesus Maria', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1279', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150114', 'La Molina', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1280', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150115', 'La Victoria', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1281', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150116', 'Lince', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1282', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150117', 'Los Olivos', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1283', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150118', 'Lurigancho', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1284', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150119', 'Lurin', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1285', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150120', 'Magdalena del Mar', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1286', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150121', 'Magdalena Vieja', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1287', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150122', 'Miraflores', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1288', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150123', 'Pachacamac', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1289', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150124', 'Pucusana', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1290', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150125', 'Puente Piedra', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1291', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150126', 'Punta Hermosa', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1292', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150127', 'Punta Negra', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1293', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150128', 'Rimac', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1294', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150129', 'San Bartolo', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1295', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150130', 'San Borja', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1296', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150131', 'San Isidro', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1297', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150132', 'San Juan de Lurigancho', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1298', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150133', 'San Juan de Miraflores', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1299', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150134', 'San Luis', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1300', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150135', 'San Martin de Porres', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1301', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150136', 'San Miguel', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1302', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150137', 'Santa Anita', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1303', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150138', 'Santa Maria del Mar', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1304', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150139', 'Santa Rosa', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1305', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150140', 'Santiago de Surco', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1306', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150141', 'Surquillo', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1307', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150142', 'Villa El Salvador', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1308', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150143', 'Villa Maria del Triunfo', '1', null, '129');
INSERT INTO `inicio_distrito` VALUES ('1309', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150201', 'Barranca', '1', null, '130');
INSERT INTO `inicio_distrito` VALUES ('1310', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150202', 'Paramonga', '1', null, '130');
INSERT INTO `inicio_distrito` VALUES ('1311', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150203', 'Pativilca', '1', null, '130');
INSERT INTO `inicio_distrito` VALUES ('1312', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150204', 'Supe', '1', null, '130');
INSERT INTO `inicio_distrito` VALUES ('1313', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150205', 'Supe Puerto', '1', null, '130');
INSERT INTO `inicio_distrito` VALUES ('1314', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150301', 'Cajatambo', '1', null, '131');
INSERT INTO `inicio_distrito` VALUES ('1315', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150302', 'Copa', '1', null, '131');
INSERT INTO `inicio_distrito` VALUES ('1316', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150303', 'Gorgor', '1', null, '131');
INSERT INTO `inicio_distrito` VALUES ('1317', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150304', 'Huancapon', '1', null, '131');
INSERT INTO `inicio_distrito` VALUES ('1318', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150305', 'Manas', '1', null, '131');
INSERT INTO `inicio_distrito` VALUES ('1319', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150401', 'Canta', '1', null, '132');
INSERT INTO `inicio_distrito` VALUES ('1320', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150402', 'Arahuay', '1', null, '132');
INSERT INTO `inicio_distrito` VALUES ('1321', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150403', 'Huamantanga', '1', null, '132');
INSERT INTO `inicio_distrito` VALUES ('1322', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150404', 'Huaros', '1', null, '132');
INSERT INTO `inicio_distrito` VALUES ('1323', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150405', 'Lachaqui', '1', null, '132');
INSERT INTO `inicio_distrito` VALUES ('1324', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150406', 'San Buenaventura', '1', null, '132');
INSERT INTO `inicio_distrito` VALUES ('1325', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150407', 'Santa Rosa de Quives', '1', null, '132');
INSERT INTO `inicio_distrito` VALUES ('1326', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150501', 'San Vicente de Caqete', '1', null, '133');
INSERT INTO `inicio_distrito` VALUES ('1327', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150502', 'Asia', '1', null, '133');
INSERT INTO `inicio_distrito` VALUES ('1328', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150503', 'Calango', '1', null, '133');
INSERT INTO `inicio_distrito` VALUES ('1329', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150504', 'Cerro Azul', '1', null, '133');
INSERT INTO `inicio_distrito` VALUES ('1330', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150505', 'Chilca', '1', null, '133');
INSERT INTO `inicio_distrito` VALUES ('1331', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150506', 'Coayllo', '1', null, '133');
INSERT INTO `inicio_distrito` VALUES ('1332', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150507', 'Imperial', '1', null, '133');
INSERT INTO `inicio_distrito` VALUES ('1333', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150508', 'Lunahuana', '1', null, '133');
INSERT INTO `inicio_distrito` VALUES ('1334', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150509', 'Mala', '1', null, '133');
INSERT INTO `inicio_distrito` VALUES ('1335', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150510', 'Nuevo Imperial', '1', null, '133');
INSERT INTO `inicio_distrito` VALUES ('1336', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150511', 'Pacaran', '1', null, '133');
INSERT INTO `inicio_distrito` VALUES ('1337', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150512', 'Quilmana', '1', null, '133');
INSERT INTO `inicio_distrito` VALUES ('1338', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150513', 'San Antonio', '1', null, '133');
INSERT INTO `inicio_distrito` VALUES ('1339', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150514', 'San Luis', '1', null, '133');
INSERT INTO `inicio_distrito` VALUES ('1340', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150515', 'Santa Cruz de Flores', '1', null, '133');
INSERT INTO `inicio_distrito` VALUES ('1341', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150516', 'Zuqiga', '1', null, '133');
INSERT INTO `inicio_distrito` VALUES ('1342', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150601', 'Huaral', '1', null, '134');
INSERT INTO `inicio_distrito` VALUES ('1343', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150602', 'Atavillos Alto', '1', null, '134');
INSERT INTO `inicio_distrito` VALUES ('1344', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150603', 'Atavillos Bajo', '1', null, '134');
INSERT INTO `inicio_distrito` VALUES ('1345', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150604', 'Aucallama', '1', null, '134');
INSERT INTO `inicio_distrito` VALUES ('1346', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150605', 'Chancay', '1', null, '134');
INSERT INTO `inicio_distrito` VALUES ('1347', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150606', 'Ihuari', '1', null, '134');
INSERT INTO `inicio_distrito` VALUES ('1348', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150607', 'Lampian', '1', null, '134');
INSERT INTO `inicio_distrito` VALUES ('1349', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150608', 'Pacaraos', '1', null, '134');
INSERT INTO `inicio_distrito` VALUES ('1350', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150609', 'San Miguel de Acos', '1', null, '134');
INSERT INTO `inicio_distrito` VALUES ('1351', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150610', 'Santa Cruz de Andamarca', '1', null, '134');
INSERT INTO `inicio_distrito` VALUES ('1352', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150611', 'Sumbilca', '1', null, '134');
INSERT INTO `inicio_distrito` VALUES ('1353', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150612', 'Veintisiete de Noviembre', '1', null, '134');
INSERT INTO `inicio_distrito` VALUES ('1354', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150701', 'Matucana', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1355', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150702', 'Antioquia', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1356', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150703', 'Callahuanca', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1357', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150704', 'Carampoma', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1358', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150705', 'Chicla', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1359', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150706', 'Cuenca', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1360', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150707', 'Huachupampa', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1361', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150708', 'Huanza', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1362', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150709', 'Huarochiri', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1363', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150710', 'Lahuaytambo', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1364', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150711', 'Langa', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1365', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150712', 'Laraos', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1366', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150713', 'Mariatana', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1367', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150714', 'Ricardo Palma', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1368', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150715', 'San Andres de Tupicocha', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1369', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150716', 'San Antonio', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1370', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150717', 'San Bartolome', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1371', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150718', 'San Damian', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1372', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150719', 'San Juan de Iris', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1373', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150720', 'San Juan de Tantaranche', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1374', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150721', 'San Lorenzo de Quinti', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1375', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150722', 'San Mateo', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1376', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150723', 'San Mateo de Otao', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1377', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150724', 'San Pedro de Casta', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1378', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150725', 'San Pedro de Huancayre', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1379', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150726', 'Sangallaya', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1380', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150727', 'Santa Cruz de Cocachacra', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1381', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150728', 'Santa Eulalia', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1382', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150729', 'Santiago de Anchucaya', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1383', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150730', 'Santiago de Tuna', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1384', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150731', 'Santo Domingo de los Olleros', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1385', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150732', 'Surco', '1', null, '135');
INSERT INTO `inicio_distrito` VALUES ('1386', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150801', 'Huacho', '1', null, '136');
INSERT INTO `inicio_distrito` VALUES ('1387', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150802', 'Ambar', '1', null, '136');
INSERT INTO `inicio_distrito` VALUES ('1388', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150803', 'Caleta de Carquin', '1', null, '136');
INSERT INTO `inicio_distrito` VALUES ('1389', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150804', 'Checras', '1', null, '136');
INSERT INTO `inicio_distrito` VALUES ('1390', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150805', 'Hualmay', '1', null, '136');
INSERT INTO `inicio_distrito` VALUES ('1391', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150806', 'Huaura', '1', null, '136');
INSERT INTO `inicio_distrito` VALUES ('1392', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150807', 'Leoncio Prado', '1', null, '136');
INSERT INTO `inicio_distrito` VALUES ('1393', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150808', 'Paccho', '1', null, '136');
INSERT INTO `inicio_distrito` VALUES ('1394', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150809', 'Santa Leonor', '1', null, '136');
INSERT INTO `inicio_distrito` VALUES ('1395', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150810', 'Santa Maria', '1', null, '136');
INSERT INTO `inicio_distrito` VALUES ('1396', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150811', 'Sayan', '1', null, '136');
INSERT INTO `inicio_distrito` VALUES ('1397', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150812', 'Vegueta', '1', null, '136');
INSERT INTO `inicio_distrito` VALUES ('1398', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150901', 'Oyon', '1', null, '137');
INSERT INTO `inicio_distrito` VALUES ('1399', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150902', 'Andajes', '1', null, '137');
INSERT INTO `inicio_distrito` VALUES ('1400', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150903', 'Caujul', '1', null, '137');
INSERT INTO `inicio_distrito` VALUES ('1401', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150904', 'Cochamarca', '1', null, '137');
INSERT INTO `inicio_distrito` VALUES ('1402', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150905', 'Navan', '1', null, '137');
INSERT INTO `inicio_distrito` VALUES ('1403', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '150906', 'Pachangara', '1', null, '137');
INSERT INTO `inicio_distrito` VALUES ('1404', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151001', 'Yauyos', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1405', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151002', 'Alis', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1406', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151003', 'Ayauca', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1407', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151004', 'Ayaviri', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1408', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151005', 'Azangaro', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1409', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151006', 'Cacra', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1410', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151007', 'Carania', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1411', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151008', 'Catahuasi', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1412', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151009', 'Chocos', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1413', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151010', 'Cochas', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1414', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151011', 'Colonia', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1415', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151012', 'Hongos', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1416', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151013', 'Huampara', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1417', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151014', 'Huancaya', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1418', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151015', 'Huangascar', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1419', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151016', 'Huantan', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1420', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151017', 'Huaqec', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1421', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151018', 'Laraos', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1422', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151019', 'Lincha', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1423', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151020', 'Madean', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1424', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151021', 'Miraflores', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1425', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151022', 'Omas', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1426', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151023', 'Putinza', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1427', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151024', 'Quinches', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1428', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151025', 'Quinocay', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1429', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151026', 'San Joaquin', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1430', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151027', 'San Pedro de Pilas', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1431', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151028', 'Tanta', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1432', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151029', 'Tauripampa', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1433', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151030', 'Tomas', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1434', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151031', 'Tupe', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1435', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151032', 'Viqac', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1436', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '151033', 'Vitis', '1', null, '138');
INSERT INTO `inicio_distrito` VALUES ('1437', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160101', 'Iquitos', '1', null, '139');
INSERT INTO `inicio_distrito` VALUES ('1438', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160102', 'Alto Nanay', '1', null, '139');
INSERT INTO `inicio_distrito` VALUES ('1439', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160103', 'Fernando Lores', '1', null, '139');
INSERT INTO `inicio_distrito` VALUES ('1440', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160104', 'Indiana', '1', null, '139');
INSERT INTO `inicio_distrito` VALUES ('1441', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160105', 'Las Amazonas', '1', null, '139');
INSERT INTO `inicio_distrito` VALUES ('1442', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160106', 'Mazan', '1', null, '139');
INSERT INTO `inicio_distrito` VALUES ('1443', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160107', 'Napo', '1', null, '139');
INSERT INTO `inicio_distrito` VALUES ('1444', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160108', 'Punchana', '1', null, '139');
INSERT INTO `inicio_distrito` VALUES ('1445', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160109', 'Putumayo', '1', null, '139');
INSERT INTO `inicio_distrito` VALUES ('1446', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160110', 'Torres Causana', '1', null, '139');
INSERT INTO `inicio_distrito` VALUES ('1447', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160111', 'Yaquerana', '1', null, '139');
INSERT INTO `inicio_distrito` VALUES ('1448', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160112', 'Belén', '1', null, '139');
INSERT INTO `inicio_distrito` VALUES ('1449', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160113', 'San Juan Bautista', '1', null, '139');
INSERT INTO `inicio_distrito` VALUES ('1450', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160201', 'Yurimaguas', '1', null, '140');
INSERT INTO `inicio_distrito` VALUES ('1451', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160202', 'Balsapuerto', '1', null, '140');
INSERT INTO `inicio_distrito` VALUES ('1452', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160203', 'Barranca', '1', null, '140');
INSERT INTO `inicio_distrito` VALUES ('1453', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160204', 'Cahuapanas', '1', null, '140');
INSERT INTO `inicio_distrito` VALUES ('1454', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160205', 'Jeberos', '1', null, '140');
INSERT INTO `inicio_distrito` VALUES ('1455', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160206', 'Lagunas', '1', null, '140');
INSERT INTO `inicio_distrito` VALUES ('1456', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160207', 'Manseriche', '1', null, '140');
INSERT INTO `inicio_distrito` VALUES ('1457', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160208', 'Morona', '1', null, '140');
INSERT INTO `inicio_distrito` VALUES ('1458', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160209', 'Pastaza', '1', null, '140');
INSERT INTO `inicio_distrito` VALUES ('1459', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160210', 'Santa Cruz', '1', null, '140');
INSERT INTO `inicio_distrito` VALUES ('1460', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160211', 'Teniente Cesar Lopez Rojas', '1', null, '140');
INSERT INTO `inicio_distrito` VALUES ('1461', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160301', 'Nauta', '1', null, '141');
INSERT INTO `inicio_distrito` VALUES ('1462', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160302', 'Parinari', '1', null, '141');
INSERT INTO `inicio_distrito` VALUES ('1463', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160303', 'Tigre', '1', null, '141');
INSERT INTO `inicio_distrito` VALUES ('1464', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160304', 'Trompeteros', '1', null, '141');
INSERT INTO `inicio_distrito` VALUES ('1465', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160305', 'Urarinas', '1', null, '141');
INSERT INTO `inicio_distrito` VALUES ('1466', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160401', 'Ramon Castilla', '1', null, '142');
INSERT INTO `inicio_distrito` VALUES ('1467', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160402', 'Pebas', '1', null, '142');
INSERT INTO `inicio_distrito` VALUES ('1468', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160403', 'Yavari', '1', null, '142');
INSERT INTO `inicio_distrito` VALUES ('1469', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160404', 'San Pablo', '1', null, '142');
INSERT INTO `inicio_distrito` VALUES ('1470', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160501', 'Requena', '1', null, '143');
INSERT INTO `inicio_distrito` VALUES ('1471', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160502', 'Alto Tapiche', '1', null, '143');
INSERT INTO `inicio_distrito` VALUES ('1472', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160503', 'Capelo', '1', null, '143');
INSERT INTO `inicio_distrito` VALUES ('1473', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160504', 'Emilio San Martin', '1', null, '143');
INSERT INTO `inicio_distrito` VALUES ('1474', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160505', 'Maquia', '1', null, '143');
INSERT INTO `inicio_distrito` VALUES ('1475', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160506', 'Puinahua', '1', null, '143');
INSERT INTO `inicio_distrito` VALUES ('1476', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160507', 'Saquena', '1', null, '143');
INSERT INTO `inicio_distrito` VALUES ('1477', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160508', 'Soplin', '1', null, '143');
INSERT INTO `inicio_distrito` VALUES ('1478', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160509', 'Tapiche', '1', null, '143');
INSERT INTO `inicio_distrito` VALUES ('1479', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160510', 'Jenaro Herrera', '1', null, '143');
INSERT INTO `inicio_distrito` VALUES ('1480', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160511', 'Yaquerana', '1', null, '143');
INSERT INTO `inicio_distrito` VALUES ('1481', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160601', 'Contamana', '1', null, '144');
INSERT INTO `inicio_distrito` VALUES ('1482', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160602', 'Inahuaya', '1', null, '144');
INSERT INTO `inicio_distrito` VALUES ('1483', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160603', 'Padre Marquez', '1', null, '144');
INSERT INTO `inicio_distrito` VALUES ('1484', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160604', 'Pampa Hermosa', '1', null, '144');
INSERT INTO `inicio_distrito` VALUES ('1485', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160605', 'Sarayacu', '1', null, '144');
INSERT INTO `inicio_distrito` VALUES ('1486', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '160606', 'Vargas Guerra', '1', null, '144');
INSERT INTO `inicio_distrito` VALUES ('1487', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '170101', 'Tambopata', '1', null, '145');
INSERT INTO `inicio_distrito` VALUES ('1488', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '170102', 'Inambari', '1', null, '145');
INSERT INTO `inicio_distrito` VALUES ('1489', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '170103', 'Las Piedras', '1', null, '145');
INSERT INTO `inicio_distrito` VALUES ('1490', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '170104', 'Laberinto', '1', null, '145');
INSERT INTO `inicio_distrito` VALUES ('1491', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '170201', 'Manu', '1', null, '146');
INSERT INTO `inicio_distrito` VALUES ('1492', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '170202', 'Fitzcarrald', '1', null, '146');
INSERT INTO `inicio_distrito` VALUES ('1493', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '170203', 'Madre de Dios', '1', null, '146');
INSERT INTO `inicio_distrito` VALUES ('1494', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '170204', 'Huepetuhe', '1', null, '146');
INSERT INTO `inicio_distrito` VALUES ('1495', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '170301', 'Iqapari', '1', null, '147');
INSERT INTO `inicio_distrito` VALUES ('1496', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '170302', 'Iberia', '1', null, '147');
INSERT INTO `inicio_distrito` VALUES ('1497', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '170303', 'Tahuamanu', '1', null, '147');
INSERT INTO `inicio_distrito` VALUES ('1498', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '180101', 'Moquegua', '1', null, '148');
INSERT INTO `inicio_distrito` VALUES ('1499', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '180102', 'Carumas', '1', null, '148');
INSERT INTO `inicio_distrito` VALUES ('1500', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '180103', 'Cuchumbaya', '1', null, '148');
INSERT INTO `inicio_distrito` VALUES ('1501', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '180104', 'Samegua', '1', null, '148');
INSERT INTO `inicio_distrito` VALUES ('1502', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '180105', 'San Cristobal', '1', null, '148');
INSERT INTO `inicio_distrito` VALUES ('1503', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '180106', 'Torata', '1', null, '148');
INSERT INTO `inicio_distrito` VALUES ('1504', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '180201', 'Omate', '1', null, '149');
INSERT INTO `inicio_distrito` VALUES ('1505', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '180202', 'Chojata', '1', null, '149');
INSERT INTO `inicio_distrito` VALUES ('1506', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '180203', 'Coalaque', '1', null, '149');
INSERT INTO `inicio_distrito` VALUES ('1507', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '180204', 'Ichuqa', '1', null, '149');
INSERT INTO `inicio_distrito` VALUES ('1508', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '180205', 'La Capilla', '1', null, '149');
INSERT INTO `inicio_distrito` VALUES ('1509', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '180206', 'Lloque', '1', null, '149');
INSERT INTO `inicio_distrito` VALUES ('1510', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '180207', 'Matalaque', '1', null, '149');
INSERT INTO `inicio_distrito` VALUES ('1511', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '180208', 'Puquina', '1', null, '149');
INSERT INTO `inicio_distrito` VALUES ('1512', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '180209', 'Quinistaquillas', '1', null, '149');
INSERT INTO `inicio_distrito` VALUES ('1513', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '180210', 'Ubinas', '1', null, '149');
INSERT INTO `inicio_distrito` VALUES ('1514', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '180211', 'Yunga', '1', null, '149');
INSERT INTO `inicio_distrito` VALUES ('1515', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '180301', 'Ilo', '1', null, '150');
INSERT INTO `inicio_distrito` VALUES ('1516', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '180302', 'El Algarrobal', '1', null, '150');
INSERT INTO `inicio_distrito` VALUES ('1517', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '180303', 'Pacocha', '1', null, '150');
INSERT INTO `inicio_distrito` VALUES ('1518', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190101', 'Chaupimarca', '1', null, '151');
INSERT INTO `inicio_distrito` VALUES ('1519', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190102', 'Huachon', '1', null, '151');
INSERT INTO `inicio_distrito` VALUES ('1520', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190103', 'Huariaca', '1', null, '151');
INSERT INTO `inicio_distrito` VALUES ('1521', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190104', 'Huayllay', '1', null, '151');
INSERT INTO `inicio_distrito` VALUES ('1522', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190105', 'Ninacaca', '1', null, '151');
INSERT INTO `inicio_distrito` VALUES ('1523', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190106', 'Pallanchacra', '1', null, '151');
INSERT INTO `inicio_distrito` VALUES ('1524', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190107', 'Paucartambo', '1', null, '151');
INSERT INTO `inicio_distrito` VALUES ('1525', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190108', 'San Fco.De Asis de Yarusyacan', '1', null, '151');
INSERT INTO `inicio_distrito` VALUES ('1526', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190109', 'Simon Bolivar', '1', null, '151');
INSERT INTO `inicio_distrito` VALUES ('1527', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190110', 'Ticlacayan', '1', null, '151');
INSERT INTO `inicio_distrito` VALUES ('1528', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190111', 'Tinyahuarco', '1', null, '151');
INSERT INTO `inicio_distrito` VALUES ('1529', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190112', 'Vicco', '1', null, '151');
INSERT INTO `inicio_distrito` VALUES ('1530', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190113', 'Yanacancha', '1', null, '151');
INSERT INTO `inicio_distrito` VALUES ('1531', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190201', 'Yanahuanca', '1', null, '152');
INSERT INTO `inicio_distrito` VALUES ('1532', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190202', 'Chacayan', '1', null, '152');
INSERT INTO `inicio_distrito` VALUES ('1533', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190203', 'Goyllarisquizga', '1', null, '152');
INSERT INTO `inicio_distrito` VALUES ('1534', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190204', 'Paucar', '1', null, '152');
INSERT INTO `inicio_distrito` VALUES ('1535', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190205', 'San Pedro de Pillao', '1', null, '152');
INSERT INTO `inicio_distrito` VALUES ('1536', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190206', 'Santa Ana de Tusi', '1', null, '152');
INSERT INTO `inicio_distrito` VALUES ('1537', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190207', 'Tapuc', '1', null, '152');
INSERT INTO `inicio_distrito` VALUES ('1538', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190208', 'Vilcabamba', '1', null, '152');
INSERT INTO `inicio_distrito` VALUES ('1539', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190301', 'Oxapampa', '1', null, '153');
INSERT INTO `inicio_distrito` VALUES ('1540', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190302', 'Chontabamba', '1', null, '153');
INSERT INTO `inicio_distrito` VALUES ('1541', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190303', 'Huancabamba', '1', null, '153');
INSERT INTO `inicio_distrito` VALUES ('1542', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190304', 'Palcazu', '1', null, '153');
INSERT INTO `inicio_distrito` VALUES ('1543', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190305', 'Pozuzo', '1', null, '153');
INSERT INTO `inicio_distrito` VALUES ('1544', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190306', 'Puerto Bermudez', '1', null, '153');
INSERT INTO `inicio_distrito` VALUES ('1545', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '190307', 'Villa Rica', '1', null, '153');
INSERT INTO `inicio_distrito` VALUES ('1546', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200101', 'Piura', '1', null, '154');
INSERT INTO `inicio_distrito` VALUES ('1547', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200104', 'Castilla', '1', null, '154');
INSERT INTO `inicio_distrito` VALUES ('1548', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200105', 'Catacaos', '1', null, '154');
INSERT INTO `inicio_distrito` VALUES ('1549', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200107', 'Cura Mori', '1', null, '154');
INSERT INTO `inicio_distrito` VALUES ('1550', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200108', 'El Tallan', '1', null, '154');
INSERT INTO `inicio_distrito` VALUES ('1551', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200109', 'La Arena', '1', null, '154');
INSERT INTO `inicio_distrito` VALUES ('1552', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200110', 'La Union', '1', null, '154');
INSERT INTO `inicio_distrito` VALUES ('1553', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200111', 'Las Lomas', '1', null, '154');
INSERT INTO `inicio_distrito` VALUES ('1554', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200114', 'Tambo Grande', '1', null, '154');
INSERT INTO `inicio_distrito` VALUES ('1555', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200201', 'Ayabaca', '1', null, '155');
INSERT INTO `inicio_distrito` VALUES ('1556', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200202', 'Frias', '1', null, '155');
INSERT INTO `inicio_distrito` VALUES ('1557', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200203', 'Jilili', '1', null, '155');
INSERT INTO `inicio_distrito` VALUES ('1558', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200204', 'Lagunas', '1', null, '155');
INSERT INTO `inicio_distrito` VALUES ('1559', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200205', 'Montero', '1', null, '155');
INSERT INTO `inicio_distrito` VALUES ('1560', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200206', 'Pacaipampa', '1', null, '155');
INSERT INTO `inicio_distrito` VALUES ('1561', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200207', 'Paimas', '1', null, '155');
INSERT INTO `inicio_distrito` VALUES ('1562', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200208', 'Sapillica', '1', null, '155');
INSERT INTO `inicio_distrito` VALUES ('1563', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200209', 'Sicchez', '1', null, '155');
INSERT INTO `inicio_distrito` VALUES ('1564', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200210', 'Suyo', '1', null, '155');
INSERT INTO `inicio_distrito` VALUES ('1565', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200301', 'Huancabamba', '1', null, '156');
INSERT INTO `inicio_distrito` VALUES ('1566', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200302', 'Canchaque', '1', null, '156');
INSERT INTO `inicio_distrito` VALUES ('1567', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200303', 'El Carmen de la Frontera', '1', null, '156');
INSERT INTO `inicio_distrito` VALUES ('1568', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200304', 'Huarmaca', '1', null, '156');
INSERT INTO `inicio_distrito` VALUES ('1569', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200305', 'Lalaquiz', '1', null, '156');
INSERT INTO `inicio_distrito` VALUES ('1570', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200306', 'San Miguel de El Faique', '1', null, '156');
INSERT INTO `inicio_distrito` VALUES ('1571', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200307', 'Sondor', '1', null, '156');
INSERT INTO `inicio_distrito` VALUES ('1572', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200308', 'Sondorillo', '1', null, '156');
INSERT INTO `inicio_distrito` VALUES ('1573', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200401', 'Chulucanas', '1', null, '157');
INSERT INTO `inicio_distrito` VALUES ('1574', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200402', 'Buenos Aires', '1', null, '157');
INSERT INTO `inicio_distrito` VALUES ('1575', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200403', 'Chalaco', '1', null, '157');
INSERT INTO `inicio_distrito` VALUES ('1576', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200404', 'La Matanza', '1', null, '157');
INSERT INTO `inicio_distrito` VALUES ('1577', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200405', 'Morropon', '1', null, '157');
INSERT INTO `inicio_distrito` VALUES ('1578', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200406', 'Salitral', '1', null, '157');
INSERT INTO `inicio_distrito` VALUES ('1579', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200407', 'San Juan de Bigote', '1', null, '157');
INSERT INTO `inicio_distrito` VALUES ('1580', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200408', 'Santa Catalina de Mossa', '1', null, '157');
INSERT INTO `inicio_distrito` VALUES ('1581', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200409', 'Santo Domingo', '1', null, '157');
INSERT INTO `inicio_distrito` VALUES ('1582', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200410', 'Yamango', '1', null, '157');
INSERT INTO `inicio_distrito` VALUES ('1583', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200501', 'Paita', '1', null, '158');
INSERT INTO `inicio_distrito` VALUES ('1584', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200502', 'Amotape', '1', null, '158');
INSERT INTO `inicio_distrito` VALUES ('1585', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200503', 'Arenal', '1', null, '158');
INSERT INTO `inicio_distrito` VALUES ('1586', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200504', 'Colan', '1', null, '158');
INSERT INTO `inicio_distrito` VALUES ('1587', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200505', 'La Huaca', '1', null, '158');
INSERT INTO `inicio_distrito` VALUES ('1588', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200506', 'Tamarindo', '1', null, '158');
INSERT INTO `inicio_distrito` VALUES ('1589', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200507', 'Vichayal', '1', null, '158');
INSERT INTO `inicio_distrito` VALUES ('1590', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200601', 'Sullana', '1', null, '159');
INSERT INTO `inicio_distrito` VALUES ('1591', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200602', 'Bellavista', '1', null, '159');
INSERT INTO `inicio_distrito` VALUES ('1592', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200603', 'Ignacio Escudero', '1', null, '159');
INSERT INTO `inicio_distrito` VALUES ('1593', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200604', 'Lancones', '1', null, '159');
INSERT INTO `inicio_distrito` VALUES ('1594', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200605', 'Marcavelica', '1', null, '159');
INSERT INTO `inicio_distrito` VALUES ('1595', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200606', 'Miguel Checa', '1', null, '159');
INSERT INTO `inicio_distrito` VALUES ('1596', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200607', 'Querecotillo', '1', null, '159');
INSERT INTO `inicio_distrito` VALUES ('1597', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200608', 'Salitral', '1', null, '159');
INSERT INTO `inicio_distrito` VALUES ('1598', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200701', 'Pariqas', '1', null, '160');
INSERT INTO `inicio_distrito` VALUES ('1599', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200702', 'El Alto', '1', null, '160');
INSERT INTO `inicio_distrito` VALUES ('1600', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200703', 'La Brea', '1', null, '160');
INSERT INTO `inicio_distrito` VALUES ('1601', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200704', 'Lobitos', '1', null, '160');
INSERT INTO `inicio_distrito` VALUES ('1602', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200705', 'Los Organos', '1', null, '160');
INSERT INTO `inicio_distrito` VALUES ('1603', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200706', 'Mancora', '1', null, '160');
INSERT INTO `inicio_distrito` VALUES ('1604', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200801', 'Sechura', '1', null, '161');
INSERT INTO `inicio_distrito` VALUES ('1605', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200802', 'Bellavista de la Union', '1', null, '161');
INSERT INTO `inicio_distrito` VALUES ('1606', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200803', 'Bernal', '1', null, '161');
INSERT INTO `inicio_distrito` VALUES ('1607', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200804', 'Cristo Nos Valga', '1', null, '161');
INSERT INTO `inicio_distrito` VALUES ('1608', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200805', 'Vice', '1', null, '161');
INSERT INTO `inicio_distrito` VALUES ('1609', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '200806', 'Rinconada Llicuar', '1', null, '161');
INSERT INTO `inicio_distrito` VALUES ('1610', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210101', 'Puno', '1', null, '162');
INSERT INTO `inicio_distrito` VALUES ('1611', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210102', 'Acora', '1', null, '162');
INSERT INTO `inicio_distrito` VALUES ('1612', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210103', 'Amantani', '1', null, '162');
INSERT INTO `inicio_distrito` VALUES ('1613', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210104', 'Atuncolla', '1', null, '162');
INSERT INTO `inicio_distrito` VALUES ('1614', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210105', 'Capachica', '1', null, '162');
INSERT INTO `inicio_distrito` VALUES ('1615', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210106', 'Chucuito', '1', null, '162');
INSERT INTO `inicio_distrito` VALUES ('1616', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210107', 'Coata', '1', null, '162');
INSERT INTO `inicio_distrito` VALUES ('1617', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210108', 'Huata', '1', null, '162');
INSERT INTO `inicio_distrito` VALUES ('1618', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210109', 'Maqazo', '1', null, '162');
INSERT INTO `inicio_distrito` VALUES ('1619', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210110', 'Paucarcolla', '1', null, '162');
INSERT INTO `inicio_distrito` VALUES ('1620', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210111', 'Pichacani', '1', null, '162');
INSERT INTO `inicio_distrito` VALUES ('1621', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210112', 'Plateria', '1', null, '162');
INSERT INTO `inicio_distrito` VALUES ('1622', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210113', 'San Antonio', '1', null, '162');
INSERT INTO `inicio_distrito` VALUES ('1623', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210114', 'Tiquillaca', '1', null, '162');
INSERT INTO `inicio_distrito` VALUES ('1624', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210115', 'Vilque', '1', null, '162');
INSERT INTO `inicio_distrito` VALUES ('1625', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210201', 'Azangaro', '1', null, '163');
INSERT INTO `inicio_distrito` VALUES ('1626', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210202', 'Achaya', '1', null, '163');
INSERT INTO `inicio_distrito` VALUES ('1627', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210203', 'Arapa', '1', null, '163');
INSERT INTO `inicio_distrito` VALUES ('1628', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210204', 'Asillo', '1', null, '163');
INSERT INTO `inicio_distrito` VALUES ('1629', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210205', 'Caminaca', '1', null, '163');
INSERT INTO `inicio_distrito` VALUES ('1630', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210206', 'Chupa', '1', null, '163');
INSERT INTO `inicio_distrito` VALUES ('1631', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210207', 'Jose Domingo Choquehuanca', '1', null, '163');
INSERT INTO `inicio_distrito` VALUES ('1632', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210208', 'Muqani', '1', null, '163');
INSERT INTO `inicio_distrito` VALUES ('1633', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210209', 'Potoni', '1', null, '163');
INSERT INTO `inicio_distrito` VALUES ('1634', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210210', 'Saman', '1', null, '163');
INSERT INTO `inicio_distrito` VALUES ('1635', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210211', 'San Anton', '1', null, '163');
INSERT INTO `inicio_distrito` VALUES ('1636', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210212', 'San Jose', '1', null, '163');
INSERT INTO `inicio_distrito` VALUES ('1637', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210213', 'San Juan de Salinas', '1', null, '163');
INSERT INTO `inicio_distrito` VALUES ('1638', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210214', 'Santiago de Pupuja', '1', null, '163');
INSERT INTO `inicio_distrito` VALUES ('1639', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210215', 'Tirapata', '1', null, '163');
INSERT INTO `inicio_distrito` VALUES ('1640', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210301', 'Macusani', '1', null, '164');
INSERT INTO `inicio_distrito` VALUES ('1641', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210302', 'Ajoyani', '1', null, '164');
INSERT INTO `inicio_distrito` VALUES ('1642', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210303', 'Ayapata', '1', null, '164');
INSERT INTO `inicio_distrito` VALUES ('1643', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210304', 'Coasa', '1', null, '164');
INSERT INTO `inicio_distrito` VALUES ('1644', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210305', 'Corani', '1', null, '164');
INSERT INTO `inicio_distrito` VALUES ('1645', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210306', 'Crucero', '1', null, '164');
INSERT INTO `inicio_distrito` VALUES ('1646', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210307', 'Ituata', '1', null, '164');
INSERT INTO `inicio_distrito` VALUES ('1647', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210308', 'Ollachea', '1', null, '164');
INSERT INTO `inicio_distrito` VALUES ('1648', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210309', 'San Gaban', '1', null, '164');
INSERT INTO `inicio_distrito` VALUES ('1649', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210310', 'Usicayos', '1', null, '164');
INSERT INTO `inicio_distrito` VALUES ('1650', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210401', 'Juli', '1', null, '165');
INSERT INTO `inicio_distrito` VALUES ('1651', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210402', 'Desaguadero', '1', null, '165');
INSERT INTO `inicio_distrito` VALUES ('1652', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210403', 'Huacullani', '1', null, '165');
INSERT INTO `inicio_distrito` VALUES ('1653', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210404', 'Kelluyo', '1', null, '165');
INSERT INTO `inicio_distrito` VALUES ('1654', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210405', 'Pisacoma', '1', null, '165');
INSERT INTO `inicio_distrito` VALUES ('1655', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210406', 'Pomata', '1', null, '165');
INSERT INTO `inicio_distrito` VALUES ('1656', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210407', 'Zepita', '1', null, '165');
INSERT INTO `inicio_distrito` VALUES ('1657', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210501', 'Ilave', '1', null, '166');
INSERT INTO `inicio_distrito` VALUES ('1658', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210502', 'Capazo', '1', null, '166');
INSERT INTO `inicio_distrito` VALUES ('1659', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210503', 'Pilcuyo', '1', null, '166');
INSERT INTO `inicio_distrito` VALUES ('1660', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210504', 'Santa Rosa', '1', null, '166');
INSERT INTO `inicio_distrito` VALUES ('1661', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210505', 'Conduriri', '1', null, '166');
INSERT INTO `inicio_distrito` VALUES ('1662', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210601', 'Huancane', '1', null, '167');
INSERT INTO `inicio_distrito` VALUES ('1663', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210602', 'Cojata', '1', null, '167');
INSERT INTO `inicio_distrito` VALUES ('1664', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210603', 'Huatasani', '1', null, '167');
INSERT INTO `inicio_distrito` VALUES ('1665', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210604', 'Inchupalla', '1', null, '167');
INSERT INTO `inicio_distrito` VALUES ('1666', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210605', 'Pusi', '1', null, '167');
INSERT INTO `inicio_distrito` VALUES ('1667', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210606', 'Rosaspata', '1', null, '167');
INSERT INTO `inicio_distrito` VALUES ('1668', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210607', 'Taraco', '1', null, '167');
INSERT INTO `inicio_distrito` VALUES ('1669', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210608', 'Vilque Chico', '1', null, '167');
INSERT INTO `inicio_distrito` VALUES ('1670', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210701', 'Lampa', '1', null, '168');
INSERT INTO `inicio_distrito` VALUES ('1671', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210702', 'Cabanilla', '1', null, '168');
INSERT INTO `inicio_distrito` VALUES ('1672', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210703', 'Calapuja', '1', null, '168');
INSERT INTO `inicio_distrito` VALUES ('1673', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210704', 'Nicasio', '1', null, '168');
INSERT INTO `inicio_distrito` VALUES ('1674', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210705', 'Ocuviri', '1', null, '168');
INSERT INTO `inicio_distrito` VALUES ('1675', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210706', 'Palca', '1', null, '168');
INSERT INTO `inicio_distrito` VALUES ('1676', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210707', 'Paratia', '1', null, '168');
INSERT INTO `inicio_distrito` VALUES ('1677', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210708', 'Pucara', '1', null, '168');
INSERT INTO `inicio_distrito` VALUES ('1678', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210709', 'Santa Lucia', '1', null, '168');
INSERT INTO `inicio_distrito` VALUES ('1679', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210710', 'Vilavila', '1', null, '168');
INSERT INTO `inicio_distrito` VALUES ('1680', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210801', 'Ayaviri', '1', null, '169');
INSERT INTO `inicio_distrito` VALUES ('1681', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210802', 'Antauta', '1', null, '169');
INSERT INTO `inicio_distrito` VALUES ('1682', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210803', 'Cupi', '1', null, '169');
INSERT INTO `inicio_distrito` VALUES ('1683', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210804', 'Llalli', '1', null, '169');
INSERT INTO `inicio_distrito` VALUES ('1684', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210805', 'Macari', '1', null, '169');
INSERT INTO `inicio_distrito` VALUES ('1685', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210806', 'Nuqoa', '1', null, '169');
INSERT INTO `inicio_distrito` VALUES ('1686', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210807', 'Orurillo', '1', null, '169');
INSERT INTO `inicio_distrito` VALUES ('1687', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210808', 'Santa Rosa', '1', null, '169');
INSERT INTO `inicio_distrito` VALUES ('1688', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210809', 'Umachiri', '1', null, '169');
INSERT INTO `inicio_distrito` VALUES ('1689', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210901', 'Moho', '1', null, '170');
INSERT INTO `inicio_distrito` VALUES ('1690', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210902', 'Conima', '1', null, '170');
INSERT INTO `inicio_distrito` VALUES ('1691', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210903', 'Huayrapata', '1', null, '170');
INSERT INTO `inicio_distrito` VALUES ('1692', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '210904', 'Tilali', '1', null, '170');
INSERT INTO `inicio_distrito` VALUES ('1693', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211001', 'Putina', '1', null, '171');
INSERT INTO `inicio_distrito` VALUES ('1694', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211002', 'Ananea', '1', null, '171');
INSERT INTO `inicio_distrito` VALUES ('1695', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211003', 'Pedro Vilca Apaza', '1', null, '171');
INSERT INTO `inicio_distrito` VALUES ('1696', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211004', 'Quilcapuncu', '1', null, '171');
INSERT INTO `inicio_distrito` VALUES ('1697', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211005', 'Sina', '1', null, '171');
INSERT INTO `inicio_distrito` VALUES ('1698', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211101', 'Juliaca', '1', null, '172');
INSERT INTO `inicio_distrito` VALUES ('1699', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211102', 'Cabana', '1', null, '172');
INSERT INTO `inicio_distrito` VALUES ('1700', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211103', 'Cabanillas', '1', null, '172');
INSERT INTO `inicio_distrito` VALUES ('1701', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211104', 'Caracoto', '1', null, '172');
INSERT INTO `inicio_distrito` VALUES ('1702', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211201', 'Sandia', '1', null, '173');
INSERT INTO `inicio_distrito` VALUES ('1703', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211202', 'Cuyocuyo', '1', null, '173');
INSERT INTO `inicio_distrito` VALUES ('1704', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211203', 'Limbani', '1', null, '173');
INSERT INTO `inicio_distrito` VALUES ('1705', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211204', 'Patambuco', '1', null, '173');
INSERT INTO `inicio_distrito` VALUES ('1706', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211205', 'Phara', '1', null, '173');
INSERT INTO `inicio_distrito` VALUES ('1707', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211206', 'Quiaca', '1', null, '173');
INSERT INTO `inicio_distrito` VALUES ('1708', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211207', 'San Juan del Oro', '1', null, '173');
INSERT INTO `inicio_distrito` VALUES ('1709', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211208', 'Yanahuaya', '1', null, '173');
INSERT INTO `inicio_distrito` VALUES ('1710', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211209', 'Alto Inambari', '1', null, '173');
INSERT INTO `inicio_distrito` VALUES ('1711', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211301', 'Yunguyo', '1', null, '174');
INSERT INTO `inicio_distrito` VALUES ('1712', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211302', 'Anapia', '1', null, '174');
INSERT INTO `inicio_distrito` VALUES ('1713', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211303', 'Copani', '1', null, '174');
INSERT INTO `inicio_distrito` VALUES ('1714', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211304', 'Cuturapi', '1', null, '174');
INSERT INTO `inicio_distrito` VALUES ('1715', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211305', 'Ollaraya', '1', null, '174');
INSERT INTO `inicio_distrito` VALUES ('1716', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211306', 'Tinicachi', '1', null, '174');
INSERT INTO `inicio_distrito` VALUES ('1717', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '211307', 'Unicachi', '1', null, '174');
INSERT INTO `inicio_distrito` VALUES ('1718', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220101', 'Moyobamba', '1', null, '175');
INSERT INTO `inicio_distrito` VALUES ('1719', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220102', 'Calzada', '1', null, '175');
INSERT INTO `inicio_distrito` VALUES ('1720', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220103', 'Habana', '1', null, '175');
INSERT INTO `inicio_distrito` VALUES ('1721', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220104', 'Jepelacio', '1', null, '175');
INSERT INTO `inicio_distrito` VALUES ('1722', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220105', 'Soritor', '1', null, '175');
INSERT INTO `inicio_distrito` VALUES ('1723', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220106', 'Yantalo', '1', null, '175');
INSERT INTO `inicio_distrito` VALUES ('1724', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220201', 'Bellavista', '1', null, '176');
INSERT INTO `inicio_distrito` VALUES ('1725', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220202', 'Alto Biavo', '1', null, '176');
INSERT INTO `inicio_distrito` VALUES ('1726', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220203', 'Bajo Biavo', '1', null, '176');
INSERT INTO `inicio_distrito` VALUES ('1727', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220204', 'Huallaga', '1', null, '176');
INSERT INTO `inicio_distrito` VALUES ('1728', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220205', 'San Pablo', '1', null, '176');
INSERT INTO `inicio_distrito` VALUES ('1729', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220206', 'San Rafael', '1', null, '176');
INSERT INTO `inicio_distrito` VALUES ('1730', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220301', 'San Jose de Sisa', '1', null, '177');
INSERT INTO `inicio_distrito` VALUES ('1731', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220302', 'Agua Blanca', '1', null, '177');
INSERT INTO `inicio_distrito` VALUES ('1732', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220303', 'San Martin', '1', null, '177');
INSERT INTO `inicio_distrito` VALUES ('1733', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220304', 'Santa Rosa', '1', null, '177');
INSERT INTO `inicio_distrito` VALUES ('1734', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220305', 'Shatoja', '1', null, '177');
INSERT INTO `inicio_distrito` VALUES ('1735', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220401', 'Saposoa', '1', null, '178');
INSERT INTO `inicio_distrito` VALUES ('1736', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220402', 'Alto Saposoa', '1', null, '178');
INSERT INTO `inicio_distrito` VALUES ('1737', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220403', 'El Eslabon', '1', null, '178');
INSERT INTO `inicio_distrito` VALUES ('1738', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220404', 'Piscoyacu', '1', null, '178');
INSERT INTO `inicio_distrito` VALUES ('1739', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220405', 'Sacanche', '1', null, '178');
INSERT INTO `inicio_distrito` VALUES ('1740', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220406', 'Tingo de Saposoa', '1', null, '178');
INSERT INTO `inicio_distrito` VALUES ('1741', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220501', 'Lamas', '1', null, '179');
INSERT INTO `inicio_distrito` VALUES ('1742', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220502', 'Alonso de Alvarado', '1', null, '179');
INSERT INTO `inicio_distrito` VALUES ('1743', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220503', 'Barranquita', '1', null, '179');
INSERT INTO `inicio_distrito` VALUES ('1744', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220504', 'Caynarachi', '1', null, '179');
INSERT INTO `inicio_distrito` VALUES ('1745', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220505', 'Cuqumbuqui', '1', null, '179');
INSERT INTO `inicio_distrito` VALUES ('1746', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220506', 'Pinto Recodo', '1', null, '179');
INSERT INTO `inicio_distrito` VALUES ('1747', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220507', 'Rumisapa', '1', null, '179');
INSERT INTO `inicio_distrito` VALUES ('1748', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220508', 'San Roque de Cumbaza', '1', null, '179');
INSERT INTO `inicio_distrito` VALUES ('1749', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220509', 'Shanao', '1', null, '179');
INSERT INTO `inicio_distrito` VALUES ('1750', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220510', 'Tabalosos', '1', null, '179');
INSERT INTO `inicio_distrito` VALUES ('1751', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220511', 'Zapatero', '1', null, '179');
INSERT INTO `inicio_distrito` VALUES ('1752', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220601', 'Juanjui', '1', null, '180');
INSERT INTO `inicio_distrito` VALUES ('1753', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220602', 'Campanilla', '1', null, '180');
INSERT INTO `inicio_distrito` VALUES ('1754', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220603', 'Huicungo', '1', null, '180');
INSERT INTO `inicio_distrito` VALUES ('1755', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220604', 'Pachiza', '1', null, '180');
INSERT INTO `inicio_distrito` VALUES ('1756', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220605', 'Pajarillo', '1', null, '180');
INSERT INTO `inicio_distrito` VALUES ('1757', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220701', 'Picota', '1', null, '181');
INSERT INTO `inicio_distrito` VALUES ('1758', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220702', 'Buenos Aires', '1', null, '181');
INSERT INTO `inicio_distrito` VALUES ('1759', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220703', 'Caspisapa', '1', null, '181');
INSERT INTO `inicio_distrito` VALUES ('1760', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220704', 'Pilluana', '1', null, '181');
INSERT INTO `inicio_distrito` VALUES ('1761', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220705', 'Pucacaca', '1', null, '181');
INSERT INTO `inicio_distrito` VALUES ('1762', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220706', 'San Cristobal', '1', null, '181');
INSERT INTO `inicio_distrito` VALUES ('1763', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220707', 'San Hilarion', '1', null, '181');
INSERT INTO `inicio_distrito` VALUES ('1764', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220708', 'Shamboyacu', '1', null, '181');
INSERT INTO `inicio_distrito` VALUES ('1765', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220709', 'Tingo de Ponasa', '1', null, '181');
INSERT INTO `inicio_distrito` VALUES ('1766', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220710', 'Tres Unidos', '1', null, '181');
INSERT INTO `inicio_distrito` VALUES ('1767', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220801', 'Rioja', '1', null, '182');
INSERT INTO `inicio_distrito` VALUES ('1768', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220802', 'Awajun', '1', null, '182');
INSERT INTO `inicio_distrito` VALUES ('1769', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220803', 'Elias Soplin Vargas', '1', null, '182');
INSERT INTO `inicio_distrito` VALUES ('1770', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220804', 'Nueva Cajamarca', '1', null, '182');
INSERT INTO `inicio_distrito` VALUES ('1771', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220805', 'Pardo Miguel', '1', null, '182');
INSERT INTO `inicio_distrito` VALUES ('1772', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220806', 'Posic', '1', null, '182');
INSERT INTO `inicio_distrito` VALUES ('1773', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220807', 'San Fernando', '1', null, '182');
INSERT INTO `inicio_distrito` VALUES ('1774', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220808', 'Yorongos', '1', null, '182');
INSERT INTO `inicio_distrito` VALUES ('1775', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220809', 'Yuracyacu', '1', null, '182');
INSERT INTO `inicio_distrito` VALUES ('1776', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220901', 'Tarapoto', '1', null, '183');
INSERT INTO `inicio_distrito` VALUES ('1777', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220902', 'Alberto Leveau', '1', null, '183');
INSERT INTO `inicio_distrito` VALUES ('1778', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220903', 'Cacatachi', '1', null, '183');
INSERT INTO `inicio_distrito` VALUES ('1779', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220904', 'Chazuta', '1', null, '183');
INSERT INTO `inicio_distrito` VALUES ('1780', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220905', 'Chipurana', '1', null, '183');
INSERT INTO `inicio_distrito` VALUES ('1781', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220906', 'El Porvenir', '1', null, '183');
INSERT INTO `inicio_distrito` VALUES ('1782', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220907', 'Huimbayoc', '1', null, '183');
INSERT INTO `inicio_distrito` VALUES ('1783', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220908', 'Juan Guerra', '1', null, '183');
INSERT INTO `inicio_distrito` VALUES ('1784', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220909', 'La Banda de Shilcayo', '1', null, '183');
INSERT INTO `inicio_distrito` VALUES ('1785', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220910', 'Morales', '1', null, '183');
INSERT INTO `inicio_distrito` VALUES ('1786', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220911', 'Papaplaya', '1', null, '183');
INSERT INTO `inicio_distrito` VALUES ('1787', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220912', 'San Antonio', '1', null, '183');
INSERT INTO `inicio_distrito` VALUES ('1788', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220913', 'Sauce', '1', null, '183');
INSERT INTO `inicio_distrito` VALUES ('1789', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '220914', 'Shapaja', '1', null, '183');
INSERT INTO `inicio_distrito` VALUES ('1790', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '221001', 'Tocache', '1', null, '184');
INSERT INTO `inicio_distrito` VALUES ('1791', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '221002', 'Nuevo Progreso', '1', null, '184');
INSERT INTO `inicio_distrito` VALUES ('1792', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '221003', 'Polvora', '1', null, '184');
INSERT INTO `inicio_distrito` VALUES ('1793', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '221004', 'Shunte', '1', null, '184');
INSERT INTO `inicio_distrito` VALUES ('1794', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '221005', 'Uchiza', '1', null, '184');
INSERT INTO `inicio_distrito` VALUES ('1795', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230101', 'Tacna', '1', null, '185');
INSERT INTO `inicio_distrito` VALUES ('1796', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230102', 'Alto de la Alianza', '1', null, '185');
INSERT INTO `inicio_distrito` VALUES ('1797', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230103', 'Calana', '1', null, '185');
INSERT INTO `inicio_distrito` VALUES ('1798', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230104', 'Ciudad Nueva', '1', null, '185');
INSERT INTO `inicio_distrito` VALUES ('1799', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230105', 'Inclan', '1', null, '185');
INSERT INTO `inicio_distrito` VALUES ('1800', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230106', 'Pachia', '1', null, '185');
INSERT INTO `inicio_distrito` VALUES ('1801', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230107', 'Palca', '1', null, '185');
INSERT INTO `inicio_distrito` VALUES ('1802', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230108', 'Pocollay', '1', null, '185');
INSERT INTO `inicio_distrito` VALUES ('1803', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230109', 'Sama', '1', null, '185');
INSERT INTO `inicio_distrito` VALUES ('1804', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230110', 'Cor Gregorio Albarracín', '1', null, '185');
INSERT INTO `inicio_distrito` VALUES ('1805', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230201', 'Candarave', '1', null, '186');
INSERT INTO `inicio_distrito` VALUES ('1806', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230202', 'Cairani', '1', null, '186');
INSERT INTO `inicio_distrito` VALUES ('1807', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230203', 'Camilaca', '1', null, '186');
INSERT INTO `inicio_distrito` VALUES ('1808', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230204', 'Curibaya', '1', null, '186');
INSERT INTO `inicio_distrito` VALUES ('1809', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230205', 'Huanuara', '1', null, '186');
INSERT INTO `inicio_distrito` VALUES ('1810', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230206', 'Quilahuani', '1', null, '186');
INSERT INTO `inicio_distrito` VALUES ('1811', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230301', 'Locumba', '1', null, '187');
INSERT INTO `inicio_distrito` VALUES ('1812', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230302', 'Ilabaya', '1', null, '187');
INSERT INTO `inicio_distrito` VALUES ('1813', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230303', 'Ite', '1', null, '187');
INSERT INTO `inicio_distrito` VALUES ('1814', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230401', 'Tarata', '1', null, '188');
INSERT INTO `inicio_distrito` VALUES ('1815', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230402', 'Chucatamani', '1', null, '188');
INSERT INTO `inicio_distrito` VALUES ('1816', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230403', 'Estique', '1', null, '188');
INSERT INTO `inicio_distrito` VALUES ('1817', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230404', 'Estique-Pampa', '1', null, '188');
INSERT INTO `inicio_distrito` VALUES ('1818', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230405', 'Sitajara', '1', null, '188');
INSERT INTO `inicio_distrito` VALUES ('1819', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230406', 'Susapaya', '1', null, '188');
INSERT INTO `inicio_distrito` VALUES ('1820', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230407', 'Tarucachi', '1', null, '188');
INSERT INTO `inicio_distrito` VALUES ('1821', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '230408', 'Ticaco', '1', null, '188');
INSERT INTO `inicio_distrito` VALUES ('1822', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '240101', 'Tumbes', '1', null, '189');
INSERT INTO `inicio_distrito` VALUES ('1823', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '240102', 'Corrales', '1', null, '189');
INSERT INTO `inicio_distrito` VALUES ('1824', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '240103', 'La Cruz', '1', null, '189');
INSERT INTO `inicio_distrito` VALUES ('1825', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '240104', 'Pampas de Hospital', '1', null, '189');
INSERT INTO `inicio_distrito` VALUES ('1826', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '240105', 'San Jacinto', '1', null, '189');
INSERT INTO `inicio_distrito` VALUES ('1827', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '240106', 'San Juan de la Virgen', '1', null, '189');
INSERT INTO `inicio_distrito` VALUES ('1828', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '240201', 'Zorritos', '1', null, '190');
INSERT INTO `inicio_distrito` VALUES ('1829', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '240202', 'Casitas', '1', null, '190');
INSERT INTO `inicio_distrito` VALUES ('1830', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '240301', 'Zarumilla', '1', null, '191');
INSERT INTO `inicio_distrito` VALUES ('1831', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '240302', 'Aguas Verdes', '1', null, '191');
INSERT INTO `inicio_distrito` VALUES ('1832', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '240303', 'Matapalo', '1', null, '191');
INSERT INTO `inicio_distrito` VALUES ('1833', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '240304', 'Papayal', '1', null, '191');
INSERT INTO `inicio_distrito` VALUES ('1834', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '250101', 'Calleria', '1', null, '192');
INSERT INTO `inicio_distrito` VALUES ('1835', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '250102', 'Campoverde', '1', null, '192');
INSERT INTO `inicio_distrito` VALUES ('1836', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '250103', 'Iparia', '1', null, '192');
INSERT INTO `inicio_distrito` VALUES ('1837', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '250104', 'Masisea', '1', null, '192');
INSERT INTO `inicio_distrito` VALUES ('1838', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '250105', 'Yarinacocha', '1', null, '192');
INSERT INTO `inicio_distrito` VALUES ('1839', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '250106', 'Nueva Requena', '1', null, '192');
INSERT INTO `inicio_distrito` VALUES ('1840', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '250201', 'Raymondi', '1', null, '193');
INSERT INTO `inicio_distrito` VALUES ('1841', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '250202', 'Sepahua', '1', null, '193');
INSERT INTO `inicio_distrito` VALUES ('1842', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '250203', 'Tahuania', '1', null, '193');
INSERT INTO `inicio_distrito` VALUES ('1843', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '250204', 'Yurua', '1', null, '193');
INSERT INTO `inicio_distrito` VALUES ('1844', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '250301', 'Padre Abad', '1', null, '194');
INSERT INTO `inicio_distrito` VALUES ('1845', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '250302', 'Irazola', '1', null, '194');
INSERT INTO `inicio_distrito` VALUES ('1846', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '250303', 'Curimana', '1', null, '194');
INSERT INTO `inicio_distrito` VALUES ('1847', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '250401', 'Purus', '1', null, '195');

-- ----------------------------
-- Table structure for inicio_especialidad
-- ----------------------------
DROP TABLE IF EXISTS `inicio_especialidad`;
CREATE TABLE `inicio_especialidad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `inicio_especialidad_creador_id_a8b0f332_fk_auth_user_id` (`creador_id`),
  KEY `inicio_especialidad_editor_id_7ca2d3dd_fk_auth_user_id` (`editor_id`),
  CONSTRAINT `inicio_especialidad_creador_id_a8b0f332_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `inicio_especialidad_editor_id_7ca2d3dd_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of inicio_especialidad
-- ----------------------------
INSERT INTO `inicio_especialidad` VALUES ('1', '2017-06-16 12:12:37.000000', '2017-06-16 21:52:21.000000', 'Administrador de Empresas', '1', '2');
INSERT INTO `inicio_especialidad` VALUES ('3', '2017-06-16 20:19:30.000000', '2017-06-16 20:19:30.000000', 'Redes y Telecomunicaciones', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('4', '2017-06-16 21:42:40.000000', '2017-06-16 21:42:40.000000', 'Especialidad 1', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('5', '2017-06-16 21:42:46.000000', '2017-06-16 21:42:46.000000', 'Especialidad 2', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('6', '2017-06-16 21:42:52.000000', '2017-06-16 21:42:52.000000', 'Especialidad 3', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('7', '2017-06-16 21:42:57.000000', '2017-06-16 21:42:57.000000', 'Especialidad 4', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('8', '2017-06-16 21:43:02.000000', '2017-06-18 22:13:46.000000', 'Especialidad 15', '2', '2');
INSERT INTO `inicio_especialidad` VALUES ('9', '2017-06-16 21:43:07.000000', '2017-06-16 21:43:07.000000', 'Especialidad 6', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('10', '2017-06-16 21:43:12.000000', '2017-06-16 21:43:12.000000', 'Especialidad 7', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('11', '2017-06-16 21:43:16.000000', '2017-06-16 21:43:16.000000', 'Especialidad 8', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('12', '2017-06-16 21:43:22.000000', '2017-06-18 20:38:37.000000', 'Especialidad 9', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('13', '2017-06-16 21:43:27.000000', '2017-06-16 21:43:27.000000', 'Especialidad 10', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('30', '2017-06-19 19:24:21.000000', '2017-06-19 19:24:21.000000', 'Especialidad 11', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('31', '2017-06-19 19:24:21.000000', '2017-06-19 19:24:21.000000', 'Especialidad 12', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('32', '2017-06-19 19:24:21.000000', '2017-06-19 19:24:21.000000', 'Especialidad 13', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('33', '2017-06-19 19:24:21.000000', '2017-06-19 19:24:21.000000', 'Especialidad 14', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('35', '2017-06-19 19:24:49.000000', '2017-06-19 19:24:49.000000', 'Especialidad 16', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('36', '2017-06-19 19:24:49.000000', '2017-06-19 19:24:49.000000', 'Especialidad 17', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('37', '2017-06-19 19:24:49.000000', '2017-06-19 19:24:49.000000', 'Especialidad 18', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('38', '2017-06-19 19:24:49.000000', '2017-06-19 19:24:49.000000', 'Especialidad 19', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('39', '2017-06-19 19:24:49.000000', '2017-06-19 19:24:49.000000', 'Especialidad 20', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('40', '2017-06-19 19:24:50.000000', '2017-06-19 19:24:50.000000', 'Especialidad 21', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('41', '2017-06-19 19:24:50.000000', '2017-06-19 19:24:50.000000', 'Especialidad 22', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('42', '2017-06-19 19:24:50.000000', '2017-06-19 19:24:50.000000', 'Especialidad 23', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('43', '2017-06-19 19:24:50.000000', '2017-06-19 19:24:50.000000', 'Especialidad 24', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('44', '2017-06-19 19:24:50.000000', '2017-06-19 19:24:50.000000', 'Especialidad 25', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('45', '2017-06-19 19:24:50.000000', '2017-06-19 19:24:50.000000', 'Especialidad 26', '2', null);
INSERT INTO `inicio_especialidad` VALUES ('46', '2017-06-19 19:24:50.000000', '2017-06-22 12:00:34.000000', 'Especialidad 27', '2', '2');

-- ----------------------------
-- Table structure for inicio_mes
-- ----------------------------
DROP TABLE IF EXISTS `inicio_mes`;
CREATE TABLE `inicio_mes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `numero` smallint(6) NOT NULL,
  `nombre` varchar(15) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `numero` (`numero`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `inicio_mes_creador_id_602c3cc2_fk_auth_user_id` (`creador_id`),
  KEY `inicio_mes_editor_id_627fb389_fk_auth_user_id` (`editor_id`),
  CONSTRAINT `inicio_mes_creador_id_602c3cc2_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `inicio_mes_editor_id_627fb389_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of inicio_mes
-- ----------------------------
INSERT INTO `inicio_mes` VALUES ('1', '2017-06-28 09:10:14.000000', '2017-06-28 09:10:14.000000', '1', 'Enero', '1', null);
INSERT INTO `inicio_mes` VALUES ('2', '2017-06-28 09:10:22.000000', '2017-06-28 09:10:22.000000', '2', 'Febrero', '1', null);
INSERT INTO `inicio_mes` VALUES ('3', '2017-06-28 09:10:31.000000', '2017-06-28 09:10:31.000000', '3', 'Marzo', '1', null);
INSERT INTO `inicio_mes` VALUES ('4', '2017-06-28 09:10:45.000000', '2017-06-28 09:10:45.000000', '4', 'Abril', '1', null);
INSERT INTO `inicio_mes` VALUES ('5', '2017-06-28 09:11:01.000000', '2017-06-28 09:11:01.000000', '5', 'Mayo', '1', null);
INSERT INTO `inicio_mes` VALUES ('6', '2017-06-28 09:11:15.000000', '2017-06-28 09:11:15.000000', '6', 'Junio', '1', null);
INSERT INTO `inicio_mes` VALUES ('7', '2017-07-02 15:25:46.000000', '2017-07-02 15:25:46.000000', '7', 'Julio', '1', null);
INSERT INTO `inicio_mes` VALUES ('8', '2017-07-02 15:25:54.000000', '2017-07-02 15:25:54.000000', '8', 'Agosto', '1', null);
INSERT INTO `inicio_mes` VALUES ('9', '2017-07-02 15:26:02.000000', '2017-07-02 15:26:02.000000', '9', 'Septiembre', '1', null);
INSERT INTO `inicio_mes` VALUES ('10', '2017-07-02 15:26:14.000000', '2017-07-02 15:26:14.000000', '10', 'Octubre', '1', null);
INSERT INTO `inicio_mes` VALUES ('11', '2017-07-02 15:26:28.000000', '2017-07-02 15:26:28.000000', '11', 'Noviembre', '1', null);
INSERT INTO `inicio_mes` VALUES ('12', '2017-07-02 15:26:38.000000', '2017-07-02 15:26:38.000000', '12', 'Diciembre', '1', null);

-- ----------------------------
-- Table structure for inicio_profesion
-- ----------------------------
DROP TABLE IF EXISTS `inicio_profesion`;
CREATE TABLE `inicio_profesion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `inicio_profesion_creador_id_aea6a1cc_fk_auth_user_id` (`creador_id`),
  KEY `inicio_profesion_editor_id_ce1aaa3c_fk_auth_user_id` (`editor_id`),
  CONSTRAINT `inicio_profesion_creador_id_aea6a1cc_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `inicio_profesion_editor_id_ce1aaa3c_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of inicio_profesion
-- ----------------------------
INSERT INTO `inicio_profesion` VALUES ('1', '2017-06-16 12:13:02.000000', '2017-06-20 08:06:42.000000', 'Administracion 3', '2', null);
INSERT INTO `inicio_profesion` VALUES ('2', '2017-06-16 12:13:04.000000', '2017-06-20 08:06:37.000000', 'Administración 2', '2', null);
INSERT INTO `inicio_profesion` VALUES ('3', '2017-06-16 12:13:04.000000', '2017-06-20 08:06:46.000000', 'Administración 1', '2', null);
INSERT INTO `inicio_profesion` VALUES ('4', '2017-07-22 23:09:51.589709', '2017-07-22 23:10:06.777370', 'Profesión', '1', '1');
INSERT INTO `inicio_profesion` VALUES ('5', '2017-07-24 22:30:50.159357', '2017-07-24 22:31:12.018808', 'Ingeniero de Minas', '1', '1');
INSERT INTO `inicio_profesion` VALUES ('6', '2017-07-24 22:31:22.051367', '2017-07-24 22:31:22.051409', 'Geografo', '1', null);

-- ----------------------------
-- Table structure for inicio_provincia
-- ----------------------------
DROP TABLE IF EXISTS `inicio_provincia`;
CREATE TABLE `inicio_provincia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `codigo` varchar(4) NOT NULL,
  `nombre` varchar(75) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `departamento_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo` (`codigo`),
  KEY `inicio_provincia_creador_id_0dd280f4_fk_auth_user_id` (`creador_id`),
  KEY `inicio_provincia_departamento_id_acb06bb6_fk_inicio_de` (`departamento_id`),
  KEY `inicio_provincia_editor_id_25e5c1a9_fk_auth_user_id` (`editor_id`),
  CONSTRAINT `inicio_provincia_creador_id_0dd280f4_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `inicio_provincia_departamento_id_acb06bb6_fk_inicio_de` FOREIGN KEY (`departamento_id`) REFERENCES `inicio_departamento` (`id`),
  CONSTRAINT `inicio_provincia_editor_id_25e5c1a9_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=196 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of inicio_provincia
-- ----------------------------
INSERT INTO `inicio_provincia` VALUES ('1', '2017-06-16 12:14:18.000000', '2017-06-16 12:14:18.000000', '0601', 'Cajamarca', '1', '1', null);
INSERT INTO `inicio_provincia` VALUES ('2', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0101', 'Chachapoyas', '1', '2', null);
INSERT INTO `inicio_provincia` VALUES ('3', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0102', 'Bagua', '1', '2', null);
INSERT INTO `inicio_provincia` VALUES ('4', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0103', 'Bongara', '1', '2', null);
INSERT INTO `inicio_provincia` VALUES ('5', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0104', 'Condorcanqui', '1', '2', null);
INSERT INTO `inicio_provincia` VALUES ('6', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0105', 'Luya', '1', '2', null);
INSERT INTO `inicio_provincia` VALUES ('7', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0106', 'Rodriguez de Mendoza', '1', '2', null);
INSERT INTO `inicio_provincia` VALUES ('8', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0107', 'Utcubamba', '1', '2', null);
INSERT INTO `inicio_provincia` VALUES ('9', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0201', 'Huaraz', '1', '3', null);
INSERT INTO `inicio_provincia` VALUES ('10', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0202', 'Aija', '1', '3', null);
INSERT INTO `inicio_provincia` VALUES ('11', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0203', 'Antonio Raymondi', '1', '3', null);
INSERT INTO `inicio_provincia` VALUES ('12', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0204', 'Asuncion', '1', '3', null);
INSERT INTO `inicio_provincia` VALUES ('13', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0205', 'Bolognesi', '1', '3', null);
INSERT INTO `inicio_provincia` VALUES ('14', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0206', 'Carhuaz', '1', '3', null);
INSERT INTO `inicio_provincia` VALUES ('15', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0207', 'Carlos Fermin Fitzcarrald', '1', '3', null);
INSERT INTO `inicio_provincia` VALUES ('16', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0208', 'Casma', '1', '3', null);
INSERT INTO `inicio_provincia` VALUES ('17', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0209', 'Corongo', '1', '3', null);
INSERT INTO `inicio_provincia` VALUES ('18', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0210', 'Huari', '1', '3', null);
INSERT INTO `inicio_provincia` VALUES ('19', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0211', 'Huarmey', '1', '3', null);
INSERT INTO `inicio_provincia` VALUES ('20', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0212', 'Huaylas', '1', '3', null);
INSERT INTO `inicio_provincia` VALUES ('21', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0213', 'Mariscal Luzuriaga', '1', '3', null);
INSERT INTO `inicio_provincia` VALUES ('22', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0214', 'Ocros', '1', '3', null);
INSERT INTO `inicio_provincia` VALUES ('23', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0215', 'Pallasca', '1', '3', null);
INSERT INTO `inicio_provincia` VALUES ('24', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0216', 'Pomabamba', '1', '3', null);
INSERT INTO `inicio_provincia` VALUES ('25', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0217', 'Recuay', '1', '3', null);
INSERT INTO `inicio_provincia` VALUES ('26', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0218', 'Santa', '1', '3', null);
INSERT INTO `inicio_provincia` VALUES ('27', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0219', 'Sihuas', '1', '3', null);
INSERT INTO `inicio_provincia` VALUES ('28', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0220', 'Yungay', '1', '3', null);
INSERT INTO `inicio_provincia` VALUES ('29', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0301', 'Abancay', '1', '4', null);
INSERT INTO `inicio_provincia` VALUES ('30', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0302', 'Andahuaylas', '1', '4', null);
INSERT INTO `inicio_provincia` VALUES ('31', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0303', 'Antabamba', '1', '4', null);
INSERT INTO `inicio_provincia` VALUES ('32', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0304', 'Aymaraes', '1', '4', null);
INSERT INTO `inicio_provincia` VALUES ('33', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0305', 'Cotabambas', '1', '4', null);
INSERT INTO `inicio_provincia` VALUES ('34', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0306', 'Chincheros', '1', '4', null);
INSERT INTO `inicio_provincia` VALUES ('35', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0307', 'Grau', '1', '4', null);
INSERT INTO `inicio_provincia` VALUES ('36', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0401', 'Arequipa', '1', '5', null);
INSERT INTO `inicio_provincia` VALUES ('37', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0402', 'Camana', '1', '5', null);
INSERT INTO `inicio_provincia` VALUES ('38', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0403', 'Caraveli', '1', '5', null);
INSERT INTO `inicio_provincia` VALUES ('39', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0404', 'Castilla', '1', '5', null);
INSERT INTO `inicio_provincia` VALUES ('40', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0405', 'Caylloma', '1', '5', null);
INSERT INTO `inicio_provincia` VALUES ('41', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0406', 'Condesuyos', '1', '5', null);
INSERT INTO `inicio_provincia` VALUES ('42', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0407', 'Islay', '1', '5', null);
INSERT INTO `inicio_provincia` VALUES ('43', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0408', 'La Union', '1', '5', null);
INSERT INTO `inicio_provincia` VALUES ('44', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0501', 'Huamanga', '1', '6', null);
INSERT INTO `inicio_provincia` VALUES ('45', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0502', 'Cangallo', '1', '6', null);
INSERT INTO `inicio_provincia` VALUES ('46', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0503', 'Huanca Sancos', '1', '6', null);
INSERT INTO `inicio_provincia` VALUES ('47', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0504', 'Huanta', '1', '6', null);
INSERT INTO `inicio_provincia` VALUES ('48', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0505', 'La Mar', '1', '6', null);
INSERT INTO `inicio_provincia` VALUES ('49', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0506', 'Lucanas', '1', '6', null);
INSERT INTO `inicio_provincia` VALUES ('50', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0507', 'Parinacochas', '1', '6', null);
INSERT INTO `inicio_provincia` VALUES ('51', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0508', 'Paucar del Sara Sara', '1', '6', null);
INSERT INTO `inicio_provincia` VALUES ('52', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0509', 'Sucre', '1', '6', null);
INSERT INTO `inicio_provincia` VALUES ('53', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0510', 'Victor Fajardo', '1', '6', null);
INSERT INTO `inicio_provincia` VALUES ('54', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0511', 'Vilcas Huaman', '1', '6', null);
INSERT INTO `inicio_provincia` VALUES ('56', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0602', 'Cajabamba', '1', '1', null);
INSERT INTO `inicio_provincia` VALUES ('57', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0603', 'Celendin', '1', '1', null);
INSERT INTO `inicio_provincia` VALUES ('58', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0604', 'Chota', '1', '1', null);
INSERT INTO `inicio_provincia` VALUES ('59', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0605', 'Contumaza', '1', '1', null);
INSERT INTO `inicio_provincia` VALUES ('60', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0606', 'Cutervo', '1', '1', null);
INSERT INTO `inicio_provincia` VALUES ('61', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0607', 'Hualgayoc', '1', '1', null);
INSERT INTO `inicio_provincia` VALUES ('62', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0608', 'Jaen', '1', '1', null);
INSERT INTO `inicio_provincia` VALUES ('63', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0609', 'San Ignacio', '1', '1', null);
INSERT INTO `inicio_provincia` VALUES ('64', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0610', 'San Marcos', '1', '1', null);
INSERT INTO `inicio_provincia` VALUES ('65', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0611', 'San Miguel', '1', '1', null);
INSERT INTO `inicio_provincia` VALUES ('66', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0612', 'San Pablo', '1', '1', null);
INSERT INTO `inicio_provincia` VALUES ('67', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0613', 'Santa Cruz', '1', '1', null);
INSERT INTO `inicio_provincia` VALUES ('68', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0701', 'Callao', '1', '7', null);
INSERT INTO `inicio_provincia` VALUES ('69', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0801', 'Cusco', '1', '8', null);
INSERT INTO `inicio_provincia` VALUES ('70', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0802', 'Acomayo', '1', '8', null);
INSERT INTO `inicio_provincia` VALUES ('71', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0803', 'Anta', '1', '8', null);
INSERT INTO `inicio_provincia` VALUES ('72', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0804', 'Calca', '1', '8', null);
INSERT INTO `inicio_provincia` VALUES ('73', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0805', 'Canas', '1', '8', null);
INSERT INTO `inicio_provincia` VALUES ('74', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0806', 'Canchis', '1', '8', null);
INSERT INTO `inicio_provincia` VALUES ('75', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0807', 'Chumbivilcas', '1', '8', null);
INSERT INTO `inicio_provincia` VALUES ('76', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0808', 'Espinar', '1', '8', null);
INSERT INTO `inicio_provincia` VALUES ('77', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0809', 'La Convencion', '1', '8', null);
INSERT INTO `inicio_provincia` VALUES ('78', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0810', 'Paruro', '1', '8', null);
INSERT INTO `inicio_provincia` VALUES ('79', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0811', 'Paucartambo', '1', '8', null);
INSERT INTO `inicio_provincia` VALUES ('80', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0812', 'Quispicanchi', '1', '8', null);
INSERT INTO `inicio_provincia` VALUES ('81', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0813', 'Urubamba', '1', '8', null);
INSERT INTO `inicio_provincia` VALUES ('82', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0901', 'Huancavelica', '1', '9', null);
INSERT INTO `inicio_provincia` VALUES ('83', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0902', 'Acobamba', '1', '9', null);
INSERT INTO `inicio_provincia` VALUES ('84', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0903', 'Angaraes', '1', '9', null);
INSERT INTO `inicio_provincia` VALUES ('85', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0904', 'Castrovirreyna', '1', '9', null);
INSERT INTO `inicio_provincia` VALUES ('86', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0905', 'Churcampa', '1', '9', null);
INSERT INTO `inicio_provincia` VALUES ('87', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0906', 'Huaytara', '1', '9', null);
INSERT INTO `inicio_provincia` VALUES ('88', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '0907', 'Tayacaja', '1', '9', null);
INSERT INTO `inicio_provincia` VALUES ('89', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1001', 'Huanuco', '1', '10', null);
INSERT INTO `inicio_provincia` VALUES ('90', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1002', 'Ambo', '1', '10', null);
INSERT INTO `inicio_provincia` VALUES ('91', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1003', 'Dos de Mayo', '1', '10', null);
INSERT INTO `inicio_provincia` VALUES ('92', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1004', 'Huacaybamba', '1', '10', null);
INSERT INTO `inicio_provincia` VALUES ('93', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1005', 'Huamalies', '1', '10', null);
INSERT INTO `inicio_provincia` VALUES ('94', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1006', 'Leoncio Prado', '1', '10', null);
INSERT INTO `inicio_provincia` VALUES ('95', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1007', 'Maraqon', '1', '10', null);
INSERT INTO `inicio_provincia` VALUES ('96', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1008', 'Pachitea', '1', '10', null);
INSERT INTO `inicio_provincia` VALUES ('97', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1009', 'Puerto Inca', '1', '10', null);
INSERT INTO `inicio_provincia` VALUES ('98', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1010', 'Lauricocha', '1', '10', null);
INSERT INTO `inicio_provincia` VALUES ('99', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1011', 'Yarowilca', '1', '10', null);
INSERT INTO `inicio_provincia` VALUES ('100', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1101', 'Ica', '1', '11', null);
INSERT INTO `inicio_provincia` VALUES ('101', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1102', 'Chincha', '1', '11', null);
INSERT INTO `inicio_provincia` VALUES ('102', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1103', 'Nazca', '1', '11', null);
INSERT INTO `inicio_provincia` VALUES ('103', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1104', 'Palpa', '1', '11', null);
INSERT INTO `inicio_provincia` VALUES ('104', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1105', 'Pisco', '1', '11', null);
INSERT INTO `inicio_provincia` VALUES ('105', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1201', 'Huancayo', '1', '12', null);
INSERT INTO `inicio_provincia` VALUES ('106', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1202', 'Concepcion', '1', '12', null);
INSERT INTO `inicio_provincia` VALUES ('107', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1203', 'Chanchamayo', '1', '12', null);
INSERT INTO `inicio_provincia` VALUES ('108', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1204', 'Jauja', '1', '12', null);
INSERT INTO `inicio_provincia` VALUES ('109', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1205', 'Junin', '1', '12', null);
INSERT INTO `inicio_provincia` VALUES ('110', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1206', 'Satipo', '1', '12', null);
INSERT INTO `inicio_provincia` VALUES ('111', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1207', 'Tarma', '1', '12', null);
INSERT INTO `inicio_provincia` VALUES ('112', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1208', 'Yauli', '1', '12', null);
INSERT INTO `inicio_provincia` VALUES ('113', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1209', 'Chupaca', '1', '12', null);
INSERT INTO `inicio_provincia` VALUES ('114', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1301', 'Trujillo', '1', '13', null);
INSERT INTO `inicio_provincia` VALUES ('115', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1302', 'Ascope', '1', '13', null);
INSERT INTO `inicio_provincia` VALUES ('116', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1303', 'Bolivar', '1', '13', null);
INSERT INTO `inicio_provincia` VALUES ('117', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1304', 'Chepen', '1', '13', null);
INSERT INTO `inicio_provincia` VALUES ('118', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1305', 'Julcan', '1', '13', null);
INSERT INTO `inicio_provincia` VALUES ('119', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1306', 'Otuzco', '1', '13', null);
INSERT INTO `inicio_provincia` VALUES ('120', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1307', 'Pacasmayo', '1', '13', null);
INSERT INTO `inicio_provincia` VALUES ('121', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1308', 'Pataz', '1', '13', null);
INSERT INTO `inicio_provincia` VALUES ('122', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1309', 'Sanchez Carrion', '1', '13', null);
INSERT INTO `inicio_provincia` VALUES ('123', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1310', 'Santiago de Chuco', '1', '13', null);
INSERT INTO `inicio_provincia` VALUES ('124', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1311', 'Gran Chimu', '1', '13', null);
INSERT INTO `inicio_provincia` VALUES ('125', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1312', 'Viru', '1', '13', null);
INSERT INTO `inicio_provincia` VALUES ('126', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1401', 'Chiclayo', '1', '14', null);
INSERT INTO `inicio_provincia` VALUES ('127', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1402', 'Ferreqafe', '1', '14', null);
INSERT INTO `inicio_provincia` VALUES ('128', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1403', 'Lambayeque', '1', '14', null);
INSERT INTO `inicio_provincia` VALUES ('129', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1501', 'Lima', '1', '15', null);
INSERT INTO `inicio_provincia` VALUES ('130', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1502', 'Barranca', '1', '15', null);
INSERT INTO `inicio_provincia` VALUES ('131', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1503', 'Cajatambo', '1', '15', null);
INSERT INTO `inicio_provincia` VALUES ('132', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1504', 'Canta', '1', '15', null);
INSERT INTO `inicio_provincia` VALUES ('133', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1505', 'Caqete', '1', '15', null);
INSERT INTO `inicio_provincia` VALUES ('134', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1506', 'Huaral', '1', '15', null);
INSERT INTO `inicio_provincia` VALUES ('135', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1507', 'Huarochiri', '1', '15', null);
INSERT INTO `inicio_provincia` VALUES ('136', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1508', 'Huaura', '1', '15', null);
INSERT INTO `inicio_provincia` VALUES ('137', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1509', 'Oyon', '1', '15', null);
INSERT INTO `inicio_provincia` VALUES ('138', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1510', 'Yauyos', '1', '15', null);
INSERT INTO `inicio_provincia` VALUES ('139', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1601', 'Maynas', '1', '16', null);
INSERT INTO `inicio_provincia` VALUES ('140', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1602', 'Alto Amazonas', '1', '16', null);
INSERT INTO `inicio_provincia` VALUES ('141', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1603', 'Loreto', '1', '16', null);
INSERT INTO `inicio_provincia` VALUES ('142', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1604', 'Mariscal Ramon Castilla', '1', '16', null);
INSERT INTO `inicio_provincia` VALUES ('143', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1605', 'Requena', '1', '16', null);
INSERT INTO `inicio_provincia` VALUES ('144', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1606', 'Ucayali', '1', '16', null);
INSERT INTO `inicio_provincia` VALUES ('145', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1701', 'Tambopata', '1', '17', null);
INSERT INTO `inicio_provincia` VALUES ('146', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1702', 'Manu', '1', '17', null);
INSERT INTO `inicio_provincia` VALUES ('147', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1703', 'Tahuamanu', '1', '17', null);
INSERT INTO `inicio_provincia` VALUES ('148', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1801', 'Mariscal Nieto', '1', '18', null);
INSERT INTO `inicio_provincia` VALUES ('149', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1802', 'General Sanchez Cerro', '1', '18', null);
INSERT INTO `inicio_provincia` VALUES ('150', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1803', 'Ilo', '1', '18', null);
INSERT INTO `inicio_provincia` VALUES ('151', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1901', 'Pasco', '1', '19', null);
INSERT INTO `inicio_provincia` VALUES ('152', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1902', 'Daniel Alcides Carrion', '1', '19', null);
INSERT INTO `inicio_provincia` VALUES ('153', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '1903', 'Oxapampa', '1', '19', null);
INSERT INTO `inicio_provincia` VALUES ('154', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2001', 'Piura', '1', '20', null);
INSERT INTO `inicio_provincia` VALUES ('155', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2002', 'Ayabaca', '1', '20', null);
INSERT INTO `inicio_provincia` VALUES ('156', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2003', 'Huancabamba', '1', '20', null);
INSERT INTO `inicio_provincia` VALUES ('157', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2004', 'Morropon', '1', '20', null);
INSERT INTO `inicio_provincia` VALUES ('158', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2005', 'Paita', '1', '20', null);
INSERT INTO `inicio_provincia` VALUES ('159', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2006', 'Sullana', '1', '20', null);
INSERT INTO `inicio_provincia` VALUES ('160', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2007', 'Talara', '1', '20', null);
INSERT INTO `inicio_provincia` VALUES ('161', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2008', 'Sechura', '1', '20', null);
INSERT INTO `inicio_provincia` VALUES ('162', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2101', 'Puno', '1', '21', null);
INSERT INTO `inicio_provincia` VALUES ('163', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2102', 'Azangaro', '1', '21', null);
INSERT INTO `inicio_provincia` VALUES ('164', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2103', 'Carabaya', '1', '21', null);
INSERT INTO `inicio_provincia` VALUES ('165', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2104', 'Chucuito', '1', '21', null);
INSERT INTO `inicio_provincia` VALUES ('166', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2105', 'El Collao', '1', '21', null);
INSERT INTO `inicio_provincia` VALUES ('167', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2106', 'Huancane', '1', '21', null);
INSERT INTO `inicio_provincia` VALUES ('168', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2107', 'Lampa', '1', '21', null);
INSERT INTO `inicio_provincia` VALUES ('169', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2108', 'Melgar', '1', '21', null);
INSERT INTO `inicio_provincia` VALUES ('170', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2109', 'Moho', '1', '21', null);
INSERT INTO `inicio_provincia` VALUES ('171', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2110', 'San Antonio de Putina', '1', '21', null);
INSERT INTO `inicio_provincia` VALUES ('172', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2111', 'San Roman', '1', '21', null);
INSERT INTO `inicio_provincia` VALUES ('173', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2112', 'Sandia', '1', '21', null);
INSERT INTO `inicio_provincia` VALUES ('174', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2113', 'Yunguyo', '1', '21', null);
INSERT INTO `inicio_provincia` VALUES ('175', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2201', 'Moyobamba', '1', '22', null);
INSERT INTO `inicio_provincia` VALUES ('176', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2202', 'Bellavista', '1', '22', null);
INSERT INTO `inicio_provincia` VALUES ('177', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2203', 'El Dorado', '1', '22', null);
INSERT INTO `inicio_provincia` VALUES ('178', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2204', 'Huallaga', '1', '22', null);
INSERT INTO `inicio_provincia` VALUES ('179', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2205', 'Lamas', '1', '22', null);
INSERT INTO `inicio_provincia` VALUES ('180', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2206', 'Mariscal Caceres', '1', '22', null);
INSERT INTO `inicio_provincia` VALUES ('181', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2207', 'Picota', '1', '22', null);
INSERT INTO `inicio_provincia` VALUES ('182', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2208', 'Rioja', '1', '22', null);
INSERT INTO `inicio_provincia` VALUES ('183', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2209', 'San Martin', '1', '22', null);
INSERT INTO `inicio_provincia` VALUES ('184', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2210', 'Tocache', '1', '22', null);
INSERT INTO `inicio_provincia` VALUES ('185', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2301', 'Tacna', '1', '23', null);
INSERT INTO `inicio_provincia` VALUES ('186', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2302', 'Candarave', '1', '23', null);
INSERT INTO `inicio_provincia` VALUES ('187', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2303', 'Jorge Basadre', '1', '23', null);
INSERT INTO `inicio_provincia` VALUES ('188', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2304', 'Tarata', '1', '23', null);
INSERT INTO `inicio_provincia` VALUES ('189', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2401', 'Tumbes', '1', '24', null);
INSERT INTO `inicio_provincia` VALUES ('190', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2402', 'Contralmirante Villar', '1', '24', null);
INSERT INTO `inicio_provincia` VALUES ('191', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2403', 'Zarumilla', '1', '24', null);
INSERT INTO `inicio_provincia` VALUES ('192', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2501', 'Coronel Portillo', '1', '25', null);
INSERT INTO `inicio_provincia` VALUES ('193', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2502', 'Atalaya', '1', '25', null);
INSERT INTO `inicio_provincia` VALUES ('194', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2503', 'Padre Abad', '1', '25', null);
INSERT INTO `inicio_provincia` VALUES ('195', '2017-06-16 12:14:12.000000', '2017-06-16 12:14:12.000000', '2504', 'Purus', '1', '25', null);

-- ----------------------------
-- Table structure for inicio_tipoorganizacion
-- ----------------------------
DROP TABLE IF EXISTS `inicio_tipoorganizacion`;
CREATE TABLE `inicio_tipoorganizacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `nombre` varchar(75) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `inicio_tipoorganizacion_creador_id_9196bc3c_fk_auth_user_id` (`creador_id`),
  KEY `inicio_tipoorganizacion_editor_id_e4b3e6fa_fk_auth_user_id` (`editor_id`),
  CONSTRAINT `inicio_tipoorganizacion_creador_id_9196bc3c_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `inicio_tipoorganizacion_editor_id_e4b3e6fa_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of inicio_tipoorganizacion
-- ----------------------------
INSERT INTO `inicio_tipoorganizacion` VALUES ('1', '2017-06-21 18:34:20.000000', '2017-06-21 18:34:20.000000', 'Tipo Org 01', '1', '1', null);
INSERT INTO `inicio_tipoorganizacion` VALUES ('2', '2017-07-01 04:28:09.000000', '2017-07-15 12:47:02.469000', 'Tipo Org 02', '1', '2', '2');
INSERT INTO `inicio_tipoorganizacion` VALUES ('3', '2017-07-22 23:10:46.196676', '2017-07-22 23:11:09.999731', 'Comunidad de Manchay', '1', '1', '1');
INSERT INTO `inicio_tipoorganizacion` VALUES ('4', '2017-07-22 23:10:56.590074', '2017-07-22 23:10:56.590112', 'Junta comunal', '1', '1', null);
INSERT INTO `inicio_tipoorganizacion` VALUES ('5', '2017-07-24 22:32:15.857638', '2017-07-24 22:32:15.857674', 'Publica', '1', '1', null);
INSERT INTO `inicio_tipoorganizacion` VALUES ('6', '2017-07-24 22:32:20.741546', '2017-07-24 22:32:25.643363', 'Privada P', '1', '1', '1');

-- ----------------------------
-- Table structure for inicio_unidadmedida
-- ----------------------------
DROP TABLE IF EXISTS `inicio_unidadmedida`;
CREATE TABLE `inicio_unidadmedida` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `abreviatura` varchar(10) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  UNIQUE KEY `abreviatura` (`abreviatura`),
  KEY `inicio_unidadmedida_creador_id_14fb0abb_fk_auth_user_id` (`creador_id`),
  KEY `inicio_unidadmedida_editor_id_903602e6_fk_auth_user_id` (`editor_id`),
  CONSTRAINT `inicio_unidadmedida_creador_id_14fb0abb_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `inicio_unidadmedida_editor_id_903602e6_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of inicio_unidadmedida
-- ----------------------------
INSERT INTO `inicio_unidadmedida` VALUES ('1', '2017-06-16 08:25:28.000000', '2017-06-16 08:27:54.000000', 'Kilogramo', 'Kg', '3', '2');
INSERT INTO `inicio_unidadmedida` VALUES ('2', '2017-06-20 19:42:16.000000', '2017-06-20 19:47:49.000000', 'Metro', 'm', '2', '2');
INSERT INTO `inicio_unidadmedida` VALUES ('4', '2017-06-21 12:14:09.000000', '2017-06-21 12:14:14.000000', 'Metro Cuadrado', 'm2', '2', '2');
INSERT INTO `inicio_unidadmedida` VALUES ('5', '2017-07-24 20:32:38.253282', '2017-07-24 20:32:38.253352', 'Organización', 'ORG', '1', null);
INSERT INTO `inicio_unidadmedida` VALUES ('6', '2017-07-24 22:23:32.387304', '2017-07-24 22:23:40.606586', 'Gramo', 'gr', '1', '1');

-- ----------------------------
-- Table structure for planificacion_accionestrategica
-- ----------------------------
DROP TABLE IF EXISTS `planificacion_accionestrategica`;
CREATE TABLE `planificacion_accionestrategica` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  `nombre` longtext NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `objetivoespecifico_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo` (`codigo`),
  KEY `planificacion_accion_creador_id_43c56982_fk_auth_user` (`creador_id`),
  KEY `planificacion_accion_editor_id_241bd5b9_fk_auth_user` (`editor_id`),
  KEY `planificacion_accion_objetivoespecifico_i_99b90857_fk_planifica` (`objetivoespecifico_id`),
  CONSTRAINT `planificacion_accion_creador_id_43c56982_fk_auth_user` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_accion_editor_id_241bd5b9_fk_auth_user` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_accion_objetivoespecifico_i_99b90857_fk_planifica` FOREIGN KEY (`objetivoespecifico_id`) REFERENCES `planificacion_objetivoespecifico` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planificacion_accionestrategica
-- ----------------------------
INSERT INTO `planificacion_accionestrategica` VALUES ('1', '2017-07-21 17:43:24.237000', '2017-07-24 03:08:03.441101', '2.1.1.1', 'Impulsar la gestión empresarial y el desarrollo de capacidades en los sectores productivos', '1', '1', '1');
INSERT INTO `planificacion_accionestrategica` VALUES ('3', '2017-07-21 20:08:50.994000', '2017-07-24 03:09:51.049338', '2.1.1.2', 'Promover inversiones departamentales para el desarrollo productivo en concordancia con la propuesta de ZEE y la estrategia departamental de la biodiversidad', '1', '1', '1');
INSERT INTO `planificacion_accionestrategica` VALUES ('4', '2017-07-24 03:11:08.113593', '2017-07-24 03:11:08.113637', '2.1.1.3', 'Fomentar el desarrollo del Turismo Sostenible', '1', null, '1');
INSERT INTO `planificacion_accionestrategica` VALUES ('5', '2017-07-24 03:12:53.427743', '2017-07-24 03:12:53.427789', '2.1.1.4', 'Fomentar el crecimiento y formalización de la actividad minera artesanal y alianzas estratégicas para el desarrollo departamental', '1', null, '1');
INSERT INTO `planificacion_accionestrategica` VALUES ('6', '2017-07-24 03:14:49.221726', '2017-07-24 03:14:49.221772', '2.1.1.5', 'Orientar inversión público-privada para la dotación de infraestructura de servicios y comercialización en las zonas con ppotencial productivo y con énfasis en la zona rural', '1', null, '1');
INSERT INTO `planificacion_accionestrategica` VALUES ('7', '2017-07-24 03:16:19.972537', '2017-07-24 03:16:19.972579', '2.1.1.6', 'Fomentar la transformación de la producción para dar valor agregado y generar empleo productivo', '1', null, '1');
INSERT INTO `planificacion_accionestrategica` VALUES ('8', '2017-07-24 03:21:12.718330', '2017-07-24 03:21:12.718369', '2.1.2.1', 'Promover la inversión en tecnología e infraestructura de riego', '1', null, '2');
INSERT INTO `planificacion_accionestrategica` VALUES ('9', '2017-07-24 03:22:18.625584', '2017-07-24 03:22:18.625665', '2.1.2.2', 'Fomentar la inversión de la infraestructura eléctrica', '1', null, '2');
INSERT INTO `planificacion_accionestrategica` VALUES ('10', '2017-07-24 03:23:12.472748', '2017-07-24 03:23:12.472787', '2.1.2.3', 'Promover inversiones en infraestructura de transporte y telecomunicaciones', '1', null, '2');
INSERT INTO `planificacion_accionestrategica` VALUES ('11', '2017-07-24 03:27:53.413926', '2017-07-24 03:27:53.414003', '2.1.3.1', 'Promover la formalización y fortalecimiento de capacidades para el desarrollo de MYPES rurales y organizaciones de productores', '1', null, '3');
INSERT INTO `planificacion_accionestrategica` VALUES ('12', '2017-07-24 03:28:46.563467', '2017-07-24 03:28:46.563529', '2.1.3.2', 'Orientar inversiones en la reducción de vulnerabilidad de la población rural, fortaleciendo sus medios de vida', '1', null, '3');
INSERT INTO `planificacion_accionestrategica` VALUES ('13', '2017-07-24 03:30:00.636673', '2017-07-24 03:30:00.636717', '2.1.3.3', 'Fomentar la productividad de la micro parcela agrícola y la acumulación de activos productivos en los ámbitos rurales', '1', null, '3');
INSERT INTO `planificacion_accionestrategica` VALUES ('14', '2017-07-24 03:30:36.155464', '2017-07-24 03:30:36.155524', '2.1.3.4', 'Fomentar tecnologías sostenibles y viables para el desarrollo agropecuario', '1', null, '3');
INSERT INTO `planificacion_accionestrategica` VALUES ('15', '2017-07-24 03:31:47.697042', '2017-07-24 03:31:47.697086', '2.1.3.5', 'Impulsar fondos de apoyo financiero para el desarrollo de la economía rural', '1', null, '3');

-- ----------------------------
-- Table structure for planificacion_accionobra
-- ----------------------------
DROP TABLE IF EXISTS `planificacion_accionobra`;
CREATE TABLE `planificacion_accionobra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `km` decimal(11,2) NOT NULL,
  `volumen` decimal(11,2) NOT NULL,
  `conduce` decimal(11,2) NOT NULL,
  `presupuesto` decimal(11,2) NOT NULL,
  `ha` decimal(11,2) NOT NULL,
  `aexpop_id` int(11) NOT NULL,
  `cadenaproductiva_id` int(11) NOT NULL,
  `caserio_id` int(11) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `planificacion_accion_aexpop_id_05b23874_fk_planifica` (`aexpop_id`),
  KEY `planificacion_accion_cadenaproductiva_id_f614e2a6_fk_planifica` (`cadenaproductiva_id`),
  KEY `planificacion_accion_caserio_id_da2126d9_fk_inicio_ca` (`caserio_id`),
  KEY `planificacion_accionobra_creador_id_b0047ff6_fk_auth_user_id` (`creador_id`),
  KEY `planificacion_accionobra_editor_id_7f37a75e_fk_auth_user_id` (`editor_id`),
  CONSTRAINT `planificacion_accion_aexpop_id_05b23874_fk_planifica` FOREIGN KEY (`aexpop_id`) REFERENCES `planificacion_aexpop` (`id`),
  CONSTRAINT `planificacion_accion_cadenaproductiva_id_f614e2a6_fk_planifica` FOREIGN KEY (`cadenaproductiva_id`) REFERENCES `planificacion_cadenaproductiva` (`id`),
  CONSTRAINT `planificacion_accion_caserio_id_da2126d9_fk_inicio_ca` FOREIGN KEY (`caserio_id`) REFERENCES `inicio_caserio` (`id`),
  CONSTRAINT `planificacion_accionobra_creador_id_b0047ff6_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_accionobra_editor_id_7f37a75e_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planificacion_accionobra
-- ----------------------------
INSERT INTO `planificacion_accionobra` VALUES ('1', '2017-07-25 12:57:52.431639', '2017-07-25 12:57:52.431688', '300.00', '500.00', '800.00', '15700.00', '30.00', '5', '4', '1', '1', null);

-- ----------------------------
-- Table structure for planificacion_actividad
-- ----------------------------
DROP TABLE IF EXISTS `planificacion_actividad`;
CREATE TABLE `planificacion_actividad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `aexpop_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `cadenaproductiva_id` int(11) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `divisionfuncional_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `funcion_id` int(11) NOT NULL,
  `grupofuncional_id` int(11) NOT NULL,
  `actividadcatalogo_id` int(11) NOT NULL,
  `anio` int(11) NOT NULL,
  `indicador_id` int(11) NOT NULL,
  `unidadmedida_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `planificacion_activi_aexpop_id_a3a5778c_fk_planifica` (`aexpop_id`),
  KEY `planificacion_activi_area_id_6aa2f8bb_fk_administr` (`area_id`),
  KEY `planificacion_activi_cadenaproductiva_id_edb15e77_fk_planifica` (`cadenaproductiva_id`),
  KEY `planificacion_actividad_creador_id_84151d50_fk_auth_user_id` (`creador_id`),
  KEY `planificacion_activi_divisionfuncional_id_856c27d4_fk_planifica` (`divisionfuncional_id`),
  KEY `planificacion_actividad_editor_id_b26f215b_fk_auth_user_id` (`editor_id`),
  KEY `planificacion_activi_funcion_id_8bb62b27_fk_planifica` (`funcion_id`),
  KEY `planificacion_activi_grupofuncional_id_e9a74a7d_fk_planifica` (`grupofuncional_id`),
  KEY `planificacion_activi_indicador_id_212de5ab_fk_planifica` (`indicador_id`),
  KEY `planificacion_activi_unidadmedida_id_20f0a69a_fk_inicio_un` (`unidadmedida_id`),
  CONSTRAINT `planificacion_activi_aexpop_id_a3a5778c_fk_planifica` FOREIGN KEY (`aexpop_id`) REFERENCES `planificacion_aexpop` (`id`),
  CONSTRAINT `planificacion_activi_area_id_6aa2f8bb_fk_administr` FOREIGN KEY (`area_id`) REFERENCES `administracion_area` (`id`),
  CONSTRAINT `planificacion_activi_cadenaproductiva_id_edb15e77_fk_planifica` FOREIGN KEY (`cadenaproductiva_id`) REFERENCES `planificacion_cadenaproductiva` (`id`),
  CONSTRAINT `planificacion_activi_divisionfuncional_id_856c27d4_fk_planifica` FOREIGN KEY (`divisionfuncional_id`) REFERENCES `planificacion_divisionfuncional` (`id`),
  CONSTRAINT `planificacion_activi_funcion_id_8bb62b27_fk_planifica` FOREIGN KEY (`funcion_id`) REFERENCES `planificacion_funcion` (`id`),
  CONSTRAINT `planificacion_activi_grupofuncional_id_e9a74a7d_fk_planifica` FOREIGN KEY (`grupofuncional_id`) REFERENCES `planificacion_grupofuncional` (`id`),
  CONSTRAINT `planificacion_activi_indicador_id_212de5ab_fk_planifica` FOREIGN KEY (`indicador_id`) REFERENCES `planificacion_indicador` (`id`),
  CONSTRAINT `planificacion_activi_unidadmedida_id_20f0a69a_fk_inicio_un` FOREIGN KEY (`unidadmedida_id`) REFERENCES `inicio_unidadmedida` (`id`),
  CONSTRAINT `planificacion_actividad_creador_id_84151d50_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_actividad_editor_id_b26f215b_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planificacion_actividad
-- ----------------------------
INSERT INTO `planificacion_actividad` VALUES ('1', '2017-07-24 22:54:31.132181', '2017-07-24 22:54:31.132226', '1', '1', '4', '1', '2', null, '3', '1', '1', '2017', '2', '1');
INSERT INTO `planificacion_actividad` VALUES ('2', '2017-07-26 21:36:36.843956', '2017-07-26 21:36:36.844227', '5', '6', '4', '1', '2', null, '2', '1', '2', '2017', '1', '2');
INSERT INTO `planificacion_actividad` VALUES ('3', '2017-07-26 22:53:39.065550', '2017-07-26 22:53:39.065603', '8', '2', '5', '1', '3', null, '3', '1', '3', '2017', '2', '5');
INSERT INTO `planificacion_actividad` VALUES ('4', '2017-07-30 15:51:42.278000', '2017-07-30 16:48:38.878000', '4', '7', '4', '1', '2', '1', '3', '1', '1', '2017', '1', '6');

-- ----------------------------
-- Table structure for planificacion_actividadcatalogo
-- ----------------------------
DROP TABLE IF EXISTS `planificacion_actividadcatalogo`;
CREATE TABLE `planificacion_actividadcatalogo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `codigo` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  UNIQUE KEY `planificacion_actividadcatalogo_codigo_c6e027cd_uniq` (`codigo`),
  KEY `planificacion_activi_creador_id_e9116d3d_fk_auth_user` (`creador_id`),
  KEY `planificacion_activi_editor_id_0e07f78f_fk_auth_user` (`editor_id`),
  CONSTRAINT `planificacion_activi_creador_id_e9116d3d_fk_auth_user` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_activi_editor_id_0e07f78f_fk_auth_user` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planificacion_actividadcatalogo
-- ----------------------------
INSERT INTO `planificacion_actividadcatalogo` VALUES ('1', '2017-07-24 22:54:31.132181', '2017-07-24 22:54:31.132226', 'Actividad 01', '1', null, 'A01');
INSERT INTO `planificacion_actividadcatalogo` VALUES ('2', '2017-07-26 21:36:36.843956', '2017-07-26 21:36:36.844227', 'Actividad 02', '1', null, 'A02');
INSERT INTO `planificacion_actividadcatalogo` VALUES ('3', '2017-07-26 22:53:39.065550', '2017-07-26 22:53:39.065603', 'Actividad 03', '1', null, 'A03');

-- ----------------------------
-- Table structure for planificacion_aexpop
-- ----------------------------
DROP TABLE IF EXISTS `planificacion_aexpop`;
CREATE TABLE `planificacion_aexpop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `accionestrategica_id` int(11) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `productoproyecto_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `planificacion_aexpop_accionestrategica_id_df9b822d_fk_planifica` (`accionestrategica_id`),
  KEY `planificacion_aexpop_creador_id_1a9d3120_fk_auth_user_id` (`creador_id`),
  KEY `planificacion_aexpop_editor_id_4535dab5_fk_auth_user_id` (`editor_id`),
  KEY `planificacion_aexpop_productoproyecto_id_f629d357_fk_planifica` (`productoproyecto_id`),
  CONSTRAINT `planificacion_aexpop_accionestrategica_id_df9b822d_fk_planifica` FOREIGN KEY (`accionestrategica_id`) REFERENCES `planificacion_accionestrategica` (`id`),
  CONSTRAINT `planificacion_aexpop_creador_id_1a9d3120_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_aexpop_editor_id_4535dab5_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_aexpop_productoproyecto_id_f629d357_fk_planifica` FOREIGN KEY (`productoproyecto_id`) REFERENCES `planificacion_productoproyecto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planificacion_aexpop
-- ----------------------------
INSERT INTO `planificacion_aexpop` VALUES ('1', '2017-07-24 20:36:45.100803', '2017-07-24 20:36:45.100842', '1', '1', null, '1');
INSERT INTO `planificacion_aexpop` VALUES ('2', '2017-07-24 22:21:42.292261', '2017-07-24 22:21:42.292298', '12', '1', null, '1');
INSERT INTO `planificacion_aexpop` VALUES ('3', '2017-07-24 22:21:52.566509', '2017-07-24 22:21:52.566546', '4', '1', null, '1');
INSERT INTO `planificacion_aexpop` VALUES ('4', '2017-07-24 22:22:03.634797', '2017-07-24 22:22:03.634845', '6', '1', null, '1');
INSERT INTO `planificacion_aexpop` VALUES ('5', '2017-07-25 12:56:52.439800', '2017-07-25 12:56:52.439851', '5', '1', null, '4');
INSERT INTO `planificacion_aexpop` VALUES ('6', '2017-07-25 12:56:57.864725', '2017-07-25 12:56:57.864776', '10', '1', null, '4');
INSERT INTO `planificacion_aexpop` VALUES ('7', '2017-07-25 16:29:13.412779', '2017-07-25 16:29:13.412828', '15', '1', null, '1');
INSERT INTO `planificacion_aexpop` VALUES ('8', '2017-07-26 22:51:33.157936', '2017-07-26 22:51:33.157994', '4', '1', null, '5');
INSERT INTO `planificacion_aexpop` VALUES ('9', '2017-07-26 22:51:47.391367', '2017-07-26 22:51:47.391424', '9', '1', null, '5');

-- ----------------------------
-- Table structure for planificacion_cadenaproductiva
-- ----------------------------
DROP TABLE IF EXISTS `planificacion_cadenaproductiva`;
CREATE TABLE `planificacion_cadenaproductiva` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `tipo` varchar(2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `planificacion_cadena_creador_id_575be776_fk_auth_user` (`creador_id`),
  KEY `planificacion_cadena_editor_id_293c1ee9_fk_auth_user` (`editor_id`),
  CONSTRAINT `planificacion_cadena_creador_id_575be776_fk_auth_user` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_cadena_editor_id_293c1ee9_fk_auth_user` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planificacion_cadenaproductiva
-- ----------------------------
INSERT INTO `planificacion_cadenaproductiva` VALUES ('4', '2017-07-24 22:52:03.873137', '2017-07-24 22:52:03.873187', 'Crianza de cuyes', '1', '1', null, 'CR');
INSERT INTO `planificacion_cadenaproductiva` VALUES ('5', '2017-07-24 22:52:14.215539', '2017-07-24 22:52:14.215577', 'Crianza de vacas', '1', '1', null, 'CR');
INSERT INTO `planificacion_cadenaproductiva` VALUES ('6', '2017-07-28 13:11:30.327988', '2017-07-28 13:11:30.328061', 'Cadena productiva de la palta', '1', '1', null, 'CU');
INSERT INTO `planificacion_cadenaproductiva` VALUES ('7', '2017-07-28 13:11:53.226785', '2017-07-28 13:11:53.226840', 'Cadena productiva de tara', '1', '1', null, 'CU');
INSERT INTO `planificacion_cadenaproductiva` VALUES ('8', '2017-07-28 13:12:42.267318', '2017-07-28 13:12:42.267364', 'Cadena productiva de menestras', '1', '1', null, 'CU');
INSERT INTO `planificacion_cadenaproductiva` VALUES ('9', '2017-07-28 13:12:58.177996', '2017-07-28 13:12:58.178072', 'Cadena productiva de maíz morada', '1', '1', null, 'CU');
INSERT INTO `planificacion_cadenaproductiva` VALUES ('10', '2017-07-28 13:13:13.018299', '2017-07-28 13:13:13.018346', 'Cadena productiva de papa', '1', '1', null, 'CU');
INSERT INTO `planificacion_cadenaproductiva` VALUES ('11', '2017-07-28 13:13:36.116192', '2017-07-28 13:13:36.116243', 'Cadena productiva de quinua', '1', '1', null, 'CU');
INSERT INTO `planificacion_cadenaproductiva` VALUES ('12', '2017-07-28 13:13:47.963827', '2017-07-28 13:13:47.963877', 'Cadena productiva de maíz amiláceo', '1', '1', null, 'CU');
INSERT INTO `planificacion_cadenaproductiva` VALUES ('13', '2017-07-28 13:14:04.732133', '2017-07-28 13:14:04.732209', 'Cadena productiva de café', '1', '1', null, 'CU');
INSERT INTO `planificacion_cadenaproductiva` VALUES ('14', '2017-07-28 13:14:24.096843', '2017-07-28 13:14:24.096908', 'Cadena productiva de caña de azúcar', '1', '1', null, 'CU');
INSERT INTO `planificacion_cadenaproductiva` VALUES ('15', '2017-07-28 13:14:37.180978', '2017-07-28 13:14:37.181026', 'Cadena productiva de aguaymanto', '1', '1', null, 'CU');
INSERT INTO `planificacion_cadenaproductiva` VALUES ('16', '2017-07-28 13:15:47.845516', '2017-07-28 13:15:47.845566', 'Cadena productiva de piña', '1', '1', null, 'CU');
INSERT INTO `planificacion_cadenaproductiva` VALUES ('17', '2017-07-28 13:16:01.344768', '2017-07-28 13:16:01.344820', 'Cadena productiva de chirimoya', '1', '1', null, 'CU');
INSERT INTO `planificacion_cadenaproductiva` VALUES ('18', '2017-07-28 13:16:13.054087', '2017-07-28 13:16:13.054138', 'Cadena productiva de uva', '1', '1', null, 'CU');
INSERT INTO `planificacion_cadenaproductiva` VALUES ('19', '2017-07-28 13:16:24.730840', '2017-07-28 13:16:24.730888', 'Cadena productiva de arroz', '1', '1', null, 'CU');
INSERT INTO `planificacion_cadenaproductiva` VALUES ('20', '2017-07-28 13:16:38.175112', '2017-07-28 13:16:38.175161', 'Cadena productiva de banano', '1', '1', null, 'CU');
INSERT INTO `planificacion_cadenaproductiva` VALUES ('21', '2017-07-28 13:16:47.756711', '2017-07-28 13:16:47.756796', 'Cadena productiva de cacao', '1', '1', null, 'CU');
INSERT INTO `planificacion_cadenaproductiva` VALUES ('22', '2017-07-28 13:17:01.445761', '2017-07-28 13:17:48.849713', 'Cadena productiva de Cuyes', '1', '1', '1', 'CR');
INSERT INTO `planificacion_cadenaproductiva` VALUES ('23', '2017-07-28 13:17:23.571935', '2017-07-28 13:17:23.572016', 'Cadena productiva de Vacunos', '1', '1', null, 'CR');
INSERT INTO `planificacion_cadenaproductiva` VALUES ('24', '2017-07-28 13:17:39.882786', '2017-07-28 13:17:39.882880', 'Cadena productiva de Camélidos', '1', '1', null, 'CR');

-- ----------------------------
-- Table structure for planificacion_divisionfuncional
-- ----------------------------
DROP TABLE IF EXISTS `planificacion_divisionfuncional`;
CREATE TABLE `planificacion_divisionfuncional` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `planificacion_divisi_creador_id_f1cfab30_fk_auth_user` (`creador_id`),
  KEY `planificacion_divisi_editor_id_23f6567a_fk_auth_user` (`editor_id`),
  CONSTRAINT `planificacion_divisi_creador_id_f1cfab30_fk_auth_user` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_divisi_editor_id_23f6567a_fk_auth_user` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planificacion_divisionfuncional
-- ----------------------------
INSERT INTO `planificacion_divisionfuncional` VALUES ('2', '2017-06-22 15:47:32.000000', '2017-07-02 19:03:33.000000', 'División Funcional 1', '1', '2');
INSERT INTO `planificacion_divisionfuncional` VALUES ('3', '2017-07-02 19:03:38.000000', '2017-07-02 19:03:38.000000', 'División Funcional 2', '2', null);

-- ----------------------------
-- Table structure for planificacion_ejeecon
-- ----------------------------
DROP TABLE IF EXISTS `planificacion_ejeecon`;
CREATE TABLE `planificacion_ejeecon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `nombre` varchar(75) NOT NULL,
  `descripcion` longtext NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `planificacion_ejeecon_creador_id_cdacc818_fk_auth_user_id` (`creador_id`),
  KEY `planificacion_ejeecon_editor_id_0101d32b_fk_auth_user_id` (`editor_id`),
  CONSTRAINT `planificacion_ejeecon_creador_id_cdacc818_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_ejeecon_editor_id_0101d32b_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planificacion_ejeecon
-- ----------------------------
INSERT INTO `planificacion_ejeecon` VALUES ('1', '2017-06-22 16:36:28.000000', '2017-07-24 02:49:25.289582', '2. Eje Económico', '.', '1', '1', '1');
INSERT INTO `planificacion_ejeecon` VALUES ('4', '2017-07-28 14:05:26.596593', '2017-07-28 14:05:26.596666', 'Eje Económico', 'qw', '1', '1', null);

-- ----------------------------
-- Table structure for planificacion_funcion
-- ----------------------------
DROP TABLE IF EXISTS `planificacion_funcion`;
CREATE TABLE `planificacion_funcion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `planificacion_funcion_creador_id_a249ba73_fk_auth_user_id` (`creador_id`),
  KEY `planificacion_funcion_editor_id_7011c074_fk_auth_user_id` (`editor_id`),
  CONSTRAINT `planificacion_funcion_creador_id_a249ba73_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_funcion_editor_id_7011c074_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planificacion_funcion
-- ----------------------------
INSERT INTO `planificacion_funcion` VALUES ('2', '2017-06-22 16:32:37.000000', '2017-07-02 19:03:06.000000', 'Función 02', '1', '2');
INSERT INTO `planificacion_funcion` VALUES ('3', '2017-06-22 16:32:44.000000', '2017-07-02 19:02:57.000000', 'Función 01', '1', '2');

-- ----------------------------
-- Table structure for planificacion_grupofuncional
-- ----------------------------
DROP TABLE IF EXISTS `planificacion_grupofuncional`;
CREATE TABLE `planificacion_grupofuncional` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `planificacion_grupofuncional_creador_id_212d652c_fk_auth_user_id` (`creador_id`),
  KEY `planificacion_grupofuncional_editor_id_af3d1025_fk_auth_user_id` (`editor_id`),
  CONSTRAINT `planificacion_grupofuncional_creador_id_212d652c_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_grupofuncional_editor_id_af3d1025_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planificacion_grupofuncional
-- ----------------------------
INSERT INTO `planificacion_grupofuncional` VALUES ('1', '2017-06-22 08:18:51.000000', '2017-06-22 08:18:51.000000', 'Grupo Funcional 1', '2', null);
INSERT INTO `planificacion_grupofuncional` VALUES ('2', '2017-06-22 16:33:26.000000', '2017-07-02 19:02:36.000000', 'Grupo Funcional 2', '1', '2');

-- ----------------------------
-- Table structure for planificacion_indicador
-- ----------------------------
DROP TABLE IF EXISTS `planificacion_indicador`;
CREATE TABLE `planificacion_indicador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `planificacion_indicador_creador_id_6ba2863d_fk_auth_user_id` (`creador_id`),
  KEY `planificacion_indicador_editor_id_9f472a5e_fk_auth_user_id` (`editor_id`),
  CONSTRAINT `planificacion_indicador_creador_id_6ba2863d_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_indicador_editor_id_9f472a5e_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planificacion_indicador
-- ----------------------------
INSERT INTO `planificacion_indicador` VALUES ('1', '2017-06-22 08:19:03.000000', '2017-06-22 08:19:03.000000', 'Indicador 1', '2', null);
INSERT INTO `planificacion_indicador` VALUES ('2', '2017-07-02 15:44:40.000000', '2017-07-02 15:44:40.000000', 'Indicador 2', '1', null);
INSERT INTO `planificacion_indicador` VALUES ('3', '2017-07-24 22:23:53.537306', '2017-07-24 22:24:01.416167', 'Indicador 03', '1', '1');

-- ----------------------------
-- Table structure for planificacion_metaff
-- ----------------------------
DROP TABLE IF EXISTS `planificacion_metaff`;
CREATE TABLE `planificacion_metaff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `actividad_id` int(11) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `mes_id` int(11) NOT NULL,
  `metafinanciera_ejecutada` decimal(11,2) DEFAULT NULL,
  `metafinanciera_programada` decimal(11,2) DEFAULT NULL,
  `metafisica_ejecutada` int(11) DEFAULT NULL,
  `metafisica_programada` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `planificacion_metaff_actividad_id_18d1f6ff_fk_planifica` (`actividad_id`),
  KEY `planificacion_metaff_creador_id_d1384d94_fk_auth_user_id` (`creador_id`),
  KEY `planificacion_metaff_editor_id_1947cd76_fk_auth_user_id` (`editor_id`),
  KEY `planificacion_metaff_mes_id_0f1cdc48_fk_inicio_mes_id` (`mes_id`),
  CONSTRAINT `planificacion_metaff_actividad_id_18d1f6ff_fk_planifica` FOREIGN KEY (`actividad_id`) REFERENCES `planificacion_actividad` (`id`),
  CONSTRAINT `planificacion_metaff_creador_id_d1384d94_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_metaff_editor_id_1947cd76_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_metaff_mes_id_0f1cdc48_fk_inicio_mes_id` FOREIGN KEY (`mes_id`) REFERENCES `inicio_mes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planificacion_metaff
-- ----------------------------
INSERT INTO `planificacion_metaff` VALUES ('1', '2017-07-02 15:43:56.000000', '2017-07-02 15:43:56.000000', '1', '1', null, '1', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('2', '2017-07-02 15:44:54.000000', '2017-07-02 15:45:43.000000', '1', '1', '2', '2', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('3', '2017-07-24 22:55:11.261938', '2017-07-24 22:55:11.261977', '1', '1', null, '3', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('4', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '1', '1', null, '4', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('5', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '1', '1', null, '5', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('6', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '1', '1', null, '6', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('7', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '1', '1', null, '7', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('8', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '1', '1', null, '8', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('9', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '1', '1', null, '9', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('10', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '1', '1', null, '10', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('11', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '1', '1', null, '11', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('12', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '1', '1', null, '12', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('13', '2017-07-02 15:43:56.000000', '2017-07-02 15:43:56.000000', '2', '1', null, '1', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('14', '2017-07-02 15:44:54.000000', '2017-07-02 15:45:43.000000', '2', '1', '2', '2', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('15', '2017-07-24 22:55:11.261938', '2017-07-24 22:55:11.261977', '2', '1', null, '3', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('16', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '2', '1', null, '4', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('17', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '2', '1', null, '5', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('18', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '2', '1', null, '6', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('19', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '2', '1', null, '7', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('20', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '2', '1', null, '8', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('21', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '2', '1', null, '9', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('22', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '2', '1', null, '10', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('23', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '2', '1', null, '11', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('24', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '2', '1', null, '12', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('25', '2017-07-02 15:43:56.000000', '2017-07-02 15:43:56.000000', '3', '1', null, '1', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('26', '2017-07-02 15:44:54.000000', '2017-07-02 15:45:43.000000', '3', '1', '2', '2', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('27', '2017-07-24 22:55:11.261938', '2017-07-24 22:55:11.261977', '3', '1', null, '3', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('28', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '3', '1', null, '4', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('29', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '3', '1', null, '5', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('30', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '3', '1', null, '6', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('31', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '3', '1', null, '7', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('32', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '3', '1', null, '8', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('33', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '3', '1', null, '9', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('34', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '3', '1', null, '10', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('35', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '3', '1', null, '11', null, null, null, null);
INSERT INTO `planificacion_metaff` VALUES ('36', '2017-07-24 22:55:37.274136', '2017-07-24 22:55:37.274204', '3', '1', null, '12', null, null, null, null);

-- ----------------------------
-- Table structure for planificacion_objetivoespecifico
-- ----------------------------
DROP TABLE IF EXISTS `planificacion_objetivoespecifico`;
CREATE TABLE `planificacion_objetivoespecifico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `descripcion` longtext NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `objetivopdrc_id` int(11) NOT NULL,
  `anio` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo` (`codigo`),
  KEY `planificacion_objeti_creador_id_dc65d108_fk_auth_user` (`creador_id`),
  KEY `planificacion_objeti_editor_id_2ddd919d_fk_auth_user` (`editor_id`),
  KEY `planificacion_objeti_objetivopdrc_id_295c577f_fk_planifica` (`objetivopdrc_id`),
  CONSTRAINT `planificacion_objeti_creador_id_dc65d108_fk_auth_user` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_objeti_editor_id_2ddd919d_fk_auth_user` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_objeti_objetivopdrc_id_295c577f_fk_planifica` FOREIGN KEY (`objetivopdrc_id`) REFERENCES `planificacion_objetivopdrc` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planificacion_objetivoespecifico
-- ----------------------------
INSERT INTO `planificacion_objetivoespecifico` VALUES ('1', '2017-07-21 15:58:39.721000', '2017-07-24 02:50:56.213305', '2.1.1', 'Desarrollo competitivo de cadenas de valor en la actividad agropecuaria, turismo y mineria', '1', '1', null, '2', '2017');
INSERT INTO `planificacion_objetivoespecifico` VALUES ('2', '2017-07-24 02:52:09.634945', '2017-07-24 02:52:09.634990', '2.1.2', 'Desarrollo de la conectividad territorial e infraestructura productiva', '1', '1', null, '2', '2017');
INSERT INTO `planificacion_objetivoespecifico` VALUES ('3', '2017-07-24 02:53:34.556873', '2017-07-24 02:53:34.556920', '2.1.3', 'Desarrollo de ámbitos rurales con poblaciones pobres', '1', '1', null, '2', '2017');

-- ----------------------------
-- Table structure for planificacion_objetivopdrc
-- ----------------------------
DROP TABLE IF EXISTS `planificacion_objetivopdrc`;
CREATE TABLE `planificacion_objetivopdrc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `descripcion` longtext NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `pdrc_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo` (`codigo`),
  KEY `planificacion_objetivopdrc_creador_id_8a66e69a_fk_auth_user_id` (`creador_id`),
  KEY `planificacion_objetivopdrc_editor_id_7f2dc8d6_fk_auth_user_id` (`editor_id`),
  KEY `planificacion_objeti_pdrc_id_6ca2dd40_fk_planifica` (`pdrc_id`),
  CONSTRAINT `planificacion_objeti_pdrc_id_6ca2dd40_fk_planifica` FOREIGN KEY (`pdrc_id`) REFERENCES `planificacion_pdrc` (`id`),
  CONSTRAINT `planificacion_objetivopdrc_creador_id_8a66e69a_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_objetivopdrc_editor_id_7f2dc8d6_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planificacion_objetivopdrc
-- ----------------------------
INSERT INTO `planificacion_objetivopdrc` VALUES ('2', '2017-06-27 11:16:49.000000', '2017-07-24 02:49:07.940468', '2.1', 'COMPETITIVIDAD TERRITORIAL SOSTENIBLE CON GENERACIÓN DE RIQUEZA Y EMPLEO DIGNO UTILIZANDO TECNOLOGÍAS ADECUADAS', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for planificacion_pdrc
-- ----------------------------
DROP TABLE IF EXISTS `planificacion_pdrc`;
CREATE TABLE `planificacion_pdrc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `descripcion` longtext NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `ejeecon_id` int(11) NOT NULL,
  `periodo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo` (`codigo`),
  KEY `planificacion_pdrc_creador_id_0d97a988_fk_auth_user_id` (`creador_id`),
  KEY `planificacion_pdrc_editor_id_5d664c15_fk_auth_user_id` (`editor_id`),
  KEY `planificacion_pdrc_ejeecon_id_597482e4_fk_planifica` (`ejeecon_id`),
  KEY `planificacion_pdrc_periodo_id_71fa5850_fk_planifica` (`periodo_id`),
  CONSTRAINT `planificacion_pdrc_creador_id_0d97a988_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_pdrc_editor_id_5d664c15_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_pdrc_ejeecon_id_597482e4_fk_planifica` FOREIGN KEY (`ejeecon_id`) REFERENCES `planificacion_ejeecon` (`id`),
  CONSTRAINT `planificacion_pdrc_periodo_id_71fa5850_fk_planifica` FOREIGN KEY (`periodo_id`) REFERENCES `planificacion_periodo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planificacion_pdrc
-- ----------------------------
INSERT INTO `planificacion_pdrc` VALUES ('1', '2017-06-22 16:37:36.000000', '2017-06-27 09:42:08.000000', 'PDRC 2021', 'PDRC con miras al 2021', '1', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for planificacion_periodo
-- ----------------------------
DROP TABLE IF EXISTS `planificacion_periodo`;
CREATE TABLE `planificacion_periodo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `planificacion_periodo_creador_id_713d88cc_fk_auth_user_id` (`creador_id`),
  KEY `planificacion_periodo_editor_id_ffd985e4_fk_auth_user_id` (`editor_id`),
  CONSTRAINT `planificacion_periodo_creador_id_713d88cc_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_periodo_editor_id_ffd985e4_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planificacion_periodo
-- ----------------------------
INSERT INTO `planificacion_periodo` VALUES ('1', '2017-06-22 08:18:33.000000', '2017-07-24 01:47:04.168321', '2016-2021', '1', '2', '1');

-- ----------------------------
-- Table structure for planificacion_productoproyecto
-- ----------------------------
DROP TABLE IF EXISTS `planificacion_productoproyecto`;
CREATE TABLE `planificacion_productoproyecto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` longtext NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `indicador_id` int(11) NOT NULL,
  `programapresupuestal_id` int(11) NOT NULL,
  `unidadmedida_id` int(11) NOT NULL,
  `tipo` varchar(2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo` (`codigo`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `planificacion_produc_creador_id_4e2962cb_fk_auth_user` (`creador_id`),
  KEY `planificacion_produc_editor_id_fe9a7318_fk_auth_user` (`editor_id`),
  KEY `planificacion_produc_indicador_id_24352cd2_fk_planifica` (`indicador_id`),
  KEY `planificacion_produc_programapresupuestal_c99657ec_fk_planifica` (`programapresupuestal_id`),
  KEY `planificacion_produc_unidadmedida_id_83b0cfb8_fk_inicio_un` (`unidadmedida_id`),
  CONSTRAINT `planificacion_produc_creador_id_4e2962cb_fk_auth_user` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_produc_editor_id_fe9a7318_fk_auth_user` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_produc_indicador_id_24352cd2_fk_planifica` FOREIGN KEY (`indicador_id`) REFERENCES `planificacion_indicador` (`id`),
  CONSTRAINT `planificacion_produc_programapresupuestal_c99657ec_fk_planifica` FOREIGN KEY (`programapresupuestal_id`) REFERENCES `planificacion_programapresupuestal` (`id`),
  CONSTRAINT `planificacion_produc_unidadmedida_id_83b0cfb8_fk_inicio_un` FOREIGN KEY (`unidadmedida_id`) REFERENCES `inicio_unidadmedida` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planificacion_productoproyecto
-- ----------------------------
INSERT INTO `planificacion_productoproyecto` VALUES ('1', '2017-07-24 20:35:57.094746', '2017-07-24 20:35:57.094820', '1.1', 'Desarrollo de capacidades para la gestión técnico productiva', '.', '1', null, '1', '1', '5', 'PR');
INSERT INTO `planificacion_productoproyecto` VALUES ('4', '2017-07-25 12:56:15.714658', '2017-07-25 12:56:15.714710', 'PY001', 'Proyecto 001', '...', '1', null, '3', '1', '4', 'PY');
INSERT INTO `planificacion_productoproyecto` VALUES ('5', '2017-07-26 22:50:43.948908', '2017-07-27 17:46:57.582000', '23253', 'construcción de sistema', 'desarrollo y diseño', '1', '1', '3', '2', '1', 'PY');
INSERT INTO `planificacion_productoproyecto` VALUES ('6', '2017-07-27 17:44:26.665000', '2017-07-27 17:45:21.545000', 'P0002', 'Producto 002', 'dadasdsada dasdas', '1', '1', '3', '2', '6', 'PR');
INSERT INTO `planificacion_productoproyecto` VALUES ('7', '2017-07-27 17:47:16.226000', '2017-07-27 17:47:16.226000', 'PY005', 'Proyecto 005', 'dsadsadsada', '1', null, '1', '2', '4', 'PY');
INSERT INTO `planificacion_productoproyecto` VALUES ('8', '2017-07-27 18:10:25.659794', '2017-07-27 18:10:25.659842', '1212', '12º1', 'º12', '1', null, '3', '2', '6', 'PY');
INSERT INTO `planificacion_productoproyecto` VALUES ('9', '2017-07-27 18:10:54.804863', '2017-07-27 18:10:54.804940', '123', '12312', '123123', '1', null, '3', '2', '6', 'PR');

-- ----------------------------
-- Table structure for planificacion_programapresupuestal
-- ----------------------------
DROP TABLE IF EXISTS `planificacion_programapresupuestal`;
CREATE TABLE `planificacion_programapresupuestal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` longtext NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `indicador_id` int(11) NOT NULL,
  `unidadmedida_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo` (`codigo`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `planificacion_progra_creador_id_d7efd0b1_fk_auth_user` (`creador_id`),
  KEY `planificacion_progra_editor_id_186b3c0a_fk_auth_user` (`editor_id`),
  KEY `planificacion_progra_indicador_id_41a659f5_fk_planifica` (`indicador_id`),
  KEY `planificacion_progra_unidadmedida_id_d6cc6799_fk_inicio_un` (`unidadmedida_id`),
  CONSTRAINT `planificacion_progra_creador_id_d7efd0b1_fk_auth_user` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_progra_editor_id_186b3c0a_fk_auth_user` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_progra_indicador_id_41a659f5_fk_planifica` FOREIGN KEY (`indicador_id`) REFERENCES `planificacion_indicador` (`id`),
  CONSTRAINT `planificacion_progra_unidadmedida_id_d6cc6799_fk_inicio_un` FOREIGN KEY (`unidadmedida_id`) REFERENCES `inicio_unidadmedida` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planificacion_programapresupuestal
-- ----------------------------
INSERT INTO `planificacion_programapresupuestal` VALUES ('1', '2017-07-24 20:32:53.909914', '2017-07-25 16:19:30.788313', 'P.1', 'Fortalecimiento organizacional y mejoramiento de la gestión empresarial', '.sadas', '1', '1', '1', '5');
INSERT INTO `planificacion_programapresupuestal` VALUES ('2', '2017-07-25 16:18:50.264157', '2017-07-25 16:18:50.264207', 'P.12', 'Fortalecimiento de la Organizacion', 'organizacion', '1', null, '3', '6');
INSERT INTO `planificacion_programapresupuestal` VALUES ('3', '2017-07-25 16:19:24.471545', '2017-07-25 16:27:43.708833', 'P11', 'Organizar', 'Documentos', '1', '1', '1', '4');

-- ----------------------------
-- Table structure for planificacion_subactividadcatalogo
-- ----------------------------
DROP TABLE IF EXISTS `planificacion_subactividadcatalogo`;
CREATE TABLE `planificacion_subactividadcatalogo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `planificacion_subact_creador_id_29eb50d0_fk_auth_user` (`creador_id`),
  KEY `planificacion_subact_editor_id_b672808c_fk_auth_user` (`editor_id`),
  CONSTRAINT `planificacion_subact_creador_id_29eb50d0_fk_auth_user` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_subact_editor_id_b672808c_fk_auth_user` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planificacion_subactividadcatalogo
-- ----------------------------
INSERT INTO `planificacion_subactividadcatalogo` VALUES ('1', '2017-07-26 21:42:12.614353', '2017-07-26 21:42:12.614431', 'Sub Actividad 01', '1', null);
INSERT INTO `planificacion_subactividadcatalogo` VALUES ('2', '2017-07-26 22:56:39.464889', '2017-07-26 22:57:46.544862', 'Sub Actividad 02', '1', null);

-- ----------------------------
-- Table structure for planificacion_subactividadcomponente
-- ----------------------------
DROP TABLE IF EXISTS `planificacion_subactividadcomponente`;
CREATE TABLE `planificacion_subactividadcomponente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  `actividad_id` int(11) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `indicador_id` int(11) NOT NULL,
  `unidadmedida_id` int(11) NOT NULL,
  `subactividadcatalogo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo` (`codigo`),
  KEY `planificacion_subact_actividad_id_73e693cc_fk_planifica` (`actividad_id`),
  KEY `planificacion_subact_creador_id_24464d9b_fk_auth_user` (`creador_id`),
  KEY `planificacion_subact_editor_id_96adee55_fk_auth_user` (`editor_id`),
  KEY `planificacion_subact_indicador_id_d9b5e25f_fk_planifica` (`indicador_id`),
  KEY `planificacion_subact_unidadmedida_id_ee6170a3_fk_inicio_un` (`unidadmedida_id`),
  CONSTRAINT `planificacion_subact_actividad_id_73e693cc_fk_planifica` FOREIGN KEY (`actividad_id`) REFERENCES `planificacion_actividad` (`id`),
  CONSTRAINT `planificacion_subact_creador_id_24464d9b_fk_auth_user` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_subact_editor_id_96adee55_fk_auth_user` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_subact_indicador_id_d9b5e25f_fk_planifica` FOREIGN KEY (`indicador_id`) REFERENCES `planificacion_indicador` (`id`),
  CONSTRAINT `planificacion_subact_unidadmedida_id_ee6170a3_fk_inicio_un` FOREIGN KEY (`unidadmedida_id`) REFERENCES `inicio_unidadmedida` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planificacion_subactividadcomponente
-- ----------------------------
INSERT INTO `planificacion_subactividadcomponente` VALUES ('1', '2017-07-26 21:42:12.614353', '2017-07-26 21:42:12.614431', 'SA001', '2', '1', null, '3', '4', '1');
INSERT INTO `planificacion_subactividadcomponente` VALUES ('3', '2017-07-26 22:56:39.464889', '2017-07-26 22:57:46.544862', '0001', '3', '1', '1', '3', '6', '2');

-- ----------------------------
-- Table structure for planificacion_tareacatalogo
-- ----------------------------
DROP TABLE IF EXISTS `planificacion_tareacatalogo`;
CREATE TABLE `planificacion_tareacatalogo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `planificacion_tareacatalogo_creador_id_4143c290_fk_auth_user_id` (`creador_id`),
  KEY `planificacion_tareacatalogo_editor_id_585b673a_fk_auth_user_id` (`editor_id`),
  CONSTRAINT `planificacion_tareacatalogo_creador_id_4143c290_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_tareacatalogo_editor_id_585b673a_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planificacion_tareacatalogo
-- ----------------------------
INSERT INTO `planificacion_tareacatalogo` VALUES ('1', '2017-07-26 23:03:36.463134', '2017-07-26 23:03:36.463184', 'Tarea 01', '1', null);

-- ----------------------------
-- Table structure for planificacion_tareacpcr
-- ----------------------------
DROP TABLE IF EXISTS `planificacion_tareacpcr`;
CREATE TABLE `planificacion_tareacpcr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `subactividadcomponente_id` int(11) NOT NULL,
  `adjunto` varchar(1000) DEFAULT NULL,
  `base_id` int(11) NOT NULL,
  `descripcion` longtext,
  `estado` varchar(30) DEFAULT NULL,
  `evidencia` varchar(1000) DEFAULT NULL,
  `fin` date DEFAULT NULL,
  `inicio` date DEFAULT NULL,
  `nombre` varchar(300) NOT NULL,
  `porcentaje_completado` decimal(11,2) DEFAULT NULL,
  `prioridad` varchar(20) DEFAULT NULL,
  `compromiso` varchar(200) DEFAULT NULL,
  `desarrollo_tema` varchar(200) DEFAULT NULL,
  `metodologia` varchar(100) DEFAULT NULL,
  `persona_id` int(11) NOT NULL,
  `requerimiento` varchar(200) DEFAULT NULL,
  `tema` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `planificacion_tareacpcr_creador_id_9b4fd8b8_fk_auth_user_id` (`creador_id`),
  KEY `planificacion_tareacpcr_editor_id_f4e54e90_fk_auth_user_id` (`editor_id`),
  KEY `planificacion_tareac_subactividadcomponen_b2617a2d_fk_planifica` (`subactividadcomponente_id`),
  KEY `planificacion_tareacpcr_base_id_2c812e72_fk_agricola_base_id` (`base_id`),
  KEY `planificacion_tareac_persona_id_145022ed_fk_administr` (`persona_id`),
  CONSTRAINT `planificacion_tareac_persona_id_145022ed_fk_administr` FOREIGN KEY (`persona_id`) REFERENCES `administracion_persona` (`id`),
  CONSTRAINT `planificacion_tareac_subactividadcomponen_b2617a2d_fk_planifica` FOREIGN KEY (`subactividadcomponente_id`) REFERENCES `planificacion_subactividadcomponente` (`id`),
  CONSTRAINT `planificacion_tareacpcr_base_id_2c812e72_fk_agricola_base_id` FOREIGN KEY (`base_id`) REFERENCES `agricola_base` (`id`),
  CONSTRAINT `planificacion_tareacpcr_creador_id_9b4fd8b8_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_tareacpcr_editor_id_f4e54e90_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planificacion_tareacpcr
-- ----------------------------

-- ----------------------------
-- Table structure for planificacion_tareacpcu
-- ----------------------------
DROP TABLE IF EXISTS `planificacion_tareacpcu`;
CREATE TABLE `planificacion_tareacpcu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creado` datetime(6) NOT NULL,
  `editado` datetime(6) NOT NULL,
  `referencia` longtext,
  `base_id` int(11) NOT NULL,
  `creador_id` int(11) NOT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `subactividadcomponente_id` int(11) NOT NULL,
  `trabajador_id` int(11) NOT NULL,
  `adjunto` varchar(1000) DEFAULT NULL,
  `descripcion` longtext,
  `estado` varchar(30) DEFAULT NULL,
  `evidencia` varchar(1000) DEFAULT NULL,
  `fin` date DEFAULT NULL,
  `inicio` date DEFAULT NULL,
  `porcentaje_completado` decimal(11,2) DEFAULT NULL,
  `prioridad` varchar(20) DEFAULT NULL,
  `tareacatalogo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `planificacion_tareacpcu_base_id_cff46a88_fk_agricola_base_id` (`base_id`),
  KEY `planificacion_tareacpcu_creador_id_9e4b987a_fk_auth_user_id` (`creador_id`),
  KEY `planificacion_tareacpcu_editor_id_93994a50_fk_auth_user_id` (`editor_id`),
  KEY `planificacion_tareac_subactividadcomponen_6fd4e2ff_fk_planifica` (`subactividadcomponente_id`),
  KEY `planificacion_tareac_trabajador_id_59f6fa44_fk_administr` (`trabajador_id`),
  CONSTRAINT `planificacion_tareac_subactividadcomponen_6fd4e2ff_fk_planifica` FOREIGN KEY (`subactividadcomponente_id`) REFERENCES `planificacion_subactividadcomponente` (`id`),
  CONSTRAINT `planificacion_tareac_trabajador_id_59f6fa44_fk_administr` FOREIGN KEY (`trabajador_id`) REFERENCES `administracion_trabajador` (`id`),
  CONSTRAINT `planificacion_tareacpcu_base_id_cff46a88_fk_agricola_base_id` FOREIGN KEY (`base_id`) REFERENCES `agricola_base` (`id`),
  CONSTRAINT `planificacion_tareacpcu_creador_id_9e4b987a_fk_auth_user_id` FOREIGN KEY (`creador_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `planificacion_tareacpcu_editor_id_93994a50_fk_auth_user_id` FOREIGN KEY (`editor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planificacion_tareacpcu
-- ----------------------------
INSERT INTO `planificacion_tareacpcu` VALUES ('1', '2017-07-26 23:03:36.463134', '2017-07-26 23:03:36.463184', null, '2', '1', null, '1', '4', '', 'dagfhasjash', null, '', '2017-07-26', '2017-07-24', null, null, '1');
